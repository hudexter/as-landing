## Clear/Delete App
make clean

## Install App
npm install -g gulp bower
bower install
npm install

## serve App
npm serve

## PORT
localhost:3000

# Info
Base Px 16px
breakpoints: <480 (30em) xs, <768(48em) sm, <1024 (64em) md , <1440(90em)lg

# Deploy to Production Process
Make sure to do these steps before deploying

##bump version
gulp bump
git push origin master

##update Production CDN
make publish

##update UAT CDN
make publish-uat

##deploy
git push `remote` `branch`:master
