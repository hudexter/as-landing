/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var bump = require('gulp-bump');
var wrench = require('wrench');

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});

/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function() {
  gulp.start('build');
});

gulp.task('bump', function(){
  gulp.src(['./bower.json', './package.json'])
  .pipe(bump({key:'version',type:'patch'}))
  .pipe(gulp.dest('./'));
});

gulp.task('bump_major', function(){
  gulp.src(['./bower.json', './package.json'])
  .pipe(bump({key:'version',type:'major'}))
  .pipe(gulp.dest('./'));
});

gulp.task('bump_minor', function(){
  gulp.src(['./bower.json', './package.json'])
  .pipe(bump({key:'version',type:'minor'}))
  .pipe(gulp.dest('./'));
});

gulp.task('bump_pre', function(){
  gulp.src(['./bower.json', './package.json'])
  .pipe(bump({key:'version',type:'prerelease'}))
  .pipe(gulp.dest('./'));
});
