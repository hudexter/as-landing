.PHONY: default clean build
t=staging-web
b = $(shell git rev-parse --symbolic-full-name --abbrev-ref HEAD)

clean:
	rm -rf node_modules
	rm -rf src/bower_components
	rm -rf bower_components
	rm -rf dist
	rm -rf .tmp

copy:
	rm -rf dist
	test -d dist/ || mkdir dist/
	cp -r src/assets dist/

publish:
	make copy && BUILD_TARGET=production-web node_modules/.bin/gulp compile
	BUILD_TARGET=production-web node_modules/.bin/gulp upload

publish-uat:
	make copy && BUILD_TARGET=UAT-web node_modules/.bin/gulp compileUTA
	BUILD_TARGET=UAT-web node_modules/.bin/gulp upload
