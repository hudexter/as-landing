'use strict';
require('newrelic');
var http = require('http');
var express = require('express');
// var kraken = require('kraken-js');
var prerender = require('prerender-node');
var path = require('path');
var fs = require('file-system');

// var favicon = require('serve-favicon');


var options, app, server;

/*
 * Create and configure application. Also exports application instance for use by tests.
 * See https://github.com/krakenjs/kraken-js#options for additional configuration options.
 */
options = {
    onconfig: function(config, next) {
        /*
         * Add any additional config setup or overrides here. `config` is an initialized
         * `confit` (https://github.com/krakenjs/confit/) configuration object.
         */
        next(null, config);
    }
};

app = module.exports = express();

var env = process.env.NODE_ENV;

var forceSsl = function(req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
        return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    return next();
};
if (env === 'production-web') {
    //app.use(forceSsl);
}

app.use(prerender.set('prerenderToken', 'siWPrHweEWZAzXzVk2OZ')
    .set('protocol', 'https'));
// app.use(kraken(options));
app.set('view engine', 'html');
app.on('start', function() {
    // console.log('Application ready to serve requests.');
    // console.log('Environment: %s', app.kraken.get('env:env'));
});

var aasa = fs.readFileSync(__dirname + '/static/apple-app-site-association');

app.use('/styles', express.static(__dirname + '/../dist/styles'));
app.use('/scripts', express.static(__dirname + '/../dist/scripts'));
app.use('/favicon', express.static(__dirname + '/../dist/favicon.ico'));
app.use('/serve', express.static(__dirname + '/../dist/serve'));
app.use('/templates.js', express.static(__dirname + '/../dist/templates.js'));
app.use('/bower', express.static(__dirname + '/../dist/bower'));
app.use('/assets', express.static(__dirname + '/../dist/assets'));

app.get('/', function(req, res) {
    res.sendFile('index.html', {
        root: __dirname + '/../dist/'
    });
})

app.get('/termsprivacy', function(req, res) {
    res.sendFile('termsprivacy.html', {
        root: __dirname + '/../dist/'
    });
})

app.get('/robots.txt', function(req, res) {
    res.sendFile('robots.txt', {
        root: __dirname + '/../dist/'
    });
})

app.get('/googleeb0ecf07c36d96aa.html', function(req, res) {
    res.sendFile('googleeb0ecf07c36d96aa.html', {
        root: __dirname + '/../dist/'
    });
})

app.get('/apple-app-site-association', function(req, res, next) {
    res.set('Content-Type', 'application/pkcs7-mime');
    res.status(200)
        .send(aasa);
});

app.all('/*', function(req, res) {

    res.sendFile(path.resolve(__dirname + '/../dist/index.html'));
})

// app.get('/product/:id', function (req, res) {
// 	res.sendFile(path.resolve(__dirname + '/../dist/index.html'));
// })




// app.get('/store/:id/products', function (req, res) {
// 	  res.sendFile(path.resolve( __dirname + '/../dist/index.html'));
// })



// app.get('/*', function(req, res) {
//   // AJAX requests are aren't expected to be redirected to the AngularJS app
//   // if (req.xhr) {
//   //   return res.status(404).send(req.url + ' not found');
//   // }
//
//   // `sendfile` requires the safe, resolved path to your AngularJS app
//   res.sendFile(path.resolve( __dirname + '/../dist/index.html'));
// });


// app.get('/product/:id', function(req, res){
//   // 	res.sendFile('index.html', {
//   // 		root: __dirname + '/../dist/'
//   // 	});
//   res.redirect('http://localhost:8000/#/product/' + req.params.id);
// });




/*
 * Create and start HTTP server.
 */
if (!module.parent) {
    /*
     * This is only done when this module is run directly, e.g. `node .` to allow for the
     * application to be used in tests without binding to a port or file descriptor.
     */
    server = http.createServer(app);
    server.listen(process.env.PORT || 8000);
    server.on('listening', function() {
        console.log('Server listening on http://localhost:%d', this.address()
            .port);
    });

}