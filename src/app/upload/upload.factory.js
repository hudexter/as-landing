'user strict'

antsquare
  .factory('UploadFatory', ['$http', 'Config', 'uuid4', function ($http, Config, uuid4) {

    var uploadUrl = Config.UPLOAD_URL;

    return {
      getSignUrl: function (filetype) {
        return $http({
          url: (uploadUrl + '/s3Policy'),
          method: 'GET',
          params: {
            mimeType: filetype
          }
        });
      },
      getImagePath: function (type) {
        
        type = type.replace('image/', '.');
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = '0' + dd
        }
        if (mm < 10) {
          mm = '0' + mm
        }
        var path = 'image/' + yyyy + '/' + mm + '/' + dd + '/' + uuid4.generate() + type;
        return path
      },
      getVideoPath: function (fileType) {
        fileType = fileType.replace('video/', '.');
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = '0' + dd
        }
        if (mm < 10) {
          mm = '0' + mm
        }
        var path = 'video/' + yyyy + '/' + mm + '/' + dd + '/' + uuid4.generate() + fileType;
        return path
      },
      getCSVPath: function (fileType) {
        fileType = fileType.replace('text/', '.');
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = '0' + dd
        }
        if (mm < 10) {
          mm = '0' + mm
        }
        var path = 'image/' + yyyy + '/' + mm + '/' + dd + uuid4.generate() + fileType;
        return path
      }
    }

  }])