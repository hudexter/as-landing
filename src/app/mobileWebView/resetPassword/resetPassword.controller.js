angular.module('antsquare')
	.controller('ResetPasswordCtrl', ['$scope', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', '$cookies', 'GeoLocationService', '$rootScope', '$stateParams', function ($scope, StoreFactory, Auth, AccountsFactory, $q, $cookies, GeoLocationService, $rootScope, $stateParams) {

		$scope.fadeOut = false;
		$scope.loading = false;
		$scope.success = null;
		$scope.data = {
			code: $stateParams.code,
			dail_country: '1',
			input: $stateParams.input,
			country: null,
		};
		$scope.submited = false;
		$scope.submit = function () {
			$scope.loading = true;
			$scope.success = null;
			$scope.error = null;

			if ($scope.data.newpass === $scope.data.confirm) {

				$scope.submited = true;
				AccountsFactory.renewPasswordWithCode($scope.data)
					.then(function (data) {
						$scope.success = data.data.message;
						$scope.loading = false;
					})
					.catch(function (error) {
						$scope.error = error.data.message;
						$scope.submited = false;
						$scope.loading = false;
					})
			} else {
				$scope.error = "Password does not match.";
				$scope.loading = false;
			}
		};
}]);
