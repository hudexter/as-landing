angular.module('antsquare')
	.controller('ForgotPasswordCtrl', ['$scope', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', '$cookies', 'GeoLocationService', '$rootScope', '$state', function ($scope, StoreFactory, Auth, AccountsFactory, $q, $cookies, GeoLocationService, $rootScope,$state) {

		$scope.fadeOut = false;
		$scope.loading = true;

		$scope.data = {
			code: '',
			dail_country: '1',
			phone_number: "",
			country: null,
		};

		$scope.submit = function () {
			$scope.data.phone_number = '+' + $scope.data.country.code + $scope.data.phone;
			$scope.data.input = $scope.data.phone_number;
			AccountsFactory.resetPassword($scope.data)
				.then(function (data) {
          $state.go('resetPassword', {
            input: $scope.data.input
          })

				})
				.catch(function (error) {
					$scope.error = error.data.message;
				})

		};

		if (GeoLocationService.hasLocation()) {
			GeoLocationService.getCountry()
				.then(function (data) {
					$scope.data.country = data;
				})
				.catch(function (error) {
					$scope.data.country = {
						name: 'United States',
						class: 'us',
						code: '1'
					};
				})
		}

		$rootScope.$on('setLocation', function () {
			GeoLocationService.getCountry()
				.then(function (data) {
					$scope.data.country = data;
				})
				.catch(function (error) {
					console.log(error)
					$scope.data.country = {
						name: 'United States',
						class: 'us',
						code: '1'
					};
				})
		});

			}]);
