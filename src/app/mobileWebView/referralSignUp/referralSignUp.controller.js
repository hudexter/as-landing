'use strict';

antsquare
	.controller('referralSignUpCtrl', ['$scope', '$state', 'AccountsFactory', 'Auth', 'ezfb', 'ModalStorage', 'ModalService', 'GeoLocationService', '$rootScope', '$timeout', '$cookies', '$q', 'StoreFactory', 'vcRecaptchaService', '$stateParams', 'GeoLocationFactory', function ($scope, $state, AccountsFactory, Auth, ezfb, ModalStorage, ModalService, GeoLocationService, $rootScope, $timeout, $cookies, $q, StoreFactory, vcRecaptchaService, $stateParams, GeoLocationFactory) {
		$scope.signupLoading = false;
		$scope.country = null;
		$scope.signUpToggle = false;
		$scope.recaptchaInfo = {
			response: null,
			remoteip: null
		}
		$scope.referral_code = $stateParams.referral_code;

		$scope.setWidgetId = function (widgetId) {
			// store the `widgetId` for future usage.
			// For example for getting the response with
			// `recaptcha.getResponse(widgetId)`.
		};

		GeoLocationFactory.getLocationByIp()
			.then(function (data) {
				$scope.recaptchaInfo.remoteip = data.data.ip
			})

		$scope.setResponse = function (response) {
			// send the `response` to your server for verification.
			$scope.recaptchaInfo.response = response
			AccountsFactory.reCaptchaVerify($scope.recaptchaInfo)
				.then(function (data) {
					$scope.signUpToggle = data.data.success;
				})
				.catch(function (error) {
					console.log(error)
				})
		};

		$scope.cbExpiration = function () {
			// reset the 'response' object that is on scope
		};

		Auth.reset();
		$scope.form = {};

		$scope.submitForm = function () {
			if (!$scope.signupLoading && $scope.signUpToggle) {
				$scope.error = "";
				// if (!$scope.gRecaptchaResponse) {
				// 	$scope.error = "Please prove you're not a robot";
				// 	return
				// }
				$scope.signupLoading = true;
				$scope.form.country = $scope.country;
				$scope.form.input = '+' + $scope.country.code + $scope.form.phone;
				$scope.form.referralCode = $scope.referral_code;
				AccountsFactory.registerUser($scope.form)
					.then(function (data) {
						// $mixpanel.track('signup');
						// console.log(data)
						var modalData = {
							token: data.data,
							userInfo: $scope.form
						};
						$scope.phoneVerify(modalData);
						$scope.response = data;
						$scope.signupLoading = false;
						$scope.error = null;
					})
					.catch(function (error) {
						$scope.signupLoading = false;
						if (error.status != 500) {
							$scope.error = error.data.message
						}
						// $modalInstance.close(error);
					});
			}
		};

		$scope.addPhone = function (result) {
			ModalStorage.set(result);
			ModalService.showModal({
					templateUrl: "app/components/modal/modalAddPhone/modalAddPhone.html",
					controller: "ModalAddPhoneCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						ModalStorage.end();
						if (result) {
							$scope.phoneVerify(result);
						}
					});
				});
		};

		$scope.phoneVerify = function (data) {
			ModalStorage.set(data);
			ModalService.showModal({
					templateUrl: "app/components/modal/modalPhoneVerify/modalPhoneVerify.html",
					controller: "ModalPhoneVerifyCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						ModalStorage.end();

						if (Auth.isLoggedIn()) {
							$state.go('index.home');
						}

					});
				});
		};

		var getStore = function () {
			var deferred = $q.defer();
			StoreFactory.me()
				.then(function (data) {
					Auth.setStore(data.data);
					deferred.resolve(data);
				})
				.catch(function (error) {
					console.log(error)
					deferred.reject(error);
				})
			return deferred.promise;
		};

		var getUser = function () {
			var deferred = $q.defer();
			AccountsFactory.me()
				.then(function (data) {
					Auth.setUser(data.data);
					deferred.resolve(data);
				})
				.catch(function (error) {
					console.log(error)
					deferred.reject(error);
				})
			return deferred.promise;
		};

		$scope.authenticate = function (provider) {
			ezfb.getLoginStatus(function (res) {
				console.log(res)
			});
			ezfb.login(function (response) {
					$cookies.put('fb_token', response.authResponse.accessToken);
					if (response.status === 'connected') {
						AccountsFactory.userLinking(provider, response.authResponse.accessToken)
							.then(function (response) {
								if (response.data.phone_verified) {
									Auth.setToken(response);
									AccountsFactory.me()
										.then(function (response) {
											$q.all([getStore(), getUser()])
												.then(function (value) {
													$state.go('index.home')
												}, function (error) {
													console.log(error)
												});
										});
								} else {
									$scope.addPhone(response.data);
								}
							})
							.catch(function (error) {
								console.log(error)
								$scope.loading = "error";
								$timeout(function () {
									$scope.loading = false;
								}, 1500);
							});
					} else {
						console.log("not conencted")
						$scope.loading = "error";
						$timeout(function () {
							$scope.loading = false;
						}, 1500);
					}
				})
				.catch(function (error) {
					console.log(error);
					$scope.loading = "error";
					$timeout(function () {
						$scope.loading = false;
					}, 1500);
				});
		};

	}]);
