'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
   function ($stateProvider, $urlRouterProvider) {


		$stateProvider
			.state('forgotPassword', {
				url: '/forgotPassword',
				templateUrl: 'app/mobileWebView/forgotPassword/forgotPassword.html',
				controller: 'ForgotPasswordCtrl',
				onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
					seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
					seoFactory.setTitle("Antsquare");
					seoFactory.setType("article");
					var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
					seoFactory.setImage1(link);
					seoFactory.setUrl($location.absUrl());
          }]
			})
			.state('resetPassword', {
				url: '/resetPassword?code&input',
				templateUrl: 'app/mobileWebView/resetPassword/resetPassword.html',
				controller: 'ResetPasswordCtrl',
				onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
					seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
					seoFactory.setTitle("Antsquare");
					seoFactory.setType("article");
					var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
					seoFactory.setImage1(link);
					seoFactory.setUrl($location.absUrl());
          }]
			})
			.state('referralSignUp', {
				url: '/referralSignUp?referral_code',
				templateUrl: 'app/mobileWebView/referralSignUp/referralSignUp.html',
				controller: 'referralSignUpCtrl',
				onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
					seoFactory.setDescription("I discovered a better way to shop local and wants to share with you. Sign up with this link and we both get $5!");
					seoFactory.setTitle("Get $5 off your first purchase!");
					seoFactory.setType("article");
					var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
					seoFactory.setImage1(link);
					seoFactory.setUrl($location.absUrl());
          }]
			})

}])
