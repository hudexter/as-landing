'use strict';
antsquare
	.directive('submitBtn', ['$compile', '$timeout', function ($compile, $timeout) {
		return {
			restrict: 'A',
			scope: {
				status: '='
			},
			link: function (scope, element, attr) {

				var content = element.find('#btn-name');
				var loading = '<i class="icon-spin5 icon-sm spiral animate-spin"></i>';
				var originalText = element.find('#btn-name').text();

				function resetStatus() {
					setTimeout(function () {
						scope.status = null;
						content.text(originalText);
						element.removeClass('background-error');
						element.removeClass('border-error');
						element.removeClass('background-success');
						element.removeClass('border-success');
					}, 1500);
				}

				scope.$watch('status', function () {
					if (scope.status === "loading") {
						content.text('');
						content.append(loading);
					} else if (scope.status === true) {
						content.text('');
						// element.addClass('background-success');
						element.addClass('border-success');
						content.append('<i class="icon-verified icon-sm success-color"></i>');
						resetStatus();
					} else if (scope.status === false) {
						content.text('');
						// element.addClass('background-error');
						element.addClass('border-error');
						content.append('<i class="icon-unverified icon-sm error-color"></i>');
						resetStatus();
					}
				});

				//
				//
				// element.text(scope.name)
				// scope.$watch('loading', function () {
				//
				// 	if (scope.loading === 'loading') {
				// 		element.text('');
				// 		element.append('<i class="icon-spin5 icon-sm spiral"></i>');
				// 		// element.width(element.width())
				// 	} else if (scope.loading === 'done') {
				// 		element.text('');
				// 		element.removeClass('background-error');
				// 		element.removeClass('border-error');
				// 		element.addClass('background-success');
				// 		element.addClass('border-success');
				// 		element.append('<i class="icon-verified icon-sm"></i>');
				// 		reset()
				// 	} else if (scope.loading === 'error') {
				// 		element.text('')
				// 		element.removeClass('background-success');
				// 		element.removeClass('border-success');
				// 		element.addClass('background-error');
				// 		element.addClass('border-error');
				// 		element.append('<i class="icon-unverified icon-sm"></i>');
				// 		reset()
				// 	} else {
				// 		element.text(scope.name);
				// 		element.removeClass('background-error');
				// 		element.removeClass('border-error');
				// 		element.removeClass('background-success');
				// 		element.removeClass('border-success');
				//
				// 	}
				// });


			}
		}
}])
