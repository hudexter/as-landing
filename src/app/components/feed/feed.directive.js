'use strict';

angular.module('antsquare')
  .directive('feed', ['$rootScope', 'GeoLocationService', 'AccountsFactory', 'Auth', function($rootScope, GeoLocationService, AccountsFactory, Auth) {
    return {
      restrict: 'E',
      scope: {
        feed: '='
      },
      link: function(scope, element, attr) {
        scope.userInfo = null;
        if (Auth.isLoggedIn()) {
          scope.userInfo = Auth.getUserFn();
        }
        // Helpers
        function getDisplayNameInHtml(user) {
          if (user) {
            if (user.store_info.as_business) {
              var result = '<a href="/store/' + user.store_info.id + '">' + user.store_info.store_name + '</a>';
              return result
            } else {
              var result = '<a href="/store/' + user.store_info.id + '">' + user.name + '</a>';
              return result
            }
          } else {
            return null
          }
        };
        scope.followToggle = function() {
          if (!scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')
          scope.feed.actor.is_following = !scope.feed.actor.is_following;

          if (!scope.feed.actor.is_following) {
            AccountsFactory.unfollowUser(scope.feed.actor.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.feed.actor.is_following = !scope.feed.actor.is_following;
              })
          } else {
            AccountsFactory.followUser(scope.feed.actor.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.feed.actor.is_following = !scope.feed.actor.is_following;
              })
          }
        };

        function getProductInHtml(product) {
          var result = '<a href="/product/' + product.id + '">' + product.name + '</a>'
          return result
        }
        scope.feed.product = scope.feed.target_json;
        if (!scope.feed.product) {
          var productInHtml = "";
        } else {
          var productInHtml = getProductInHtml(scope.feed.product)
        }
        var actorNameInHtml = getDisplayNameInHtml(scope.feed.actor);
        var targetNameInHtml = getDisplayNameInHtml(scope.feed.target);

        switch (scope.feed.verb) {
          case 'new_product':
            scope.feed.message = actorNameInHtml + ' posted a new item, ' + productInHtml;
            break;
          case 'like_moment':
            scope.feed.message = actorNameInHtml + ' <i class="icon-totallikes2016 pos-rlt red"> </i> <br>' + ' liked your moment! ' + productInHtml;
            break;
          case 'disapprove_product':
            scope.feed.message = 'Your item, ' + productInHtml + ' has been removed for a violation.';
            break;
          case 'like_product':
            scope.feed.message = actorNameInHtml + ' <i class="icon-totallikes2016 pos-rlt red"> </i> <br>' + scope.feed.ui_message;
            break;
          case 'comment_product':
            scope.feed.message = actorNameInHtml + ' <i class="icon-message2016 pos-rlt red"> </i> <br>' + ' commented on item ' + productInHtml;
            break;
          case 'comment_moment':
            scope.feed.message = actorNameInHtml + ' <i class="icon-message2016 pos-rlt red"> </i> <br>' + ' commented on item ' + productInHtml;
            break;
          case 'new_order':
            scope.feed.message = actorNameInHtml + ' purchased your item, ' + productInHtml + '. ';
            break;
          case 'cancel_order':
            scope.feed.message = 'Your sale of ' + productInHtml + ' has been cancelled.';
            break;
          case 'new_chat':
            scope.feed.message = actorNameInHtml + ' sent you a message: ' + scope.feed.message;
            break;
          case 'new_user_mention':
            scope.feed.message = actorNameInHtml + ' <b class="pos-rlt red">@</b> <br>' + scope.feed.ui_message + ' ' + productInHtml;;
            break;
          case 'new_following':
            scope.feed.product = null;

            if (scope.feed.target.id == scope.userInfo.id) {

              scope.feed.target = null;
              scope.feed.message = actorNameInHtml + ' started following you.';
            } else {
              scope.feed.message = actorNameInHtml + ' started following ' + targetNameInHtml;
            }
            break;
          case 'store_review':
            scope.feed.product = null;
            scope.feed.message = actorNameInHtml + ' has left you a review.';
            break;
        };

        scope.getFeedTemplate = function() {
          return 'app/components/feed/feed.html'
        };
      },
      template: "<div ng-include='getFeedTemplate()'></div>"
    }
  }]);