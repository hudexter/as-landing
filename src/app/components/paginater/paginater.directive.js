'use strict';
antsquare
  .directive('paginater', ['SearchDataIntent', function(SearchDataIntent) {
    return {
      restrict: 'EA',
      scope: {
        pageDetail: '=',
        search: '&',
        type: '&',
        version: '@'
      },
      templateUrl: 'app/components/paginater/paginater.html',
      link: function(scope, element, attr) {
        scope.loading = false;
        scope.first = null
        scope.back1 = null;
        scope.backEllip = null;
        scope.up1 = null;
        scope.upEllip = null;
        scope.last = null;
        scope.current = null;

        scope.$watch('pageDetail', function() {
          if (scope.pageDetail) {
            if (!scope.pageDetail.pages) {
              scope.pageDetail.pages = Math.ceil(scope.pageDetail.total / scope.pageDetail.per_page);
            }
            scope.loading = false;
            scope.first = null
            scope.back1 = null;
            scope.backEllip = null;
            scope.up1 = null;
            scope.upEllip = null;
            scope.last = null;
            scope.current = scope.pageDetail.page;
            update();
          };
        });

        function update() {
          if (scope.current > 2) {
            scope.backBtn = true;
            scope.back1 = scope.current - 1;
          } else if (scope.current > 1) {
            scope.backBtn = true;
          } else {
            scope.backBtn = false;
          }

          if (scope.current + 1 < scope.pageDetail.pages) {
            scope.up1 = scope.current + 1;
          }

          if (scope.current == scope.pageDetail.pages) {
            scope.nextBtn = false;
          } else {
            scope.nextBtn = true;
          }

          if (scope.current > 3) {
            scope.backEllip = true;
          } else if (scope.current == scope.pageDetail.pages && scope.pageDetail.pages > 2) {
            scope.backEllip = false;
          } else {
            scope.backEllip = false;
          }

          if (scope.pageDetail.pages - scope.current > 2) {
            scope.upEllip = true;
          } else if (scope.current == 1 && scope.pageDetail.pages > 3) {
            scope.upEllip = true;
          } else {
            scope.upEllip = false;
          }

          if (scope.current == 1) {
            scope.first = false;
          } else {
            scope.first = true;
          }

          if (scope.current == scope.pageDetail.pages) {
            scope.last = false;
          } else {
            scope.last = true;
          }

        };

        scope.toPage = function(pageNum) {

          if (scope.type() == 'comment') {
            scope.$parent.comments.page = pageNum;
            scope.$parent.comments.last_top = true;
          } else if (scope.version === 'v2') {
            scope.pageDetail.page = pageNum;


          } else {
            SearchDataIntent.setPage(pageNum);

          }
          scope.search();

        };

      }
    }
  }]);