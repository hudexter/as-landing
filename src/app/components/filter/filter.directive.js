'use strict';

angular.module('antsquare')
	.directive('filter', ['$rootScope', 'URLS', 'deviceDetector', '$q', 'SearchDataIntent', 'ProductsFactory', function ($rootScope, URLS, deviceDetector, $q, SearchDataIntent, ProductsFactory) {
		return {
			restrict: 'E',
			// templateUrl: 'app/components/filter/filter.html',
			scope: {
				backgroundColor: '@',
				updateFormData: '&',
				formData: '=',
				view: '@',
				mobile: '@'
			},
			link: function (scope, element, attr) {
				scope.template = 'app/components/filter/filter.html';

				if (scope.mobile) {
					scope.template = 'app/components/filter/filterSm.html';
				}

				scope.data = scope.formData;

				scope.categories = [];
				scope.serviceCheckBox = false;
				scope.productCheckBox = false;
				scope.requestCheckBox = false;

				scope.productList = false;
				scope.serviceList = false;

				var temp = [];

				if (scope.data.category) {
					temp = scope.data.category.split(",");
				}
				// if (scope.data.type) {
				// 	var tempType = scope.data.type.split(",");
				// 	tempType.map(function (type) {
				// 		if (type == 'request') {
				// 			scope.requestCheckBox = true;
				// 		}
				// 	});
				// }

				// Categories
				function isListEmpty(type) {
					var empty = false;
					if (_.findIndex(scope.categories[type], {
							'selected': true
						}) >= 0) {
						if (type === 'products') {
							scope.productCheckBox = true;
						} else if (type === 'services') {
							scope.serviceCheckBox = true;
						} else if (type === 'requests') {
							scope.requestCheckBox = true;
						}
					} else {
						if (type === 'products') {
							scope.productCheckBox = false;
						} else if (type === 'services') {
							scope.serviceCheckBox = false;
						} else if (type === 'requests') {
							scope.requestCheckBox = false;
						}
					}
				};

				function updateList() {
					scope.data.category = [];
					for (var type in scope.categories) {
						scope.categories[type].map(function (category) {
							if (category.selected) {
								scope.data.category.push(category.id);
							}
						});
					};
					// if (scope.requestCheckBox) {
					// 	scope.categories.requests.map(function (category) {
					// 			scope.data.category.push(category.id);
					// 	});
					// }

					scope.data.category = scope.data.category.join(',');

					scope.data.type = null;
					// scope.data.type = [];
					// if (scope.serviceCheckBox) scope.data.type.push('service');
					// if (scope.requestCheckBox) scope.data.type.push('request');
					// if (scope.productCheckBox) scope.data.type.push('product');
					// scope.data.type = scope.data.type.join(',');

					scope.data.page = 1;
					scope.updateFormData(scope.data);
				};

				scope.selectCategoryType = function (index, type) {
					if (scope.categories[type][index].selected) {
						scope.categories[type][index].selected = false;
					} else {
						scope.categories[type][index].selected = true;
					}
					isListEmpty(type);
					updateList();
				};

				scope.selectType = function (type, status) {
					if (type === 'requests') {
						scope.requestCheckBox = !scope.requestCheckBox;
						status = scope.requestCheckBox;
					}
					scope.categories[type].map(function (list) {
							return list.selected = status;
						})
						// } else {
						// 	scope.requestCheckBox = !scope.requestCheckBox;
						// }
					isListEmpty(type);
					updateList();
				};


				// Slider
				scope.slider = {
					min: 0,
					max: 1000,
					options: {
						floor: 0,
						ceil: 1000,
						step: 1,
						onEnd: function () {
							scope.data.min_price = scope.slider.min;
							scope.data.page = 1;
							scope.updateFormData(scope.data);
						}
					}
				};
				scope.$watch("slider.max", function () {
					if (scope.slider.max >= 1000) {
						scope.data.max_price = 99999;
						scope.showMax = '1000+';
					} else {
						scope.data.max_price = scope.slider.max;
						scope.showMax = scope.slider.max;
					}
				});

				if (scope.formData.min_price) {
					scope.slider.min = scope.formData.min_price;
				}
				scope.showMin = scope.formData.min_price;

				// Get Categories
				ProductsFactory.getCategories()
					.then(function (data) {
						scope.categories = data.data;
						for (var key in scope.categories) {
							scope.categories[key].map(function (category) {
								temp.forEach(function (selectedId) {
									if (category.id == selectedId) {
										category.selected = true;
										isListEmpty(key);
										return category
									}
								})
							});
						};
					});

				scope.filterRelease = false;
				scope.filterShow = false;

				$rootScope.$on('refreshSlider', function () {
					scope.$broadcast('rzSliderForceRender');
				});

				$rootScope.$on('showFilter', function (event, data) {
					scope.filterRelease = true;
					scope.filterShow = data;
					setTimeout(function () {
						scope.$broadcast('rzSliderForceRender');
					});

					if (data) {
						angular.element(document.querySelector('#cardPlateContainer'))
							.addClass('filterShow');
					} else {
						angular.element(document.querySelector('#cardPlateContainer'))
							.removeClass('filterShow');
					}
				});

				$rootScope.$on('filterClose', function (event, data) {
					scope.filterShow = false;
					angular.element(document.querySelector('#cardPlateContainer'))
						.removeClass('filterShow');
					setTimeout(function () {
						scope.$broadcast('rzSliderForceRender');
					});

				});
			},
			template: "<div><ng-include src='template'></ng-include></div>"
		}
  }]);
