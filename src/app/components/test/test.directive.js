'use strict';

angular.module('antsquare')
	.directive('test', ['ProductsFactory', '$swipe', function (ProductsFactory, $swipe) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/test/test.html',
			scope: {},
			link: function (scope, element, attr) {


				$swipe.bind(element, {
					move: function (touch) {
						console.log('Swiped:', touch);
						// unbind();
					}
				});


			}
		}
			}]);
