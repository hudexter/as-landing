'use strict'

angular.module('antsquare')
	.directive('recommendation', ['$stateParams', 'ProductsFactory', 'CardIntent', 'angularGridInstance', 'GeoLocationService', 'RequestFactory', function ($stateParams, ProductsFactory, CardIntent, angularGridInstance, GeoLocationService, RequestFactory) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/recommendation/recommendation.html',
			scope: {
				product: '='
			},
			link: function (scope, element, attr) {
				var fetch = false;
				var location = {};
				scope.$watch('product', function () {
					if (scope.product && scope.product.lat) {
						location.lat = GeoLocationService.getCoords()
							.latitude;
						location.lon = GeoLocationService.getCoords()
							.longitude;
						if (scope.product.type === 'request') {
							RequestFactory.getRecommend(scope.product.id, location)
								.then(function (data) {
									scope.recommendProducts = {};
									data.data = data.data.splice(0, 6);
									scope.recommendProducts.products = data.data.map(function (card) {
										return CardIntent.set(card);
									});
								})
								.catch(function (error) {
									console.log(error);
								});
						} else {
							ProductsFactory.getRecommendProducts($stateParams.product_id, location)
								.then(function (data) {
									scope.recommendProducts = {};
									data.data = data.data.splice(0, 6);
									scope.recommendProducts.products = data.data.map(function (card) {
										return CardIntent.set(card);
									});
								})
								.catch(function (error) {
									console.log(error);
								});
						}



					}
				});
			}
		}
    }]);
