'use strict';

angular.module('antsquare')
  .directive('navbar', ['$window', 'URLS', 'GeoLocationService', '$rootScope', 'ModalStorage', 'ModalService', 'Auth', '$state', 'intercomFactory', 'deviceDetector', 'Config', '$firebaseArray', '$firebaseObject', 'SearchDataIntent', function($window, URLS, GeoLocationService, $rootScope, ModalStorage, ModalService, Auth, $state, intercomFactory, deviceDetector, Config, $firebaseArray, $firebaseObject, SearchDataIntent) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
        target: '@',
        pinned: '@',
        tab: '@',
      },
      controller: ['$scope', function($scope) {

        $scope.navHeight = {
          "height": '52px'
        }
        if ($scope.tab) {
          $scope.navHeight = {
            "height": '100px'
          }
        } else {
          $scope.navHeight = {
            "height": '52px'
          }
        }

        $scope.userInfo = null;
        if (Auth.isLoggedIn()) {
          $scope.userInfo = Auth.getUserFn();
          var url = '/totalBadges/' + $scope.userInfo.id;
          var ref = new Firebase(Config.FIREBASE_BASEURL + url);
          var ref = $firebaseObject(ref);
          ref.$loaded()
            .then(function(data) {
              $scope.msgBadge = data;
            })
        }
        $scope.store = Auth.getStoreFn();
        $rootScope.$on('refreshFirebaseToken', function() {
          var url = '/totalBadges/' + $scope.userInfo.id;
          var ref = new Firebase(Config.FIREBASE_BASEURL + url);
          var ref = $firebaseObject(ref);
          ref.$loaded()
            .then(function(data) {
              $scope.msgBadge = data;
            })
        });
        // Check Mobile
        $scope.isMobile = null;
        if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
          $scope.isMobile = true;
        } else {
          $scope.isMobile = false;
        }



        $scope.clearBadge = function() {
          $scope.msgBadge.count = 0;
          $scope.msgBadge.$save();
        };

        $scope.menuToggle = function() {
          $scope.menuActive = !$scope.menuActive;
        };

        $scope.loginModal = function(trigger) {
          ModalStorage.set(trigger);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalLogin/modalLogin.html",
              controller: "ModalLoginCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
                if (result) {
                  if ('state' in result) {
                    if (result.state === "forgetPassword") {
                      $scope.forgotPassword();
                    } else if (result.state === "comfirmCode") {
                      $state.go('index.confirmCode');
                    }
                  } else if (!result.phone_verified) {
                    $scope.addPhone(result);
                  }
                }
              });
            });
        };

        $scope.addPhone = function(result) {
          ModalStorage.set(result);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalAddPhone/modalAddPhone.html",
              controller: "ModalAddPhoneCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
                if (result) {
                  $scope.phoneVerify(result);
                }

              });
            });
        };

        $scope.phoneVerify = function(data) {
          ModalStorage.set(data);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalPhoneVerify/modalPhoneVerify.html",
              controller: "ModalPhoneVerifyCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
                if (Auth.isLoggedIn()) {
                  $state.go('index.home');
                }
              });
            });
        };

        $scope.forgotPassword = function() {
          ModalService.showModal({
              templateUrl: "app/components/modal/modalForgotPassword/modalForgotPassword.html",
              controller: "ModalForgotPasswordCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
              });
            });
        };

        $rootScope.$on('openLoginModal', function(event, data) {
          $scope.loginModal(data);
        });

        $rootScope.$on('updateUser', function(event, data) {
          intercomFactory.updateUser(data)
          $scope.userInfo = data;
        });

        $scope.logout = function() {
          Auth.logout();
        };

      }],
      link: function(scope, element, attr) {

        scope.pinned = false;
        scope.country = "";
        scope.url = Config;
        scope.urls = URLS;
        scope.link = URLS.appStore;
        scope.login = URLS.login;
        scope.autoSelectLocation = true;

        scope.countries = [{
          name: 'Canada',
          coords: {
            latitude: '49.2827',
            longitude: '-123.1207'
          }
        }, {
          name: 'United States',
          coords: {
            latitude: '47.6097',
            longitude: '-122.3331'
          }
        }]

        if (GeoLocationService.hasLocation() && scope.autoSelectLocation) {
          GeoLocationService.getCountry()
            .then(function(data) {
              scope.country = data;
            })
            .catch(function(error) {
              console.log(error)
              scope.country = "United States";
            })
        }
        $rootScope.$on('$stateChangeStart',
          function(event, toState, toStateParams, fromState, fromStateParams) {
            if (event.name != 'index.home') {

              scope.target = null;
              var animationInProgress = false;
              var animationState = "top";

              var lastScrollY = $(this)
                .scrollTop();

              scope.menuActive = false;

              $('#navbar')
                .addClass('pos-fix z-xl background-white');
              $('#navbar')
                .removeClass('pos-abt');
              $('#navbar div')
                .first()
                .removeClass('container - edge');
              $('#navbar div')
                .first()
                .addClass('background-white container-edge2');
              $('#nav-logo')
                .removeClass('hide');
              $('#nav-location')
                .addClass('hide');
              $('#nav-list')
                .find('a')
                .removeClass('white')
              $('#nav-list')
                .find('a')
                .addClass('bodyColor');
              $('.icon-dropdown')
                .removeClass('white');
              $('.icon-dropdown')
                .addClass('bodyColor');
              $('#nav-list')
                .find('button')
                .addClass('btn-trn white');
              $('#nav-list')
                .find('button')
                .removeClass('btn-trn white');
              $('.sticky-banner')
                .addClass('background-white');
            } else {
              scope.target = '#below-nav-bar';
              var animationInProgress = false;
              var animationState = "top";
              var lastScrollY = $(this)
                .scrollTop();
              scope.menuActive = false;

              $('.sticky-banner')
                .addClass('background-white-transparent');
              $('.sticky-banner')
                .removeClass('background-white');

              if (!$('.sticky-banner')
                .hasClass('background-white-transparent')) {

                $('#nav-list li a')
                  .addClass('white');
                $('#nav-list li a')
                  .removeClass('bodyColor');
                $('#nav-list button')
                  .removeClass('btn-trn-blk bodyColor');
                $('#nav-list button')
                  .addClass('btn-trn white');
                $('.icon-dropdown')
                  .addClass('white');
                $('.icon-dropdown')
                  .removeClass('bodyColor');
              } else {

                $('#nav-list li a')
                  .removeClass('white');
                $('.icon-dropdown')
                  .removeClass('white');
                $('.icon-dropdown')
                  .addClass('bodyColor');
                $('#nav-list li a')
                  .addClass('bodyColor');
                $('#nav-list button')
                  .addClass('btn-trn white');
                $('#nav-list button')
                  .removeClass('btn-trn white');
              }

            }
          });

        $rootScope.$on('setLocation', function() {
          if (scope.autoSelectLocation) {
            GeoLocationService.getCountry()
              .then(function(data) {
                scope.country = data;
              })
              .catch(function(error) {
                console.log(error)
                scope.country = "United States";
              })
          }
        });

        scope.selectCountry = function(data) {
          scope.country = data.name;
          GeoLocationService.setCoords(data.coords)
          GeoLocationService.reverseGeolocation(data.coords)
            .then(function(data) {
              scope.autoSelectLocation = false;
              // Search when set Country
              GeoLocationService.set(data);
            });
        };

        // aniamtions

        var animationInProgress = false;
        var animationState = "top";
        var lastScrollY = $(this)
          .scrollTop();
        scope.menuActive = false;
        if (!scope.target) {

          $('#navbar')
            .addClass('pos-fix z-xl background-white');
          $('#navbar')
            .removeClass('pos-abt');
          $('#navbar div')
            .first()
            .removeClass('container-edge');
          $('#navbar div')
            .first()
            .addClass('background-white container-edge2');
          $('#nav-logo')
            .removeClass('hide');
          $('#nav-location')
            .addClass('hide');
          $('#nav-list')
            .find('a')
            .removeClass('white')
          $('#nav-list')
            .find('a')
            .addClass('bodyColor');
          $('.icon-dropdown')
            .removeClass('white');
          $('.icon-dropdown')
            .addClass('bodyColor');
          $('#nav-list')
            .find('button')
            .addClass('btn-trn white');
          $('#nav-list')
            .find('button')
            .removeClass('btn-trn white');
        } else {
          $(function() {
            $(window)
              .scroll(function() {

                if (scope.target && $window.innerWidth >= 768) {
                  if (animationState === "down") {

                    $('.sticky-banner')
                      .addClass('background-white-transparent');
                    $('.sticky-banner')
                      .removeClass('background-white');

                    if (!$('.sticky-banner')
                      .hasClass('background-white-transparent')) {

                      $('#nav-list li a')
                        .addClass('white');
                      $('#nav-list li a')
                        .removeClass('bodyColor');
                      $('#nav-list button')
                        .removeClass('btn-trn-blk bodyColor');
                      $('#nav-list button')
                        .addClass('btn-trn white');
                      $('.icon-dropdown')
                        .addClass('white');
                      $('.icon-dropdown')
                        .removeClass('bodyColor');
                    } else {

                      $('#nav-list li a')
                        .removeClass('white');
                      $('.icon-dropdown')
                        .removeClass('white');
                      $('.icon-dropdown')
                        .addClass('bodyColor');
                      $('#nav-list li a')
                        .addClass('bodyColor');
                      $('#nav-list button')
                        .addClass('btn-trn white');
                      $('#nav-list button')
                        .removeClass('btn-trn white');
                    }
                  }

                  scope.menuActive = false;
                } else {

                  // scope.menuActive = true;

                  $('#nav-list li a')
                    .removeClass('white');
                  $('#nav-list li a')
                    .addClass('bodyColor');
                  $('.icon-dropdown')
                    .removeClass('white');
                  $('.icon-dropdown')
                    .addClass('bodyColor');
                  $('#nav-list button')
                    .addClass('btn-trn white');
                  $('#nav-list button')
                    .removeClass('btn-trn white');
                  $('.sticky-banner')
                    .removeClass('background-white-transparent');
                  $('.sticky-banner')
                    .addClass('background-white');

                }

                if ($(this)
                  .scrollTop() >= ($('.nav-header')
                    .height() + $(scope.target)
                    .height()) && animationInProgress === false && animationState !== "down" && scope.target) {

                  if (!$('.sticky-banner')
                    .hasClass('banner-stick')) {
                    // DROP
                    animationInProgress = true;
                    animationState = "down";

                    $('#nav-logo')
                      .removeClass('hide');
                    $('#nav-location')
                      .addClass('hide');
                    $('.sticky-banner')
                      .addClass('background-white-transparent');
                    $('.sticky-banner-grid')
                      .addClass('container-edge2');
                    $('#nav-list')
                      .find('a')
                      .removeClass('white');
                    $('#nav-list')
                      .find('a')
                      .addClass('bodyColor');
                    $('.icon-dropdown')
                      .removeClass('white');
                    $('.icon-dropdown')
                      .addClass('bodyColor');
                    $('#nav-list')
                      .find('button')
                      .addClass('btn-trn-blk bodyColor');
                    $('#nav-list')
                      .find('button')
                      .removeClass('btn-trn white');

                    $('.sticky-banner')
                      .addClass('banner-stick')
                      .animate({
                        top: '0',
                      }, 250, function() {
                        animationInProgress = false;
                      });
                  }

                } else if (scope.target && $(this)
                  .scrollTop() < 52 && animationState != "top") {
                  // Top
                  $('#nav-location')
                    .removeClass('hide');
                  $('#nav-logo')
                    .addClass('hide');
                  $('.sticky-banner')
                    .removeClass('background-white-transparent');
                  $('.sticky-banner')
                    .removeClass('banner-stick');
                  $('.sticky-banner-grid')
                    .removeClass('container-edge2');
                  $('.icon-dropdown')
                    .addClass('white');
                  $('.icon-dropdown')
                    .removeClass('bodyColor');
                  $('.sticky-banner')
                    .removeClass('background-white');

                  if ($window.innerWidth >= 768) {
                    $('#nav-list')
                      .find('a')
                      .addClass('white');
                    $('#nav-list')
                      .find('a')
                      .removeClass('bodyColor');
                    $('#nav-list')
                      .find('button')
                      .removeClass('btn-trn-blk bodyColor');
                    $('#nav-list')
                      .find('button')
                      .addClass('btn-trn white');
                    $('.sticky-banner')
                      .removeClass('background-white');
                  }

                  animationInProgress = false;
                  animationState = "top"

                } else if (scope.target && animationInProgress == false && (animationState != "up" || animationState != "top") && $(this)
                  .scrollTop() < lastScrollY && $(this)
                  .scrollTop() <= ($('.nav-header')
                    .height() + $(scope.target)
                    .height())) {
                  // HIDE
                  animationInProgress = true;
                  animationState = "up"

                  $('.sticky-banner')
                    .animate({
                      top: '-52px'
                    }, 250, function() {
                      $('.icon-dropdown')
                        .addClass('white');
                      $('.icon-dropdown')
                        .removeClass('bodyColor');
                      $('#nav-location')
                        .removeClass('hide');
                      $('#nav-logo')
                        .addClass('hide');
                      $('.sticky-banner')
                        .removeClass('banner-stick');
                      $('.sticky-banner')
                        .removeClass('background-white-transparent');
                      $('.sticky-banner-grid')
                        .removeClass('container-edge2');
                      animationInProgress = false;
                    });

                }
                lastScrollY = $(this)
                  .scrollTop();
              });
          });
        }

        scope.$watch(function() {
          return $window.innerWidth
        }, function() {
          if ($window.innerWidth >= 768) {
            scope.pinned = false;
            scope.menuActive = false;
            if (scope.target) {
              if (animationState === 'down') {
                $('#nav-list a')
                  .addClass('white');
                $('#nav-list a')
                  .removeClass('bodyColor');
                $('#nav-list button')
                  .removeClass('btn-trn-blk bodyColor');
                $('#nav-list button')
                  .addClass('btn-trn white');
                $('.icon-dropdown')
                  .addClass('white');
                $('.sticky-banner')
                  .removeClass('background-white');
              } else {
                $('#nav-list a')
                  .addClass('white');
                $('#nav-list a')
                  .removeClass('bodyColor');
                $('#nav-list button')
                  .removeClass('btn-trn-blk bodyColor');
                $('#nav-list button')
                  .addClass('btn-trn white');
                $('.icon-dropdown')
                  .addClass('white');
              }

            }

            $('#sticky-banner')
              .removeClass('background-white');

          } else {
            scope.pinned = true;

            $('#nav-list a')
              .removeClass('white');
            $('.icon-dropdown')
              .removeClass('white');
            $('.icon-dropdown')
              .addClass('bodyColor');
            $('#nav-list a')
              .addClass('bodyColor');
            $('#nav-list button')
              .addClass('btn-trn-blk bodyColor');
            $('#nav-list button')
              .removeClass('btn-trn white');
          }
        });

        // scope.formData = SearchDataIntent.resetPagination();
        // scope.formData.type = 'product';

      }
    }
  }]);