'use strict'

angular.module('antsquare')
  .directive('comments2', ['ProductsFactory', 'EventsFactory', 'MomentsFactory', '$rootScope', 'Auth', function(ProductsFactory, EventsFactory, MomentsFactory, $rootScope, Auth) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/comments/comments2.html',
      scope: {
        product: '=',
        user: '=',
        commentsOf: '='
      },
      link: function(scope, element, attr) {
        scope.user = Auth.getUserFn();
        element.css("max-height", (element.height() - 25))
        element.css("overflow-y", "scroll")
        scope.show = true;
        scope.gotAll = false;
        // scope.user = null;
        scope.loading = true;
        scope.vm = {};

        scope.$watch('product', function() {
          if (scope.product) {
            scope.comments = {
              comments: [],
              page: 1,
              per_page: 5,
              last_top: false,
            };
            scope.search();
          }
        });
        scope.form = {
          content: ""
        };
        scope.search = function() {
          scope.vm = {};
          scope.comments.comments = [];
          if (scope.product.object_type == "product") {
            ProductsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else if (scope.product.object_type == "event") {
            EventsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else if (scope.product.object_type == "moment") {
            MomentsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          }

        };

        scope.getAllComments = function() {
          scope.gotAll = true;
          scope.vm = {};
          // scope.comments.comments = [];
          scope.comments = {
            comments: [],
            page: 1,
            per_page: 9999,
            last_top: false
          };
          if (scope.product.object_type == "product") {
            ProductsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else if (scope.product.object_type == "event") {
            EventsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else if (scope.product.object_type == "moment") {
            MomentsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          }
        };

        scope.submited = false;

        $rootScope.$on('postComment', function(event, data) {

          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
          if (!scope.submited) {

            scope.submited = true;
            scope.form.content = data;

            if (scope.product.object_type == "product") {
              ProductsFactory.postComment(scope.form, scope.product.id)
                .then(function(data) {
                  data.data.user_info = {
                    name: scope.user.name
                  }
                  scope.vm.comments.push(data.data);
                  //scope.vm.total++;
                  scope.comments.total++;
                  scope.form.content = "";
                  scope.submited = false;
                })
                .catch(function(error) {
                  scope.submited = false;
                })
            } else if (scope.product.object_type == "event") {
              EventsFactory.postComment(scope.form, scope.product.id)
                .then(function(data) {
                  data.data.user_info = {
                    name: scope.user.name
                  }
                  scope.vm.comments.push(data.data);
                  //scope.vm.total++;
                  scope.comments.total++;
                  scope.form.content = "";
                  scope.submited = false;
                })
                .catch(function(error) {
                  scope.submited = false;
                })
            } else if (scope.product.object_type == "moment") {
              MomentsFactory.postComment(scope.form, scope.product.id)
                .then(function(data) {
                  data.data.user_info = {
                    name: scope.user.name
                  }
                  scope.vm.comments.push(data.data);
                  //scope.vm.total++;
                  scope.comments.total++;
                  scope.form.content = "";
                  scope.submited = false;
                })
                .catch(function(error) {
                  scope.submited = false;
                })
            }
          }

        })

        $rootScope.$on('postCommentProductPop', function(event, data) {



          if (data.product_id === scope.product.id) {

            if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
            if (!scope.submited) {

              scope.submited = true;
              scope.form.content = data.commentContent;
              scope.product.id = data.product_id;

              ProductsFactory.postComment(scope.form, scope.product.id)
                .then(function(data) {
                  data.data.user_info = {
                    name: scope.user.name
                  }
                  scope.vm.comments.push(data.data);
                  //scope.vm.total++;
                  scope.comments.total++;
                  scope.form.content = "";
                  scope.submited = false;
                })
                .catch(function(error) {
                  scope.submited = false;
                })
            }
          }

        })

        scope.postComment = function() {
          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
          if (!scope.submited) {
            scope.submited = true;
            ProductsFactory.postComment(scope.form, scope.product.id)
              .then(function(data) {
                console.log(data)
                scope.vm.comments.push(data.data);
                //scope.vm.total++;
                scope.comments.total++;
                scope.form.content = "";
                scope.submited = false;
              })
              .catch(function(error) {
                console.log(error)
                scope.submited = false;
              })
          }
        };

        scope.flagComment = function(id, index) {
          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'flagComment');
          // scope.flagged = true;
          scope.comments.comments[index].flagged = true;
          ProductsFactory.flagComment(id)
            .then(function(data) {

            })
            .catch(function(error) {
              console.log(error)
                // scope.flagged = false;
              scope.comments.comments[index].flagged = false;
            });

        }

      }
    }
  }]);