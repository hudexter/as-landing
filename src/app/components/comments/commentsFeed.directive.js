'use strict'

angular.module('antsquare')
  .directive('commentsFeed', ['ProductsFactory', '$rootScope', 'Auth', 'MomentsFactory', function(ProductsFactory, $rootScope, Auth, MomentsFactory) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/comments/comments2.html',
      scope: {
        product: '=',
        user: '=',
        type: '@'
      },
      link: function(scope, element, attr) {
        scope.user = Auth.getUserFn();
        element.css("max-height", (element.height() - 25))
        element.css("overflow-y", "scroll")
        scope.show = true;
        scope.gotAll = false;
        // scope.user = null;
        scope.loading = true;
        scope.vm = {};

        scope.$watch('product', function() {
          if (scope.product) {
            scope.comments = {
              comments: [],
              page: 1,
              per_page: 5,
              last_top: false,
            };
            scope.search();
          }
        });
        scope.form = {
          content: ""
        };

        scope.search = function() {
          if (scope.product.type === 'product') {
            scope.vm = {};
            scope.comments.comments = [];
            ProductsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else {
            scope.vm = {};
            scope.comments.comments = [];
            MomentsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          }
        };

        scope.getAllComments = function() {
          if (scope.product.type === 'product') {
            scope.gotAll = true;
            scope.vm = {};
            // scope.comments.comments = [];
            scope.comments = {
              comments: [],
              page: 1,
              per_page: 9999,
              last_top: true
            };
            ProductsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          } else {
            scope.gotAll = true;
            scope.vm = {};
            // scope.comments.comments = [];
            scope.comments = {
              comments: [],
              page: 1,
              per_page: 9999,
              last_top: true
            };
            MomentsFactory.getComments(scope.comments, scope.product.id)
              .then(function(data) {
                scope.loading = false;
                scope.comments = data.data;
                scope.vm = data.data;
              })
              .catch(function(data) {
                scope.loading = false;
              })
          }

        };

        scope.submited = false;

        $rootScope.$on('postComment', function(event, data) {
          if (data.target_json.id === scope.product.id) {
            if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
            if (!scope.submited) {
              console.log("in post comment");
              scope.submited = true;
              scope.form.content = data.commentContent;
              scope.product.id = data.target_json.id;
              if (data.target_json.type == 'product') {
                ProductsFactory.postComment(scope.form, scope.product.id)
                  .then(function(data) {
                    data.data.user_info = {
                      name: scope.user.name
                    }
                    scope.vm.comments.unshift(data.data);
                    scope.vm.total++;
                    // scope.comments.total++;
                    scope.form.content = "";
                    scope.submited = false;
                  })
                  .catch(function(error) {
                    scope.submited = false;
                  })
              } else if (data.target_json.type == 'moment') {
                MomentsFactory.postComment(scope.form, scope.product.id)
                  .then(function(data) {
                    data.data.user_info = {
                      name: scope.user.name
                    }
                    scope.vm.comments.unshift(data.data);
                    scope.vm.total++;
                    scope.form.content = "";
                    scope.submited = false;
                  })
                  .catch(function(error) {
                    scope.submited = false;
                  })
              }

            }
          }
        })

        // scope.submitComment = funtion(feedTargetJson, index){
        //   console.log()
        // }

        // scope.postComment = function() {
        // if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
        // if (!scope.submited) {
        //   scope.submited = true;
        //   ProductsFactory.postComment(scope.form, scope.product.id)
        //     .then(function(data) {
        //       console.log(data)
        //       scope.vm.comments.unshift(data.data);
        //       scope.vm.total++;
        //       scope.comments.total++;
        //       scope.form.content = "";
        //       scope.submited = false;
        //     })
        //     .catch(function(error) {
        //       console.log(error)
        //       scope.submited = false;
        //     })
        // }
        // };

        scope.flagComment = function(id, index) {
          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'flagComment');
          // scope.flagged = true;
          scope.comments.comments[index].flagged = true;
          ProductsFactory.flagComment(id)
            .then(function(data) {

            })
            .catch(function(error) {
              console.log(error)
                // scope.flagged = false;
              scope.comments.comments[index].flagged = false;
            });

        }

      }
    }
  }]);