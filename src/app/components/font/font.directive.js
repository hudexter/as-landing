'use strict';

angular.module('antsquare')
	.directive('font', [function () {
		return {
			restrict: 'E',
      templateUrl: 'app/components/font/font.html',
      scope: {},
      link: function (scope, element, attr) {

      }
    }
  }]);
