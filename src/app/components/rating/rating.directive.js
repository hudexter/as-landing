'use strict'

angular.module('antsquare')
	.directive('rating', [function () {
		return {
			restrict: 'E',
			templateUrl: 'app/components/rating/rating.html',
			scope: {
				star: '=',
				total: '=',
				max: '@'
			},
			link: function (scope, element, attr) {
				scope.max = scope.max || 5;
				scope.avgRating = 0;
				scope.rating = [];

				scope.$watch('star', function () {
					if (scope.star) {
						scope.avgRating = scope.star;
						getList();
					} else {
						scope.avgRating = 0;
						getList();
					}
				});

				function getList() {

					scope.rating = [];
					for (var i = 0; i < scope.max; i++) {
						scope.rating.push({
							icon: 'icon-star-empty'
						});
					};

					var halfStar = false;
					scope.avgRating = parseFloat(scope.avgRating);
					if (scope.avgRating % 1 > 0.0 && scope.avgRating % 1 <= 0.5) {
						// create half star
						halfStar = true;
						var index = Math.floor(scope.avgRating)
					} else {
						var index = Math.round(scope.avgRating);
					}
					for (var i = 0; i < index; i++) {
						scope.rating[i].icon = 'icon-starfilled';
					}
					if (halfStar) {
						scope.rating[index].icon = 'icon-star-half-alt';
					}
				};


			}
		}
    }]);
