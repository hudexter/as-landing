'use strict';

angular.module('antsquare')
  .directive('card', ['$rootScope', 'GeoLocationService', '$mixpanel', 'ModalService', 'ProductsFactory', 'toastr', 'ModalStorage', 'RequestFactory', 'Auth', 'AccountsFactory', 'MomentsFactory', '$location', function($rootScope, GeoLocationService, $mixpanel, ModalService, ProductsFactory, toastr, ModalStorage, RequestFactory, Auth, AccountsFactory, MomentsFactory, $location) {
    return {
      restrict: 'E',
      // templateUrl: 'app/components/cardPlate/card/card.html',
      link: function(scope, element, attr) {
        // if (scope.vm.cards[scope.vm.cards.length-1].id === scope.card.id) {
        //  $rootScope.$on('loadCard
        // }
        scope.UserFollowProfile = true;
        scope.StoreFollowProduct = true;
        //scope.ShowHoverPage = true;
        // User
        if (Auth.isLoggedIn()) {
          scope.userInfo = Auth.getUserFn();

        } else {
          scope.userInfo = null;

        }

        $rootScope.$on('updateUser', function(event, data) {
          if (data) {
            scope.userInfo = data;

          } else {
            scope.userInfo = null;

          }
        });
        //for hide follow button user profile
        if (scope.userInfo) {

          if (scope.userInfo.id == scope.card.user_id) {

            scope.UserFollowProfile = false;
            scope.StoreFollowProduct = false;

          } else {

            scope.UserFollowProfile = true;
            scope.StoreFollowProduct = true;
          }
        }

        if (GeoLocationService.hasLocation()) {
          GeoLocationService.getCountry()
            .then(function(data) {
              scope.country = data;
            })
        }

        $rootScope.$on('setLocation', function() {
          GeoLocationService.getCountry()
            .then(function(data) {
              scope.country = data;
            })
        });

        function getMap(lat, lon, width, height) {
          return "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=16&markers=icon:http://goo.gl/CRWwl2" + "%7C" + lat + "," + lon;
        }

        scope.card.mapImg = getMap(scope.card.lat, scope.card.lon, 300, 300);
        if (scope.card.images) {
          scope.card.images[0] = scope.card.images[0] + '?w=500&auto=format,compress';
        }

        scope.card.link = "https://dashboard.antsquare.com/product/" + scope.card.id

        // commenting
        // scope.loadMore = function() {
        //   $rootScope.$broadcast('loadMore');
        // };
        // scope.loadMoreTemp = function() {
        //   var bntContent = element.find('#loadMoreTemp');
        //   bntContent.text('')
        //   bntContent.append('<i class="icon-spin5 icon-sm spiral animate-spin"></i>');
        //   $rootScope.$broadcast('loadMoreTemp');
        //   $mixpanel.track('l-loadMore');
        // };

        scope.toTop = function() {
          $rootScope.$broadcast('toTop');
        }

        scope.deletePrompt = function(id) {

          ModalStorage.set(id, ProductsFactory.deleteProduct);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalSure/modalSure.html",
              controller: "ModalSureCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                if (result) {
                  element.parent()
                    .remove();
                  angularGridInstance[scope.cardPlateId].refresh();
                  scope.$emit('deleteProduct');
                }
              });
            });

        };

        scope.detailView = function(card) {
          if (card.type === "product")
            $location.path('/product/' + card.id)
          else if (card.type === "moment")
            $location.path('/moment/' + card.id)
            // ModalStorage.set(card);
            // console.log(card);
            // ModalService.showModal({
            //     templateUrl: "app/components/modal/modalDetailView/modalDetailView.html",
            //     controller: "ModalDetailViewCtrl"
            //   })
            //   .then(function(modal) {
            //     modal.close.then(function(result) {
            //       ModalStorage.end();
            //       // if (result) {
            //       // element.parent()
            //       //   .remove();
            //       // scope.$emit('deleteProduct');
            //       // }
            //     });
            //   });
        };
        scope.detailMomentView = function(card) {

          ModalStorage.set(card);
          console.log(card);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalMomentDetailView/modalMomentDetailView.html",
              controller: "ModalMomentDetailViewCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
                // if (result) {
                // element.parent()
                //   .remove();
                // scope.$emit('deleteProduct');
                // }
              });
            });
        };
        scope.momentDeletePrompt = function(id) {
          ModalStorage.set(id, MomentsFactory.deleteMoment);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalSure/modalSure.html",
              controller: "ModalSureCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                if (result) {
                  element.parent()
                    .remove();
                  scope.$emit('deleteProduct');
                }
              });
            });
        };
        scope.likeToggle = function() {
          if (!scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')
          scope.card.is_liked = !scope.card.is_liked;
          if (scope.card.is_liked) {
            AccountsFactory.likeProduct(scope.card.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.card.is_liked = !scope.card.is_liked
              })
          } else {
            AccountsFactory.unlikeProduct(scope.card.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error);
                scope.card.is_liked = !scope.card.is_liked;
              })
          }
        };
        scope.likeMomentToggle = function() {
          if (!scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')
          scope.card.is_liked = !scope.card.is_liked;
          if (scope.card.is_liked) {
            AccountsFactory.likeMoment(scope.card.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.card.is_liked = !scope.card.is_liked
              })
          } else {
            AccountsFactory.unlikeMoment(scope.card.id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error);
                scope.card.is_liked = !scope.card.is_liked;
              })
          }
        };
        scope.followToggle = function() {
          if (!scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')
          scope.card.user_info.is_following = !scope.card.user_info.is_following;

          if (!scope.card.user_info.is_following) {
            AccountsFactory.unfollowUser(scope.card.user_id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.card.user_info.is_following = !scope.card.user_info.is_following;
              })
          } else {
            AccountsFactory.followUser(scope.card.user_id)
              .then(function(data) {})
              .catch(function(error) {
                console.log(error)
                scope.card.user_info.is_following = !scope.card.user_info.is_following;
              })
          }
        };

        scope.requestDeletePrompt = function(id) {
          ModalStorage.set(id, RequestFactory.deleteRequest);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalSure/modalSure.html",
              controller: "ModalSureCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                if (result) {
                  element.parent()
                    .remove();
                  scope.$emit('deleteProduct');
                }
              });
            });
        }
        scope.getCardTemplate = function() {
          console.log(scope.card);
          if (scope.card.type === 'product' || scope.card.type === 'service') {
            return 'app/components/cardPlate/card/productCard2.html'
          } else if ((scope.card.type === 'has_more' || scope.card.type === 'has_more_temp' || scope.card.type === 'no_more') && (scope.noMore == !scope.noMore || scope.noMore == undefined)) {
            return 'app/components/cardPlate/card/moreCard.html'
          } else if (scope.card.type === 'request') {
            return 'app/components/cardPlate/card/requestCard.html'
          } else if (scope.card.type === 'moment') {
            return 'app/components/cardPlate/card/productCard2.html'
          } else if (scope.card.type === 'store') {
            return 'app/components/cardPlate/card/storeCard.html'
          } else if (scope.card.type === 'user') {
            return 'app/components/cardPlate/card/userCard.html'
          } else if (scope.card.type === 'tag') {
            return 'app/components/cardPlate/card/tagCard.html'
          } else {
            return 'app/components/cardPlate/card/card.html'
          }
        };


        scope.followToggle = function(user, index) {
          if (!scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

          scope.card.is_following = !scope.card.is_following;
          scope.card.isLoading = true;
          scope.card.user_id = user;
          if (!scope.card.is_following) {
            AccountsFactory.unfollowUser(scope.card.user_id)
              .then(function(data) {
                scope.card.isLoading = false;
              })
              .catch(function(error) {
                scope.card.is_following = !scope.card.is_following;
                scope.card.isLoading = false;
              })
          } else {
            AccountsFactory.followUser(scope.card.user_id)
              .then(function(data) {
                scope.card.isLoading = false;
              })
              .catch(function(error) {
                scope.card.isLoading = false;
                scope.card.is_following = !scope.card.is_following;
              })
          }
        };
        scope.getTags = function(tag) {
          $rootScope.$broadcast('selectTag', tag)
        }

      },
      template: "<div ng-include='getCardTemplate()'></div>"
    }
  }]);