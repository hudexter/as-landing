'use strict';

angular.module('antsquare')
  .directive('cardPlate', ['angularGridInstance', '$window', '$rootScope', function(angularGridInstance, $window, $rootScope) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/cardPlate/cardPlate.html',
      scope: {
        vm: '=',
        cardPlateId: '@',
        noMore: '@',
        mode: '@',
        type: '@'
      },
      link: function(scope, element, attr) {

        scope.$on('deleteProduct', function() {

          angularGridInstance[scope.cardPlateId].refresh();
        });

        // $rootScope.$on('loadCard', function () {
        // angularGridInstance[scope.cardPlateId].refresh();
        // });

        scope.$watch('vm', function() {
          if (scope.vm) {
            scope.cards = null;
            if (scope.cardPlateId === 'infoWindowCard') {
              setTimeout(function() {
                scope.cards = _.cloneDeep(scope.vm);
                angularGridInstance[scope.cardPlateId].refresh();
              })
            }
            setTimeout(function() {
              angularGridInstance[scope.cardPlateId].refresh();
            })
          }
        });

        if (!scope.cardPlateId) {
          scope.cardPlateId = 'gallery';
        }
        scope.refresh = function() {
            if (scope.cardPlateId === 'infoWindowCard') {
              scope.cards = null;
              scope.cards = _.cloneDeep(scope.vm);
              setTimeout(function() {
                scope.cards = _.cloneDeep(scope.vm);
                angularGridInstance.infoWindowCard.refresh();
                angularGridInstance[scope.cardPlateId].refresh();
              }, 300)
            }
          }
          // Set card width
        scope.cardHover = function(card) {
          $rootScope.$broadcast('cardHover', card)
        }

        scope.cardLeave = function(card) {
          $rootScope.$broadcast('cardLeave', card)
        }

        scope.imgWidth = 200;

        if (scope.cardPlateId === 'myStore') {
          scope.imgWidth = 70;

        }
        scope.$watch(function() {
          return $window.innerWidth
        }, function() {
          // if ($window.innerWidth < 480) {
          //  scope.imgWidth = 128
          // } else {
          // scope.imgWidth = 200;
          // };
          scope.imgWidth = 200;
          if (scope.cardPlateId === 'myStore') {
            scope.imgWidth = 130;
          }

        });
      }
    }
  }]);