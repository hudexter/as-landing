'use strict';

angular.module('antsquare')
	.directive('searchViewSwitch', ['angularGridInstance', '$rootScope', 'URLS', 'deviceDetector', '$q', 'SearchDataIntent', '$window', function (angularGridInstance, $rootScope, URLS, deviceDetector, $q, SearchDataIntent, $window) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/searchViewSwitch/searchViewSwitch.html',
			scope: false,
			link: function (scope, element, attr) {

				scope.currentState = 2;
				scope.filterShow = false;

				$rootScope.$on('showFilter', function (event, data) {
					scope.filterShow = data;
					fullMapView();
				});

				scope.$watch('currentState', function () {
					fullMapView();
				})

				scope.$watch(function () {
					return $window.innerWidth
				}, function () {
					if ($window.innerWidth <= 1023) {
						if (scope.currentState == 2) {
							scope.switchView(1);
						}
					}
				});

				function fullMapView() {
					if (scope.currentState === 3 && scope.filterShow) {
						angular.element(document.querySelector('.angular-google-map-container'))
							.addClass('shrink-map');
						angular.element(document.querySelector('.map-view-map-container'))
							.addClass('shrink-map');
					} else {
						angular.element(document.querySelector('.angular-google-map-container'))
							.removeClass('shrink-map');
							angular.element(document.querySelector('.map-view-map-container'))
								.removeClass('shrink-map');
					}
				}

				scope.switchView = function (view) {
					scope.currentState = view;
					if (view === 1) {
						angular.element(document.querySelector('#searchMapContainer'))
							.addClass('hide-map-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.addClass('expand-grid-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.removeClass('hide-grid-container');
						angular.element(document.querySelector('#searchMapContainer'))
							.removeClass('expand-map-container');
						angular.element(document.querySelector('#filterContainer'))
							.removeClass('filter-expand');
					} else if (view === 2) {
						angular.element(document.querySelector('#searchMapContainer'))
							.removeClass('hide-map-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.removeClass('expand-grid-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.removeClass('hide-grid-container');
						angular.element(document.querySelector('#searchMapContainer'))
							.removeClass('expand-map-container');
						angular.element(document.querySelector('#filterContainer'))
							.removeClass('filter-expand');
						$rootScope.$broadcast('fullMapView');
					} else if (view === 3) {
						scope.filterShow = false;
						$rootScope.$broadcast('filterClose');
						angular.element(document.querySelector('#searchMapContainer'))
							.removeClass('hide-map-container');
						angular.element(document.querySelector('#searchMapContainer'))
							.addClass('expand-map-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.removeClass('expand-grid-container');
						angular.element(document.querySelector('#searchGridContainer'))
							.addClass('hide-grid-container');
						angular.element(document.querySelector('#filterContainer'))
							.addClass('filter-expand');
						$rootScope.$broadcast('fullMapView');
					};

					var time = 5
					for (var start = 1; start < 60; start++) {
						setTimeout(function () {
							(function (i) {
								$rootScope.$broadcast('refreshSlider');
							})(start)
						}, time * start);
					}
					setTimeout(function () {
						angularGridInstance.mapSearch.refresh();
					}, 300)
					$rootScope.$broadcast('switchView',view);

					if ($window.innerWidth <= 1023) {
						if (scope.currentState == 2) {
							scope.switchView(1);
						}
					}
					$rootScope.$broadcast('switchView', view);
				};
			}
		}
  }]);
