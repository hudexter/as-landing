'use strict'

angular.module('antsquare')
  .directive('countrySelector', ['GeoLocationService', '$rootScope', 'AccountsFactory', function(GeoLocationService, $rootScope, AccountsFactory) {
    return {
      restrict: 'EA',
      templateUrl: 'app/components/countrySelector/countrySelector.html',
      scope: {
        country: '=',
      },
      link: function(scope, element, attr) {

        scope.countries = [];

        // if (!scope.country) {
        // 	scope.country = {
        // 		name: 'United States',
        // 		iso2: "US",
        // 		class: 'us',
        // 		code: '1'
        // 	};
        // }

        if (GeoLocationService.hasLocation()) {
          if (!scope.country) {
            GeoLocationService.getCountry()
              .then(function(data) {
                scope.country = data;
              })
              .catch(function(error) {
                console.log(error)
                scope.country = {
                  name: 'United States',
                  class: 'us',
                  code: '1'
                };
              })
          }
        }

        $rootScope.$on('setLocation', function() {
          if (!scope.country) {
            GeoLocationService.getCountry()
              .then(function(data) {
                scope.country = data;
              })
              .catch(function(error) {
                console.log(error)
                scope.country = {
                  name: 'United States',
                  class: 'us',
                  code: '1'
                };
              })
          }
        });

        // scope.$watch('country', function() {
        if (!scope.country) {
          scope.country = {
            name: 'United States',
            iso2: "US",
            class: 'us',
            code: '1'
          };
        }
        if (scope.countries.length <= 0) {
          AccountsFactory.getCountryCode()
            .then(function(data) {
              scope.countries = data.data.countries
              if (typeof scope.country === 'string') {
                for (var i in scope.countries) {
                  if (scope.countries[i].name === scope.country || scope.countries[i].iso2 === scope.country) {
                    scope.country = scope.countries[i];
                  }
                };
              }
            })
        } else {
          for (var i in scope.countries) {
            if (scope.countries[i].name === scope.country.name || scope.countries[i].name === scope.country || scope.countries[i].iso2 === scope.country) {
              scope.country = scope.countries[i];
            }
          };
        }
        // })

        scope.selectCountry = function(country) {
          scope.country = country;
          console.log(country);
        };

        AccountsFactory.getCountryCode()
          .then(function(data) {
            scope.countries = data.data.countries
          })

      }
    }
  }]);