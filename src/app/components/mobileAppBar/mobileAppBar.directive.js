'use strict';

angular.module('antsquare')
	.directive('mobileAppBar', ['$state', '$location', 'deviceDetector', '$mixpanel', function ($state, $location, deviceDetector, $mixpanel) {

		return {
			restrict: 'E',
			scope: false,
			templateUrl: 'app/components/mobileAppBar/mobileAppBar.html',
			link: function (scope, element, attr) {

				if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
					scope.showBar = true;
				} else {
					scope.showBar = false;
				}

				if (deviceDetector.device === 'android') {
					scope.isAndroid = true;
				} else if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
					scope.isIOS = true;
				}

				scope.barToggle = function () {
					scope.showBar = false;
				};

				scope.openApp = function () {
					if (scope.isIOS) {
						setTimeout(function () {
							location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
							$mixpanel.track('l-m-cta-ios');
						}, 500);
					} else {
						var url = $location.url();
						url = 'antsquarecom://antsquare.com' + url;
						if (url === '/') {
							url = 'home';
						}
						location = url;
						setTimeout(function () {
							location = "https://ad.apps.fm/LRMs2kfpF0YQK_qEfUeyCF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfISXbMQwI-2AOqLYV5gygIechIMlYYx5zMHmF83sYo5BAfQUdy_B62HXVVdz_7CSe"
							$mixpanel.track('l-m-cta-android');
						}, 500);
					}
				};
			}
		}
  }]);
