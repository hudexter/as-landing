'use strict';

angular.module('antsquare')
  .directive('searchBox', [function() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/searchBox/searchBox.html',
      scope: {
        backgroundColor: '@'
      },
      link: function(scope, element, attr) {
        scope.data = {
          radius: 5,
          input: null,
          location: null,
          category: null
        };

      }
    }
  }]);