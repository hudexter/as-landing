angular.module('antsquare')
	.controller('ModalForgotPasswordCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', '$cookies', 'GeoLocationService', '$rootScope', '$state', '$rootScope', function ($scope, close, ModalStorage, StoreFactory, Auth, AccountsFactory, $q, $cookies, GeoLocationService, $rootScope, $state) {



		$scope.fadeOut = false;
		$scope.loading = true;

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};



		$scope.data = {
			code: '',
			dail_country: '1',
			phone_number: "",
			country: null,
		};

		$scope.submit = function () {
			$scope.data.phone_number = '+' + $scope.data.country.code + $scope.data.phone;
			$scope.data.input = $scope.data.phone_number;
			AccountsFactory.resetPassword($scope.data)
				.then(function (data) {
					$scope.data.state = 'comfirmCode';
					$state.go('resetPassword', {
						input: $scope.data.input
					})
					close($scope.data.state, 150);
				})
				.catch(function (error) {
					$scope.error = error.data.message;
				})

		};

		if (GeoLocationService.hasLocation()) {
			GeoLocationService.getCountry()
				.then(function (data) {
					$scope.data.country = data;
				})
				.catch(function (error) {
					$scope.data.country = {
						name: 'United States',
						class: 'us',
						code: '1'
					};
				})
		}

		$rootScope.$on('setLocation', function () {
			GeoLocationService.getCountry()
				.then(function (data) {
					$scope.data.country = data;
				})
				.catch(function (error) {
					console.log(error)
					$scope.data.country = {
						name: 'United States',
						class: 'us',
						code: '1'
					};
				})
		});

			}]);
