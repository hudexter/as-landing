angular.module('antsquare')
	.controller('ModalCreditCardCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', 'Auth', 'AccountsFactory', '$rootScope', 'StripeKey', function ($scope, close, ModalStorage, StoreFactory, Auth, AccountsFactory, $rootScope, StripeKey) {

		StripeKey.getKey();
		var paymentData = {
			stripe_token: null
		};
		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};
		$scope.loading = false;
		$scope.stripeCallback = function (code, result) {
			$scope.loading = true;
			if (result.error) {
				$scope.loading = false;
			} else {
				paymentData.stripe_token = result.id;
				AccountsFactory.newCard(paymentData)
					.success(function (data, status, headers, config) {
						$scope.number = null;
						$scope.cvc = null;
						$scope.expiry = null;
						$scope.loading = false;
						close(result, 150);
					})
					.error(function (data, status, headers, config) {
						$scope.number = null;
						$scope.cvc = null;
						$scope.expiry = null;
						$scope.loading = false;
					});
			}
		};

	}]);
