angular.module('antsquare')
    .controller('ModalPhoneVerifyCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', '$cookies', '$state', function($scope, close, ModalStorage, StoreFactory, Auth, AccountsFactory, $q, $cookies, $state) {

        // $scope.data = ModalStorage.get()[0];
        $scope.fadeOut = false;
        $scope.loading = true;
        $scope.submting = false;
        $scope.close = function(result) {
            $scope.fadeOut = true;
            close(result, 150);
        };
        $scope.data = {
            access_token: ModalStorage.get()[0].token.access_token,
            id: ModalStorage.get()[0].token.id || ModalStorage.get()[0].userInfo.id,
            // code: '',
            // dail_country: '1',
            phone_number: ModalStorage.get()[0].userInfo.input,
        };

        var getStore = function() {
            var deferred = $q.defer();
            StoreFactory.me()
                .then(function(data) {
                    Auth.setStore(data.data);
                    deferred.resolve(data);
                })
                .catch(function(error) {
                    console.log(error)
                    deferred.reject(error);
                })
            return deferred.promise;
        };

        var getUser = function() {
            var deferred = $q.defer();
            AccountsFactory.me()
                .then(function(data) {
                    Auth.setUser(data.data);
                    deferred.resolve(data);
                })
                .catch(function(error) {
                    console.log(error)
                    deferred.reject(error);
                })
            return deferred.promise;
        };

        $scope.submit = function() {
            $scope.submting = true;
            AccountsFactory.verifyPhoneNumber($scope.data)
                .then(function(response) {
                    $scope.loading = 'done';
                    Auth.setToken(response);
                    AccountsFactory.me()
                        .then(function(response) {
                            $q.all([getStore(), getUser()])
                                .then(function(value) {
                                    close(value, 150);
                                    $scope.submting = false;
                                    // $state.go('index.home');
                                }, function(error) {
                                    console.log(error)
                                    $scope.submting = false;
                                });
                        });

                })
                .catch(function(error) {
                    $scope.loading = 'error';
                    $scope.error = error.data.message;
                    $scope.submting = false;
                });
        };

        $scope.resendCode = function() {
            AccountsFactory.setMobile($scope.data)
                .then(function(data) {})
                .catch(function(error) {
                    $scope.error = error.data.message;
                    console.log(error);
                });
        };



    }]);