angular.module('antsquare')
    .controller('ModalUserLikesCtrl', ['$scope', 'close', 'ModalStorage', 'AccountsFactory', 'Auth', '$rootScope', function($scope, close, ModalStorage, AccountsFactory, Auth, $rootScope) {
        $scope.fadeOut = false;
        $scope.loading = true;
        $scope.close = function(result) {
            $scope.fadeOut = true;
            close(result, 150);
        };


        $scope.followToggle = function(user, index) {
            if (Auth.isLoggedIn()) {
                $scope.userList[index].isLoading = true;
                $scope.followAction = true;
                $scope.userList[index].is_following = !$scope.userList[index].is_following
                if (!user.is_following) {
                    AccountsFactory.unfollowUser(user.id)
                        .then(function(data) {
                            $scope.userList[index].isLoading = false;
                        })
                        .catch(function(error) {
                            $scope.userList[index].isLoading = false;
                            $scope.userList[index].is_following = !$scope.userList[index].is_following
                            console.log(error)
                        })
                } else {
                    AccountsFactory.followUser(user.id)
                        .then(function(data) {
                            $scope.userList[index].isLoading = false;
                        })
                        .catch(function(error) {
                            $scope.userList[index].isLoading = false;
                            $scope.userList[index].is_following = !$scope.userList[index].is_following
                            console.log(error)
                        })
                }
            } else {
                return $rootScope.$broadcast('openLoginModal', 'follow');
            }
        };

        $scope.user = ModalStorage.get()[0];
        $scope.userList = [];
        $scope.params = {
            page: 1,
            per_page: 20,
            store: true,
        };

        $scope.loadMore = function() {
            if ($scope.params.has_more && !$scope.seraching) {
                delete $scope.params.users
                getList();
            }
        };

        function getList() {
            $scope.seraching = true;
            AccountsFactory.getUserProfileLikes($scope.user, $scope.params)
                .then(function(data) {
                    $scope.params = data.data;
                    $scope.params.page++;
                    $scope.userList = $scope.userList.concat(data.data.users);
                    $scope.loading = false;
                    $scope.seraching = false;
                })
                .catch(function(error) {
                    $scope.seraching = false;
                    $scope.loading = false;
                    console.log(error)
                })
        };

        getList();
    }]);