angular.module('antsquare')
  .controller('ModalDetailViewCtrl', ['$scope', 'close', 'StoreFactory', 'ModalStorage', 'ModalService', 'ProductsFactory', 'toastr', 'RequestFactory', 'MomentsFactory', 'GeoLocationService', '$rootScope', 'Auth', 'branchFactory', 'deviceDetector', '$stateParams', 'AccountsFactory', '$window', 'MapService', function($scope, close, StoreFactory, ModalStorage, ModalService, ProductsFactory, toastr, RequestFactory, MomentsFactory, GeoLocationService, $rootScope, Auth, branchFactory, deviceDetector, $stateParams, AccountsFactory, $window, MapService) {

    $scope.product = ModalStorage.get()[0];
    $scope.productId = ModalStorage.get()[0].id;
    $scope.fadeOut = false;
    $scope.loading = false;
    StoreFactory.getStoreById($scope.product).then(function(data) {
      $scope.product.store_info = data.data;
    })
    $scope.members = [];

    $scope.close = function(result) {
      $scope.fadeOut = true;
      close(result, 150);
    };

    if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
      $scope.isIOS = true;
    } else {
      $scope.isIOS = false;
    }
    $scope.message = "String including Emoji codes :smiley:";

    if ($rootScope.branchReady) {
      branchFactory.link('product', $scope.productId)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }

        })
    }

    $rootScope.$on('branchReady', function(event, data) {
      branchFactory.link('product', $scope.productId)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }
        })
    });

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
    } else {
      $scope.userInfo = null;
    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        $scope.StoreFollowProduct = false;
        start();
      } else {
        $scope.userInfo = null;
        $scope.StoreFollowProduct = false;
      }
    });

    $scope.orderOnApp = function() {
      if ($rootScope.isMobile) {
        if ($scope.isIOS) {
          // var url = $location.url();
          // url = 'antsquarecom://antsquare.com' + url;
          setTimeout(function() {
            location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
          }, 500);

          // redirect to 'antsquare://www.antsquare.com/product/:id'

        } else {
          var url = $location.url();
          url = 'antsquarecom://antsquare.com' + url;
          location = url;
          setTimeout(function() {
            location = "https://ad.apps.fm/LRMs2kfpF0YQK_qEfUeyCF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfISXbMQwI-2AOqLYV5gygIechIMlYYx5zMHmF83sYo5BAfQUdy_B62HXVVdz_7CSe"
          }, 500);
        }
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      } else {
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      }
    };

    // Product
    var distance = {};

    $scope.characterLimit = 16;
    $scope.coords = undefined;
    $scope.product = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.product) {
        distance = MapService.getDistance($scope.product.lat, $scope.product.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.product[key] = distance[key];
        }
      }
    });

    $scope.slideLeft = function() {
      if ($scope.slideIndex !== 0) {
        $scope.slideIndex = $scope.slideIndex - 1;
      }
    };

    $scope.slideRight = function() {
      if ($scope.slideIndex < $scope.product.images.length) {
        $scope.slideIndex = $scope.slideIndex + 1;
      }
    };
    $scope.toSlide = function(index) {
      $scope.slideIndex = index;
    }

    function getShareLinks(product) {
      // $scope.root = $location.host();
      $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/product/" + product.id;

      $scope.twLink = "http://twitter.com/intent/tweet?status=" + product.name + "+%40antsquare+" + "https://www.antsquare.com/product/" + product.id;
      "http://twitter.com/intent/tweet?status=" + product.name + "@antsquare" + "https://www.antsquare.com/product/" + product.id;

      $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/product/" + product.id + "&amp;media=" + product.images[0] + "&amp;description=Check it out!"

      $scope.mailLink = "mailto:?Subject=Check out " + product.name + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/product/" + product.id;
    };
    $scope.deletePrompt = function(id) {

      ProductsFactory.deleteProduct($scope.productId)
        .then(function(data) {
          //$window.location.reload();
          $scope.userid = $scope.userInfo.id;

          $state.go('user/:user_id', { user_id: $scope.userid });
          $scope.close();

        })
        .catch(function(error) {

        });

    };

    function checkImgContainerSize() {
      // if (window.innerWidth < 768) {
      var imgContainer = angular.element(document.querySelector('#rn-carousel'));
      var x = imgContainer.width();
      imgContainer.height(x);
      // }
    };

    function getPage() {
      $scope.coords = GeoLocationService.getCoords();
      var input = {
        product_id: $scope.productId,
        viewer_id: null,
        lat: null,
        lon: null

      };

      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id;
        var userid = $scope.userInfo.id;
      };
      if ($scope.coords) {
        input.lat = $scope.coords.latitude;
        input.lon = $scope.coords.longitude;
      }


      ProductsFactory.getProductPageView(input)
        .then(function(data) {
          $scope.product = data.data;

          if (userid != $scope.product.user_id) {
            $scope.StoreFollowProduct = true;
          }
          $scope.$broadcast('productLoaded');
          if (GeoLocationService.hasLocation()) {
            $scope.coords = GeoLocationService.getCoords();
            distance = MapService.getDistance($scope.product.lat, $scope.product.lon, $scope.coords.latitude, $scope.coords.longitude);
            for (var key in distance) {
              $scope.product[key] = distance[key];
            }
          }

          getShareLinks($scope.product);

          $scope.width = $window.innerWidth;
          $scope.card = document.getElementById('card').offsetWidth;
          $scope.cardHeight = document.getElementById('card').offsetHeight;
          $scope.left = document.getElementById('leftPanel').offsetWidth;

          if ($scope.card - 5 > $scope.left) {
            $scope.productDescription = {
              'max-width': $scope.card - $scope.left - 19 + 'px',
              'word-break': 'break-all'
            }
            $scope.rightscrollHeight = {
              'max-height': $scope.cardHeight - 131 + 'px',
              'min-height': $scope.cardHeight - 131 + 'px',
              'max-width': $scope.card - $scope.left - 19 + 'px',
              'overflow-y': 'scroll',
              'word-break': 'break-all'
            }
          } else {
            $scope.rightscrollHeight = {
              'max-height': $scope.cardHeight - 131 + 'px',
              'min-height': $scope.cardHeight - 131 + 'px',
              'max-width': $scope.left + 'px',
              'overflow-y': 'scroll',
              'word-break': 'break-all'
            }
            $scope.productDescription = {
              'max-width': $scope.left + 'px',
              'word-break': 'break-all'
            }
          }

        })
        .catch(function(err) {
          $scope.product = err.message;
        });
    };

    function start() {
      $scope.coords = undefined;
      $scope.product = undefined;
      getPage();
      checkImgContainerSize();
    }

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.product.user_info.is_following = !$scope.product.user_info.is_following;

      if (!$scope.product.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.product.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.product.user_info.is_following = !$scope.product.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.product.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.product.user_info.is_following = !$scope.product.user_info.is_following;
          })
      }
    };

    $scope.likeToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.product.liked = !$scope.product.liked;
      if ($scope.product.liked) {
        AccountsFactory.likeProduct($scope.productId)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.product.liked = !$scope.product.liked
          })
      } else {
        AccountsFactory.unlikeProduct($scope.productId)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.product.liked = !$scope.product.liked;
          })
      }
    };

    $scope.flagProduct = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagProduct')
      $scope.flagged = true;
      ProductsFactory.flagProduct($scope.productId)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    $scope.showProductLikes = function() {
      ModalStorage.set($scope.productId);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalLikes/modalLikes.html",
          controller: "ModalProductPopupLikesCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };


    $scope.submitComment = function() {

      var commentdata = {
        product_id: $scope.productId,
        commentContent: $scope.content


      };


      $rootScope.$broadcast('postCommentProductPop', commentdata)
      $scope.content = null;
    }
    start();
    window.onresize = checkImgContainerSize;
    $scope.ready = false;

    $scope.ready = true;

    angular.element($window).bind('resize', function() {
      $scope.ready = false;
      $scope.card = document.getElementById('card').offsetWidth;
      $scope.left = document.getElementById('leftPanel').offsetWidth;
      if ($scope.card - 5 > $scope.left) {
        $scope.productDescription = {
          'max-width': $scope.card - $scope.left - 19 + 'px'
        }
      } else {
        $scope.productDescription = {
          'max-width': $scope.left + 'px'
        }
      }

      $scope.ready = true;
      $scope.$digest();
    });
    // });

  }]);