angular.module('antsquare')
	.controller('ModalAddPhoneCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', '$cookies', 'GeoLocationService', '$rootScope', function ($scope, close, ModalStorage, StoreFactory, Auth, AccountsFactory, $q, $cookies, GeoLocationService, $rootScope) {

		$scope.userInfo = ModalStorage.get()[0];
		$scope.fadeOut = false;
		$scope.loading = true;

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};

		$scope.data = {
			access_token: $scope.userInfo.access_token,
			id: $scope.userInfo.id,
			code: null,
			dail_country: null,
			phone_number: $scope.userInfo.phone_number,
			country: null
		};

		$scope.loading = false;
		$scope.submit = function () {
			$scope.data.code = $scope.data.country.code;
			$scope.data.dail_country = $scope.data.country.code;
			$scope.loading = true;
			$scope.data.phone_number = '+' + $scope.data.country.code + $scope.data.phone;
			$scope.data.input = $scope.data.phone_number;
			AccountsFactory.setMobile($scope.data)
				.then(function (data) {
					var modalData = {
						token: $scope.data,
						userInfo: $scope.data
					};
					close(modalData, 150);
					$scope.loading = false;
				})
				.catch(function (error) {
					$scope.loading = false;
					$scope.error = error.data.message;
				})
		};

	}]);
