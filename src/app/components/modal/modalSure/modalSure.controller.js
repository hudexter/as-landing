angular.module('antsquare')
    .controller('ModalSureCtrl', ['$scope', 'close', 'StoreFactory', 'ModalStorage', function($scope, close, StoreFactory, ModalStorage) {

        $scope.function = ModalStorage.get()[1];
        $scope.fadeOut = false;
        $scope.loading = false;
        $scope.close = function(result) {
            $scope.fadeOut = true;
            close(result, 150);
        };

        $scope.submit = function() {
            $scope.loading = true;
            $scope.function(ModalStorage.get()[0]).then(function(data) {
                $scope.loading = false;
                close(true, 150);
            }).catch(function(error) {
                $scope.error = error.data.message;
            })
        };

    }]);