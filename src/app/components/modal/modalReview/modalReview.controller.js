angular.module('antsquare')
	.controller('ModalReviewCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', function ($scope, close, ModalStorage, StoreFactory) {
		$scope.fadeOut = false;
		$scope.loading = true;
		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};
		$scope.reviewList = [];
		$scope.storeInfo = ModalStorage.get()[0];
		var params = {
			page: 1,
			per_page: null,
			store: true
		};
		function getList() {
			StoreFactory.getReviews($scope.storeInfo.id, params)
				.then(function (data) {
					$scope.reviewList = data.data;
					$scope.loading = false;
				})
				.catch(function (error) {
					console.log(error)
					$scope.loading = false;
				})
		};
		getList();
	}]);
