angular.module('antsquare')
	.controller('ModalCategoryCtrl', ['$scope', 'close', 'ModalStorage', 'ProductsFactory', function ($scope, close, ModalStorage, ProductsFactory) {

		$scope.product = ModalStorage.get()[0];

		ProductsFactory.getCategories()
			.then(function (data) {
				if (ModalStorage.get()[0].type === 'product') {
					$scope.categories = data.data.products;
				} else if (ModalStorage.get()[0].type === 'service') {
					$scope.categories = data.data.services;
				} else if (ModalStorage.get()[0].type === 'request') {
					$scope.categories = data.data.requests;
				}
			})

		$scope.fadeOut = false;
		$scope.loading = true;

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};

		$scope.loading = false;
		$scope.submit = function (category) {
			close(category, 150);
		};

	}]);
