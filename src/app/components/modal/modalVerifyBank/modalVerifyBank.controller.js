angular.module('antsquare')
	.controller('ModalVerifyBankCtrl', ['$scope', 'close', 'ModalStorage', 'AccountsFactory', function ($scope, close, ModalStorage, AccountsFactory) {

		$scope.data = {};
		$scope.error = null;
		$scope.product = {};
		$scope.loading = false;

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};

		$scope.verifyBank = function () {
			$scope.loading = true;
			AccountsFactory.verifyBank($scope.data)
				.then(function (data) {
					$scope.close(data);
					$scope.loading = false;
					$scope.error = null;
				})
				.catch(function (error) {
					$scope.error = error.data.message;
					$scope.loading = false;
				})
		}

	}]);
