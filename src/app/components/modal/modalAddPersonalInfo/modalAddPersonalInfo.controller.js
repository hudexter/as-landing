angular.module('antsquare')
	.controller('ModalAddBankCtrl', ['$scope', 'close', 'ModalStorage', 'StripeKey', 'AccountsFactory', 'GeoLocationService', function ($scope, close, ModalStorage, StripeKey, AccountsFactory, GeoLocationService) {

		StripeKey.getKey();
		var paymentData = {
			stripe_token: null
		};
		$scope.data = {};
		$scope.data = ModalStorage.get()[0];

		$scope.loading = false;

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};
		$scope.viewToggle = true;
		$scope.typeToggle = function (boolean) {
			$scope.viewToggle = boolean;
			$scope.input = {};
			$scope.error = "";
		};

		$scope.addBank = function () {
			$scope.error = "";
			$scope.loading = true;
			if ($scope.data.country === 'CA') {
				$scope.addBankFn();
			} else {
			$scope.addBankUS();
			}
		};

		$scope.addBankFn = function () {
			AccountsFactory.addBankAcc($scope.data)
				.then(function (data) {
					$scope.loading = false;
					$scope.close(data)
				})
				.catch(function (data) {
					$scope.error = data.data;
					$scope.loading = false;
				});
		}

		$scope.stripeCallback = function (code, result) {
			$scope.loading = true;
			if (result.error) {
				$scope.loading = false;
			} else {
				paymentData.stripe_token = result.id;
				AccountsFactory.newCard(paymentData)
					.success(function (data, status, headers, config) {
						$scope.number = null;
						$scope.cvc = null;
						$scope.expiry = null;
						$scope.loading = false;
						close(result, 150);
					})
					.error(function (data, status, headers, config) {
						$scope.number = null;
						$scope.cvc = null;
						$scope.expiry = null;
						$scope.loading = false;
					});
			}
		};

		$scope.addBankUS = function () {
			if ($scope.viewToggle) {
				$scope.addBankFn();
			} else {
				// Call stripeCallBack
			}
		}
	}]);
