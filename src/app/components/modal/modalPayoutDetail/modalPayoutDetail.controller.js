angular.module('antsquare')
	.controller('ModalPayoutDetailCtrl', ['$scope', 'close', 'ModalStorage', 'AccountsFactory', function ($scope, close, ModalStorage, AccountsFactory) {

		$scope.data = ModalStorage.get()[0];

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};
		$scope.loading = false;

		AccountsFactory.getPayoutDetails($scope.data.id)
			.then(function (data) {
				$scope.payout = data.data;
				if (data.data.status === 'completed' || data.data.status === 'new') {
					$scope.message = 'Bank Transfer Complete';
				} else {
					$scope.message = 'Bank Transfer In Progress';
				}
			})
			.catch(function (error) {
				console.log(error)
			});

	}]);
