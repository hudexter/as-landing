antsquare
  .factory('ModalStorage', [function () {
    var modal = [];
    return {
      set: function () {
        var args = arguments;
        for (var i = 0; i < args.length; i++) {
          modal.push(args[i]);
        };
        return modal
      },
      get: function () {
        return modal
      },
      end: function () {
        modal = [];
        return modal;
      }
    };
  }]);