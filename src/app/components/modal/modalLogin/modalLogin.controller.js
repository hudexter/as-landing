angular.module('antsquare')
  .controller('ModalLoginCtrl', ['$scope', 'close', 'ModalStorage', 'StoreFactory', 'Auth', 'AccountsFactory', '$q', 'ezfb', '$cookies', '$location', '$rootScope', 'Config', function($scope, close, ModalStorage, StoreFactory, Auth, AccountsFactory, $q, ezfb, $cookies, $location, $rootScope, Config) {

    $scope.loginLoading = false;
    $scope.fbLoginLoading = false;
    $scope.onMobile = true;
    $scope.isMobile = $rootScope.isMobile;
    $scope.fadeOut = false;
    $scope.loading = true;
    $scope.message = null;
    $scope.data = {};
    $scope.close = function(result) {
      $scope.fadeOut = true;
      close(result, 150);
    };

    switch (ModalStorage.get()[0]) {
      case 'setting':
        $scope.message = 'access your account.';
        break;
      case 'like':
        $scope.message = 'like a product.';
        break;
      case 'follow':
        $scope.message = 'follow a user.';
        break;
      case 'comment':
        $scope.message = 'leave a comment.';
        break;
      case 'flagComment':
        $scope.message = 'flag a comment.';
        break;
      case 'flagProduct':
        $scope.message = 'flag a product.';
        break;
    };

    var getStore = function() {
      var deferred = $q.defer();
      StoreFactory.me()
        .then(function(data) {
          Auth.setStore(data.data);
          deferred.resolve(data);
        })
        .catch(function(error) {
          console.log(error)
          deferred.reject(error);
        })
      return deferred.promise;
    };

    var getUser = function() {
      var deferred = $q.defer();
      AccountsFactory.me()
        .then(function(data) {
          Auth.setUser(data.data);
          deferred.resolve(data);
        })
        .catch(function(error) {
          console.log(error)
          deferred.reject(error);
        })
      return deferred.promise;
    };

    $scope.inputToggle = function(boolean) {
      $scope.onMobile = boolean;
    };


    $scope.login = function() {
      $scope.loginLoading = true;
      $scope.error = "";
      // if (!_.isEmpty($scope.data)) {
      var clone = _.clone($scope.data)
      if ($scope.onMobile) {
        clone.input = '+' + $scope.data.country.code + clone.input;
      }
      // }
      Auth.login(clone)
        .then(function(data) {
          $scope.loginLoading = false;
          Auth.reset();
          Auth.setToken(data);
          // need to be uncommented
          $q.all([getStore(), getUser()])
            .then(function(value) {
              close(null, 150);
              $location.path('/feed');
            }, function(error) {
              $scope.error = error.data.message
                //console.log(error);
            });


        })
        .catch(function(error) {
          $scope.error = error.data.message
          Auth.reset();
          $scope.loginLoading = false;
          //console.log(error);
        });
    };

    $scope.forgetPassword = function() {
      $scope.reset = {
        state: 'forgetPassword'
      };
      close($scope.reset, 150);
    }

    $scope.authenticate = function(provider) {
      $scope.fbLoginLoading = true;
      $scope.error = "";
      ezfb.getLoginStatus(function(res) {
        //console.log(res.status);      
      });
      ezfb.login(function(response) {
          $cookies.put('fb_token', response.authResponse.accessToken, {
            domain: Config.COOKIE_DOMAIN
          });
          if (response.status === 'connected') {

            AccountsFactory.userLinking(provider, response.authResponse.accessToken)
              .then(function(response) {
                Auth.setToken(response);
                AccountsFactory.me()
                  .then(function(response) {
                    $q.all([getStore(), getUser()])
                      .then(function(value) {
                        close(null, 100);
                      }, function(error) {
                        // console.log(error)
                      });
                    $location.path('/feed');
                  });

              })
              .catch(function(error) {
                $scope.error = error.data.message
                $scope.loading = "error";
                $timeout(function() {
                  $scope.loading = false;
                }, 1500);
              });
          } else {
            console.log("not conencted")
            $scope.error = error.data.message
            $scope.loading = "error";
            $timeout(function() {
              $scope.loading = false;
            }, 1500);
          }
        })
        .catch(function(error) {
          $scope.fbLoginLoading = false;
          $scope.error = error.data.message
          $scope.loading = "error";
          $timeout(function() {
            $scope.loading = false;
          }, 1500);
        });
    }
  }]);