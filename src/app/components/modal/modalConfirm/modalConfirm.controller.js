angular.module('antsquare')
	.controller('ModalConfirmCtrl', ['$scope', 'close', 'ModalStorage', function ($scope, close, ModalStorage) {

		$scope.data = ModalStorage.get()[0];

		$scope.close = function (result) {
			$scope.fadeOut = true;
			close(result, 150);
		};
		$scope.loading = false;
		$scope.submit = function () {
			$scope.loading = true;
			$scope.data.fn($scope.data.card).then(function(data){
				close(data ,150)
				$scope.loading = false;
			})
			.catch(function(error){
				$scope.loading = false;
				$scope.error = error.data.message
			})

		};





	}]);
