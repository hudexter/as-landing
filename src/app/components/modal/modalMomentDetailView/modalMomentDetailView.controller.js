angular.module('antsquare')
  .controller('ModalMomentDetailViewCtrl', ['$scope', 'close', 'StoreFactory', 'ProductsFactory', 'toastr', '$state', 'ModalStorage', 'ModalService', 'RequestFactory', 'MomentsFactory', 'GeoLocationService', '$rootScope', 'Auth', 'branchFactory', 'deviceDetector', '$stateParams', 'AccountsFactory', '$window', 'MapService', function($scope, close, StoreFactory, ProductsFactory, toastr, $state, ModalStorage, ModalService, RequestFactory, MomentsFactory, GeoLocationService, $rootScope, Auth, branchFactory, deviceDetector, $stateParams, AccountsFactory, $window, MapService) {

    $scope.moment = ModalStorage.get()[0];

    $scope.momentId = ModalStorage.get()[0].id;

    $scope.fadeOut = false;
    $scope.loading = false;
    StoreFactory.getUserById($scope.moment).then(function(data) {
      $scope.moment.store_info = data.data;
    })
    $scope.members = [];

    $scope.close = function(result) {
      $scope.fadeOut = true;
      close(result, 150);
    };

    if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
      $scope.isIOS = true;
    } else {
      $scope.isIOS = false;
    }
    $scope.message = "String including Emoji codes :smiley:";

    if ($rootScope.branchReady) {
      branchFactory.link('moment', $scope.momentId)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }

        })
    }

    $rootScope.$on('branchReady', function(event, data) {
      branchFactory.link('moment', $scope.momentId)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }
        })
    });

    // User

    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
    } else {
      $scope.userInfo = null;
    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        $scope.StoreFollowProduct = false;
        start();
      } else {
        $scope.userInfo = null;
        $scope.StoreFollowProduct = false;
      }
    });

    $scope.orderOnApp = function() {
      if ($rootScope.isMobile) {
        if ($scope.isIOS) {
          // var url = $location.url();
          // url = 'antsquarecom://antsquare.com' + url;
          setTimeout(function() {
            location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
          }, 500);

          // redirect to 'antsquare://www.antsquare.com/moment/:id'

        } else {
          var url = $location.url();
          url = 'antsquarecom://antsquare.com' + url;
          location = url;
          setTimeout(function() {
            location = "https://ad.apps.fm/LRMs2kfpF0YQK_qEfUeyCF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfISXbMQwI-2AOqLYV5gygIechIMlYYx5zMHmF83sYo5BAfQUdy_B62HXVVdz_7CSe"
          }, 500);
        }
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      } else {
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      }
    };
    // Moment
    var distance = {};

    $scope.characterLimit = 16;
    $scope.coords = undefined;
    $scope.moment = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.moment) {
        distance = MapService.getDistance($scope.moment.lat, $scope.moment.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.moment[key] = distance[key];
        }
      }
    });

    $scope.slideLeft = function() {
      if ($scope.slideIndex !== 0) {
        $scope.slideIndex = $scope.slideIndex - 1;
      }
    };

    $scope.slideRight = function() {
      if ($scope.slideIndex < $scope.moment.images.length) {
        $scope.slideIndex = $scope.slideIndex + 1;
      }
    };
    $scope.toSlide = function(index) {
      $scope.slideIndex = index;
    }

    function getShareLinks(moment) {

      // $scope.root = $location.host();
      $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.twLink = "http://twitter.com/intent/tweet?status=" + moment.description + "+%40antsquare+" + "https://www.antsquare.com/moment/" + moment.id;
      "http://twitter.com/intent/tweet?status=" + moment.description + "@antsquare" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/moment/" + moment.id + "&amp;media=" + moment.images[0] + "&amp;description=Check it out!"

      $scope.mailLink = "mailto:?Subject=Check out " + moment.description + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/moment/" + moment.id;
    };
    $scope.momentDeletePrompt = function(id) {

      MomentsFactory.deleteMoment($scope.momentId)
        .then(function(data) {
          $scope.userid = $scope.userInfo.id;

          $state.go('user/:user_id', { user_id: $scope.userid });
          $scope.close();

        })
        .catch(function(error) {

        });
    };

    function checkImgContainerSize() {
      // if (window.innerWidth < 768) {
      var imgContainer = angular.element(document.querySelector('#rn-carousel'));
      var x = imgContainer.width();
      imgContainer.height(x);
      // }
    };

    function getPage() {

      $scope.coords = GeoLocationService.getCoords();
      var input = {
        moment_id: $scope.momentId,
        viewer_id: null,
        lat: null,
        lon: null

      };

      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id;
        var userid = $scope.userInfo.id;
      };
      if ($scope.coords) {
        input.lat = $scope.coords.latitude;
        input.lon = $scope.coords.longitude;
      }

      MomentsFactory.getMomentPageView(input)
        .then(function(data) {
          $scope.moment = data.data;
          if (userid != $scope.moment.user_id) {
            $scope.StoreFollowProduct = true;
          }
          $scope.$broadcast('productLoaded');
          if (GeoLocationService.hasLocation()) {
            $scope.coords = GeoLocationService.getCoords();
            distance = MapService.getDistance($scope.moment.lat, $scope.moment.lon, $scope.coords.latitude, $scope.coords.longitude);
            for (var key in distance) {
              $scope.moment[key] = distance[key];
            }
          }
          getShareLinks($scope.moment);

          $scope.width = $window.innerWidth;
          $scope.card = document.getElementById('card').offsetWidth;
          $scope.cardHeight = document.getElementById('card').offsetHeight;
          $scope.left = document.getElementById('leftPanel').offsetWidth;

          if ($scope.card - 5 > $scope.left) {
            $scope.productDescription = {
              'max-width': $scope.card - $scope.left - 19 + 'px',
              'word-break': 'break-all'
            }
            $scope.rightscrollHeight = {
              'max-height': $scope.cardHeight - 131 + 'px',
              'min-height': $scope.cardHeight - 131 + 'px',
              'max-width': $scope.card - $scope.left - 19 + 'px',
              'overflow-y': 'scroll',
              'word-break': 'break-all'
            }
          } else {
            $scope.rightscrollHeight = {
              'max-height': $scope.cardHeight - 131 + 'px',
              'min-height': $scope.cardHeight - 131 + 'px',
              'max-width': $scope.left + 'px',
              'overflow-y': 'scroll',
              'word-break': 'break-all'
            }
            $scope.productDescription = {
              'max-width': $scope.left + 'px',
              'word-break': 'break-all'
            }
          }
        })
        .catch(function(err) {
          $scope.moment = err.message;
        });
    };

    function start() {
      $scope.coords = undefined;
      $scope.moment = undefined;
      getPage();
      checkImgContainerSize();
    }

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;

      if (!$scope.moment.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.moment.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.moment.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;
          })
      }
    };

    $scope.likeToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.moment.liked = !$scope.moment.liked;
      if ($scope.moment.liked) {
        AccountsFactory.likeMoment($scope.momentId)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.liked = !$scope.moment.liked
          })
      } else {
        AccountsFactory.unlikeMoment($scope.momentId)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.moment.liked = !$scope.moment.liked;
          })
      }
    };

    $scope.flagMoment = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagMoment')
      $scope.flagged = true;
      MomentsFactory.flagMoment($scope.momentId)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    $scope.showMomentLikes = function() {
      ModalStorage.set($scope.momentId);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalLikes/modalMomentLikes.html",
          controller: "ModalMomentPopupLikesCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };


    $scope.submitComment = function(content) {
      $scope.content = content;
      var commentdata = {
        moment_id: $scope.momentId,
        commentContent: $scope.content


      };


      $rootScope.$broadcast('postCommentMomentPop', commentdata)
      $scope.content = null;
    }




    start();
    window.onresize = checkImgContainerSize;
    $scope.ready = false;

    $scope.ready = true;

    angular.element($window).bind('resize', function() {
      $scope.ready = false;
      $scope.card = document.getElementById('card').offsetWidth;
      $scope.left = document.getElementById('leftPanel').offsetWidth;
      if ($scope.card - 5 > $scope.left) {
        $scope.productDescription = {
          'max-width': $scope.card - $scope.left - 19 + 'px'
        }
      } else {
        $scope.productDescription = {
          'max-width': $scope.left + 'px'
        }
      }

      $scope.ready = true;
      $scope.$digest();
    });

  }]);