angular.module('antsquare')
  .controller('ModalVideoCtrl', ['$scope', 'close', 'StoreFactory', 'ModalStorage', function ($scope, close, StoreFactory, ModalStorage) {


    $scope.video_url = "https://antsquare-www.imgix.net/video/landing4.webm";
    $scope.function = ModalStorage.get()[1];
    $scope.fadeOut = false;
    $scope.loading = false;
    $scope.close = function (result) {
      $scope.fadeOut = true;
      close(result, 150);
    };


  }]);
