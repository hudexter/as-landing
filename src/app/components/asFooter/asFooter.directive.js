'use strict';

angular.module('antsquare')
  .directive('asFooter', ['deviceDetector', '$rootScope', '$http', 'Config', '$mixpanel', 'URLS', '$document', '$state',
    function (deviceDetector, $rootScope, $http, Config, $mixpanel, URLS, $document, $state) {
      return {
        restrict: 'E',
        link: function (scope, element, attr) {
          $rootScope.isMobile = null;

          scope.getFooter = function () {
            var template = 'app/components/asFooter/asFooter2.html';
            if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
              template = 'app/components/asFooter/asFooter2.mobile.html';
              $rootScope.isMobile = true;
            } else {
              template = 'app/components/asFooter/asFooter2.html';
              $rootScope.isMobile = false;
            }
            return template
          }

          scope.data = {
            address: ""
          };

          scope.urls = URLS;
          scope.currentYear = new Date()
            .getFullYear();

          scope.status = null;

          scope.subscribe = function () {
            if (scope.data.address) {
              scope.status = 'loading';
              $http({
                  url: Config.CORE_BASEURL + "/v5/all/mailSubscribe",
                  method: "POST",
                  data: scope.data
                })
                .then(function (data) {
                  scope.data = {
                    address: ""
                  };
                  scope.status = true;
                })
                .catch(function (error) {
                  scope.status = true;
                })
            }
          };

          scope.trackIos = function () {
            $mixpanel.track('f-ios');
            // location = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8";
          };

          scope.trackAndroid = function () {
            $mixpanel.track('f-android');
            // location = "https://play.google.com/store/apps/details?id=com.antsquare.android";
          };

          scope.toTop = function (state) {
            if ($state.current.name === state) {
              $document.duScrollTo(0, 0, 1000);
            }
          };

          scope.callReady = false;
          scope.callConnected = false;

          scope.callSupport = function () {
            scope.callConnected = true;
            Twilio.Device.connect();
          }

          scope.hangUp = function () {
            scope.callConnected = false;
            Twilio.Device.disconnectAll();
          }

          Twilio.Device.error(function (e) {
            scope.callErrorMsg = 'Micophone access denied';
            scope.callConnected = false;
            Twilio.Device.disconnectAll();
          });

          $rootScope.$on('TwilioReady', function () {
            scope.callReady = true;
            scope.callConnected = false;
          })
        },
        template: "<div ng-include='getFooter()'></div>"
      }

    }
  ]);