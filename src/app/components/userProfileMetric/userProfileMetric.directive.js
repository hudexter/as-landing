'use strict'

angular.module('antsquare')
  .directive('userProfileMetric', ['ModalService', 'ModalStorage', function(ModalService, ModalStorage) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/userProfileMetric/userProfileMetric.html',
      scope: {
        moment: '='
      },
      link: function(scope, element, attr) {

        scope.showFollowers = function() {
          ModalStorage.set(scope.moment);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalFollower/modalFollower.html",
              controller: "ModalFollowerCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
              });
            });
        };

        scope.showFollowings = function() {
          ModalStorage.set(scope.moment);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
              controller: "ModalFollowingCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
              });
            });
        };

        scope.showReviews = function() {
          ModalStorage.set(scope.moment.store_info);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalReview/modalReview.html",
              controller: "ModalReviewCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
              });
            });
        };


      }
    }
  }]);