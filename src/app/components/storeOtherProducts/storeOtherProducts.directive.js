'use strict'

angular.module('antsquare')
	.directive('storeOtherProducts', ['$window', 'SIZE_METRIC', function ($window, SIZE_METRIC) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/storeOtherProducts/storeOtherProducts.html',
			scope: {
				product: '='
			},
			link: function (scope, element, attr) {

				function setSize() {
					setTimeout(function () {
						var x = element.find('.other-product-image');
						x.height(x.width());
					}, 150);
				};

				function generateList() {

					var temp = [];
					temp = scope.leftOvers;

					if ($window.innerWidth < SIZE_METRIC.sm) {
						scope.sm = true;
						if (scope.product.store_info.products.length > 2) {
							scope.leftOvers = scope.product.store_info.products.splice(2, scope.product.store_info.products.length - 1);
							scope.leftOvers = scope.leftOvers.concat(temp);
						}
					} else {
						scope.sm = false;
						if (scope.leftOvers.length > 1) {
							scope.product.store_info.products = scope.product.store_info.products.concat(scope.leftOvers);
						}

						if (scope.product.store_info.products.length >= 4) {
							scope.leftOvers = scope.product.store_info.products.splice(4, scope.product.store_info.products.length - 1);
						} else if (scope.product.store_info.products.length >= 2) {
							scope.leftOvers = scope.product.store_info.products.splice(2, scope.product.store_info.products.length - 1);
							scope.leftOvers = scope.leftOvers.concat(temp);
						}
					}
				};

				scope.leftOvers = [];
				scope.sm = false;
				scope.loading = true;

				// scope.$watch('product', function () {
				// 	setTimeout(function () {
				// 		setSize();
				// 		generateList();
				// 		scope.loading = false;
				// 	},300);
				// });

				scope.$on('productLoaded', function () {
					setTimeout(function () {
						setSize();
						generateList();
						scope.loading = false;
						setTimeout(function () {
							setSize();
							generateList();
						}, 300);
					});
				});

				angular.element($window)
					.on('resize', function (e) {
						setSize();
						generateList();
					});
			}
		}
    }]);
