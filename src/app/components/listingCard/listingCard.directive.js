// 'use strict';
// angular.module('antsquare')
//   .directive('textcomplete', ['Textcomplete', 'AccountsFactory', function(Textcomplete, AccountsFactory) {
//     return {
//       restrict: 'EA',
//       scope: {
//         members: '=',
//         message: '='
//       },
//       // template: '<textarea ng-model=\'message\' type=\'text\'></textarea>',
//       link: function(scope, iElement, iAttrs) {

//         scope.usernamesList = [];
//         scope.tagsList = [];
//         scope.storeHandleList = [];

//         var lastQuery = '';
//         AccountsFactory.getUsernames(lastQuery)
//           .then(function(response) {
//             for (var i = 0; i < response.data.total; i++) {
//               scope.usernamesList.push(response.data.data[i].username);
//             }

//           });

//         var usernameMentions = scope.usernamesList;

//         var ta = iElement.find('textarea');
//         var textcomplete = new Textcomplete(ta, [{
//           match: /(^|\s)@(\w*)$/,
//           search: function(term, callback) {
//             callback($.map(usernameMentions, function(usernameMention) {
//               return usernameMention.toLowerCase().indexOf(term.toLowerCase()) === 0 ? usernameMention : null;
//             }));
//           },
//           index: 2,
//           replace: function(usernameMention) {

//             return '$1@' + usernameMention + ' ';
//           }
//         }]);
//         AccountsFactory.getTags(lastQuery)
//           .then(function(response) {
//             for (var i = 0; i < response.data.total; i++) {
//               scope.tagsList.push(response.data.data[i].tag);
//             }

//           });

//         var tagMentions = scope.tagsList;

//         var textcomplete = new Textcomplete(ta, [{
//           match: /(^|\s)#(\w*)$/,
//           search: function(term, callback) {
//             callback($.map(tagMentions, function(tagMention) {
//               return tagMention.toLowerCase().indexOf(term.toLowerCase()) === 0 ? tagMention : null;
//             }));
//           },
//           index: 2,
//           replace: function(tagMention) {

//             return '$1#' + tagMention + ' ';
//           }
//         }]);
//         AccountsFactory.getStoreHandles(lastQuery)
//           .then(function(response) {
//             //alert(response.data.data[0].store_name);
//             for (var i = 0; i < response.data.total; i++) {
//               scope.storeHandleList.push(response.data.data[i].store_name);
//             }

//           });

//         var handleMentions = scope.storeHandleList;

//         var textcomplete = new Textcomplete(ta, [{
//           match: /(^|\s)\$(\w*)$/,
//           search: function(term, callback) {
//             callback($.map(handleMentions, function(handleMention) {
//               return handleMention.toLowerCase().indexOf(term.toLowerCase()) === 0 ? handleMention : null;
//             }));
//           },
//           index: 2,
//           replace: function(handleMention) {

//             return '$1$' + handleMention + ' ';
//           }
//         }]);

//         $(textcomplete).on({
//           'textComplete:select': function(e, value) {
//             scope.$apply(function() {
//               scope.message = value
//             })
//           },
//           'textComplete:show': function(e) {
//             $(this).data('autocompleting', true);
//           },
//           'textComplete:hide': function(e) {
//             $(this).data('autocompleting', false);
//           }
//         });
//       }
//     }
//   }]);

angular.module('antsquare')
  .directive('listingCard', ['ModalStorage', 'ModalService', 'UploadFatory', 'Upload', 'Config', 'Auth', 'GeoLocationService', 'AccountsFactory', 'ezfb', '$cookies', '$q', function(ModalStorage, ModalService, UploadFatory, Upload, Config, Auth, GeoLocationService, AccountsFactory, ezfb, $cookies, $q) {
    return {
      restrict: 'E',
      scope: {
        type: '=',
        data: '=',
        index: '=',
        form: '='
      },
      link: function(scope, element, attr) {

        scope.store = Auth.getStoreFn()
          .stores[0];
        scope.user = Auth.getUserFn();

        scope.$watch('data', function() {
          // scope.data = scope.data;
          if (scope.data && scope.data.type === 'promo') {
            scope.data.images.forEach(function(data, index) {
              var image = {
                preview: data,
                url: data
              };
              scope.product_imgs[index] = image;
            })
            scope.product_videothumbnail = scope.data.video_thumbnail;
          }
        })
        scope.product_imgs = [];
        scope.product_video = null;
        scope.product_videothumbnail = null;
        var yourLocation = null;
        yourLocation = GeoLocationService.getCoords();

        yourLocation.lat = yourLocation.latitude;
        yourLocation.lon = yourLocation.longitude;

        scope.map = {
          center: {
            latitude: yourLocation.lat,
            longitude: yourLocation.lon
          },
          zoom: 15,
          options: {
            scrollwheel: false
          },
          dragging: false,
          events: {
            idle: function() {
              reverseGeocoding(scope.map.center.latitude, scope.map.center.longitude)
                .then(function(data) {
                  scope.mapResult = data;
                })
            }
          }
        };





        scope.mapNoResult = false;
        scope.$on('g-places-autocomplete:no-result', function(e, data) {
          scope.mapNoResult = !!data;
        })
        scope.$on('g-places-autocomplete:select', function(e, place) {
          scope.mapNoResult = false;
          if (!place.geometry) {
            return;
          } else {
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();

            reverseGeocoding(lat, lon)
              .then(function(data) {
                console.log(data)
                scope.mapResult = data;
              })
          }
        });
        scope.$on('setLocation', function() {

          scope.map.center.latitude = GeoLocationService.getCoords()
            .latitude;
          scope.map.center.longitude = GeoLocationService.getCoords()
            .longitude;
          //console.log(GeoLocationService.getCoords().latitude);
          reverseGeocoding(scope.map.center.latitude, scope.map.center.longitude)
            .then(function(data) {
              console.log(data)
              scope.mapResult = data;
            })

        });


        scope.mapResult = {
          address1: scope.data.address1,
          address2: scope.data.address2,

        };


        scope.$watch('mapResult', function(data) {


          scope.data.address1 = scope.mapResult.address1;
          scope.data.address2 = scope.mapResult.address2;
          scope.data.location = {
            lat: scope.map.center.latitude,
            lon: scope.map.center.longitude
          }


        })

        function reverseGeocoding(lat, lon) {
          var geocoder = new google.maps.Geocoder();
          var latlng = new google.maps.LatLng(lat, lon);
          var deferred = $q.defer();
          scope.map.center.latitude = latlng.lat();
          scope.map.center.longitude = latlng.lng();

          var map = new google.maps.Map(document.getElementById('map'), {
            center: {
              lat: scope.map.center.latitude,
              lng: scope.map.center.longitude
            }
          });

          var request = {
            location: latlng,
            radius: '200'
          };

          var result = {
            nearBy: null,
            address1: null,
            address2: null,
          };

          var service = new google.maps.places.PlacesService(map);
          service.nearbySearch(request, function(results, status) {
            if (status === "OK") {
              result.nearBy = results;
              geocoder.geocode({
                'latLng': latlng
              }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  results.forEach(function(obj) {
                    for (var key in obj) {
                      if (obj.types[0] === 'street_address') {

                        result.address1 = obj.address_components[0].long_name + " " + obj.address_components[1].short_name;
                        deferred.resolve(result)
                      }
                      if (obj[key][0] === 'political' || obj[key][0] === 'locality') {
                        result.address2 = obj.formatted_address;
                        deferred.resolve(result)
                      }
                      if (obj[key][0] === 'country') {
                        result.country = obj.address_components[0].short_name;
                      }
                    }
                  })
                }
              });

            }
          });
          return deferred.promise;
        }

        if (scope.type === 'listing.products_services') {
          scope.data.type = 'product';
          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
          } else {
            scope.data.tags = [];
          }
          scope.data.price_type = 'normal';
          scope.template = 'app/components/listingCard/productService.html';
        } else if (scope.type === 'listing.product_service') {
          if (scope.data.images.length > 0) {
            scope.data.files = scope.data.images.length;
          }
          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
            scope.data.tags = temp.map(function(tag, index) {
              return tag
            });
          } else {
            scope.data.tags = [];
          }
          scope.data.images.forEach(function(data, index) {
            var image = {
              preview: data,
              url: data
            };
            scope.product_imgs[index] = image;
          })
          scope.template = 'app/components/listingCard/productService.html';
        } else if (scope.type === 'listing.request') {

          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
          } else {
            scope.data.tags = [];
          }

          scope.data.type === 'request'
          scope.template = 'app/components/listingCard/request.html';
        } else if (scope.type === 'listing.requestEdit') {
          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
            scope.data.tags = temp.map(function(tag, index) {
              return tag
            });
          } else {
            scope.data.tags = [];
          }
          scope.data.expires_at = new Date(scope.data.expires_at);
          scope.data.type === 'request'
          scope.template = 'app/components/listingCard/request.html';
        } else if (scope.type === 'listing.promotion') {

          scope.data.type = 'promo';
          scope.template = 'app/components/listingCard/promotion.html';
        } else if (scope.type === 'listing.promotionEdit') {

          // scope.data.images.forEach(function (data, index) {
          //  var image = {
          //    preview: data,
          //    url: data
          //  };
          //  scope.product_imgs[index] = image;
          // })

          // scope.data.type = 'promo';
          scope.template = 'app/components/listingCard/promotion.html';
        } else if (scope.type === 'listing.moment') {
          scope.data.type = 'moment';
          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
          } else {
            scope.data.tags = [];
          }
          scope.data.price_type = 'normal';
          scope.template = 'app/components/listingCard/moment.html';
        } else if (scope.type === 'listing.momentEdit') {
          if (scope.data.images.length > 0) {
            scope.data.files = scope.data.images.length;
          }
          if (scope.data.tags) {
            var temp = scope.data.tags.split(" ");
            scope.data.tempTag = temp.map(function(tag, index) {
              return {
                text: tag,
                id: index
              }
            });
            scope.data.tags = temp.map(function(tag, index) {
              return tag
            });
          } else {
            scope.data.tags = [];
          }
          scope.data.images.forEach(function(data, index) {
            var image = {
              preview: data,
              url: data
            };
            scope.product_imgs[index] = image;
          })
          scope.product_videothumbnail = scope.data.video_thumbnail;
          scope.template = 'app/components/listingCard/moment.html';
        }

        scope.linkFacebook = function() {
          ezfb.login(function(response) {
            $cookies.put('fb_token', response.authResponse.accessToken, {
              domain: Config.COOKIE_DOMAIN
            });
            scope.token = Auth.getToken();
            ezfb.api('/me', function(FBresponse) {
              FBresponse.access_token = response.authResponse.accessToken;
              AccountsFactory.putFBInfo(FBresponse)
                .then(function(response) {
                  scope.user.fb_share_to = FBresponse.id;
                  scope.shareTo = {
                    name: FBresponse.name,
                    id: FBresponse.id,
                  };
                  scope.socialInfo.facebook = FBresponse;
                  scope.user.fb_publish_enabled = true;
                  scope.user.fb_share_to = FBresponse.id;
                  AccountsFactory.putProfile({
                      fb_publish_enabled: true,
                      fb_share_to: scope.shareTo.id
                    })
                    .then(function(data) {
                      scope.data.fbShare = !scope.data.fbShare;
                      Auth.setUser(scope.user);
                    })
                    .catch(function(error) {
                      console.log(error)
                    });
                })
                .catch(function(error) {
                  console.log(error)
                })
            });
          }, {
            scope: 'publish_actions,manage_pages,publish_pages',
            return_scopes: true,
            auth_type: 'rerequest',
            enable_profile_selector: true
          });
        };

        if (scope.data) {
          scope.data.fbShare = false;
        }

        scope.fbShareToggle = function() {
          if (scope.user.fb_share_to) {
            scope.data.fbShare = !scope.data.fbShare;
          } else {
            scope.linkFacebook();
          }

        }

        scope.typeToggle = function(typeInput) {
          scope.data.type = typeInput;
          scope.data.category = null;
          scope.data.category_info = null;

          if (typeInput === 'product') {
            scope.data.price_type = 'normal';
          } else {
            scope.data.price_type = 'perhour'
          }

        };

        scope.close = function() {
          scope.$parent.removeListing(scope.index)
        }

        scope.selectCategory = function() {
          ModalStorage.set(scope.data);
          ModalService.showModal({
              templateUrl: "app/components/modal/modalCategory/ModalCategory.html",
              controller: "ModalCategoryCtrl"
            })
            .then(function(modal) {
              modal.close.then(function(result) {
                ModalStorage.end();
                scope.data.category = result.name;
                scope.data.category_info = result;
              });
            });
        }

        scope.onTagRemoved = function(tag) {
          scope.data.tags.map(function(li, index) {
            if (li === '#' + tag.text || li === tag.text) {
              scope.data.tags.splice(index, 1);
            }
          })
        };

        scope.addHash = function(tag, product_tags) {
          scope.data.tags.push('#' + tag.text)
        };

        function previewImg(input, index) {
          scope.product_imgs[index] = {};
          var reader = new FileReader();
          reader.onload = function(e) {
            scope.product_imgs[index].preview = e.target.result;
          };
          reader.readAsDataURL(input);
        }

        function saveImages() {
          var img_urls_arr = [];
          scope.product_imgs.forEach(function(ele, index) {
            img_urls_arr.push(ele.url);
          });
          // console.log("images")
          scope.data.images = img_urls_arr;
          scope.data.files = scope.data.images.length;
        };

        function saveVideo() {

          scope.data.video_url = scope.product_video;
          scope.data.video_thumbnail = scope.product_videothumbnail;
          // scope.data.files = scope.product_video.length;

        };

        scope.removeImg = function(index) {
          _.remove(scope.product_imgs, scope.product_imgs[index]);
          saveImages();
        };
        scope.removeVideo = function() {
          scope.product_video = null;
          scope.product_videothumbnail = null;
          saveVideo();

        };

        scope.invalidFile = function(s) {
          console.log('invalidFile')
          console.log(s)
        }

        scope.uploadFiles = function(files) {
          var index = scope.product_imgs.length;
          var photoListLength = 4;
          if (scope.data.type === 'promo') {
            photoListLength = 1;
          }

          if ((files[0].type == "video/mp4") || (files[0].type == "video/ogg")) {
            if (files[0].size < 10000000) {
              scope.uploadVideo(files[0]);

            } else {
              $window.alert("The file needs to be under 10MB")
            }
          } else {
            if (files && files.length && index < photoListLength) {
              for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (file.size < 10000000) {
                  console.log(file)
                  UploadFatory.getSignUrl(file.type)
                    .then(function(data) {
                      previewImg(file, index);
                      scope.uploadImage(data.data, file, index)
                    })
                    .catch(function(error) {
                      console.log(error)
                    })

                } else {
                  $window.alert("The file needs to be under 10MB")
                }
              }
            }
          }
        }

        scope.uploadImage = function(data, file, index) {
          var path = UploadFatory.getImagePath(file.type);
          Upload.upload({
              url: Config.S3_URL,
              method: 'POST',
              fields: {
                'key': path,
                'acl': 'public-read',
                'Content-Type': file.type,
                'AWSAccessKeyId': data.AWSAccessKeyId,
                'success_action_status': '201',
                'Policy': data.s3Policy,
                'Signature': data.s3Signature
              },
              sendObjectsAsJsonBlob: false,
              file: file
            })
            .progress(function(evt) {
              // if (!scope.imageUploading) {
              scope.imageUploading = true;
              scope.$emit('imageUploading', scope.imageUploading);
              // }
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
              scope.fImage = progressPercentage;
              scope.filenameImage = evt.config.file.name;
            })
            .error(function(e) {
              // console.error("Failed:", e);
              scope.imageUploading = false;
              scope.$emit('imageUploading', scope.imageUploading);
            })
            .success(function(data, status, headers, config) {
              scope.imageUploading = false;
              scope.$emit('imageUploading', scope.imageUploading);
              scope.product_imgs[index].url = Config.IMAGE_URL + '/' + path;

              setTimeout(function() {
                  scope.data.files = scope.product_imgs.length;
                })
                // console.log(scope.data)
              saveImages();
              scope.data.video_url = null;
              scope.data.video_thumbnail = null;
            });
        }

        scope.uploadVideo = function(file) {

          Upload.upload({
              url: Config.UPLOAD_URL + '/video',
              method: 'POST',
              file: file
            })
            .progress(function(evt) {
              // if (!scope.videoUploading) {
              scope.videoUploading = false;
              scope.$emit('videoUploading', scope.videoUploading);
              // }
              var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
              console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
              scope.f = progressPercentage;
              scope.filename = evt.config.file.name;

            })
            .error(function(e) {
              // console.error("Failed:", e);
              scope.videoUploading = false;
              scope.$emit('videoUploading', scope.videoUploading);
            })
            .success(function(data, status, headers, config) {
              scope.videoUploading = false;
              scope.$emit('videoUploading', scope.videoUploading);

              scope.product_video = data.url;
              scope.product_videothumbnail = data.thumbnail;

              setTimeout(function() {
                  scope.data.files = scope.product_video.length;
                })
                //console.log(scope.data.files);

              saveVideo();
              scope.data.images = [];
              scope.product_imgs = [];
            });
        }

        scope.getUsernames = function(query) {
          AccountsFactory.getUsernames(query)
            .then(function(data) {
              var usernames = data.data;
              console.log(data.data);

            })

        }

        AccountsFactory.getSocial()
          .then(function(response) {
            scope.socialInfo = response.data;
          });

      },
      template: "<div><ng-include src='template'></ng-include></div>"
    }
  }]);