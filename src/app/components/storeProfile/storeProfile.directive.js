'use strict'

angular.module('antsquare')
	.directive('storeProfile', [ function () {
		return {
			restrict: 'E',
			templateUrl: 'app/components/storeProfile/storeProfile.html',
			scope: {
				product: '='
			},
			link: function (scope, element, attr) {

			}
    }
    }]);
