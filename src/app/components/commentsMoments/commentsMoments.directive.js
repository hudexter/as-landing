'use strict'

angular.module('antsquare')
  .directive('commentsMoments', ['MomentsFactory', '$rootScope', function (MomentsFactory, $rootScope) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/commentsMoments/commentsMoments.html',
      scope: {
        moment: '=',
        show: '=',
        user: '='
      },
      link: function (scope, element, attr) {
        // scope.user = null;
        scope.loading = true;
        scope.vm = {};

        scope.$watch('moment', function () {
          if (scope.moment) {
            scope.comments = {
              comments: [],
              page: 1,
              per_page: 5,
              last_top: true,
            };
            scope.search();
          }
        });
        scope.form = {
          content: ""
        };
        scope.search = function () {
          scope.vm = {};
          scope.comments.comments = [];
          MomentsFactory.getComments(scope.comments, scope.moment.id)
            .then(function (data) {
              scope.loading = false;
              scope.comments = data.data;
              scope.vm = data.data;
            })
            .catch(function (data) {
              scope.loading = false;
            })
        };

        scope.submited = false;
        scope.postComment = function () {
          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'comment')
          if (!scope.submited) {
            scope.submited = true;
            MomentsFactory.postComment(scope.form, scope.moment.id)
              .then(function (data) {
                console.log(data)
                scope.vm.comments.unshift(data.data);
                scope.vm.total++;
                scope.comments.total++;
                scope.form.content = "";
                scope.submited = false;
              })
              .catch(function (error) {
                console.log(error)
                scope.submited = false;
              })
          }
        };

        scope.flagComment = function (id, index) {
          if (!scope.user) return $rootScope.$broadcast('openLoginModal', 'flagComment');
          // scope.flagged = true;
          scope.comments.comments[index].flagged = true;
          MomentsFactory.flagComment(id)
            .then(function (data) {

            })
            .catch(function (error) {
              console.log(error)
                // scope.flagged = false;
              scope.comments.comments[index].flagged = false;
            });

        }

      }
    }
  }]);