'use strict';

angular.module('antsquare')
	.directive('searchBoxV3', ['GeoLocationService', '$rootScope', 'URLS', 'deviceDetector', '$q', 'SearchDataIntent', '$location', function (GeoLocationService, $rootScope, URLS, deviceDetector, $q, SearchDataIntent, $location) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/searchBoxV3/searchBoxV3.html',
			scope: {
				backgroundColor: '@',
				updateFormData: '&',
				formData: '=',
				view: '@'
			},
			link: function (scope, element, attr) {

				scope.filterShow = false;

				scope.$watch('formData', function(){
					scope.query = scope.formData.query;
				})

				scope.$watch('query', function(){
					scope.formData.query = scope.query;
				})

				if (scope.view === "map") {
					// forMap
					element.find('#explore-map')
						.remove();
					element.find('#search-container')
						.removeClass('col-md-8');
					element.find('#search-container')
						.addClass('col-md-12 container-edge-p between-xs');
					element.find('#search-container div')
						.first()
						.removeClass('p-l');
				}

				scope.filterToggle = function () {
					scope.filterShow = !scope.filterShow;
					$rootScope.$broadcast('showFilter', scope.filterShow);
				};
				$rootScope.$on('filterClose', function (event,data) {
					scope.filterShow = false;
				});
				if (scope.formData.query) {
					scope.query = scope.formData.query;
					radiusUpdate();
				};


				scope.searchData = SearchDataIntent.get();

				scope.url = URLS;
				scope.data = scope.formData;
				if (deviceDetector.device === "ipad") {
					scope.isIPadStyle = {
						"margin-left": "6px"
					};
				} else {
					scope.isIPadStyle = {
						"margin-left": "3px"
					};
				}

				function reverseGeocoding(lat, lon) {
					var geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(lat, lon);
					var deferred = $q.defer();
					geocoder.geocode({
						'latLng': latlng
					}, function (results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							results.forEach(function (obj) {
								for (var key in obj) {
									if (obj[key][0] === 'political' || obj[key][0] === 'locality') {
										deferred.resolve(obj.formatted_address)
									}
								}
							})
						}
					});
					return deferred.promise;
				}

				if (GeoLocationService.hasLocation()) {
					GeoLocationService.getCity()
						.then(function (data) {
							scope.locationName = data;
						})
						.catch(function (error) {
							console.log(error)
							scope.locationName = "Unknown";
						})
					GeoLocationService.getCountry()
						.then(function (data) {
							scope.country = data;
						})
				}

				$rootScope.$on('setLocation', function () {
					GeoLocationService.getCity()
						.then(function (data) {
							scope.locationName = data;
						})
						.catch(function (error) {
							console.log(error)
							scope.locationName = "Unknown";
						})
					GeoLocationService.getCountry()
						.then(function (data) {
							scope.country = data;
						})
				});

				scope.status = {
					input: false,
					location: false,
					radius: false,
					category: false
				}

				scope.dynamicMargin = {
					"margin-left": "0"
				};

				function radiusUpdate() {
					if (scope.data) {
						if (scope.data.radius === '5km') {
							scope.dynamicMargin = {
								"margin-left": "-1.3em"
							};
						} else if (scope.data.radius === '10km' || scope.data.radius === '15km') {
							scope.dynamicMargin = {
								"margin-left": "-0.8em"
							};
						} else {
							scope.dynamicMargin = {
								"margin-left": "0"
							};
						}
					}

				};

				scope.dropdownTrigger = function (section) {

					for (var key in scope.status) {
						if (key === section) {
							scope.status[key] = true;
						} else {
							scope.status[key] = false;
						}
					};
					if (section === 'radius') {
						radiusUpdate();
						scope.updateFormData(scope.data);
					}
				};

				scope.submitQuery = function () {
					// scope.data.query = scope.query;
					// scope.data.page = 1
					// scope.updateFormData(scope.data);

					scope.searchData = SearchDataIntent.forMap();
					SearchDataIntent.setType(scope.formData.type);
					scope.searchData = SearchDataIntent.get();

					SearchDataIntent.setQuery(scope.query)
					scope.searchData.query = scope.query;

					var str = Object.keys(SearchDataIntent.get()).map(function(key) {
							if (SearchDataIntent.get()[key]) {
									return encodeURIComponent(key) + '=' + encodeURIComponent(SearchDataIntent.get()[key]);
							}
					}).join('&');
					$location.url('/search?' + str)
							// window.location.href = $location.protocol() + "://" + $location.host() +
							// scope.updateFormData(scope.data);
				}
				scope.mapNoResult = false;
				scope.$on('g-places-autocomplete:no-result', function(e, data){
					scope.mapNoResult = !!data;
				})
				scope.$on('g-places-autocomplete:select', function (e, place) {
					scope.mapNoResult = false;
					if (!place.geometry) {
						return;
					} else {
						var lat = place.geometry.location.lat();
						var lon = place.geometry.location.lng();

						// Update Form Data
						scope.data.lat = lat;
						scope.data.lon = lon;
						reverseGeocoding(lat, lon)
							.then(function (data) {
								scope.locationName = data
							})
						scope.locationName = place.formatted_address;
						scope.updateFormData(scope.data);

						// Reset
						for (var key in scope.status) {
							scope.status[key] = false;
						}
						scope.location = null;
					}
				});

				angular.element(document)
					.bind('mousedown', function (event) {
						if (event.target.id === 'searchbox-v2-input' || event.target.parentElement.id === "searchbox-v2-input-container" || event.target.parentElement.parentElement.id === "searchbox-v2-input-container") {
							scope.status = {
								input: true,
								location: false,
								radius: false,
								category: false
							};
						} else if (event.target.id === 'searchbox-v2-location' || event.target.parentElement.id === "searchbox-v2-location-container" || event.target.parentElement.parentElement.id === "searchbox-v2-location-container" || event.target.parentElement.parentElement.parentElement.id === "searchbox-v2-location-container") {
							scope.status = {
								input: false,
								location: true,
								radius: false,
								category: false
							};
						} else {
							scope.status = {
								input: false,
								location: false,
								radius: false,
								category: false
							};
						}
						scope.$apply();
					});

			}
		}
  }]);
