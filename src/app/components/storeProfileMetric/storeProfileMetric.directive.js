'use strict'

angular.module('antsquare')
	.directive('storeProfileMetric', ['ModalService','ModalStorage', function (ModalService,ModalStorage) {
		return {
			restrict: 'E',
			templateUrl: 'app/components/storeProfileMetric/storeProfileMetric.html',
			scope: {
				product: '='
			},
			link: function (scope, element, attr) {

				scope.showFollowers = function () {
					ModalStorage.set(scope.product);
					ModalService.showModal({
							templateUrl: "app/components/modal/modalFollower/modalFollower.html",
							controller: "ModalFollowerCtrl"
						})
						.then(function (modal) {
							modal.close.then(function (result) {
								ModalStorage.end();
							});
						});
				};

				scope.showFollowings = function () {
					ModalStorage.set(scope.product);
					ModalService.showModal({
							templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
							controller: "ModalFollowingCtrl"
						})
						.then(function (modal) {
							modal.close.then(function (result) {
								ModalStorage.end();
							});
						});
				};

				scope.showReviews = function () {
					ModalStorage.set(scope.product.store_info);
					ModalService.showModal({
							templateUrl: "app/components/modal/modalReview/modalReview.html",
							controller: "ModalReviewCtrl"
						})
						.then(function (modal) {
							modal.close.then(function (result) {
								ModalStorage.end();
							});
						});
				};


			}
		}
    }]);
