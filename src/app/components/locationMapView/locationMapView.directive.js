'use strict'

angular.module('antsquare')
  .directive('locationMapView', ['MapService', 'MarkerService', function(MapService, MarkerService) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/locationMapView/locationMapView.html',
      scope: {
        product: '='
      },
      link: function(scope, element, attr) {
        // scope.mapCard = {};

        // function getMiniMapLink() {
        // lat,lon,width,height
        // 	var mapCard = angular.element(document.querySelector('#map-card'));
        // 	mapCard.width = mapCard[0].offsetWidth;
        // 	scope.mapCard.link = MapService.getMap(scope.product.lat, scope.product.lon, mapCard.width, 160);
        // };
        scope.map = {};
        scope.$watch('product', function() {

          if (scope.product && scope.product.lat) {
            scope.map = {
              center: {
                latitude: scope.product.lat,
                longitude: scope.product.lon
              },
              zoom: 15,
              options: {
                scrollwheel: false
              },
              dragging: true
            };
            scope.marker = new MarkerService(scope.product);
            scope.marker.options = {
              icon: scope.marker.icon + "?w=30&auto=format,compress"
            };
          }
        });
      }
    }
  }]);