'use strict';

angular.module('antsquare')
	.directive('addBank', ['StripeKey', 'AccountsFactory', 'GeoLocationService', function (StripeKey, AccountsFactory, GeoLocationService) {
		return {
			restrict: 'E',
			link: function (scope, element, attr) {

				scope.account = {};
				scope.getForm = function () {
					if (_.isEmpty(scope.bank)) {
						scope.data = {
							tittle: 'Add Payment Method',
							country: scope.payouts.country
						}
						scope.account.country = scope.payouts.country
					} else {
						scope.data = {
							tittle: 'Change Payment Method',
							country: scope.payouts.country
						}
						scope.account.country = scope.payouts.country
					}
					var template = 'app/components/addBank/addBankUS.html'

					if (scope.payouts.country === 'CA') {
						template = 'app/components/addBank/addBankCA.html'
					} else {
						template = 'app/components/addBank/addBankUS.html'
					}
					return template
				};

				StripeKey.getKey();
				var paymentData = {
					stripe_token: null
				};

				scope.loading = false;

				scope.viewToggle = true;
				scope.typeToggle = function (boolean) {
					scope.viewToggle = boolean;
					scope.input = {};
					scope.error = "";
				};

				scope.addBank = function () {
					scope.error = "";
					scope.loading = true;
					if (scope.payouts.country === 'CA') {
						scope.addBankFn();
					} else {
						scope.addBankUS();
					}
				};

				scope.addBankFn = function () {
					AccountsFactory.addBankAcc(scope.account)
						.then(function (data) {
							scope.loading = false;
							scope.addBankShow = false;
							scope.$parent.addBankShow = false;
							scope.$parent.getBank();
						})
						.catch(function (data) {
							scope.error = data.data.message;
							scope.loading = false;

						});
				}

				scope.stripeCallback = function (code, result) {
					scope.loading = true;
					if (result.error) {
						scope.loading = false;

					} else {
						paymentData.stripe_token = result.id;
						AccountsFactory.newCard(paymentData)
							.success(function (data, status, headers, config) {
								scope.number = null;
								scope.cvc = null;
								scope.expiry = null;
								scope.loading = false;
								scope.addBankShow = false;
								scope.$parent.addBankShow = false;
								scope.$parent.getBank();
							})
							.error(function (data, status, headers, config) {
								scope.number = null;
								scope.cvc = null;
								scope.expiry = null;
								scope.loading = false;
							});
					}
				};

				scope.close = function(){
					scope.addBankShow = false;
					scope.$parent.addBankShow = false;
				};
				scope.addBankUS = function () {
					if (scope.viewToggle) {
						scope.addBankFn();

					} else {
						// Call stripeCallBack
					}
				}

			},
			template: "<div ng-include='getForm()'></div>"
		}
  }]);
