'use strict';

antsquare
	.service('MapService', ['$q', 'Config', '$http',
	  function ($q, Config, $http) {

			function search(formData) {
				var deferredAbort = $q.defer();
				var request = $http({
					method: "GET",
					url: Config.CORE_BASEURL + '/v5/communities/search',
					params: formData,
					timeout: deferredAbort.promise
				});
				var promise = request.then(
					function (response) {
						return (response);
					},
					function (response) {
						return ($q.reject('reject stack call'));
					}
				);
				promise.abort = function () {
					deferredAbort.resolve();
				};

				promise.finally(function () {
					promise.abort = angular.noop;
					deferredAbort = request = promise = null;
				});
				return (promise);
			};

			function setMapLocation(mapData, data) {
				mapData.center.latitude = data.lat;
				mapData.center.longitude = data.lon;
				return mapData
			};

			function hasLocation(data) {
				if (data.lat && data.lon) {
					return true
						// } else if (data.bottom_left_lat && data.bottom_left_lon && data.top_right_lat && data.top_right_lon) {
						// 	return true
				} else {
					return false
				}
			};

			function setMapZoom(mapData, zoomLevel) {
				if (zoomLevel) {
					mapData.zoom = parseInt(zoomLevel);
				}
				return mapData
			};

			function zoomAdapt(mapData, searchData) {
				// if (mapData)
				// console.log(searchData)
				// return mapData
			};


			function setUrlParams(mapData, searchData) {
				var result = {};
				result = searchData || null;
				result.zoom = mapData.zoom || null;
				result.lat = mapData.center.latitude;
				result.lon = mapData.center.longitude;
				return result
			};


			function getMap(lat, lon, width, height) {

				// Waiting for link
				// var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:" + "http://antsqua.re/v24di6" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;

				return "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=13&markers=icon:http://goo.gl/t1GCnI" + "%7C" + lat + "," + lon;

			};

			function getSignMap(lat, lon, width, height) {
				// Waiting for link
				// var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:" + "http://antsqua.re/v24di6" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;
				var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=13&markers=icon:http://antsqua.re/b4451mg" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;
				var mapData = {
					'url': url
				};
				return $http
					.post('https://antsquare-loctrack-production.herokuapp.com/sign', mapData);
			};


			function round(x) {
				return Math.round(x * 10) / 10;
			};

			function deg2rad(deg) {
				return deg * (Math.PI / 180)
			};

			function getDistance(lat1, lon1, lat2, lon2) {
					var R = 6373; // km
					var φ1 = deg2rad(lat1);
					var φ2 = deg2rad(lat2);
					var λ1 = deg2rad(lon1);
					var λ2 = deg2rad(lon2);
					var Δφ = φ2 - φ1;
					var Δλ = λ2 - λ1;
					var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
					a = Math.pow(Math.sin(Δφ / 2), 2) + Math.cos(φ1) * Math.cos(φ2) * Math.pow(Math.sin(Δλ / 2), 2);
					// var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					var d = R * c;
					var result = {
						distance_in_km: String(round(d)) + "km",
						distance_in_miles: String(round(d * 0.621371)) + "mi",
					}

					return result;
			};

			return ({
				'search': search,
				'setMapLocation': setMapLocation,
				'hasLocation': hasLocation,
				'setMapZoom': setMapZoom,
				'setUrlParams': setUrlParams,
				'zoomAdapt': zoomAdapt,
				'getMap': getMap,
				'getSignMap': getSignMap,
				'getDistance': getDistance,

			});
  }]);
