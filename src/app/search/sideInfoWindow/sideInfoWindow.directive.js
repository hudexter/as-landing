'use strict';

angular.module('antsquare')
	.directive('sideInfoWindow', ['$rootScope', 'URLS', 'deviceDetector', '$q', 'SearchDataIntent', function ($rootScope, URLS, deviceDetector, $q, SearchDataIntent) {
		return {
			restrict: 'E',
			templateUrl: 'app/search/sideInfoWindow/sideInfoWindow.html',
			scope: {
				vm: '=',

			},
			link: function (scope, element, attr) {
        scope.begin = false;
        scope.$watch('vm',function(){
					scope.card = {};
					if (!_.isEmpty(scope.vm)){
						scope.card = scope.vm;
					}
        });
				scope.closeInfowWindow = function(){
					scope.$parent.closeInfowWindow();
				};
			}
		}
  }]);
