'use strict';

antsquare.controller('SearchCtrl', ['$scope', '$stateParams', '$rootScope', 'GeoLocationService', 'uiGmapIsReady', 'SearchDataIntent', '$state', 'MapService', 'CardIntent', 'MarkerService', 'angularGridInstance', '$cookies', 'Config',
  function($scope, $stateParams, $rootScope, GeoLocationService, uiGmapIsReady, SearchDataIntent, $state, MapService, CardIntent, MarkerService, angularGridInstance, $cookies, Config) {

    if ($cookies.get('viewHistory')) {
      $scope.markerHistory = $cookies.get('viewHistory')
        .split(',');
    } else {
      $scope.markerHistory = []
    }
    uiGmapIsReady.promise()
      .then(function(map_instances) {
        $scope.mapLoaded = true;
      });
    $scope.switchingView = false;
    $scope.mapLoaded = false;
    $scope.searchOff = false;
    $scope.isLoading = false;
    $scope.results = [];
    $scope.cards = null;
    $scope.markers = [];
    $scope.map = {
      center: {
        latitude: 49.5171086775325,
        longitude: -123.010166
      },
      control: {},
      zoom: 9,
      options: {
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // disableDefaultUI: true
      },
      dragging: false
    };

    $scope.userLocation = {
      id: 'thisiswhereyouareat',
      idKey: 'thisiswhereyouareat',
      options: {
        icon: 'https://antsquare-www.imgix.net/marker/userLocation.png',
        draggable: false,
      },
      draggable: false,
      show: true,
    };

    $scope.map.events = {
      idle: function() {
        if ($scope.mapLoaded) {
          SearchDataIntent.resetPagination();
        }
        if ($scope.currentView == 3) {
          SearchDataIntent.resetPagination(1, 30);
        }
        if (!$scope.switchingView) {
          $scope.search();
        }
      },
      click: function(event) {

        if (indexBucket.length > 0) {
          $scope.markers[indexBucket[0]].setState('visited');
        }
        $scope.closeInfowWindow();
        $scope.$apply();
      }
    };

    // Search
    var requestForProducts = null;
    var mapSet = false;
    var yourLocation = null;

    if (SearchDataIntent.get()) {
      $scope.formData = SearchDataIntent.get()
    } else {
      SearchDataIntent.start();
    }
    $scope.formData = SearchDataIntent.forMap();
    $scope.formData = SearchDataIntent.setCategory($stateParams.category);
    SearchDataIntent.setQuery($stateParams.query)
    $scope.formData = SearchDataIntent.get()

    // Set Map Location
    function setMapLocation() {
      if (MapService.hasLocation($stateParams)) {
        mapSet = true
        yourLocation = {};
        $scope.map = MapService.setMapZoom($scope.map, $stateParams.zoom);
        $scope.map = MapService.setMapLocation($scope.map, $stateParams);
        SearchDataIntent.set($stateParams);
        if (!$stateParams.query) {
          $scope.search();
        }
      } else if (GeoLocationService.hasLocation()) {
        yourLocation = GeoLocationService.getCoords();
        yourLocation.lat = yourLocation.latitude;
        yourLocation.lon = yourLocation.longitude;
        $scope.map = MapService.setMapLocation($scope.map, yourLocation);
        $scope.map.zoom = 12;
        SearchDataIntent.set($stateParams);
        if (!$stateParams.query) {
          $scope.search();
        }
      }
    };

    $rootScope.$on('setLocation', function() {

      yourLocation = GeoLocationService.getCoords();
      yourLocation.lat = yourLocation.latitude;
      yourLocation.lon = yourLocation.longitude;
      if (!mapSet) {
        $scope.map = MapService.setMapLocation($scope.map, yourLocation);
        $scope.map.zoom = 12;
        SearchDataIntent.set(yourLocation);
        SearchDataIntent.set($stateParams);
        if (!$stateParams.query) {
          $scope.search();
        }

      }
      $scope.userLocation.coords = {
        latitude: yourLocation.lat,
        longitude: yourLocation.lon
      };
    });
    // Cards
    $scope.getCards = function() {
      if ($scope.isLoading && requestForProducts) {
        requestForProducts.abort();
      }
      $scope.isLoading = true;
      $scope.results = [];
      $scope.markers = [];
      $scope.cards = null;
      $scope.formData = SearchDataIntent.get();
      if ($scope.currentView > 1 && $scope.formData.bottom_left_lat) {
        delete $scope.formData.lat
        delete $scope.formData.lon
      }
      (requestForProducts = MapService.search($scope.formData))
      .then(
        function(data) {
          $scope.cards = null;
          $scope.isLoading = false;
          $scope.results = data;
          $scope.cards = data.data;

          if (data.data.cards) {
            $scope.cards.products = data.data.cards.map(function(card) {
              return CardIntent.set(card);
            });


            Object.keys(data.data.markers)
              .map(function(marker, index) {
                $scope.markers[index] = new MarkerService(data.data.markers[marker][0]);
                $scope.cards.products.forEach(function(card) {
                  if (card.idKey === $scope.markers[index].idKey) {
                    $scope.markers[index].products.push(card);
                  }
                })
                $scope.markers[index].setIcon();
                $scope.markers[index].setState();
                $scope.markerHistory.map(function(historyIdKey) {
                  if (marker === historyIdKey) {
                    $scope.markers[index].setState('visited');
                  }
                });
              });
            if (!_.isEmpty(angularGridInstance.mapSearch)) {
              setTimeout(function() {
                angularGridInstance.mapSearch.refresh();
              }, 1000);
            }
          }


          setTimeout(function() {
            angularGridInstance.mapSearch.refresh();
          }, 4000);

          setTimeout(function() {
            angularGridInstance.mapSearch.refresh();
          }, 8000);


        }, (function(err) {
          $scope.cards = null;
          // console.log(err)
          $scope.isLoading = false;
        })
      )
    };

    // Search
    $scope.search = function() {
      SearchDataIntent.setBounds($scope.map.northeast, $scope.map.southwest);
      if (SearchDataIntent.isValidate()) {
        if (!$scope.searchOff) {
          $scope.closeInfowWindow();
          $scope.getCards();
        }
        setTimeout(function() {
          $state.transitionTo('search', MapService.setUrlParams($scope.map, SearchDataIntent.get()), {
            reload: false,
            notify: false
          });
        }, 100);
        $scope.searchOff = false;
      }
    };

    $scope.findMe = function() {
      if (yourLocation) {
        yourLocation.lat = yourLocation.latitude;
        yourLocation.lon = yourLocation.longitude;
        $scope.map = MapService.setMapLocation($scope.map, yourLocation);
        $scope.search();
      } else {
        yourLocation = GeoLocationService.getCoords();
        yourLocation.lat = yourLocation.latitude;
        yourLocation.lon = yourLocation.longitude;
        $scope.map = MapService.setMapLocation($scope.map, yourLocation);
        $scope.search();
      }
    }

    // SearchBar
    $scope.updateFormData = function(formData) {
      $scope.searchOff = false;
      $scope.formData = SearchDataIntent.set(formData);
      $scope.map = MapService.setMapLocation($scope.map, formData);
      console.log('5')
      $scope.search();
    };

    // Sort

    $scope.sortTypes = [{
      display: 'Nearest',
      type: null
    }, {
      display: 'Newest',
      type: 'new'
    }, {
      display: 'Price (Low to High)',
      type: 'price'
    }];
    $scope.selectedType = $scope.sortTypes[0];

    if ($stateParams.sort === 'new') {
      $scope.selectedType = $scope.sortTypes[1];
    } else if ($stateParams.sort === 'price') {
      $scope.selectedType = $scope.sortTypes[2];
    }
    $scope.sortBy = function(type) {
      $scope.selectedType = type;
      $scope.formData = SearchDataIntent.setSort(type.type);
      $scope.formData.page = 1;
      $scope.search();
    }
    $scope.infoWindow = {
      options: {
        boxClass: 'custom-info-window',
        disableAutoPan: false,
        visible: false,
        maxWidth: 280,
        pixelOffset: {
          width: -140,
          height: -275
        }
      }
    };
    $scope.sideInfoWindow = null;
    //  InfoWindow
    var indexBucket = [];

    $scope.onClick = function(data) {
      $scope.closeInfowWindow();
      $scope.sideInfoWindow = null;
      $scope.infoWindow = null;

      $scope.searchOff = true;
      var index = _.findIndex($scope.markers, function(marker) {
        return marker.idKey == data
      });

      if (indexBucket.length < 1) {
        indexBucket[0] = index;
      } else {
        $scope.markers[indexBucket[0]].setState('visited');
        indexBucket[1] = indexBucket[0];
        indexBucket[0] = index;
      }

      $scope.markers[index].setState('active');
      $scope.markers[index].displayImages = getDisplayImages($scope.markers[index]);

      var infoWindowVisible = true;

      if ($scope.currentView === 2) {
        $scope.sideInfoWindow = _.cloneDeep($scope.markers[index]);
        infoWindowVisible = false;
      }

      $scope.infoWindow = {
        product: $scope.markers[index],
        products: $scope.markers[index],
        location: {
          latitude: $scope.markers[index].latitude,
          longitude: $scope.markers[index].longitude,
        },
        options: {
          boxClass: 'custom-info-window',
          disableAutoPan: false,
          visible: infoWindowVisible,
          maxWidth: 280,
          pixelOffset: {
            width: -140,
            height: -275
          }
        },
        mouseenter: function() {
          $scope.map.options.scrollwheel = false;
        },
        mouseleave: function() {
          $scope.map.options.scrollwheel = true;
        },
        imgNext: function(marker) {
          var index = _.findIndex($scope.markers, function(marker) {
            return marker.idKey == data
          });

          var length;
          length = marker.displayImages.length;
          if (marker.displayIndex < length - 1) {
            $scope.markers[index].displayIndex = marker.displayIndex + 1
          } else {
            $scope.markers[index].displayIndex = 0
          }
        },
        imgBack: function(marker) {
          var index = _.findIndex($scope.markers, function(marker) {
            return marker.idKey == data
          });

          var length;
          length = marker.displayImages.length;
          if (marker.displayIndex > 0) {
            $scope.markers[index].displayIndex = marker.displayIndex - 1
          } else {
            $scope.markers[index].displayIndex = length - 1;
          }
        },
        click: function() {
          // console.log("click")
        }
      };

      setTimeout(function() {
        $scope.searchOff = false;
      }, 1000);

      $scope.markerHistory.push($scope.markers[index].idKey);
      var date = new Date();
      date.setDate(date.getDate() + 1);
      $cookies.put('viewHistory', $scope.markerHistory, date, {
        domain: Config.COOKIE_DOMAIN
      });
      $scope.$apply();
    };

    function getDisplayImages(marker) {
      var list = [];
      if (marker.idType === 'STORE') {
        list = marker.products.map(function(product) {
          return product.images[0];
        })
      } else if (marker.idType === 'PRODUCT') {
        list = marker.images.map(function(product) {
          return product.images[0];
        })
      }
      return list;
    }
    $scope.closeInfowWindow = function() {
      if ($scope.infoWindow) {
        if (indexBucket.length > 0) {
          $scope.markers[indexBucket[0]].setState('visited');
          indexBucket = [];
        }
        $scope.infoWindow.options.visible = false;
        $scope.sideInfoWindow = null;
      }
    };

    // Start Page
    setMapLocation();
    // Markers on Hover
    var hoverBucket = [];

    $rootScope.$on('cardHover', function(event, data) {
      var index = _.findIndex($scope.markers, function(marker) {
        return marker.idKey == data.idKey;
      });
      if (index > -1) {

        if (hoverBucket.length < 1) {
          hoverBucket[0] = $scope.markers[index];
        } else {
          hoverBucket[1] = hoverBucket[0];
          hoverBucket[0] = $scope.markers[index];
        }
        $scope.markers[index].hoverMarker();
      }
    });

    $rootScope.$on('cardLeave', function(event, data) {
      var index = _.findIndex($scope.markers, function(marker) {
        return marker.idKey == data.idKey;
      });
      if (index > -1) {
        $scope.markers[index].setState(hoverBucket[0].state);
      }
    });

    $rootScope.$on('fullMapView', function() {
      // Wait for the animation to end.
      setTimeout(function() {
        $scope.map.control.refresh($scope.map.center);
      }, 300);
    });

    $rootScope.$on('showFilter', function(event, data) {
      if ($scope.currentView === 2 && $scope.sideInfoWindow) {
        $scope.sideInfoWindow = null;
      }
    });

    $scope.currentView = 2;

    $rootScope.$on('switchView', function(event, view) {
      if (view === 1) {
        SearchDataIntent.setLocation({
          lat: $scope.map.center.latitude,
          lon: $scope.map.center.longitude,
        });
      }
      if (view !== 2) {
        $scope.closeInfowWindow();
      };
      $scope.getCards();
      $scope.currentView = view
      $scope.switchingView = true;
      setTimeout(function() {
        $scope.switchingView = false
      }, 300);
    });
    $rootScope.$on('findMe', function() {
      if (GeoLocationService.getCoords()
        .latitude && GeoLocationService.getCoords()
        .latitude !== NaN) {
        $scope.map.center = {
          latitude: GeoLocationService.getCoords()
            .latitude,
          longitude: GeoLocationService.getCoords()
            .longitude
        };
        $scope.userLocation.coords = {
          latitude: GeoLocationService.getCoords()
            .latitude,
          longitude: GeoLocationService.getCoords()
            .longitude
        };
      }
    });
  }
]);

antsquare.controller('mapControlBtnVtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  $scope.findMe = function() {
    $rootScope.$broadcast('findMe');
  };
}]);