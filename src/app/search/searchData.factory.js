'use strict';

antsquare
  .factory('SearchDataContainer', function() {

    function SearchDataContainer() {
      this.category = null;
      this.lat = null;
      this.lon = null;
      this.bottom_left_lat = null;
      this.bottom_left_lon = null;
      this.top_right_lat = null;
      this.top_right_lon = null;
      this.radius = "";
      this.min_price = 0;
      this.max_price = 99999999;
      this.query = null;
      this.tag = null;
      this.type = 'product';
      this.page = '1';
      this.sort = null;
      this.per_page = "15";
      this.is_pretty = false;
      this.viewer_id = null;
    }
    return SearchDataContainer;
  });

antsquare
  .factory('SearchDataIntent', ['SearchDataContainer', function(SearchDataContainer) {
    var serachDataContainer;
    var next = false;
    var mapPerPage = 15;
    return {
      start: function() {
        serachDataContainer = new SearchDataContainer();
        return serachDataContainer
      },
      set: function(data) {
        if (!data.page) {
          data.page = '1'
        };
        if (!data.per_page) {
          data.per_page = mapPerPage;
        }

        for (var key in data) {
          serachDataContainer[key] = data[key];
        }

        return serachDataContainer
      },
      isSet: function() {
        if (serachDataContainer) {
          return true
        } else {
          return false
        }
      },
      nextPage: function() {
        serachDataContainer.page = serachDataContainer.page + 1;
        // if (next) {
        //  serachDataContainer.page = serachDataContainer.page + 1;
        // } else {
        //  serachDataContainer.page = serachDataContainer.page + 2;
        //  serachDataContainer.per_page = 5
        // }
      },
      isValidate: function() {
        if ((serachDataContainer.lat && serachDataContainer.lon)) {
          return true
        } else if (serachDataContainer.bottom_left_lat && serachDataContainer.bottom_left_lon && serachDataContainer.top_right_lat && serachDataContainer.top_right_lon) {
          return true
        } else {
          return false
        }
      },
      resetPagination: function(page, per_page) {
        page = page || 1;
        per_page = per_page || 15
        serachDataContainer.page = page;
        serachDataContainer.per_page = per_page;
        return serachDataContainer
      },
      forMap: function() {
        serachDataContainer.page = 1;
        serachDataContainer.per_page = mapPerPage;
        serachDataContainer.type = null;
        serachDataContainer.radius = "";
        return serachDataContainer
      },
      setType: function(type) {
        if (type) {
          serachDataContainer.type = type;
        }
        return serachDataContainer
      },
      setSort: function(sort) {
        serachDataContainer.sort = sort;
        return serachDataContainer
      },
      setCategory: function(category) {
        serachDataContainer.category = category;
        return serachDataContainer
      },
      setCategoryType: function(categoryType) {
        serachDataContainer.categoryType = categoryType;
      },
      setCategoryIcon: function(categoryIcon) {
        serachDataContainer.categoryIcon = categoryIcon;
      },
      setLocation: function(location) {
        serachDataContainer.lat = location.lat;
        serachDataContainer.lon = location.lon;
        return serachDataContainer
      },
      setBounds: function(northeast, southwest) {
        if (northeast && southwest) {
          serachDataContainer.bottom_left_lat = southwest.latitude;
          serachDataContainer.bottom_left_lon = southwest.longitude;
          serachDataContainer.top_right_lat = northeast.latitude;
          serachDataContainer.top_right_lon = northeast.longitude;
        }
        return serachDataContainer
      },
      setRadius: function(radius) {
        serachDataContainer.radius = radius;
      },
      setPage: function(page) {

        serachDataContainer.page = page;
      },
      setPrice: function(slider) {
        serachDataContainer.max_price = slider.max;
        serachDataContainer.min_price = slider.min;
      },
      setPretty: function(boolean) {
        serachDataContainer.is_pretty = boolean;
        if (!boolean) {
          delete serachDataContainer.is_ugly;
          delete serachDataContainer.is_pretty;
        }

      },
      setQuery: function(query) {
        serachDataContainer.query = query;
        return serachDataContainer
      },
      setTag: function(tag) {
        serachDataContainer.tag = tag;
      },
      setViewer: function(viewer_id) {
        serachDataContainer.viewer_id = viewer_id;
      },
      get: function() {
        return serachDataContainer;
      },
      end: function() {
        serachDataContainer = {};
      }

    };

  }]);