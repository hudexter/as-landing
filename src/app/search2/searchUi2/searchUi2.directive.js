'use strict';

angular.module('antsquare')
  .directive('searchUi2', ['$rootScope', '$window', 'angularGridInstance', '$stateParams', function($rootScope, $window, angularGridInstance, $stateParams) {
    return {
      restrict: 'E',
      // templateUrl: 'app/search/searchUi/searchUi.html',
      link: function(scope, element, attr) {
        $rootScope.$on('loadMore', function() {


        })

        scope.loadMore = function() {
          $rootScope.$broadcast('loadMore');
        };

        scope.styleForPeople = {};

        scope.$watch('formData.type', function() {
          if (scope.formData.type == 'store' || scope.formData.type == 'user') {
            scope.styleForPeople = {
              // 'max-width': 'calc(100vw - 350px)'
            };
          } else {
            scope.styleForPeople = {};
          }
          if (scope.formData.type === 'store' || scope.formData.type == 'user' || scope.formData.type == 'moment' || scope.formData.type == 'tag') {

            scope.sortTypes = [{
              display: 'Nearest',
              type: null
            }, {
              display: 'Newest',
              type: 'new'
            }];
          } else if (scope.formData.type === 'service' || scope.formData.type == 'product') {

            scope.sortTypes = [{
              display: 'Nearest',
              type: null
            }, {
              display: 'Newest',
              type: 'new'
            }, {
              display: 'Price (Low to High)',
              type: 'price'
            }];
          }

        });
        scope.mapActive = true;
        scope.menuToggle = function() {
          scope.mapActive = !scope.mapActive;
        };

        scope.template = 'app/search2/searchUi2/searchUi2.html';
        scope.viewportSm = false;

        scope.$watch(function() {
          return $window.innerWidth
        }, function() {
          checkViewPort();
        });

        function checkViewPort() {
          if ($window.innerWidth <= 767) {
            scope.template = 'app/search2/searchUi2/searchUiSm2.html';
            scope.currentView = 1;
            scope.viewportSm = true;
            angular.element(document.querySelector('.angular-google-map-container'))
              .addClass('view-sm');
          } else {
            scope.template = 'app/search2/searchUi2/searchUi2.html';
            scope.viewportSm = false;
            angular.element(document.querySelector('.angular-google-map-container'))
              .removeClass('view-sm');
            angular.element(document.getElementById('map-menu-container'))
              .removeClass('hide-map-menu');
            angular.element(document.getElementById('searchGridContainer'))
              .removeClass('expand-grid');
          }
        };

        scope.hideMenu = function() {
          if (scope.viewportSm && !scope.showFilter) {
            angular.element(document.getElementById('map-menu-container'))
              .addClass('hide-map-menu');
            angular.element(document.getElementById('searchGridContainer'))
              .addClass('expand-grid');
          }
        };

        scope.showMenu = function() {
          setTimeout(function() {
            if (scope.viewportSm && !scope.showFilter) {
              if (scope.currentView !== 3) {
                angular.element(document.getElementById('map-menu-container'))
                  .removeClass('hide-map-menu');
                angular.element(document.getElementById('searchGridContainer'))
                  .removeClass('expand-grid');
              }
            }
          }, 100);
        };

        setTimeout(function() {
          checkViewPort();
        }, 100);

        scope.cardShow = true;
        scope.showFilter = false;
        scope.filterToggle = function() {
          if (!scope.showFilter) {
            scope.hideMenu();
            angular.element(document.getElementById('searchGridContainer'))
              .addClass('hidden');
            scope.showFilter = !scope.showFilter;
          } else {
            angular.element(document.getElementById('searchGridContainer'))
              .removeClass('hidden');
            scope.showFilter = !scope.showFilter;
            if (scope.currentView !== 3) {
              scope.showMenu();
            }
          }
          angularGridInstance.mapSearch.refresh();
          setTimeout(function() {
            scope.$broadcast('rzSliderForceRender');
          });
        };

        scope.toMap = function() {
          scope.hideMenu();
          scope.currentView = 3;
          setTimeout(function() {
            angular.element(document.querySelector('#searchGridContainer'))
              .addClass('hide');
            angular.element(document.querySelector('.angular-google-map-container'))
              .addClass('view-sm');
          });
          setTimeout(function() {
            scope.map.control.refresh(scope.map.center);
            angularGridInstance.mapSearch.refresh();
            $rootScope.$broadcast('refreshSlider');
          }, 300);
        }

        scope.toGrid = function() {
          scope.cardShow = true;
          scope.currentView = 1;
          setTimeout(function() {
            scope.showMenu();
            scope.currentView = 1;
            angular.element(document.querySelector('#searchGridContainer'))
              .removeClass('hide');
            angular.element(document.querySelector('.angular-google-map-container'))
              .removeClass('view-sm');
          });
          setTimeout(function() {
            angularGridInstance.mapSearch.refresh();
            $rootScope.$broadcast('refreshSlider');
          }, 250);
        }

      },
      template: "<div><ng-include src='template'></ng-include></div>"
    }
  }]);