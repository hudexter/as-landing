antsquare
	.filter('toHtml', [function () {
		return function (data) {
			if (data) {
				return data.replace(/\r?\n/g, '<br/>')
					.replace(/  +/g, '&nbsp;&nbsp;');
			}
		};
	  }]);
