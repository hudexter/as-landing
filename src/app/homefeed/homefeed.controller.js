'use strict';

antsquare
  .controller('HomefeedCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
      getHomefeed();
    } else {
      $scope.userInfo = null;

    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        getHomefeed();
      } else {
        $scope.userInfo = null;

      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        getProducts($scope.coords);
        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }

    var scrollLoad = false;

    function addLoadMoreCard(scrollLoad) {
      if ($scope.cards.has_more && !scrollLoad) {
        if (scrollLoad === undefined) {
          $scope.cards.products.push({
            type: 'has_more_temp'
          })
        } else {
          $scope.cards.products.push({
            type: 'has_more'
          })
        }

      } else if (!$scope.cards.has_more) {
        $scope.cards.products.push({
          type: 'no_more'
        })
      }
    };

    $scope.$on('loadMoreTemp', function() {
      $scope.loading = true;
      $scope.loadingMoreProducts = true;
      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

      StoreFactory.getProducts($stateParams.store_id, $scope.productsPageParams)
        .then(function(data) {
          $scope.cards.products.pop();

          $scope.newCards = data.data;

          $scope.newCards.products = data.data.products.map(function(card) {
            return CardIntent.set(card);
          });
          $scope.cards.has_more = $scope.newCards.has_more;
          $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
          addLoadMoreCard();
          refreshPlate();
          $scope.loadingMoreProducts = false;
          $scope.loading = false;
        })
        .catch(function(error) {
          $scope.loading = false;
          console.log(error)
          if (error.status === 400) {
            $scope.error = error.data.message;
          } else {
            $scope.error = error.data.message;
          }
          $scope.loadingMoreProducts = false;
        });

    });

    $scope.$on('loadMore', function() {
      if (!scrollLoad) {
        $scope.cards.products.pop();
        $scrollLoad = true;
      }
      $scope.loadMore();
    });
    $scope.loadingMoreProducts = false;

    $scope.loading = null;
    $scope.loadMore = function() {

      if (scrollLoad && !$scope.loadingMoreProducts) {
        $scope.loading = true;
        $scope.loadingMoreProducts = true;
        $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
        StoreFactory.getProducts($stateParams.store_id, $scope.productsPageParams)
          .then(function(data) {
            $scope.newCards = data.data;
            $scope.newCards.products = data.data.products.map(function(card) {
              return CardIntent.set(card);
            });
            $scope.cards.has_more = $scope.newCards.has_more;
            $scope.cards.products = $scope.cards.products.concat(scope.newCards.products);
            addLoadMoreCard(scrollLoad);
            refreshPlate();
            $scope.loadingMoreProducts = false;
            $scope.loading = false;
          })
          .catch(function(error) {
            $scope.loading = false;
            console.log(error)
            if (error.status === 400) {
              $scope.error = error.data.message;
            } else {
              $scope.error = error.data.message;
            }
            $scope.loadingMoreProducts = false;
          });
      }

    }

    $scope.homefeed = [];

    function getHomefeed() {

      StoreFactory.getSocialHomeFeed($scope.productsPageParams)
        .then(function(data) {
          $scope.homefeed = data.data.cards;

          /*for (var i = 0; i < $scope.homefeed.length; i++) {
            if ($scope.homefeed[i].target_json.user_info.id == $scope.userInfo.id) {
              $scope.HomefeedFollow = false;
            }
            $scope.HomefeedFollow = true;
          }*/
          $scope.$broadcast('productLoaded');
        })
        .catch(function(err) {
          $scope.homefeed = err.message;
        })

    };

    function refreshPlate() {
      if (angularGridInstance.home) {
        angularGridInstance.home.refresh();
      }
    };

    $scope.showFollowers = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollower/modalFollower.html",
          controller: "ModalFollowerCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };

    $scope.showFollowings = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
          controller: "ModalFollowingCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.likeProductToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
      if ($scope.homefeed[index].target_json.is_liked) {

        AccountsFactory.likeProduct($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeProduct($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
          })
      }
    };
    $scope.likeMomentToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
      if ($scope.homefeed[index].target_json.is_liked) {

        AccountsFactory.likeMoment($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeMoment($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
          })
      }
    };
    $scope.getShareLinks = function(moment) {
      // $scope.root = $location.host();
      $scope.fbLinkSF = "https://www.facebook.com/sharer/sharer.php?u=" + "https://dashboard.antsquare.com/moment/" + moment.id;

      $scope.twLinkSF = "http://twitter.com/intent/tweet?status=" + moment.name + "+%40antsquare+" + "https://dashboard.antsquare.com/moment/" + moment.id + "%26related=antsquare";
      "http://twitter.com/intent/tweet?status=" + moment.name + "@antsquare" + "https://dashboard.antsquare.com/moment/" + moment.id + "&related=antsquare";

      $scope.pinLinkSF = "https://www.pinterest.com/pin/create/button/?url=" + "https://dashboard.antsquare.com/moment/" + moment.id + "&amp;media=" + moment.images[0] + "&amp;description=Check it out!"

      $scope.mailLinkSF = "mailto:?Subject=Check out " + moment.name + " on Antsquare&body=Thought you might like this : https://dashboard.antsquare.com/moment/" + moment.id;
    };

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.store.user_info.is_following = !$scope.store.user_info.is_following;

      if (!$scope.store.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      }
    };

    $scope.followMomentToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;

      if (!$scope.homefeed[index].target_json.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.homefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.homefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;
          })
      }
    };

    $scope.productsPageParams = {
      page: 1,
      per_page: 20
    };

    $scope.submitComment = function($index) {
      $rootScope.$broadcast("postComment", $scope.homefeed[$index]);
      $scope.homefeed[$index].commentContent = null;
    }

    //getHomefeed();

    /*$scope.onSection = 1;

    var viewPort;
    viewPort = angular.element($window);
    var promo = angular.element(document.querySelector('#promo'))
    var about = angular.element(document.querySelector('#about'))
    var review = angular.element(document.querySelector('#review'))
    var card = angular.element(document.querySelector('#cardPlateContainer'))

    $scope.slideTo = function (section) {
      switch (section) {
      case 'promo':
        $document.duScrollTo(promo, 130, 300);
        break;
      case 'products':
        $document.duScrollTo(card, 130, 300);
        break;
      case 'about':
        $document.duScrollTo(about, 130, 300);
        break;
      case 'review':
        $document.duScrollTo(review, 130, 300);
        break;
      }
    };*/

    /*viewPort.on('scroll', function () {
      $scope.onSection = null;
      if ($scope.promos.total > 0) {
        if (viewPort.scrollTop() > review.position()
          .top - 150) {
          $scope.$apply(function () {
            $scope.onSection = 3;
          });
        } else if (viewPort.scrollTop() > about.position()
          .top - 150) {
          $scope.$apply(function () {
            $scope.onSection = 2;
          });
        } else if (viewPort.scrollTop() > card.position()
          .top - 150) {
          $scope.$apply(function () {
            $scope.onSection = 1;
          });
        } else {
          $scope.$apply(function () {
            $scope.onSection = 0;
          });
        }
      } else {
        if (viewPort.scrollTop() > review.position()
          .top - 150) {
          $scope.$apply(function () {
            $scope.onSection = 3;
          });
        } else if (viewPort.scrollTop() > about.position()
          .top - 150) {
          $scope.$apply(function () {
            $scope.onSection = 2;
          });
        } else {
          $scope.$apply(function () {
            $scope.onSection = 1;
          });
        }
      }

    });*/

  }]);