/* disable warnings about duplicate hash keys for this file */
/*jshint -W075 */

// /* global malarkey:false, moment:false */
// (function() {
//   "use strict";
//
//   angular
//     .module("antsquare")
//     .constant("malarkey", malarkey)
//     .constant("moment", moment);
//
// })();

"use strict";
antsquare.constant("elements", {

  "navbar": "navbar",
  "card": "card",
  "font": "font",
  "searchBox": "searchBox",
  "landing": "landing",
  "form": "form",
  "test": "test"
})

antsquare.constant("URLS", {
  "home": "https://www.antsquare.com",
  "marketplace": "https://www.antsquare.com",
  "login": "https://dashboard.antsquare.com/loading",
  "map": "https://dashboard.antsquare.com/map",
  "merchant": "https://dashboard.antsquare.com/merchant",
  "guidelines": "https://dashboard.antsquare.com/marketplaceGuidelines",
  "about": "https://dashboard.antsquare.com/about?section=about",
  "careers": "https://dashboard.antsquare.com/about?section=jobs#jobs",
  "press": "https://dashboard.antsquare.com/press",
  "tnp": "https://dashboard.antsquare.com/terms",
  "contact": "https://dashboard.antsquare.com/about?section=contact#contact",
  "blog": "https://blog.antsquare.com",
  "knowledge": "https://antsquare.zendesk.com/hc/en-us",
  "fb": "https://www.facebook.com/antsquare",
  "tw": "https://twitter.com/antsquare",
  "ig": "https://www.instagram.com/antsquare_app/",
  "appStore": "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8",
  "playStore": "https://play.google.com/store/apps/details?id=com.antsquare.android"
})

antsquare.constant("SIZE_METRIC", {
  "xs": 480,
  "sm": 768,
  "md": 1024,
  "lg": 1440,
})


/* jshint +W075 */