'use strict';
antsquare
  .factory('seoFbContainer', function () {
    function fb() {
      this.description = '';
      this.title = '';
      this.url = '';
      this.image1 = '';
      this.image2 = '';
      this.image3 = '';
      this.image4 = '';
    }
    return fb;
  });

antsquare
  .factory('seoHtmlContainer', function () {
    function html() {
      this.description = '';
      this.title = '';
    }
    return html;
  });

antsquare
  .factory('seoFactory', ['$rootScope', 'Config', 'seoFbContainer', '$http', 'seoHtmlContainer', function ($rootScope, Config, seoFbContainer, $http, seoHtmlContainer) {

    var seoFbContainer;
    $rootScope.facebookOg = new seoFbContainer();
    $rootScope.html = new seoHtmlContainer();

    return {
      setImage1: function (link) {
        $rootScope.facebookOg.image1 = link
      },
      setDescription: function (description) {
        $rootScope.facebookOg.description = description;
        $rootScope.html.description = description;
      },
      setTitle: function (title) {
        $rootScope.facebookOg.title = title;
        $rootScope.html.title = title;
      },
      setUrl: function (link) {
        $rootScope.facebookOg.url = link;
      },
      setType: function (type) {
        $rootScope.facebookOg.type = type;
      },
      setPrice: function (price) {
        $rootScope.facebookOg.price = price;
      },
      setCurrency: function (currency) {
        $rootScope.facebookOg.currency = currency;
      },
      getProductSeo: function (product_id) {
        return $http
          .get(Config.API_BASEURL + '/products/' + product_id + '/seo');
      },
      getMomentSeo: function (moment_id) {
        return $http
          .get(Config.API_BASEURL + '/moments/' + moment_id + '/seo');
      },
      getStoreSeo: function (store_id) {
        return $http
          .get(Config.API_BASEURL + '/stores/' + store_id + '/seo');
      },
      getStoreHandleSeo: function (handle) {
        return $http
          .get(Config.API_BASEURL + '/stores/$' + handle + '/seo');
      },
      getUserSeo: function (user_id) {
        return $http
          .get(Config.API_BASEURL + '/users/' + user_id + '/seo');
      },
      /*getUsernameSeo: function (username) {
        return $http
          .get(Config.API_BASEURL + '/users/' + username + '/seo');
      },*/
      getRequestSeo: function (request_id) {
        return $http
          .get(Config.API_BASEURL + '/requests/' + request_id + '/seo');
      }

    };

  }]);