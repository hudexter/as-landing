'use strict';

antsquare
  .factory('UserContainer', function() {

    function UserContainer() {
      this.user = {};
      this.loggedIn = {};
    }
    return UserContainer;
  });

antsquare
  .factory('AccountsFactory', ['$http', 'Config', '$rootScope', '$cookies', 'intercomFactory', '$location', function($http, Config, $rootScope, $cookies, intercomFactory, $location) {

    var PAY_URL = Config.PAY_URL + '/v3';
    var ver = 'v1';

    return {
      searchUser: function(data) {
        return $http({
          url: Config.CORE_BASEURL + "/v5/all/search/alluser",
          method: "GET",
          params: data
        });
      },
      getNotify: function() {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/notifications',
          method: "GET"
        });
      },
      logout: function() {
        $cookies.remove('access_token');
        $cookies.remove('refresh_token');
        $cookies.remove('firebase_token');
        $cookies.remove('fb_token');
        $cookies.remove('user_profile');
        $cookies.remove('user_store');
        intercomFactory.logout();
      },
      registerUser: function(data) {
        return $http
          .post(Config.ID_BASEURL + '/v2/api/register', data)
      },
      reCaptchaVerify: function(data) {
        return $http
          .post(Config.ID_BASEURL + '/v2/api/recaptcha', data)
      },
      loginUser: function(data) {
        if (!_.isEmpty(data)) {
          var clone = _.clone(data)
          if (clone.input !== null && !(clone.input.match(/[a-z]/))) {
            clone.input = '+1' + clone.input;
          }
        }
        return $http
          .post(Config.ID_BASEURL + '/v2/api/login', clone)
          .then(function(result) {
            return result;
          });
      },
      resetPassword: function(data) {
        return $http
          .post(Config.ID_BASEURL + '/v2/api/resetpass', data)
      },
      mobileResetPassword: function(data) {
        return $http
          .post(Config.ID_BASEURL + '/v2/api/m/resetpass', data)
      },
      renewPasswordWithCode: function(data) {
        return $http
          .put(Config.ID_BASEURL + '/v2/api/resetpass/chg', data)
      },
      getCurrentProfile: function() {
        return $http
          .get(Config.API_BASEURL + '/users')
          .then(function(data) {
            return data.data;
          });
      },
      updateUserLocation: function(location) {
        return $http
          .post(Config.API_BASEURL + '/users/location', location);
      },
      getCards: function() {
        return $http
          .get(PAY_URL + '/account/cards')
          .then(function(data) {
            return data.data;
          });
      },
      newCard: function(data) {
        return $http
          .post(PAY_URL + '/account/cards', data);
      },
      removeCard: function(cardId) {
        return $http({
          url: PAY_URL + '/account/cards/' + cardId,
          method: "DELETE"
        });
      },
      newDebitCard: function(data) {
        return $http
          .post(PAY_URL + '/account/payout/debit', data);
      },
      putProfile: function(formData) {
        return $http
          .put(Config.ID_BASEURL + '/v2/api/me', formData);
      },
      getProfile: function(user_id) {
        return $http({
            url: Config.ID_BASEURL + '/v2/api/profile',
            method: "GET",
            params: {
              id: user_id
            }
          })
          .then(function(data) {
            return data.data;
          });
      },
      getAreaCode: function() {
        return $http
          .get('js/submodules/data/slim-2edit.js');
      },
      putPhoneNumber: function(phoneData) {
        return $http
          .put(Config.API_BASEURL + '/users/phone', phoneData);
      },

      unlikeProduct: function(product_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/like/' + product_id,
          method: "DELETE"
        });
      },
      likeProduct: function(product_id) {
        product_id = {
          product_id: product_id
        };
        return $http
          .put(Config.CORE_BASEURL + '/v5/users/like', product_id);
      },
      unlikeMoment: function(moment_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/like/moment/' + moment_id,
          method: "DELETE"
        });
      },
      likeMoment: function(moment_id) {
        var moment_id = moment_id;
        return $http
          .put(Config.CORE_BASEURL + '/v5/users/like/moment/' + moment_id);
      },

      unlikeEvent: function(event_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/like/event/' + event_id,
          method: "DELETE"
        });
      },
      likeEvent: function(event_id) {
        return $http
          .put(Config.CORE_BASEURL + '/v5/users/like/event/' + event_id);
      },

      getUserLikes: function(data) {
        return $http({
          url: Config.API_BASEURL + '/users/like/' + data.user_id,
          method: "GET",
          params: {
            page: data.page,
            per_page: data.per_page
          }
        })
      },
      getLikes: function(data) {
        return $http({
          url: Config.API_BASEURL + '/users/like/',
          method: "GET",
          params: data
        })
      },
      getMyLikes: function(data) {
        return $http({
          url: Config.API_BASEURL + '/users/like/me',
          method: "GET",
          params: data
        })
      },
      me: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/me',
          method: "GET"
        });
      },
      addEmail: function(email) {

        return $http
          .put(Config.ID_BASEURL + '/v2/api/email', email);
      },
      userLinking: function(provider, accountInfo) {
        var access_token = {
          access_token: accountInfo
        };
        if (provider === 'google') {
          return $http.post(Config.ID_BASEURL + '/v2/auth/google/token', access_token);
        } else if (provider === 'facebook') {
          return $http.post(Config.ID_BASEURL + '/v2/auth/facebook/token', access_token);
        } else if (provider === 'weibo') {
          return $http.post(Config.ID_BASEURL + '/v2/auth/weibo/token', access_token);
        }
      },
      pendingTransaction: function() {
        return $http
          .get(PAY_URL + '/orders/pending');
      },
      getSellingTx: function(data) {
        return $http({
          url: PAY_URL + '/orders/me/sell',
          method: "GET",
          params: data
        })
      },
      getBuyingTx: function(data) {
        return $http({
          url: PAY_URL + '/orders/me/buy',
          method: "GET",
          params: data
        })
      },
      cancelTx: function(id) {
        return $http({
          url: PAY_URL + '/orders/' + id + '/cancel',
          method: "GET"
        })
      },
      confirmTx: function(id, code) {
        return $http({
          url: PAY_URL + '/orders/' + id + '/confirm',
          method: "PUT",
          data: {
            'confirmation_uuid': code
          }
        })
      },
      addBankAcc: function(formData) {
        return $http
          .post(PAY_URL + '/account/payout/bnkacc', formData);
      },
      getBanks: function() {
        return $http
          .get(PAY_URL + '/account/payout/accounts')
      },
      verifyBank: function(data) {
        return $http
          .post(PAY_URL + '/account/payout/bnkacc/verify', data)
      },
      getPayouts: function(data) {
        return $http({
          url: PAY_URL + '/account/payout/tx',
          method: "GET",
          params: data
        })
      },
      getPayoutDetails: function(id) {
        return $http({
          url: PAY_URL + '/account/payout/tx/' + id,
          method: "GET"
        })
      },
      addPassword: function(password) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/chgpass',
          method: "POST",
          data: password
        })
      },
      changePassword: function(password) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/chgpass',
          method: "POST",
          data: password
        })
      },
      changeEmail: function(input) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/email/change',
          method: "POST",
          data: input
        })
      },
      verifyEmail: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/email/triggerverify',
          method: "GET"
        })
      },
      changePhone: function(phoneData) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/phone',
          method: "PUT",
          data: phoneData
        })
      },
      setMobile: function(phoneData) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/phone2',
          method: "PUT",
          data: phoneData
        })
      },
      verifyPhoneNumber: function(code) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/phone/verify',
          method: "PUT",
          data: code
        })
      },
      getFollowers: function(user_id, data) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/profile/' + user_id + '/follower',
          method: "GET",
          params: data
        })
      },
      getProductLikes: function(product_id, data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/products/' + product_id + '/likers',
          method: "GET",
          params: data
        })
      },
      getMomentLikes: function(moment_id, data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/moments/' + moment_id + '/likers',
          method: "GET",
          params: data
        })
      },
      getUserProfileLikes: function(user_id, data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + user_id + '/likers',
          method: "GET",
          params: data
        })
      },
      getFollowings: function(user_id, data) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/profile/' + user_id + '/following',
          method: "GET",
          params: data
        })
      },
      followUser: function(user_id) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/profile/' + user_id + '/follow',
          method: "PUT"
        })
      },
      unfollowUser: function(user_id) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/profile/' + user_id + '/unfollow',
          method: "PUT"
        })
      },
      getSocial: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/social',
          method: "GET"
        })
      },
      getSetting: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/setting',
          method: "GET"
        })
      },
      putSetting: function(data) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/setting',
          method: "PUT",
          data: data
        })
      },
      putFBInfo: function(data) {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/facebook',
          method: "PUT",
          data: data
        })
      },
      unlinkFacebook: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/facebook',
          method: "DELETE"
        })
      },
      getFeeds: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/feed',
          method: 'GET',
          params: data
        })
      },
      getEmailVerify: function(data) {

        return $http({
          url: Config.ID_BASEURL + '/v2/api/email/verify',
          method: 'GET',
          params: data
        })
      },
      fbTokebRefresh: function() {
        return $http({
          url: Config.ID_BASEURL + '/v2/api/facebook/refresh?redirect_uri=' + $location.protocol() + "://" + $location.host(),
          method: 'GET'
        })
      },
      getAllRequest: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/requests/all',
          method: 'GET',
          params: {
            page: data.page,
            per_page: data.per_page,
            uid: data.uid
          }
        })
      },
      getCountryCode: function() {
        return $http({
          cache: true,
          url: Config.CORE_BASEURL + '/v5/status',
          method: 'GET'
        });
      },
      getUsernames: function(query) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/all/suggest/username?query=' + query,
          method: 'GET',

        });
      },
      getStoreHandles: function(query) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/all/suggest/storehandle?query=' + query,
          method: 'GET',

        });
      },
      getTags: function(query) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/all/suggest/tag?query=' + query,
          method: 'GET',

        });
      },
      getDownloadLinkOnPhone: function(phoneNumber) {
        return $http({
          url: Config.API_BASEURL + '/status/applink?to=+1' + phoneNumber,
          method: 'GET',
        });
      },
    };

  }]);