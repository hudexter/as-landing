'use strict';

antsquare
  .factory('StripeKey', ['$window', 'Config', function($window, Config) {
    // $window.Stripe.setPublishableKey(Config.STRIPE_KEY);

    return {
      getKey: function() {
        $window.Stripe.setPublishableKey(Config.STRIPE_KEY);
      }
    };
  }]);
