'use strict';

antsquare
  .factory('CardContainer', function() {

    function CardContainer(card) {
      if (card.type == 'product' || card.type == 'service' || card.type == 'moment') {
        for (var key in card) {
          this[key] = card[key]
        }

        this.store_id = card.store_id;
        this.user_id = card.user_id;
        this.id = card.id;
        this.idKey = card.idkey;
        this.address1 = card.address1;
        this.address2 = card.address2;
        this.approved = card.approved;
        this._tags = card._tags;
        // this.as_business = card.as_business;
        this.lat = card.lat;
        this.lon = card.lon;
        this.country = card.country;
        this.currency = card.currency;
        // this.flagged = card.flagged;
        this.radius = card.radius;
        this.images = card.images;
        this.description = card.description;
        this.name = card.name;
        this.category_info = card.category_info;
        this.price = card.price;
        this.price_in_cents = card.price_in_cents;
        this.distance_in_km = card.distance_in_km;
        this.distance_in_miles = card.distance_in_miles;
        this.price_type = card.price_type;
        // this.quantity = card.quantity;
        this.category = card.category;
        this.type = card.type;
        this.video_url = card.video_url;
        this.user_info = card.user_info;
        this.is_liked = card.is_liked;
        // this.is_visible = card.is_visible;
        // this.fbShare = false;
        if (card.video_thumbnail != null) {
          this.video_thumbnail = card.video_thumbnail;
        } else {
          this.video_thumbnail = card.images[0];
        }
      } else {
        for (var key in card) {
          this[key] = card[key]
        }
      }

    }
    return CardContainer;
  });

antsquare
  .factory('CardIntent', ['CardContainer', function(CardContainer) {
    var cardContainer;
    return {
      set: function(card) {
        cardContainer = new CardContainer(card);
        return cardContainer
      }
    };

  }]);