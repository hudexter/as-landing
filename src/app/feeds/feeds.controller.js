'use strict';

angular.module('antsquare')
    .controller('FeedsCtrl', ['$scope', 'AccountsFactory', function($scope, AccountsFactory) {
        $scope.feeds = [];
        $scope.pageParams = {
            has_more: false,
            per_page: 10,
            page: 1
        };
        $scope.loading = false;
        $scope.getFeed = function() {
            $scope.loading = true;
            AccountsFactory.getFeeds($scope.pageParams)
                .then(function(data) {
                    $scope.loading = false;
                    $scope.feeds = $scope.feeds.concat(data.data.cards);
                    $scope.pageParams = data.data;
                    delete $scope.pageParams.cards;
                })
                .catch(function(error) {
                    $scope.loading = false;
                    $scope.result = error;
                })
        };

        $scope.nextFeed = function() {
            if (!$scope.loading) {
                $scope.pageParams.page = $scope.pageParams.page + 1;
                $scope.getFeed();
            }
        };

        $scope.getFeed();
    }]);