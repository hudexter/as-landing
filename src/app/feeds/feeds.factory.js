'use strict';

angular.module('antsquare')
	.factory('FeedsFactory', ['$firebase', 'Config', '$firebaseAuth', '$firebaseObject',
   function ($firebase, Config, $firebaseAuth, $firebaseObject) {
			return {
				getFeedPrompt: function (id) {
					var ref = new Firebase(Config.FIREBASE_BASEURL + '/feed_notification_' + Config.ENV + '/' + id);
					return $firebaseObject(ref);
				}
			};
}]);
