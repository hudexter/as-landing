'use strict';

antsquare
  .factory('PaymentFactory', ['$http', 'Config', function($http, Config) {

    return {
      me: function(){
        return $http({
          url: Config.PAY_URL + '/v3/account/payout/identity',
          method: 'GET'
        });
      },
      setId: function(data){
        return $http({
          url: Config.PAY_URL + '/v3/account/payout/identity',
          method: 'POST',
          data: data
        });
      },
      post: function(paymentData) {
        return $http.post(payURL + '/orders', paymentData);
      },
      postClaimCoupon: function(code){
        return $http({
          url: Config.PAY_URL + '/v3/coupon/claim',
          method: 'GET',
          params: code
        });
      },
      myCredit: function(){
        return $http({
          url: Config.PAY_URL + '/v3/account/me',
          method: 'GET'
        });
      },
      orderPreview: function(orderData){
        return $http.post(Config.PAY_URL + '/v3/orders/preview', orderData);
      }
    };
  }]);
