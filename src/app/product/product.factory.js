'use strict';

antsquare
  .factory('ProductsFactory', ['$http', 'Config', '$q', function($http, Config, $q) {

    return {
      getProductPageView: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/products/' + data.product_id + '/pageview',
          method: 'GET',
          params: data
        });

      },
      getCommunities: function(obj) {
        return $http
          .get(Config.CORE_BASEURL + '/v5/communities/home', {
            params: obj
          });
      },
      search: function(obj) {
        return $http
          .get(Config.CORE_BASEURL + '/v5/communities/search', {
            params: obj
          });
      },
      findProductById: function(id) {
        return $http
          .get(Config.API_BASEURL + '/products/' + id);
      },
      bulkPost: function(productData) {
        return $http
          .post(Config.CORE_BASEURL + '/v5/all/bulk', productData);
      },
      editProduct: function(product_id, productData) {
        return $http
          .put(Config.API_BASEURL + '/products/' + product_id, productData);
      },
      deleteProduct: function(id) {
        return $http
          .delete(Config.API_BASEURL + '/products/' + id);

      },
      getCategories: function() {
        return $http({
          cache: true,
          url: Config.CORE_BASEURL + '/v5/categories',
          method: 'GET'
        });
      },
      getImages: function() {

        return $http({

          url: Config.CORE_BASEURL + '/v5/stores/featured',
          method: 'GET'
        });
      },
      flagProduct: function(id, reason) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/products/' + id + '/flag',
          method: 'PUT',
          data: {
            'reason': reason
          }
        });
      },
      flagComment: function(id, reason) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/comments/' + id + '/flag',
          method: 'PUT',
          data: {
            'reason': reason
          }
        });
      },
      getRecommendProducts: function(product_id, location) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/products/' + product_id + '/recommend',
          method: 'GET',
          params: location
        });
      },
      getComments: function(data, product_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/products/' + product_id + '/comment',
          method: 'GET',
          params: data
        });
      },
      postComment: function(content, product_id) {
        return $http
          .post(Config.CORE_BASEURL + '/v5/products/' + product_id + '/comment', content);
      },
      getTagResult: function(formData) {
        return $http({
          method: "GET",
          url: Config.CORE_BASEURL + '/v5/all/search/tags',
          params: formData
        })
      }
    }
  }]);