'use strict';

antsquare
	.factory('ProductContainer', function () {

		function ProductContainer(product) {
			if (product) {
				this.address1 = product.address1;
				this.address2 = product.address2;
				this.approved = product.approved;
			} else {
				this.address1 = null;
				this.address2 = null;
				this.approved = null;
			}
			this.as_business = null,
			this.lat = null,
			this.lon = null,
			this.id = null,
			this.country = null,
			this.currency = null,
			this.flagged = null,
			this.price_in_cents = null,
			this.radius = null,
			this.images = [];
			this.description = null;
			this.name = null;
			this.category_info = null;
			this.store_id = null;
			this.user_id = null;
			this.price = null;
			this.quantity = null;
			this.category = null;
			this.type = 'product';
			this.is_visible = true;
			this.fbShare = false;
		}

		return ProductContainer;
	});

antsquare
	.factory('ProductIntent', ['ProductContainer', function (ProductContainer) {
	var productContainer;

	return {

		start: function (product) {

			if (product) {
				productContainer = new ProductContainer(product);
			} else {
				productContainer = new ProductContainer();
			}
			return productContainer
		},
		newContainer: function (user_id, store_id) {
			var x = new ProductContainer();
			x.user_id = user_id;
			x.store_id = store_id;
			return x;
		},
		hasProduct: function () {
			return (!!productContainer)
		},

		setImages: function (images) {
			productContainer.images = images;
		},

		setType: function (type) {
			productContainer.type = type;
		},

		// setImage: function(image) {
		//   productContainer.image = image;
		// },

		setDescription: function (description) {
			productContainer.description = description;
		},

		setCategory: function (category) {
			productContainer.category = category;
		},

		setName: function (name) {
			productContainer.name = name;
		},

		setUser_id: function (user_id) {
			productContainer.user_id = user_id;
		},

		setStore_id: function (store_id) {
			productContainer.store_id = store_id;
		},

		setPrice: function (price) {
			productContainer.price = price;
		},

		setCurrency: function (currency) {
			productContainer.currency = currency;
		},

		setQuantity: function (quantity) {
			productContainer.quantity = quantity;
		},

		getContainer: function() {
			if (productContainer) {
				delete productContainer.previews
			}
			return productContainer;
		},

		end: function () {
			productContainer = {};
		},

		isValidate: function (product) {
			if (product.images.length <= 0 || (product.description === '' || product.description === null) || (product.name === '' || product.name === null) || (product.price === null || product.price === '') || (product.quantity === null || product.quantity === '') || (product.category === null || product.category === '')) {
				return false
			} else {
				return true
			}
		}

	};

  }]);
