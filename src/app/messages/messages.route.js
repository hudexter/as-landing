antsquare.config(['$stateProvider', '$urlRouterProvider',
   function ($stateProvider, $urlRouterProvider) {


		$stateProvider
			.state('messages', {
				templateUrl: 'app/messages/messages.html',
				controller: 'MessagesCtrl',
				data: 'user'
			})
      .state('messages.current', {
        url: '/inbox/:id',
        views: {
          'col-2': {
            templateUrl: 'app/messages/contactList.html',
            controller: 'ContactListCtrl',
          },
          'col-10': {
            templateUrl: 'app/messages/chatLog.html',
            controller: 'ChatLogCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
          }]
      })
      .state('messages.archived', {
        url: '/archived_inbox/:id',
        views: {
          'col-2': {
            templateUrl: 'app/messages/contactList.html',
            controller: 'ContactListCtrl',
          },
          'col-10': {
            templateUrl: 'app/messages/chatLog.html',
            controller: 'ChatLogCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
          }]
      })


  }
])
