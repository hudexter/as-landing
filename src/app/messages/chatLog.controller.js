'use strict';

antsquare
  .controller('ChatLogCtrl', ['$scope', '$rootScope', '$state', 'MessagesFactory', 'Auth', 'AccountsFactory', 'StoreFactory', '$firebaseObject', 'Config', '$firebaseArray', 'ngAudio', 'GeoLocationService', '$q', 'UploadFatory', 'Upload', function($scope, $rootScope, $state, MessagesFactory, Auth, AccountsFactory, StoreFactory, $firebaseObject, Config, $firebaseArray, ngAudio, GeoLocationService, $q, UploadFatory, Upload) {

    $scope.msgInputForm = {};
    $scope.map = {
      control: {},
      options: {
        scrollwheel: false
      },
      dragging: true,
      events: {
        idle: function() {
          reverseGeocoding(scope.map.center.latitude, scope.map.center.longitude)
            .then(function(data) {
              scope.mapResult = data;
            })
        }
      }
    };
    $scope.glue = true;
    $scope.user = Auth.getUserFn();
    $scope.params = $state.params;
    $scope.room = {};
    $scope.indexes = {
      buyer_last_read: null,
      messages: null,
      seller_last_read: null,
    };

    if (GeoLocationService.hasLocation()) {
      $scope.coords = GeoLocationService.getCoords();
      $scope.map = {
        center: {
          latitude: $scope.coords.latitude,
          longitude: $scope.coords.longitude
        },
        zoom: 15,
        control: {},
        options: {
          scrollwheel: false
        },
        dragging: true,
        events: {
          idle: function() {
            reverseGeocoding($scope.map.center.latitude, $scope.map.center.longitude)
              .then(function(data) {
                $scope.mapResult = data;
              })
          }
        }
      };
    }

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      $scope.map = {
        center: {
          latitude: $scope.coords.latitude,
          longitude: $scope.coords.longitude
        },
        zoom: 15,
        control: {},
        options: {
          scrollwheel: false
        },
        dragging: true,
        events: {
          idle: function() {
            reverseGeocoding($scope.map.center.latitude, $scope.map.center.longitude)
              .then(function(data) {
                $scope.mapResult = data;
              })
          }
        }
      };
    });

    $scope.mapResult = {
      address1: null,
      address2: null,
    };

    $scope.$watch('mapResult', function(data) {
      // $scope.data.address1 = $scope.mapResult.address1;
      // $scope.data.address2 = $scope.mapResult.address2;
      // $scope.data.location = {
      // 	lat: $scope.map.center.latitude,
      // 	lon: $scope.map.center.longitude
      // }
    })

    function reverseGeocoding(lat, lon) {
      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(lat, lon);
      var deferred = $q.defer();
      $scope.map.center.latitude = latlng.lat();
      $scope.map.center.longitude = latlng.lng();

      var map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: $scope.map.center.latitude,
          lng: $scope.map.center.longitude
        }
      });

      var request = {
        location: latlng,
        radius: '200'
      };

      var result = {
        nearBy: null,
        address1: null,
        address2: null,
      };

      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, function(results, status) {
        if (status === "OK") {

          result.nearBy = results;
          geocoder.geocode({
            'latLng': latlng
          }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              results.forEach(function(obj) {
                for (var key in obj) {
                  if (obj.types[0] === 'street_address') {

                    result.address1 = obj.address_components[0].long_name + " " + obj.address_components[1].short_name;
                    deferred.resolve(result)
                  }
                  if (obj[key][0] === 'political' || obj[key][0] === 'locality') {
                    result.address2 = obj.formatted_address;
                    deferred.resolve(result)
                  }
                  if (obj[key][0] === 'country') {
                    result.country = obj.address_components[0].short_name;
                  }
                }
              })
            }
          });

        }
      });
      return deferred.promise;
    }

    $scope.$on('g-places-autocomplete:select', function(e, place) {
      if (!place.geometry) {
        return;
      } else {
        var lat = place.geometry.location.lat();
        var lon = place.geometry.location.lng();
        reverseGeocoding(lat, lon)
          .then(function(data) {
            $scope.mapResult = data;
          })
      }
    });

    $scope.getRoom = function() {
      var url = '/newChat/' + $scope.params.id;
      var ref = new Firebase(Config.FIREBASE_BASEURL + url);
      var ref = $firebaseArray(ref);
      ref.$loaded()
        .then(function(data) {
          for (var i = 0; i < data.length; i++) {
            $scope.indexes[data[i].$id] = i;
          }
        })

      if ($scope.roomInfo.chat_room_id == 'requestSummary') {
        var chatUrl = '/newRequestChat/' + $scope.params.id + '/messages';
        var chatRef = new Firebase(Config.FIREBASE_BASEURL + chatUrl);
        chatRef = $firebaseArray(chatRef);
        chatRef.$loaded()
          .then(function(data) {
            $scope.room.messages = data;
            $scope.room.messages.map(function(msg, index) {
              if (msg.type == 'voice_chat') {
                $scope.room.messages[index].audio = ngAudio.load(msg.voice_path);
              }
            })
            data.$watch(function(data) {
              $scope.room.messages.map(function(msg, index) {
                if (msg.type == 'voice_chat') {
                  $scope.room.messages[index].audio = ngAudio.load(msg.voice_path);
                }
              })
            })
          })
      } else {
        var chatUrl = '/newChat/' + $scope.params.id + '/messages';
        var chatRef = new Firebase(Config.FIREBASE_BASEURL + chatUrl);
        chatRef = $firebaseArray(chatRef);
        chatRef.$loaded()
          .then(function(data) {
            $scope.room.messages = data;
            $scope.room.messages.map(function(msg, index) {
              if (msg.type == 'voice_chat') {
                $scope.room.messages[index].audio = ngAudio.load(msg.voice_path);
              }
            })

            data.$watch(function(data) {
              $scope.room.messages.map(function(msg, index) {
                if (msg.type == 'voice_chat') {
                  $scope.room.messages[index].audio = ngAudio.load(msg.voice_path);
                }
              })
            })

          })
      }


      if (ref.$isDestroyed || chatRef.$isDestroyed) {
        $scope.timeout = true;
        $rootScope.$broadcast('refreshFirebaseToken');
      } else {
        $scope.timeout = false;
      }
      // });
    }

    var ids = $scope.params.id.split('::');
    // $scope.roomInfo = MessagesFactory.getRoom();

    $rootScope.$watch('selectedChatRoom', function(data) {
      if (!_.isEmpty($rootScope.selectedChatRoom)) {
        $scope.roomInfo = $rootScope.selectedChatRoom;
        if (ids[0] == $scope.user.id) {
          $scope.chatHead = {
            name: $rootScope.selectedChatRoom.store.name,
            logo: $rootScope.selectedChatRoom.store.logo,
            user_id: $rootScope.selectedChatRoom.seller.id,
            store_id: $rootScope.selectedChatRoom.store.id
          };
        } else {
          $scope.chatHead = {
            name: $rootScope.selectedChatRoom.customer.name,
            logo: $rootScope.selectedChatRoom.customer.picture,
            user_id: $rootScope.selectedChatRoom.customer.id,
            store_id: $rootScope.selectedChatRoom.customer.id
          };
        }
        $scope.seller = $rootScope.selectedChatRoom.store;
        $scope.customer = $rootScope.selectedChatRoom.customer;
        $scope.getRoom();
      }
    })

    $scope.message = function(msgInput) {
      var time = new Date;
      if ($scope.user.id == ids[0]) {
        this.buyer_read = true;
        this.seller_read = false;
      } else {
        this.buyer_read = false;
        this.seller_read = true;
      }

      this.customer = ids[0];
      this.key = $scope.params.id;
      this.message_status = 2;
      // this.payment_status = "pending";
      this.silent = false;
      this.store = ids[1];
      this.text = msgInput;
      this.timestamp = time.getTime();
      this.type = "message";
      this.user_id = $scope.user.id;
      this.voice_duration = 0;
    }

    $scope.sendMsg = function(msgInput) {
      if (msgInput.replace(/^\s*/g, "")
        .length > 0) {
        var msg = new $scope.message(msgInput.replace(/^\s*/, ""));
        $scope.room.messages.$add(msg)
          .then(function(ref) {
            $scope.msgInputForm.msgInput = '';
          });
      }
    }

    $scope.chatOption = false;
    $scope.updateLocation = function() {
      if ($scope.chatOption === 'location') {
        $scope.chatOption = false;
      } else {
        $scope.chatOption = 'location';
      }
      setTimeout(function() {
        $scope.map.control.refresh($scope.map.center);
      }, 300)
    };

    $scope.updateDate = function() {
      if ($scope.chatOption === 'date') {
        $scope.chatOption = false;
      } else {
        $scope.chatOption = 'date';
      }
      $scope.data.date = $scope.roomInfo.meetup_date.date
    }

    $scope.updatePrice = function() {
      if ($scope.chatOption === 'price') {
        $scope.chatOption = false;
      } else {
        $scope.chatOption = 'price';
      }
    }

    $scope.filterShow = false;
    $scope.toggleFilter = function() {
      if ($scope.filterShow) {
        $scope.chatOption = false;
      }
      $scope.filterShow = !$scope.filterShow
    }

    $scope.submitLocation = function(location) {
      var time = new Date;
      var msg = {
        "type": "meetup_location",
        "silent": false,
        "user_id": $scope.user.id,
        "location": {
          "lat": $scope.map.center.latitude,
          "lon": $scope.map.center.longitude
        },
        "address1": $scope.mapResult.address1,
        "address2": $scope.mapResult.address2,
        "timestamp": time.getTime()
      }
      $scope.room.messages.$add(msg)
        .then(function(ref) {
          $scope.chatOption = false;
          if ($scope.roomInfo.meetup_location) {
            $scope.roomInfo.meetup_location.location = msg.location;
            $scope.roomInfo.meetup_location.address1 = msg.address1;
            $scope.roomInfo.meetup_location.address2 = msg.address2;

          } else {
            $scope.roomInfo.meetup_location = msg;
          }
        });
    }

    $scope.submitPrice = function(price) {
      var time = new Date;
      var msg = {
        "type": "price_message",
        "silent": false,
        "user_id": $scope.user.id,
        "price_in_cents": price * 100,
        "currency": $scope.roomInfo.currency,
        "timestamp": time.getTime()
      }
      $scope.room.messages.$add(msg)
        .then(function(ref) {
          setPrice.reset();
          $scope.chatOption = false;
          $scope.roomInfo.price_in_cents = price * 100;
        });
    }

    $scope.submitTime = function(time) {

      var currentTime = new Date;
      var meetTime = new Date(time)
      var msg = {
        "type": "meetup_date",
        "silent": false,
        "user_id": $scope.user.id,
        "date": meetTime.getTime(),
        // "meetup_date": Math.floor(meetTime.getTime() / 1000),
        "timestamp": currentTime.getTime()
      }

      $scope.room.messages.$add(msg)
        .then(function(ref) {
          setTime.reset();
          $scope.chatOption = false;
          if ($scope.roomInfo.meetup_date) {
            $scope.roomInfo.meetup_date.date = Math.floor(meetTime.getTime() / 1000)
          } else {
            $scope.roomInfo.meetup_date = {
              date: Math.floor(meetTime.getTime() / 1000)
            }
          }
        });
    }


    $scope.submitImage = function(image) {
      var time = new Date;
      var msg = {
        "type": "photo",
        "silent": false,
        "user_id": $scope.user.id,
        "image": image.url,
        'image_height': image.height,
        'image_width': image.width,
        "timestamp": time.getTime()
      }
      $scope.room.messages.$add(msg)
        .then(function(ref) {
          updateimage.reset();
          $scope.image = {};
          $scope.chatOption = false;
        });
    }

    $scope.cancel = function(section) {
      if (section === 'price') {
        setPrice.reset();
      } else if (section === 'date') {
        setTime.reset();
      } else if (section === 'image') {
        updateimage.reset();
        $scope.image = {};
      }
      $scope.chatOption = false;
    };

    $scope.image = {};

    function getMeta(url) {
      var img = new Image();
      img.onload = function() {
        $scope.image.width = this.width;
        $scope.image.height = this.height;
      };
      img.src = url;
    }

    $scope.beforeChange = function(file) {
      if (file) {
        $scope.chatOption = 'image';
        $scope.uploading = true
      }
    };

    $scope.uploadFiles = function(file) {

      if (file) {
        $scope.chatOption = 'image';
        if (file.size < 10000000) {
          UploadFatory.getSignUrl(file.type)
            .then(function(data) {
              $scope.uploadImage(data.data, file)
            })
            .catch(function(error) {
              console.log(error)
            })
        } else {
          $window.alert("The file needs to be under 10MB")
        }
      }
    }
    $scope.uploadImage = function(data, file) {
      $scope.uploading = true;
      var path = UploadFatory.getImagePath(file.type);
      Upload.upload({
          url: Config.S3_URL,
          method: 'POST',
          fields: {
            'key': path,
            'acl': 'public-read',
            'Content-Type': file.type,
            'AWSAccessKeyId': data.AWSAccessKeyId,
            'success_action_status': '201',
            'Policy': data.s3Policy,
            'Signature': data.s3Signature
          },
          sendObjectsAsJsonBlob: false,
          file: file
        })
        .progress(function(evt) {
          if (!$scope.uploading) {
            $scope.uploading = true;
          }
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        })
        .error(function(e) {
          // console.error("Failed:", e);
          $scope.uploading = false;
        })
        .success(function(data, status, headers, config) {
          $scope.uploading = false;
          // $scope.product_imgs[index].url = Config.IMAGE_URL + '/' + path;
          $scope.image.url = Config.IMAGE_URL + '/' + path;
          getMeta(Config.IMAGE_URL + '/' + path);

          $scope.file = null;
        });
    };
    $scope.timeout = null;

    setTimeout(function() {
      if ($scope.timeout === null) {
        $scope.timeout = true;
      }
    }, 500)

    $rootScope.$on('firebaseTokenExpired', function() {
      $scope.timeout = true;
    });

    $rootScope.$on('refreshFirebaseToken', function() {
      $state.reload();
    });

    $scope.refreshPage = function() {
      $state.reload();
    }

    //
    // MessagesFactory.getAllRooms($scope.pageParams)
    // 	.then(function (data) {
    // 		$scope.pageParams.has_more = data.data.has_more;
    // 		$scope.pageParams.pre_page = data.data.pre_page;
    // 		$scope.pageParams.page = data.data.page;
    // 		$scope.rooms = data.data.results;
    // 	})


  }]);