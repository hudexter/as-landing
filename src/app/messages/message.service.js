'use strict';

antsquare
	.service('MessageService', [function () {

		function Marker(marker) {
			for (var key in marker) {
				this[key] = marker[key]
			}

			this.store_id = marker.store_id;
			this.user_id = marker.user_id;
			this.id = marker.id;
			this.idKey = marker.idkey;
			this.idType = getIdType(marker.idkey);
			this.address1 = marker.address1;
			this.address2 = marker.address2;
			this.approved = marker.approved;
			this._tags = marker._tags;
			// this.as_business = marker.as_business;
			this.latitude = marker.lat;
			this.longitude = marker.lon;
			this.country = marker.country;
			this.currency = marker.currency;
			this.products = [];
			// this.flagged = marker.flagged;
			this.radius = marker.radius;
			this.images = marker.images;
			this.description = marker.description;
			this.name = marker.name;
			this.category_info = marker.category_info;
			this.price = marker.price;
			this.price_in_cents = marker.price_in_cents;
			this.distance_in_km = marker.distance_in_km;
			this.distance_in_miles = marker.distance_in_miles;
			// this.quantity = marker.quantity;
			this.category = marker.category;
			this.type = marker.type;
			// this.is_visible = marker.is_visible;
			// this.fbShare = false;
			this.displayImages = [];
			this.displayIndex = 0;
			this.state = null;
			this.coords = {
				latitude : marker.lat,
				longitude : marker.lon,
			}
			this.icon = 'https://antsquare-www.imgix.net/marker/markerDefault.png';
		}

		function getIdType(idkey) {
			if (idkey) {
				if (idkey.match(/STORE/g)){
					return 'STORE'
				} else if (idkey.match(/PRODUCT/g)){
					return "PRODUCT"
				} else {
					return "REQUEST"
				}
			} else {
				return 0
			}

		}

		Marker.prototype.setIcon = function (self) {
			if (self) {
				this.icon = 'https://antsquare-www.imgix.net/marker/userLocation.png';
			} else {
				this.icon = 'https://antsquare-www.imgix.net/marker/transparent.png';
			}
			return this
		};

		Marker.prototype.setState = function (state) {
			var labelContentHtml = "";
			var markerIcon = "";
			var markerBackground = "";
			var frontHtml = '<div class="no-select"><img style="width:30px;height:38px;" src="';
			var midHtml = '"><img class="mapIcon no-select" src="';
			var endHtml = '"></div></div>';
			// https://antsquare-www.imgix.net/marker/markerVisited@2x.png
			// https://antsquare-www.imgix.net/marker/markerSelected.png
			if (this.type === 'request') {
				markerIcon = "https://antsquare-www.imgix.net/marker/request2.png";
				markerBackground = "https://antsquare-www.imgix.net/marker/markerDefaultRequests.png";
				midHtml = '"><img class="mapIcon" src="';
			} else {
				markerBackground = "https://antsquare-www.imgix.net/marker/markerDefault.png";
				midHtml = '"><img class="mapIcon" src="';
				if (this.products.length > 1) {
					// midHtml = '"><img class="mapIconStore" src="';
					// markerIcon = "https://antsquare-www.imgix.net/markers/shopIcon@2x.png";
					markerIcon = 'https://antsquare-www.imgix.net/markers/' + this.products[0].category_info.icon + '.png ';
				} else {

					markerIcon = 'https://antsquare-www.imgix.net/markers/' + this.category_info.icon + '.png ';
					// markerBackground = "https://antsquare-www.imgix.net/marker/marker_2x.png";
					midHtml = '"><img class="mapIcon" src="';
				}
			}

			// Shop
			if (state === 'new') {
				this.state = "new";
			} else if (state === 'active') {
				this.state = "active";
				if (this.type === 'request') {
					markerBackground = "https://antsquare-www.imgix.net/marker/markerSelectedRequests.png";
				} else {
					markerBackground = "https://antsquare-www.imgix.net/marker/markerSelected.png";
				}
			} else if (state === 'visited') {
				this.state = "visited";
				markerBackground = "https://antsquare-www.imgix.net/marker/markerVisited.png";
			}

			labelContentHtml = frontHtml + markerBackground + midHtml + markerIcon + endHtml;
			this.options = {
				labelContent: labelContentHtml,
				labelAnchor: "15 20",
				labelClass: 'mapLabel',
				labelInBackground: true
			}
			return this
		};

		Marker.prototype.hoverMarker = function () {
			this.options.labelClass = 'mapLabelTop'
			var labelContentHtml = "";
			var frontHtml = '<div><img style="width:30px;height:38px;" src="';
			var midHtml = '"><img class="mapIcon" src="';
			var endHtml = '"></div></div>';

			var markerIcon = "";
			if (this.type === 'request') {
				var markerBackground = "https://antsquare-www.imgix.net/marker/markerSelectedRequests.png";
				markerIcon = "https://antsquare-www.imgix.net/marker/request2.png";
				midHtml = '"><img class="mapIcon" src="';
			} else {
				midHtml = '"><img class="mapIcon" src="';
				var markerBackground = "https://antsquare-www.imgix.net/marker/markerSelected.png";
				if (this.products.length > 1) {
					// midHtml = '"><img class="mapIconStore" src="';
					// markerIcon = "https://antsquare-www.imgix.net/markers/shopIcon@2x.png";
					markerIcon = 'https://antsquare-www.imgix.net/markers/' + this.products[0].category_info.icon + '.png ';
				} else {
					markerIcon = 'https://antsquare-www.imgix.net/markers/' + this.category_info.icon + '.png ';
				}
			}
			labelContentHtml = frontHtml + markerBackground + midHtml + markerIcon + endHtml;
			this.options.labelContent = labelContentHtml;
			return this
		};


		// Marker.prototype.setName = function(name) {
		// 	this.name = name;
		// 	return this
		// }

		return Marker;
	}]);
