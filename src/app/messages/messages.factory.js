'use strict';

angular.module('antsquare')
  .factory('MessagesFactory', ['$firebase', '$rootScope', 'Config', 'AccountsFactory', '$cookies', '$firebaseAuth', '$firebaseArray', '$http', '$q', '$firebaseObject',
    function($firebase, $rootScope, Config, AccountsFactory, $cookies, $firebaseAuth, $firebaseArray, $http, $q, $firebaseObject) {

      var roomStorage = {};
      return {
        // getChatBuyerLastRead: function (key) {
        //
        // 	var ref = new Firebase(Config.FIREBASE_BASEURL + '/newChat/' + key + '/buyer_last_read');
        // 	ref = $firebaseArray(ref)
        // 	return ref;
        // },
        getChatMessages: function(key) {
          var deferred = $q.defer();
          var ref = new Firebase(Config.FIREBASE_BASEURL + '/newChat/' + key);
          var ref = $firebaseArray(ref);
          deferred.resolve(ref);
          console.log(ref);
          return deferred.promise;;
        },
        // getChatMessages: function (key) {
        // 	var ref = new Firebase(Config.FIREBASE_BASEURL + '/newChat/' + key + '/messages');
        // 	ref = $firebaseArray(ref)
        // 	console.log('msg')
        // 	console.log(ref)
        // 	return ref;
        // },
        getRequest: function(buyer_id, seller_id) {
          var ref = new Firebase(Config.FIREBASE_BASEURL + '/newRequestChat/' + buyer_id + '::' + seller_id);
          return $firebaseArray(ref);
        },
        getAllRooms: function(data) {
          return $http({
            url: Config.MESSAGING_BASEURL + '/v5/chat/getChatHistory',
            method: 'GET',
            params: data,
          });
        },
        getSummary: function(data) {
          return $http({
            url: Config.MESSAGING_BASEURL + 'v5/chat/getSummary',
            method: 'GET',
            params: data,
          });
        },
        saveRoom: function(room) {
          roomStorage = room;
        },
        getRoom: function() {
          return roomStorage
        }
      };


    }
  ]);