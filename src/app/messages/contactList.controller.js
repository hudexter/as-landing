'use strict';

antsquare
  .controller('ContactListCtrl', ['$scope', '$rootScope', '$state', 'Auth', 'MessagesFactory', '$firebaseArray', '$firebaseObject', 'Config', function($scope, $rootScope, $state, Auth, MessagesFactory, $firebaseArray, $firebaseObject, Config) {

    $scope.user = Auth.getUserFn();
    $scope.store = Auth.getStoreFn();
    $rootScope.selectedChatRoom = {};
    $scope.onTx = false;

    if ($state.current.name.indexOf('settings.transaction') === 0) {
      $scope.onTx = true;
    }
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {
        if (toState.name.indexOf('settings.transaction') === 0) {
          $scope.onTx = true;
        } else {
          $scope.onTx = false
        }
      });

    if ($state.current.name == 'messages.current') {
      $scope.pageParams = {
        uid: $scope.user.id,
        archived: false,
        has_more: false,
        per_page: 999,
        page: 1
      };
      $scope.pageParams.archived = false;
      MessagesFactory.getAllRooms($scope.pageParams)
        .then(function(data) {
          $scope.pageParams.has_more = data.data.has_more;
          $scope.pageParams.pre_page = data.data.pre_page;
          $scope.pageParams.page = data.data.page;
          $scope.rooms = data.data.results;
          $scope.selectedIndex = _.findIndex($scope.rooms, function(data) {
            if ($state.params.id == data.key) {
              return data
            }
          });
          $rootScope.selectedChatRoom = $scope.rooms[$scope.selectedIndex];

          var url = '/singleBadges/' + $scope.user.id;
          var ref = new Firebase(Config.FIREBASE_BASEURL + url);
          var ref = $firebaseArray(ref);
          ref.$loaded()
            .then(function(data) {
              $scope.roomBadge = data;
              $scope.rooms.forEach(function(room, index) {
                if (data.$indexFor(room.key) > -1) {
                  $scope.rooms[index].count = data[data.$indexFor(room.key)].count;
                }
              })
            })

        })
    } else {
      $scope.pageParams = {
        uid: $scope.user.id,
        archived: false,
        has_more: false,
        per_page: 999,
        page: 1
      };
      $scope.pageParams.archived = true;
      MessagesFactory.getAllRooms($scope.pageParams)
        .then(function(data) {
          $scope.pageParams.has_more = data.data.has_more;
          $scope.pageParams.pre_page = data.data.pre_page;
          $scope.pageParams.page = data.data.page;
          $scope.rooms = data.data.results;
          $scope.selectedIndex = _.findIndex($scope.rooms, function(data) {
            if ($state.params.id == data.key) {
              return data
            }
          });
          $rootScope.selectedChatRoom = $scope.rooms[$scope.selectedIndex];

          var url = '/singleBadges/' + $scope.user.id;
          var ref = new Firebase(Config.FIREBASE_BASEURL + url);
          var ref = $firebaseArray(ref);
          ref.$loaded()
            .then(function(data) {
              $scope.roomBadge = data;
              $scope.rooms.forEach(function(room, index) {
                if (data.$indexFor(room.key) > -1) {
                  $scope.rooms[index].count = data[data.$indexFor(room.key)].count;
                }
              })
            })
        })
    }

    $scope.clearRoomBadge = function(roomKey, index) {
      setTimeout(function() {
        $scope.roomBadge[$scope.roomBadge.$indexFor(roomKey)].count = 0;
        $scope.roomBadge.$save($scope.roomBadge.$indexFor(roomKey));
        $scope.rooms[index].count = 0;
      })
    }

  }]);