'use strict';

antsquare
	.controller('MessagesCtrl', ['$scope', 'Auth', '$rootScope', 'AccountsFactory', 'deviceDetector', 'ModalStorage', 'ModalService', 'Upload', 'UploadFatory', 'Config', 'toastr', '$cookies', 'MessagesFactory', function ($scope, Auth, $rootScope, AccountsFactory, deviceDetector, ModalStorage, ModalService, Upload, UploadFatory, Config, toastr, $cookies, MessagesFactory) {

		$scope.isMobile = null;
		if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
			$scope.isMobile = true;
		} else {
			$scope.isMobile = false;
		}

		$scope.user = Auth.getUserFn();

}]);
