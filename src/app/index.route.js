'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('style', {
        url: '/style',
        templateUrl: 'app/staticInfo/test/test.html',
        controller: 'TestCtrl'
      })
      .state('index', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('signup_login', {
        url: '/signup_login?redirect_params',
        templateUrl: 'app/signup_login/signup_login.html',
        controller: 'SignupLoginCtrl',
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('search', {
        url: '/search?&view&lat&lon&bottom_left_lat&bottom_left_lon&top_right_lat&top_right_lon&radius&min_price&max_price&query&tag&category&zoom&page&sort&type&viewer_id',
        // url: '/search?&view&lat&lon&bottom_left_lat&bottom_left_lon&top_right_lat&top_right_lon&radius&min_price&max_price&query&tag&type&category&zoom&page&sort',
        templateUrl: 'app/search2/search2.html',
        controller: 'Search2Ctrl',
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })


    .state('email', {
        url: '/email/verify?id&email&code',
        templateUrl: 'app/email/verify.html',
        controller: 'EmailVerifyCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getUserSeo($stateParams.user_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })
      .state('product', {
        url: '/product/:product_id',
        templateUrl: 'app/product/product2.html',
        controller: 'ProductCtrl',
        resolve: {
          SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
            var result = null;
            seoFactory.getProductSeo($stateParams.product_id)
              .then(function(data) {
                console.log(data.data.description);
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.description);
                seoFactory.setType(data.data.type);
                seoFactory.setPrice(data.data.price);
                seoFactory.setCurrency(data.data.currency);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return result
          }]
        }
      })
      .state('product/:product_id', {
        url: '/product/:product_id',
        templateUrl: 'app/product/product2.html',
        controller: 'ProductCtrl',
        resolve: {
          SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
            var result = null;
            seoFactory.getProductSeo($stateParams.product_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setPrice(data.data.price);
                seoFactory.setCurrency(data.data.currency);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return result
          }]
        }
      })
      .state('moment', {
        url: '/moment/:moment_id',
        templateUrl: 'app/moment/moment2.html',
        controller: 'MomentCtrl',
        resolve: {
          SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
            var result = null;
            seoFactory.getMomentSeo($stateParams.moment_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setPrice(data.data.price);
                seoFactory.setCurrency(data.data.currency);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return result
          }]
        }
      })
      .state('moment/:moment_id', {
        url: '/moment/:moment_id',
        templateUrl: 'app/moment/moment2.html',
        controller: 'MomentCtrl',
        resolve: {
          SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
            var result = null;
            seoFactory.getMomentSeo($stateParams.moment_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setPrice(data.data.price);
                seoFactory.setCurrency(data.data.currency);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return result
          }]
        }
      })
      .state('event/:event_id', {
        url: '/event/:event_id',
        templateUrl: 'app/event/event.html',
        controller: 'EventCtrl',
        // resolve: {
        //   SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
        //     var result = null;
        //     seoFactory.getMomentSeo($stateParams.moment_id)
        //       .then(function(data) {
        //         seoFactory.setDescription(data.data.description);
        //         seoFactory.setTitle(data.data.title);
        //         seoFactory.setType(data.data.type);
        //         seoFactory.setPrice(data.data.price);
        //         seoFactory.setCurrency(data.data.currency);
        //         seoFactory.setImage1(data.data.image);
        //         seoFactory.setUrl($location.absUrl());
        //       })
        //     return result
        //   }]
        // }
      })
      .state('request', {
        url: '/request/:request_id',
        templateUrl: 'app/request/request.html',
        controller: 'RequestCtrl',
        resolve: {
          SEO: ['seoFactory', '$stateParams', '$location', '$q', function(seoFactory, $stateParams, $location, $q) {
            var result = null;
            seoFactory.getRequestSeo($stateParams.request_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setPrice(data.data.price);
                seoFactory.setCurrency(data.data.currency);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return result
          }]
        }
      })
      .state('store', {
        url: '/store/:store_id',
        templateUrl: 'app/store/store.html',
        controller: 'StoreCtrl',

        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getStoreSeo($stateParams.store_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]

        }
      })
      .state('store/:store_id', {
        url: '/store/:store_id',
        templateUrl: 'app/store/store.html',
        controller: 'StoreCtrl',

        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getStoreSeo($stateParams.store_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]

        }
      })
      .state('storeHandle', {
        url: '/$:store_id',
        templateUrl: 'app/store/store.html',
        controller: 'StoreHandleCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getStoreHandleSeo($stateParams.store_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })
      .state('tags', {
        url: '/hashtag/:tagname',
        templateUrl: 'app/store/tag.html',
        controller: 'TagCtrl'
      })
      .state('storeHandle2', {
        url: ':store_id',
        templateUrl: 'app/store/store.html',
        controller: 'StoreHandleCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getStoreHandleSeo($stateParams.store_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })
      .state('notification', {
        url: '/notification',
        templateUrl: 'app/feeds/feeds.html',
        controller: 'FeedsCtrl',
        data: 'user'
      })
      .state('user', {
        url: '/user/:user_id',
        templateUrl: 'app/store/userfeed.html',
        controller: 'UserCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getUserSeo($stateParams.user_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })
      .state('user/:user_id', {
        url: '/user/:user_id',
        templateUrl: 'app/store/userfeed.html',
        controller: 'UserCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getUserSeo($stateParams.user_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })

    .state('username', {
        url: '/:user_id',
        templateUrl: 'app/store/user.html',
        controller: 'UserCtrl',
        resolve: {
          Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
            seoFactory.getUserSeo($stateParams.user_id)
              .then(function(data) {
                seoFactory.setDescription(data.data.description);
                seoFactory.setTitle(data.data.title);
                seoFactory.setType(data.data.type);
                seoFactory.setImage1(data.data.image);
                seoFactory.setUrl($location.absUrl());
              })
            return null
          }]
        }
      })
      .state('setting', {
        url: '/setting',
        templateUrl: 'app/setting/setting.html',
        controller: 'SettingCtrl',
        data: 'user'
      })
      .state('demo', {
        url: '/demo',
        templateUrl: 'app/staticInfo/test/test.html',
        controller: 'TestCtrl'
      })
      .state('homefeed', {
        url: '/homefeed/social',
        templateUrl: 'app/homefeed/homefeed.html',
        controller: 'HomefeedCtrl',
        data: 'user'
      });

    // $urlRouterProvider.otherwise('/');

    $urlRouterProvider.otherwise(function($injector, $location, seoFactory) {
      var state = $injector.get('$state');
      var url = $location.url();
      if (url === '') {
        state.go('index.home');
      } else if (url.match(/^\/m\/.*/)) {
        window.location.href = "https://dashboard.antsquare.com" + url;
      } else if (url.match(/^\/email\/.*/)) {
        window.location.href = "https://dashboard.antsquare.com" + url;
      } else {
        state.go('index.home');
      }
    });

  }
])