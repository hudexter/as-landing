'use strict';
antsquare
	.factory('intercomFactory', ['uuid4', 'Config',
	function (uuid4, Config) {

			var relaunch = true;
			var userSet = false;
			var detached = false;
			return {
				start: function () {
					window.Intercom('boot', {
						app_id: Config.INTERCOM_APPID
					});
				},
				anonUser: function () {
					window.Intercom('update', {
						app_id: Config.INTERCOM_APPID
					});
				},
				updateUser: function (user) {
					if (user) {
						window.Intercom('update', {
							app_id: Config.INTERCOM_APPID,
							email: user.email,
							name: user.name,
							user_id: user.id,
							widget: {
								activator: '#IntercomDefaultWidget'
							}
						});
					} else {
						window.Intercom('update', {
							app_id: Config.INTERCOM_APPID
						});
					}
				},
				logout: function () {
					Intercom('shutdown');

					setTimeout(function () {
						window.Intercom('boot', {
							app_id: Config.INTERCOM_APPID
						});
					}, 500);
					relaunch = true;
					userSet = false;
				},
				detached: function () {
					if (!detached) {
						window.Intercom('shutdown');
						detached = true;
					}
				},
				reattach: function () {
					if (detached) {
						// window.Intercom('reattach_activator');
						window.Intercom('boot', {
							app_id: Config.INTERCOM_APPID
						});
						detached = false;
					}
				},
				rebootWithUser: function (user) {
					if (!userSet) {
						if (relaunch) {
							window.Intercom('shutdown');

							setTimeout(function () {
								window.Intercom('boot', {
									app_id: Config.INTERCOM_APPID,
									email: user.email,
									name: user.name,
									user_id: user.id,
									widget: {
										activator: '#IntercomDefaultWidget'
									}
								});
							}, 500);

							relaunch = false;
						} else {
							window.Intercom('boot', {
								app_id: Config.INTERCOM_APPID,
								email: user.email,
								name: user.name,
								user_id: user.id,
								widget: {
									activator: '#IntercomDefaultWidget'
								}
							});
						}
					}
				}
			};
  }]);
