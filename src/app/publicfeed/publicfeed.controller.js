'use strict';

antsquare
  .controller('PublicfeedCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'Reddit', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', 'MomentsFactory', 'ProductsFactory', 'moment', function($scope, $state, $stateParams, StoreFactory, $window, Reddit, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory, MomentsFactory, ProductsFactory, moment) {

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
      start();
    } else {
      $scope.userInfo = null;


    }


    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        $scope.StoreFollowProduct = false;
        start();
      } else {
        $scope.userInfo = null;
        $scope.StoreFollowProduct = false;
      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        getProducts($scope.coords);
        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }


    var scrollLoad = false;

    function addLoadMoreCard(scrollLoad) {
      if ($scope.feedcards.has_more && !scrollLoad) {
        if (scrollLoad === undefined) {
          $scope.homefeed.push({
            type: 'has_more_temp'
          })
        } else {
          $scope.homefeed.push({
            type: 'has_more'
          })
        }

      } else if (!$scope.feedcards) {
        $scope.homefeed.push({
          type: 'no_more'
        })
      }
    };




    $scope.loadingMoreProducts = false;

    $scope.loading = null;



    $scope.homefeed = [];

    function getHomefeed() {


      StoreFactory.getSocialPublicFeed($scope.productsPageParams)
        .then(function(data) {
          $scope.feedcards = data.data;
          $scope.homefeed = data.data.cards;



          /*for (var i = 0; i < $scope.homefeed.length; i++) {
            if ($scope.homefeed[i].target_json.user_info.id == $scope.userInfo.id) {
              $scope.HomefeedFollow = false;
            }
            $scope.HomefeedFollow = true;
          }*/

          $scope.$broadcast('productLoaded');


        })
        .catch(function(err) {
          $scope.homefeed = err.message;
        })

    };

    $scope.loadMore = function() {


      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;





    };


    function getHomefeed2() {

      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
      StoreFactory.getSocialPublicFeed($scope.productsPageParams)
        .then(function(data) {
          $scope.feedcards = data.data;
          $scope.newCards = data.data.cards;
          $scope.homefeed = $scope.homefeed.concat($scope.newCards);

        })
        .catch(function(err) {
          $scope.homefeed = err.message;
        })

    };

    function refreshPlate() {
      if (angularGridInstance.home) {
        angularGridInstance.home.refresh();
      }
    };

    $scope.showFollowers = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollower/modalFollower.html",
          controller: "ModalFollowerCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };

    function checkImgContainerSize() {
      // if (window.innerWidth < 768) {

      var imgContainer = angular.element(document.querySelector('#rn-carousel'));
      var x = imgContainer.width();
      imgContainer.height(x);
      // }
    };

    $scope.showFollowings = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
          controller: "ModalFollowingCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.likeProductToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
      if ($scope.homefeed[index].target_json.is_liked) {

        AccountsFactory.likeProduct($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
          })
      } else {
        AccountsFactory.unlikeProduct($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
          })
      }
    };

    $scope.flagMoment = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagMoment')
      $scope.flagged = true;
      MomentsFactory.flagMoment($scope.homefeed[index].target_json.id)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    $scope.flagProduct = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagProduct')
      $scope.flagged = true;
      ProductsFactory.flagProduct($scope.homefeed[index].target_json.id)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    function start() {
      $scope.coords = undefined;
      $scope.homefeed = undefined;
      $scope.coords = GeoLocationService.getCoords();
      $scope.reddit = new Reddit($scope.coords);
      $scope.homefeed = $scope.reddit.homefeed;
      checkImgContainerSize();
    }
    $scope.likeMomentToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
      if ($scope.homefeed[index].target_json.is_liked) {

        AccountsFactory.likeMoment($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeMoment($scope.homefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.homefeed[index].target_json.is_liked = !$scope.homefeed[index].target_json.is_liked;
          })
      }
    };
    $scope.getShareLinks = function(moment) {
      // $scope.root = $location.host();
      $scope.fbLinkSF = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.twLinkSF = "http://twitter.com/intent/tweet?status=" + moment.description + "+%40antsquare+" + "https://www.antsquare.com/moment/" + moment.id;
      "http://twitter.com/intent/tweet?status=" + moment.description + "@antsquare" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.pinLinkSF = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/moment/" + moment.id + "&amp;media=" + moment.images[0] + "&amp;description=Check it out!"

      $scope.mailLinkSF = "mailto:?Subject=Check out " + moment.description + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/moment/" + moment.id;
    };



    $scope.getShareLinksProducts = function(product) {
      // $scope.root = $location.host();
      $scope.fbLinkSFP = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/product/" + product.id;

      $scope.twLinkSFP = "http://twitter.com/intent/tweet?status=" + product.description + "+%40antsquare+" + "https://www.antsquare.com/product/" + product.id;
      "http://twitter.com/intent/tweet?status=" + product.description + "@antsquare" + "https://www.antsquare.com/product/" + product.id;

      $scope.pinLinkSFP = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/product/" + product.id + "&amp;media=" + product.images[0] + "&amp;description=Check it out!"

      $scope.mailLinkSFP = "mailto:?Subject=Check out " + product.description + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/product/" + product.id;
    };

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.store.user_info.is_following = !$scope.store.user_info.is_following;

      if (!$scope.store.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      }
    };


    $scope.deletePrompt = function(id) {

      ModalStorage.set(id, ProductsFactory.deleteProduct);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalSure/modalSure.html",
          controller: "ModalSureCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            if (result) {
              element.parent()
                .remove();
              angularGridInstance[scope.cardPlateId].refresh();
              scope.$emit('deleteProduct');
            }
          });
        });

    };
    $scope.momentDeletePrompt = function(id) {
      ModalStorage.set(id, MomentsFactory.deleteMoment);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalSure/modalSure.html",
          controller: "ModalSureCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            if (result) {
              element.parent()
                .remove();
              scope.$emit('deleteProduct');
            }
          });
        });
    };
    $scope.followMomentToggle = function(index) {

      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;

      if (!$scope.homefeed[index].target_json.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.homefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.homefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.homefeed[index].target_json.user_info.is_following = !$scope.homefeed[index].target_json.user_info.is_following;
          })
      }
    };

    $scope.productsPageParams = {
      page: 1,
      per_page: 10
    };

    $scope.submitComment = function($index) {

      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'comment')
      $rootScope.$broadcast("postComment", $scope.homefeed[$index]);
      $scope.homefeed[$index].commentContent = null;
    }

    //getHomefeed();
    start();
    window.onresize = checkImgContainerSize;
    $scope.ready = false;

    $scope.width = $window.innerWidth;
    $scope.card = 800;
    $scope.cardHeight = 400;
    $scope.left = 400;

    if ($scope.card - 5 > $scope.left) {
      $scope.productDescription = {
        'max-width': $scope.card - $scope.left - 19 + 'px',
        'word-break': 'break-all'
      }
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 95 + 'px',
        'min-height': $scope.cardHeight - 95 + 'px',
        'max-width': $scope.card - $scope.left - 0 + 'px',
        'overflow-y': 'hidden',
        'word-break': 'break-all'
      }
    } else {
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 95 + 'px',
        'min-height': $scope.cardHeight - 95 + 'px',
        'max-width': $scope.left + 'px',
        'overflow-y': 'scroll',
        'word-break': 'break-all'
      }
      $scope.productDescription = {
        'max-width': $scope.left + 'px',
        'word-break': 'break-all'
      }
    }

    $scope.ready = true;

    $scope.go = function(path) {
      console.log(path);
      window.open(path, '_blank');
      // $location.path(path);
    };

    angular.element($window).bind('resize', function() {
      $scope.ready = false;
      $scope.card = document.getElementById('card').offsetWidth;
      $scope.left = document.getElementById('leftPanel').offsetWidth;
      if ($scope.card - 5 > $scope.left) {
        $scope.productDescription = {
          'max-width': $scope.card - $scope.left - 19 + 'px'
        }
      } else {
        $scope.productDescription = {
          'max-width': $scope.left + 'px'
        }
      }

      $scope.ready = true;
      $scope.$digest();
    });


  }]);
antsquare.factory('Reddit', function($http, Config) {

  var Reddit = function(coords) {
    this.homefeed = [];
    this.busy = false;
    this.page = 1;
    this.coords = coords;
  };

  Reddit.prototype.nextPage = function() {
    if (this.busy) return;
    this.busy = true;

    var data = $.param({
      rollno: $("#hdnrollno").data('value'),
      page: this.page,
      per_page: 10
    });

    $http({
        url: Config.API_BASEURL + '/users/social?page=' + this.page + '&per_page=10&lat=' + this.coords.latitude + '&lon=' + this.coords.longitude,
        method: 'GET',
        page: 1,
        per_page: 10
      })
      .success(function(data) {
        var homefeed = data.cards;
        console.log(homefeed);
        if (homefeed.length <= 0) {
          //NProgress.done();
          return;
        }

        for (var i = 0; i < homefeed.length; i++) {

          this.homefeed.push(homefeed[i]);
        }

        this.page = this.page + 1;
        this.busy = false;
        //NProgress.done();
      }.bind(this));
  };




  return Reddit;
});