'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('listing', {
        url: '/listing',
        templateUrl: 'app/listings/listings.html',
        controller: 'ListingsCtrl',
        data: 'user'
      })
      .state('listing.products_services', {
        url: '/products_services',
        templateUrl: 'app/listings/listing.html',
        controller: 'ListingsCtrl',
        data: 'user'
      })
      .state('listing.request', {
        url: '/request',
        templateUrl: 'app/listings/request.html',
        controller: 'ListingRequestCtrl',
        data: 'user'
      })
      .state('listing.requestEdit', {
        url: '/request/:id/edit',
        templateUrl: 'app/listings/requestEdit.html',
        controller: 'ListingRequestEditCtrl',
        data: 'user'
      })
      .state('listing.promotion', {
        url: '/promotion',
        templateUrl: 'app/listings/promotion.html',
        controller: 'ListingPromotionCtrl',
        data: 'user'
      })
      .state('listing.promotionEdit', {
        url: '/promotion/:id/edit',
        templateUrl: 'app/listings/promotion.html',
        controller: 'ListingPromotionEditCtrl',
        data: 'user'
      })
      // Edit
      .state('listing.product_service', {
        url: '/product_service/:id/edit',
        templateUrl: 'app/listings/edit.html',
        controller: 'ListingCtrl',
        data: 'user'
      })
      .state('listing.moment', {
        url: '/moment',
        templateUrl: 'app/listings/moment.html',
        controller: 'ListingMomentCtrl',
        data: 'user'
      })
      .state('listing.momentEdit', {
        url: '/moment/:id/edit',
        templateUrl: 'app/listings/momentEdit.html',
        controller: 'ListingMomentEditCtrl',
        data: 'user'
      })
  }
])