'use strict';

antsquare
  .controller('ListingsCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', 'ezfb', 'StoreFactory', function($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory, ezfb, StoreFactory) {

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {
        $scope.currentState = toState.name;
      });
    $scope.userInfo = null;

    $scope.userInfo = Auth.getUserFn();



    $scope.listings = [];
    $scope.currentState = $state.current.name;

    $scope.newListing = function() {
      var listing = ProductIntent.newContainer($scope.user.id, $scope.store.id);
      $scope.listings.push(listing);
    }

    $scope.addListing = function() {
      $scope.newListing();
    }

    $scope.removeListing = function(index) {
      $scope.listings.splice(index, 1);
    }
    $scope.loading = false;

    $scope.root = $location.host();
    $scope.token = Auth.getToken();
    $scope.fbToken = {
      access_token: $scope.token.fb_token
    };
    AccountsFactory.getSocial()
      .then(function(response) {
        $scope.socialInfo = response.data;
      });

    function fbTokenRefresh() {
      var deferred = $q.defer();
      AccountsFactory.fbTokebRefresh()
        .then(function(data) {
          $cookies.put('fb_token', response.authResponse.accessToken, {
            domain: Config.COOKIE_DOMAIN
          });
          $scope.token = Auth.getToken();
          deferred.resolve(data)
        })
        .catch(function(error) {
          deferred.reject(error)
        });
      return deferred.promise
    };

    var repost = 0;

    function fbPostingAction(obj) {
      if ($scope.user.fb_share_to === $scope.socialInfo.facebook.id) {
        ezfb.api(
          "/me/feed",
          "POST", obj,
          function(response) {
            if (response && !response.error) {
              console.log(response)
            } else {
              if (repost < 3) {
                fbTokenRefresh()
                  .then(function(response) {
                    fbPostingAction(obj);
                    repost = repost + 1;
                  })
              } else {
                console.log(response)
                repost = 0;
              }

            }
          }
        );
      } else {
        ezfb.api(
          "/" + $scope.user.fb_share_to + "/feed",
          "POST", obj,
          function(response) {
            if (response && !response.error) {

            } else {

              if (repost < 3) {
                fbTokenRefresh()
                  .then(function(response) {
                    fbPostingAction(obj);
                    repost = repost + 1;
                  })
              } else {
                console.log(response)
                repost = 0;
              }
            }
          }
        );
      }
    };

    function fbPost(data) {
      var obj = {
        "link": 'https://' + $scope.root + "/product/" + data.id,
        "picture": data.images[0],
        "name": data.name,
        "description": data.description
      };

      if ($scope.user.fb_share_to === $scope.socialInfo.facebook.id) {
        fbPostingAction(obj);
      } else {
        ezfb.api('/' + $scope.socialInfo.facebook.id + '/accounts', $scope.fbToken, function(pageList) {
          if (pageList && !pageList.error) {
            pageList.data.forEach(function(value, index, array) {
              if (value.id === $scope.user.fb_share_to) {
                obj.access_token = value.access_token;
                fbPostingAction(obj);
              }
            });
          } else {
            fbTokenRefresh()
              .then(function() {
                fbPost(data);
              });
          }
        });
      };
    };

    $scope.submitListings = function() {
      $scope.loading = true;
      $scope.listings.forEach(function(listing, index) {
        if (typeof listing.tags != 'string') {
          $scope.listings[index].tags = listing.tags.join(' ')
        }
        if (listing.price_type === 'noprice') {
          $scope.listings[index].price = 0;
        }
      })

      ProductsFactory.bulkPost({
          items: $scope.listings
        })
        .then(function(data) {
          $scope.loading = false;
          angular.forEach($scope.listings, function(value, index) {
            if (value.fbShare) {
              fbPost(value);
            }
          });
          $scope.userid = $scope.userInfo.id;
          $state.go('user/:user_id', { user_id: $scope.userid });
        })
        .catch(function(error) {
          console.log(error)
          $scope.loading = false;
          $scope.error = error.data.message;
        });

    }

    if (Auth.getStoreFn() && Auth.getUserFn()) {
      $scope.store = Auth.getStoreFn().stores[0];
      $scope.user = Auth.getUserFn();;
      $scope.newListing();
    } else {
      AccountsFactory.me()
        .then(function(data) {
          Auth.setUser(data.data);
          $scope.user = data.data;
          StoreFactory.me()
            .then(function(data) {
              Auth.setStore(data.data);
              $scope.store = data.data.stores[0];
              $scope.newListing();
            })
        })

    }

  }]);