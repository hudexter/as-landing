'use strict';

antsquare
	.controller('ListingPromotionEditCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', 'StoreFactory', function ($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory, StoreFactory) {

    $rootScope.$on('$stateChangeStart',
			function (event, toState, toStateParams, fromState, fromStateParams) {
				$scope.currentState = toState.name;
			});

		$scope.store = Auth.getStoreFn().stores[0];
		$scope.user = Auth.getUserFn();

		$scope.listings = [];
		$scope.currentState = $state.current.name;

		var data = {
			id : $state.params.id,
			store_id: $scope.store.id
		}
		StoreFactory.getAnnouncementById(data)
			.then(function (data) {
				$scope.listings.push(data.data)
			})
			.catch(function (error) {
				console.log(error)
			})


		$scope.loading = false;

		$scope.root = $location.host();
		$scope.token = Auth.getToken();
		$scope.fbToken = {
			access_token: $scope.token.fb_token
		};

		$scope.submitPromotion = function () {
			$scope.loading = true;
			// if (typeof $scope.listings[0].tags != 'string') {
			// 	$scope.listings[0].tags = $scope.listings[0].tags.join(' ')
			// }
			// $scope.listings[0].id = $scope.user.id;
			// $scope.listings[0].user_id = $scope.user.id;

			StoreFactory.putAnnouncement(
					$scope.listings[0]
				)
				.then(function (data) {
					$scope.loading = false;
					$state.go('settings.store.promotions');
				})
				.catch(function (error) {
					console.log(error)
					$scope.loading = false;
					$scope.error = error.data.message;
				});

		}



	}]);
