'use strict';

antsquare
  .factory('ProductContainer', function () {

    function ProductContainer() {
      this.images = [];
      this.image = null;
      this.description = null;
      this.name = null;
      this.category_info = null;
      this.store_id = null;
      this.user_id = null;
      this.price = null;
      this.quantity = null;
      this.category = null;
      this.type = 'product';
      this.is_visible = true;
      this.fbShare = false;
    }

    return ProductContainer;
  });

antsquare
  .factory('ProductIntent', ['ProductContainer', function (ProductContainer) {
    var productContainer;

    return {

      start: function (product) {
        productContainer = new ProductContainer();

        if (product) {
          productContainer = product
        }
      },
      newContainer: function (user_id, store_id) {
        var x = new ProductContainer();
        x.user_id = user_id;
        x.store_id = store_id;
        return x;
      },
      hasProduct: function () {
        return (!!productContainer)
      },

      setImages: function (images) {
        productContainer.images = images;
      },

      setType: function (type) {
        productContainer.type = type;
      },

      // setImage: function(image) {
      //   productContainer.image = image;
      // },

      setDescription: function (description) {
        productContainer.description = description;
      },

      setCategory: function (category) {
        productContainer.category = category;
      },

      setName: function (name) {
        productContainer.name = name;
      },

      setUser_id: function (user_id) {
        productContainer.user_id = user_id;
      },

      setStore_id: function (store_id) {
        productContainer.store_id = store_id;
      },

      setPrice: function (price) {
        productContainer.price = price;
      },

      setCurrency: function (currency) {
        productContainer.currency = currency;
      },

      setQuantity: function (quantity) {
        productContainer.quantity = quantity;
      },

      getContainer: function () {
        if (productContainer) {
          delete productContainer.previews
        }
        return productContainer;
      },

      end: function () {
        productContainer = {};
      },

      isValidate: function (product) {
        if (product.images.length <= 0 || (product.description === '' || product.description === null) || (product.name === '' || product.name === null) || (product.price === null || product.price === '') || (product.category === null || product.category === '')) {
          return false
        } else {
          return true
        }
      }

    };

  }]);

antsquare
  .factory('MomentContainer', function () {

    function MomentContainer() {
      this.images = [];
      this.image = null;
      this.description = null;
      this.name = null;
      this.category_info = null;
      this.store_id = null;
      this.user_id = null;
      this.price = null;
      this.quantity = null;
      this.category = null;
      this.type = 'moment';
      this.is_visible = true;
      this.fbShare = false;
    }

    return MomentContainer;
  });

antsquare
  .factory('MomentIntent', ['MomentContainer', function (MomentContainer) {
    var momentContainer;

    return {

      start: function (moment) {
        momentContainer = new MomentContainer();

        if (moment) {
          momentContainer = moment
        }
      },
      newContainer: function (user_id, store_id) {
        var x = new MomentContainer();
        x.user_id = user_id;
        x.store_id = store_id;
        return x;
      },
      hasMoment: function () {
        return (!!momentContainer)
      },

      setImages: function (images) {
        momentContainer.images = images;
      },

      setType: function (type) {
        momentContainer.type = type;
      },

      // setImage: function(image) {
      //   productContainer.image = image;
      // },

      setDescription: function (description) {
        momentContainer.description = description;
      },

      setCategory: function (category) {
        momentContainer.category = category;
      },

      setName: function (name) {
        momentContainer.name = name;
      },

      setUser_id: function (user_id) {
        momentContainer.user_id = user_id;
      },

      setStore_id: function (store_id) {
        momentContainer.store_id = store_id;
      },

      setPrice: function (price) {
        momentContainer.price = price;
      },

      setCurrency: function (currency) {
        momentContainer.currency = currency;
      },

      setQuantity: function (quantity) {
        momentContainer.quantity = quantity;
      },

      getContainer: function () {
        if (momentContainer) {
          delete momentContainer.previews
        }
        return momentContainer;
      },

      end: function () {
        momentContainer = {};
      },

      isValidate: function (moment) {
        if (moment.images.length <= 0 || (moment.description === '' || moment.description === null) || (moment.name === '' || moment.name === null) || (moment.price === null || moment.price === '') || (moment.category === null || moment.category === '')) {
          return false
        } else {
          return true
        }
      }

    };

  }]);