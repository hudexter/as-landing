'use strict';

antsquare
  .controller('ListingRequestCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', function ($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory) {

    $rootScope.$on('$stateChangeStart',
      function (event, toState, toStateParams, fromState, fromStateParams) {
        $scope.currentState = toState.name;
      });

    $scope.store = Auth.getStoreFn().stores[0];
    $scope.user = Auth.getUserFn();

    $scope.listings = [];
    $scope.currentState = $state.current.name;

    // ProductsFactory.findProductById($state.params.id)
    // 	.then(function (data) {
    // 		$scope.listings.push(data.data)
    // 	})
    // 	.catch(function (error) {
    // 		console.log(error)
    // 	})
    //

    $scope.loading = false;

    $scope.root = $location.host();
    $scope.token = Auth.getToken();
    $scope.fbToken = {
      access_token: $scope.token.fb_token
    };

    $scope.submitListings = function () {
      $scope.loading = true;
      $scope.listings.forEach(function (listing, index) {
        $scope.listings[index].type = 'request'
        $scope.listings[index].expires_at = new Date($scope.listings[index].expires_at)
        if (typeof listing.tags != 'string') {
          $scope.listings[index].tags = listing.tags.join(' ')
        }
      })
      ProductsFactory.bulkPost({
          items: $scope.listings
        })
        .then(function (data) {
          $scope.loading = false;
          $state.go('settings.store.requests');
        })
        .catch(function (error) {
          console.log(error)
          $scope.loading = false;
          $scope.error = error.data.message;
        });
    }

    $scope.editListings = function () {
      $scope.loading = true;
      if (typeof $scope.listings[0].tags != 'string') {
        $scope.listings[0].tags = $scope.listings[0].tags.join(' ')
      }

      ProductsFactory.editProduct($scope.listings[0].id, $scope.listings[0])
        .then(function (data) {
          $scope.loading = false;
          $state.go('settings.store.products_services');
        })
        .catch(function (error) {
          console.log(error)
          $scope.loading = false;
          $scope.error = error.data.message;
        });
    }

    $scope.newListing = function () {
      var listing = ProductIntent.newContainer($scope.user.id, $scope.store.id);
      $scope.listings.push(listing);
    }

    $scope.newListing();

  }]);