'use strict';

antsquare
  .controller('ListingPromotionCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', 'StoreFactory', function($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory, StoreFactory) {

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {
        $scope.currentState = toState.name;
      });

    $scope.store = Auth.getStoreFn()
      .stores[0];
    $scope.user = Auth.getUserFn();

    $scope.listings = [];
    $scope.currentState = $state.current.name;

    // ProductsFactory.findProductById($state.params.id)
    //  .then(function (data) {
    //    $scope.listings.push(data.data)
    //  })
    //  .catch(function (error) {
    //    console.log(error)
    //  })
    //

    $scope.loading = false;

    $scope.root = $location.host();
    $scope.token = Auth.getToken();
    $scope.fbToken = {
      access_token: $scope.token.fb_token
    };

    $scope.submitPromotion = function() {
      $scope.loading = true;

      StoreFactory.postAnnouncement(
          $scope.listings[0]
        )
        .then(function(data) {
          $scope.loading = false;
          $state.go('settings.store.promotions');
        })
        .catch(function(error) {
          console.log(error)
          $scope.loading = false;
          $scope.error = error.data.message;
        });
    }

    $scope.newListing = function() {
      var listing = ProductIntent.newContainer($scope.user.id, $scope.store.id);
      $scope.listings.push(listing);
    }

    $scope.newListing();

  }]);