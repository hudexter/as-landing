'use strict';

antsquare
	.controller('ListingRequestEditCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', 'RequestFactory', function ($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory, RequestFactory) {

		$rootScope.$on('$stateChangeStart',
			function (event, toState, toStateParams, fromState, fromStateParams) {
				$scope.currentState = toState.name;
			});

		$scope.store = Auth.getStoreFn()
			.stores[0];
		$scope.user = Auth.getUserFn();

		$scope.listings = [];
		$scope.currentState = $state.current.name;

		RequestFactory.getRequest({
				id: $state.params.id
			})
			.then(function (data) {
				$scope.listings.push(data.data)
			})
			.catch(function (error) {
				console.log(error)
			})

		$scope.loading = false;

		$scope.root = $location.host();
		$scope.token = Auth.getToken();
		$scope.fbToken = {
			access_token: $scope.token.fb_token
		};

		$scope.editListings = function () {
			$scope.loading = true;

			if (typeof $scope.listings[0].tags != 'string') {
				$scope.listings[0].tags = $scope.listings[0].tags.join(' ')
			}

			RequestFactory.putRequest($scope.listings[0])
				.then(function (data) {
					$scope.loading = false;
					$state.go('settings.store.requests');
				})
				.catch(function (error) {
					console.log(error)
					$scope.loading = false;
					$scope.error = error.data.message;
				});
		}
	}]);
