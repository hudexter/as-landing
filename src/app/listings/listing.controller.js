'use strict';

antsquare
  .controller('ListingCtrl', ['$scope', '$rootScope', 'Auth', '$state', 'ProductIntent', 'ProductsFactory', '$location', 'AccountsFactory', function($scope, $rootScope, Auth, $state, ProductIntent, ProductsFactory, $location, AccountsFactory) {

    // $scope.bulkListing = bulkListing;
    // $scope.$watch('bulkListing', function(){
    //  console.log($scope.bulkListing)
    // },true)
    //

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {
        $scope.currentState = toState.name;
      });

    $scope.store = Auth.getStoreFn().stores[0];
    $scope.user = Auth.getUserFn();

    $scope.userInfo = null;

    $scope.userInfo = Auth.getUserFn();

    $scope.listings = [];
    $scope.currentState = $state.current.name;

    ProductsFactory.findProductById($state.params.id)
      .then(function(data) {
        $scope.listings.push(data.data)
      })
      .catch(function(error) {
        console.log(error)
      })

    $scope.loading = false;

    $scope.root = $location.host();
    $scope.token = Auth.getToken();
    $scope.fbToken = {
      access_token: $scope.token.fb_token
    };

    $scope.submitListings = function() {

      $scope.loading = true;
      $scope.listings.forEach(function(listing, index) {
        if (typeof listing.tags != 'string') {
          $scope.listings[index].tags = listing.tags.join(' ')
        }
      })
      ProductsFactory.bulkPost({
          items: $scope.listings
        })
        .then(function(data) {
          $scope.loading = false;
          $scope.userid = $scope.userInfo.id;
          $state.go('user/:user_id', { user_id: $scope.userid });
        })
        .catch(function(error) {
          console.log(error);
          $scope.loading = false;
          $scope.error = error.data.message;
        });
    }

    $scope.editListings = function() {
      $scope.loading = true;
      if (typeof $scope.listings[0].tags != 'string') {
        $scope.listings[0].tags = $scope.listings[0].tags.join(' ');

      }
      if ($scope.listings[0].price_type === 'noprice') {
        $scope.listings[0].price = 0;
      }
      ProductsFactory.editProduct($scope.listings[0].id, $scope.listings[0])
        .then(function(data) {
          $scope.loading = false;
          $state.go('settings.store.products_services');
        })
        .catch(function(error) {
          console.log(error);
          $scope.loading = false;
          $scope.error = error.data.message;
        });
    }

    // $scope.$watch('bulkListing', function(){
    //  console.log($scope.bulkListing)
    // })

  }]);