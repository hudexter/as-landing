'use strict';

antsquare.run(['$rootScope', 'GeoLocationFactory', 'GeoLocationService', 'intercomFactory', 'Auth', '$state', '$timeout', 'StoreFactory', 'AccountsFactory', '$cookies', 'Config', '$http', '$firebaseAuth', 'branchFactory',
  function($rootScope, GeoLocationFactory, GeoLocationService, intercomFactory, Auth, $state, $timeout, StoreFactory, AccountsFactory, $cookies, Config, $http, $firebaseAuth, branchFactory) {

    branchFactory.init();

    // $http.post('https://phonesupport.antsquare.com/token/generate')
    //   .then(function(data) {
    //     Twilio.Device.setup(data.data.token);
    //     Twilio.Device.ready(function(device) {
    //       $rootScope.$broadcast('TwilioReady')
    //     });
    //   })

    var env = $cookies.get('ENV') || null;
    if (env !== Config.ENV) {
      Auth.reset();
      $cookies.put('ENV', Config.ENV, {
        domain: Config.COOKIE_DOMAIN
      });
    };

    intercomFactory.start();
    var country = null;

    function success(pos) {
      GeoLocationService.setCoords(pos.coords, 'html5');
      GeoLocationService.reverseGeolocation(pos.coords)
        .then(function(data) {
          // if (country !== data[data.length - 1].address_components[0].short_name) {
          GeoLocationService.set(data, 'html5');
          // }
        });
    };

    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
      GeoLocationFactory.getLocationByIp()
        .then(function(data) {
          var coords = GeoLocationService.getCoordsFromIp(data.data.loc.toString());
          GeoLocationService.setCoords(coords, 'ip');
          GeoLocationService.reverseGeolocation(coords)
            .then(function(data) {
              GeoLocationService.set(data, 'ip');
            });
        });
    };

    GeoLocationFactory.getLocationByIp()
      .then(function(data) {
        country = data.data.country;
        var coords = GeoLocationService.getCoordsFromIp(data.data.loc.toString());
        GeoLocationService.setCoords(coords, 'ip');
        GeoLocationService.reverseGeolocation(coords)
          .then(function(data) {
            GeoLocationService.set(data, 'ip');
          });
      });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(success, error);
    }

    function getUserData() {
      StoreFactory.me()
        .then(function(data) {
          Auth.setStore(data.data);

        })
        .catch(function(error) {
          console.log(error)
            // 404 Page
        });
      AccountsFactory.me()
        .then(function(data) {
          Auth.setUser(data.data);
        })
        .catch(function(error) {
          console.log(error)
            // 404 Page
        });
    }

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {

        if (!Auth.isLoggedIn() && (Auth.getToken()
            .refresh_token !== 'null')) {
          var expDate = new Date(Auth.getToken()
            .exp * 1000);
          var currentDate = new Date();
          if (currentDate > expDate) {
            Auth.refresh()
              .then(function(data) {
                Auth.update(data.data);
                getUserData();
                $rootScope.$broadcast('refreshed')
              })
              .catch(function(error) {
                console.log(error)
                  // 404 Page
              })
          }
        } else if (!Auth.getToken()
          .refresh_token) {
          Auth.logout();
        }

        if (toState.name === "search") {
          intercomFactory.detached();
        } else {
          if (_.isEmpty(Auth.getUserFn())) {
            intercomFactory.reattach();
          } else {
            intercomFactory.updateUser(Auth.getUserFn())
          }
        }

        if (('data' in toState)) {
          // Require Permission
          if (Auth.isLoggedIn()) {
            if (!Auth.userRoles.indexOf(toState.data) > -1) {
              // Admin
              //$state.go('index.home'); back to where it came from OR home page
              // event.preventDefault();
            }
          } else {
            // TODO check if it has refresh token,
            // if yes then get User
            // else send to home
            // Not Logged In
            if (fromState.url === '^') {
              $timeout(function() {
                $state.go('index.home');
                //   $timeout(function () {
                //   $rootScope.$broadcast('openLoginModal',toState.name);
                // },300);
              });
              event.preventDefault();
            } else {
              // Open Modal
              $rootScope.$broadcast('openLoginModal', toState.name);
              event.preventDefault();
            }
          }
        }
      });

    if (Auth.getToken()
      .refresh_token) {
      var expDate = new Date(Auth.getToken()
        .exp * 1000);

      var currentDate = new Date();
      if (currentDate > expDate) {

        Auth.refresh()
          .then(function(data) {
            Auth.update(data.data);
            getUserData();
            $rootScope.$broadcast('refreshed')
          })
          .catch(function(error) {
            console.log(error)
              // 404 Page
          })
      }
    };

    $rootScope.FirebaseAuthObj = {};
    var refreshing = false;

    function AuthFirebase() {
      var ref = new Firebase(Config.FIREBASE_BASEURL);

      $rootScope.FirebaseAuthObj = $firebaseAuth(ref);
      var fireAuthData = $rootScope.FirebaseAuthObj.$getAuth();

      $rootScope.FirebaseAuthObj.$onAuth(function(authData) {
        if (!authData) {
          $rootScope.$broadcast('firebaseTokenExpired');
          var firebaseToken = $cookies.get('firebase_token');
          $rootScope.FirebaseAuthObj.$authWithCustomToken(firebaseToken)
            .then(function(authData) {
              $cookies.put('firebase_token', authData.token, {
                domain: Config.COOKIE_DOMAIN
              });
              if (!refreshing) {
                $rootScope.$broadcast('refreshFirebaseToken');
              }

            })
            .catch(function(error) {
              console.log('firebase expired')

              Auth.refresh()
                .then(function(data) {
                  AuthFirebase();
                  //$rootScope.$broadcast('refreshed')
                })
            });
        }
      });

      var fireAuthData = $rootScope.FirebaseAuthObj.$getAuth();
      if (fireAuthData) {
        $cookies.put('firebase_token', fireAuthData.token, {
          domain: Config.COOKIE_DOMAIN
        });

        Auth.refresh()
          .then(function(data) {
            Auth.update(data.data);
            //$rootScope.$broadcast('refreshed')
          });
        // $rootScope.$broadcast('refreshFirebaseToken');
        // console.log("User " + fireAuthData.uid + " is logged in with " + fireAuthData.provider);
      } else {
        var firebaseToken = $cookies.get('firebase_token');
        $rootScope.FirebaseAuthObj.$authWithCustomToken(firebaseToken)
          .then(function(authData) {
            $cookies.put('firebase_token', fireAuthData.token, {
              domain: Config.COOKIE_DOMAIN
            });
            // console.log("User " + authData.uid + " is logged in with " + authData.provider);
            // $rootScope.$broadcast('refreshFirebaseToken');
          })
          .catch(function(error) {
            console.log('firebase expired2')
            console.log(4)
            Auth.refresh()
              .then(function(data) {
                AuthFirebase();
                //$rootScope.$broadcast('refreshed')
              })
          });
      }
    }

    $rootScope.$on('refreshFirebaseToken', function() {
      if (!refreshing) {
        refreshing = true;
        Auth.refresh()
          .then(function(data) {
            Auth.update(data.data);
            AuthFirebase();
            //$rootScope.$broadcast('refreshed')
            setTimeout(function() {
              refreshing = false;
            }, 1000);
          });
      }
    });

    if (Auth.getToken()
      .refresh_token && (Auth.getToken()
        .refresh_token !== 'null')) {
      console.log(6)
      Auth.refresh()
        .then(function(data) {
          Auth.update(data.data);
          AuthFirebase();
          //$rootScope.$broadcast('refreshed')
        });
    }

  }
])