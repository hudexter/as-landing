'use strict';

antsquare
	.factory('RequestFactory', ['$http', 'Config', '$q', function ($http, Config, $q) {

		return {
			getRequestPageView: function (request_id, viewer_id) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/' + request_id + '/pageview',
					method: 'GET',
					params: {
						viewer_id: viewer_id,
					}
				});
			},
			getAllRequest: function (data) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/all',
					method: 'GET',
					params: {
						page: data.page,
						per_page: data.per_page,
						uid: data.uid
					}
				})
			},
			getRecommend: function (request_id, location) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/' + request_id + '/recommend',
					method: 'GET',
					params: location
				});
			},
			deleteRequest: function (id) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/' + id,
					method: 'DELETE'
				})
			},
			putRequest: function (data) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/' + data.id,
					method: 'PUT',
					data: data
				})
			},
			getRequest: function (data) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/requests/' + data.id,
					method: 'GET',
					params: data
				})
			},
			flagRequest: function (request_id, reason) {
				return $http({
					url: Config.CORE_BASEURL + '/v5/products/' + request_id + '/flag',
					method: 'PUT',
					data: {
						'reason': reason
					}
				});
			}
		}
    }]);
