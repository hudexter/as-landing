'use strict';

antsquare
	.controller('RequestCtrl', ['$scope', '$state', '$stateParams', 'RequestFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'Auth', function ($scope, $state, $stateParams, RequestFactory, $window, MapService, GeoLocationService, $rootScope, $location, Auth) {

		// User
		if (Auth.isLoggedIn()) {
			$scope.userInfo = Auth.getUserFn();
		} else {
			$scope.userInfo = null;
		}

		$rootScope.$on('updateUser', function (event, data) {
			if (data) {
				$scope.userInfo = data;
				start();
			} else {
				$scope.userInfo = null;
			}
		});

		var distance = {};

		$scope.coords = undefined;
		$scope.product = undefined;
		$rootScope.$on('setLocation', function () {
			$scope.coords = GeoLocationService.getCoords();
			if ($scope.product) {
				distance = MapService.getDistance($scope.product.lat, $scope.product.lon, $scope.coords.latitude, $scope.coords.longitude);
				for (var key in distance) {
					$scope.product[key] = distance[key];
				}
			}
		});

		RequestFactory.getRequestPageView($stateParams.request_id)
			.then(function (data) {
				$scope.product = data.data;
				$scope.$broadcast('productLoaded');
				if (GeoLocationService.hasLocation()) {
					$scope.coords = GeoLocationService.getCoords();
					distance = MapService.getDistance($scope.product.lat, $scope.product.lon, $scope.coords.latitude, $scope.coords.longitude);
					for (var key in distance) {
						$scope.product[key] = distance[key];
					}
				}

				// $scope.root = $location.host();
				$scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://dashboard.antsquare.com/product/" + $scope.product.id;

				$scope.twLink = "http://twitter.com/intent/tweet?status=" + $scope.product.name + "+%40antsquare+" + "https://dashboard.antsquare.com/product/" + $scope.product.id + "%26related=antsquare";
				"http://twitter.com/intent/tweet?status=" + $scope.product.name + "@antsquare" + "https://dashboard.antsquare.com/product/" + $scope.product.id + "&related=antsquare";

				$scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://dashboard.antsquare.com/product/" + $scope.product.id + "&amp;media=" + $scope.product.images[0] + "&amp;description=Check it out!"

				$scope.mailLink = "mailto:?Subject=Check out " + $scope.product.name + " on Antsquare&body=Thought you might like this : https://dashboard.antsquare.com/product/" + $scope.product.id;

			})
			.catch(function (err) {
				$scope.product = err.message;
			})

		function checkImgContainerSize() {
			// if (window.innerWidth < 768) {
			var imgContainer = angular.element(document.querySelector('#rn-carousel'));
			var x = imgContainer.width();
			imgContainer.height(x);
			// }
		};

		$scope.flagRequest = function () {
			if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagProduct')
			$scope.flagged = true;
			RequestFactory.flagRequest($stateParams.request_id)
				.then(function (data) {})
				.catch(function (error) {
					$scope.flagged = false;
				});
		}

		checkImgContainerSize();
		window.onresize = checkImgContainerSize;

		$scope.characterLimit = 16;

  }]);
