'use strict';

angular.module('antsquare')
	.controller('PressCtrl', ['$scope',
    function ($scope) {
      window.location.href = "https://dashboard.antsquare.com/press";
    }
  ])
