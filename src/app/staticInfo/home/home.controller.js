'use strict';

angular.module('antsquare')
  .controller('HomeCtrl', ['$scope', '$mixpanel',
    function($scope, $mixpanel) {
      $mixpanel.track('l-visit');
    }
  ])