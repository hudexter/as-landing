'use strict';

angular.module('antsquare')
  .controller('AboutCtrl', ['$scope', 'deviceDetector', '$stateParams', '$location', '$anchorScroll', '$window', '$state',
    function($scope, deviceDetector, $stateParams, $location, $anchorScroll, $window, $state) {
      $scope.isMobile = deviceDetector.isMobile();

      $scope.content = {
        title: "Reshaping the way things are bought and sold.",
        content1: "Everyone has bought and sold things locally. Yep, even us. And like you, we found the experience complicated and inconsistent. Which is why we created Antsquare, a marketplace that delivers a smooth, seamless experience for both buyers and sellers in your community.",
        content2: "   The team behind Antsquare isn’t a secret group of ninjas and wizards. We’re just everyday folks who happen to be obsessed about great user experiences and want to build a better community."
      };

      $scope.team = [{
          name: 'Daniel Ling',
          title: "CEO/Founder",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/daniel.jpg",
          linkedin: "https://www.linkedin.com/pub/daniel-ling/9a/789/7b1"
        },
        {
          name: 'Hans Xu',
          title: "Director, China Operation / MP of NewMargin Ventures",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/hans-xu.jpg",
          linkedin: "http://www.newmargin.com/site/aboutus_team_en.asp"
        },
        {
          name: 'Dexter Hu',
          title: "Vice President & Co-Founder & Back-End",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/dexter.png",
          linkedin: "https://www.linkedin.com/in/dexterhu1"
        },
        {
          name: 'Kevin Tang',
          title: "Growth Manager",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/kevintang.jpeg",
          linkedin: "https://www.linkedin.com/in/kevin-tang-59592977"
        },
        {
          name: 'Sergey Demchenko',
          title: "Senior Software Engineer (iOS)",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/sergey.jpg",
          linkedin: "https://www.linkedin.com/pub/sergey-demchenko/42/972/608/en"
        },
        {
          name: 'Kevin Mendez',
          title: "UX/UI Designer",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/kevin.jpg",
          linkedin: "https://www.linkedin.com/in/kpmendez"
        },
        {
          name: 'Pavan Chokshi',
          title: "Software Engineer Intern (Front-End)",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/pavan.jpg",
          linkedin: "https://www.linkedin.com/in/pavanchokshi"
        },
        {
          name: 'Apoorv Patel',
          title: "Software Engineer Intern (Android)",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/apoorv.jpg",
          linkedin: "https://www.linkedin.com/in/apoorvpatel"
        },

        {
          name: 'Yan Zan',
          title: "Senior Data Scientist",
          image: "https://d37pip7vd9logx.cloudfront.net/imgs/employee/yan.jpg",
          linkedin: "https://www.linkedin.com/in/yanzan"
        }
      ];

      $scope.info = [{
        title: "Sales Inquiries",
        image: 'icon-sales',
        content: 'Contact us for any sales or general inquries.',
        link: 'support@antsquare.com',
        linkName: 'support@antsquare.com',
        // number: '604-281-3111',
        email: 'true'
      }, {
        title: "Support",
        image: 'icon-support',
        content: 'Here to help with any questions with Antsquare.',
        link: 'support@antsquare.com',
        linkName: 'support@antsquare.com',
        // number: '604-281-3111',
        email: 'true'
      }, {
        title: "Help Center",
        image: 'icon-info',
        content: 'Tech support and help using Antsquare',
        link: 'https://antsquare.zendesk.com/hc/en-us',
        target: '_blank',
        linkName: 'Visit Support center'
      }];

      ///////////////////////////////////
      //  Map: Setting                 //
      ///////////////////////////////////

      $scope.map = {
        center: {
          latitude: '49.3271408',
          longitude: '-123.1439541'
        },
        zoom: 15,
        options: {
          draggable: false,
          scrollwheel: false
        },
        dragging: false,
      };

      $scope.vanLoc = {
        latitude: '49.3271408',
        longitude: '-123.1439541'
      };

      $scope.seattle = {
        center: {
          latitude: '37.3339788',
          longitude: '-121.9108941'
        },
        zoom: 15,
        options: {
          draggable: false,
          scrollwheel: false
        },
        id: 0,
        icon: 'assets/img/marker.png',
        draggable: false,
        show: false,
      };

      $scope.sanjose = {
        center: {
          latitude: '37.320798',
          longitude: '-121.9444407'
        },
        zoom: 15,
        options: {
          draggable: false,
          scrollwheel: false
        },
        id: 0,
        icon: 'assets/img/marker.png',
        draggable: false,
        show: false,
      };


      $scope.seattleLoc = {
        latitude: '37.3339788',
        longitude: '-121.9108941'
      };

      if ($stateParams.section === 'about') {
        $window.scrollTo(0, 0);
      } else {
        $location.hash($stateParams.section);
        $anchorScroll();
      }

      $scope.jobs = [{
          title: 'Senior Software Engineer - Backend',
          location: 'Vancouver / Silicon Valley',
          job_id: 'backend'
        }, {
          title: 'Senior Software Engineer - iOS',
          location: 'Vancouver',
          job_id: 'ios'
        }, {
          title: 'Senior Software Engineer - iOS',
          location: 'Silicon Valley',
          job_id: 'ios'
        }, {
          title: 'Senior Software Engineer - Android',
          location: 'Vancouver / Silicon Valley',
          job_id: 'android'
        }, {
          title: 'Senior Software Engineer - Front-end',
          location: 'Vancouver / Silicon Valley',
          job_id: 'frontEnd'
        }, {
          title: 'Senior UI/UX Designer',
          location: 'Vancouver / Silicon Valley',
          job_id: 'ux'
        }, {
          title: 'Visual Design Intern',
          location: 'Vancouver / Silicon Valley',
          job_id: 'designIntern'
        }

      ];

      $scope.toJob = function(id) {
        $state.go('menu.jobs', {
          'job_id': id
        });
      }

    }
  ]);