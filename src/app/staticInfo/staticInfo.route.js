'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('index.home', {
        url: '/',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/home/home.html',
            controller: 'HomeCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.feed', {
        url: '/feed',
        views: {
          'mainView': {
            templateUrl: 'app/publicfeed/publicfeed.html',
            controller: 'PublicfeedCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("Take control of your local sales! Antsquare helps you reach the buyers in your own community that didn’t even know you were there. Be discovered on the map, arrange for a convenient time and place to meet the buyer, and accept all major forms of payment—right from your mobile device. No hardware required.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.merchant', {
        url: '/merchant',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/merchant/merchant.html',
            controller: 'MerchantCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("Take control of your local sales! Antsquare helps you reach the buyers in your own community that didn’t even know you were there. Be discovered on the map, arrange for a convenient time and place to meet the buyer, and accept all major forms of payment—right from your mobile device. No hardware required.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.guideline', {
        url: '/marketplaceGuidelines',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/guideline/guideline.html',
            controller: 'GuidelineCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("Antsquare is a marketplace for the hyper-local buying and selling of unique products and services within our community. We want to showcase the best of what our community has to offer! Therefore, everything on Antsquare must be of high quality, integrity and professionalism.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.about', {
        url: '/about?section',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/about/about.html',
            controller: 'AboutCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("Reshaping the way things are bought and sold. Everyone has bought and sold things locally. Yep, even us. And like you, we found the experience complicated and inconsistent. Which is why we created Antsquare, a marketplace that delivers a smooth, seamless experience for both buyers and sellers in your community.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.press', {
        url: '/press',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/press/press.html',
            controller: 'PressCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("Antsquare is a marketplace for the hyper-local buying and selling of unique products and services within our community. We want to showcase the best of what our community has to offer! Therefore, everything on Antsquare must be of high quality, integrity and professionalism.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('index.getapp', {
        url: '/getapp',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/downloadApp/downloadApp.html',
            controller: 'DownloadAppCtrl',
          }
        }
      })
      .state('index.terms', {
        url: '/terms',
        views: {
          'mainView': {
            templateUrl: 'app/staticInfo/terms/terms.html',
            controller: 'TermsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      });
  }
])