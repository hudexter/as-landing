'use strict';

angular.module('antsquare')
  .controller('GuidelineCtrl', ['$scope', 'deviceDetector',
    function ($scope, deviceDetector) {

      $scope.isMobile = deviceDetector.isMobile();

      if (deviceDetector.device === 'android') {
        $scope.isAndroid = true;
      } else if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
        $scope.isIOS = true;
      }

      // $scope.onSeller = true;

      $scope.sellerTip = [{
        title: 'Be honest.',
        content: 'Take a flattering photograph, but don’t hide flaws. And be honest in the description. Include important details, good or bad, so buyers understand exactly what they’re getting. Also remember to update your inventory level from time to time.'
      }, {
        title: 'Be available.',
        content: 'With Antsquare, sellers can select time slots of availability that buyers use to arrange pickups. When a buyer commits to a purchase and picks a time within your range, don’t bail—that’s no fun for anyone. Also in the case of inquiries from the buyer within the app, please respond promptly.'
      }, {
        title: 'Be patient.',
        content: 'Once you meet with your buyer in person, don’t rush the transaction. It’s their money and it’s only fair that they are given a chance to properly inspect their purchase before finalizing the deal.'
      }, {
        title: 'Be safe.',
        content: 'We recommend sellers to communicate with buyers by using the messaging system within the app unless the seller knows the buyer.'
      }]
      $scope.buyerTip = [{
        title: 'Be careful.',
        content: 'We do our best to ensure listings are accurate, but be aware that some sellers may attempt to hide imperfections. Don’t be afraid to ask specific questions—and if something is too good to be true, it probably is.'
      }, {
        title: 'Be respectful.',
        content: 'If you commit to buy something, please pick up on time as most of sellers are busy entrepreneurs. In the case of cancellation, please give at least 24 hour notice to the seller with good reasons.'
      }, {
        title: 'Be fun.',
        content: 'Antsquare is helping to foster a local community. Feel free to share your experience with your friends, rate your experience, leave comments and sell something unique yourself!'
      }, {
        title: 'Be safe.',
        content: 'In the case that you suspect the seller misleads you in any way, please rate them accordingly or report directly to Antsquare.'
      }];

    }
  ]);