'use strict';

angular.module('antsquare')
  .controller('TestCtrl', ['$scope', '$mixpanel',
    function($scope, $mixpanel) {
      console.log("In test page");
      $mixpanel.track('l-visit');
    }
  ])