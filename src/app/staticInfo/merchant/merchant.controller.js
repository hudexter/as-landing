'use strict';

angular.module('antsquare')
  .controller('MerchantCtrl', ['$scope', '$rootScope', '$state', 'AccountsFactory', 'ProductsFactory', 'deviceDetector', '$timeout', function ($scope, $rootScope, $state, AccountsFactory, ProductsFactory, deviceDetector, $timeout) {

    $scope.isMobile = deviceDetector.isMobile();
    if (deviceDetector.device === 'android') {
      $scope.isAndroid = true;
    } else if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
      $scope.isIOS = true;
    }

    // var formData = FormDataIntent.getContainer() || null;
    // $scope.show = false;
    // if ($state.current.name === 'menu') {
    //   $scope.show = true;
    // }

    $rootScope.$on('$stateChangeStart',
      function (event, toState, toStateParams, fromState, fromStateParams) {
        if (toState.name === 'menu') {
          $scope.show = true;
        } else {
          $scope.show = false;
        }
      });

    /* for home page*/

    // Get trending products
    // ProductsFactory.getTrendingProducts()
    //   .then(function (data) {
    //     $scope.products = data.data;
    //   })
    //   .catch(function (error) {
    //     console.log(error)
    //   });

    $scope.sellingSlide = [{
      title: 'Your Business.',
      icon: 'https://d37pip7vd9logx.cloudfront.net/merchants3/ic-yourbusiness@2x.png',
      image: 'https://d37pip7vd9logx.cloudfront.net/merchants3/yourbusiness@2x.jpg',
      content: 'Make it yours. Show off your brand with a custom banner and logo, then tell your story through an about and owner bio page.'
    }, {
      title: 'Your Time.',
      icon: 'https://d37pip7vd9logx.cloudfront.net/merchants3/ic-yourtime@2x.png',
      image: 'https://d37pip7vd9logx.cloudfront.net/merchants3/yourtime@2x.jpg',
      content: 'Do business when and where it works for you with flexible pick-up time and location.'
    }, {
      title: 'Your Way.',
      icon: 'https://d37pip7vd9logx.cloudfront.net/merchants3/ic-yourway@2x.png',
      image: 'https://d37pip7vd9logx.cloudfront.net/merchants3/yourway@2x.jpg',
      content: 'Integrated payments means no hardware or readers required to accept any major credit or debit card – you can even use cash!'
    }];

    $scope.tagLine = "Your community marketplace."
    $scope.buyingFiller = {
      title: "Our app does all the work so that you don’t have to.",
      content: "When buying doesn’t feel like a chore, you’re using Antsquare. That’s because our marketplace streamlines the process so you can find something, pay for it, and arrange a pickup, all in a matter of seconds—and without ever leaving the app. Here’s how it works."
    }

    $scope.sellingFiller = {
      title: "Selling your products doesn’t have to feel like work.",
      content: "Our merchant-friendly marketplace redefines what it means to set up shop in your community. Have fun customizing your store, your items are now discoverable to customers you would have never expected. Here’s how it works."
    }

    // Sign up
    $scope.loading = false;
    $scope.data = {};
    $scope.error = null;
    $scope.response = '';

    var submited = false;

    $scope.submitForm = function () {
      if (!submited) {
        submited = true;
        AccountsFactory.registerUser($scope.data)
          .then(function (data) {
            $scope.response = data;
            $rootScope.temp_number = $scope.data.input;
            submited = false;
            $scope.phoneVerify(data.data)
            $scope.data = {};
          })
          .catch(function (error) {

            submited = false;
            if (error.status != 500) {

              $scope.error = error.data.message
              $timeout(function () {
                $scope.error = null;
              }, 3000);
            }
            // $modalInstance.close(error);
          });
      }
    };

    $scope.phoneVerify = function (data) {

      var modalInstance = $modal.open({
        templateUrl: 'app/main/settings/account/changePhone/phoneVerify.template.html',
        controller: 'PhoneVerifyCtrl',
        backdrop: 'static',
        size: 'sm',
        resolve: {
          userInfo: function () {
            return data;
          }
        }
      });

      modalInstance.result.then(function (data) {
        if (data.data.refresh_token) {
          authorization.login(data.data);
        }
      }, function () {

      });
    };

  }]);
