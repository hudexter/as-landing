'use strict';

angular.module('antsquare')
  .controller('DownloadAppCtrl', ['$scope', 'deviceDetector', '$injector', '$window', '$mixpanel',
    function($scope, deviceDetector, $injector, $window, $mixpanel) {

      var trackIos = function() {
        setTimeout(function() {
          location = 'https://goo.gl/rVSPpK';
          $mixpanel.track('l-d-cta-IOS');
        }, 100);
      }

      var trackAndroid = function() {
        setTimeout(function() {
          location = 'https://goo.gl/pVNVS4';
          $mixpanel.track('l-d-cta-Android');
        }, 100);
      }


      // if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone")
      if (deviceDetector.device === "android") {
        trackAndroid();
      } else if (deviceDetector.device === "iphone" || deviceDetector.device === "ipod") {
        trackIos();
      } else {
        var state = $injector.get('$state');
        state.go('index.home');
      }

    }
  ]);