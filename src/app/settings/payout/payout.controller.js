'use strict';

antsquare
	.controller('PayoutCtrl', ['$scope', '$state', 'ModalStorage', 'ModalService', 'AccountsFactory', 'PaymentFactory', 'toastr', 'GeoLocationService', function ($scope, $state, ModalStorage, ModalService, AccountsFactory, PaymentFactory, toastr, GeoLocationService) {

		$scope.identityVerify = false;
		$scope.addBankShow = false;
		$scope.payIdLoading = false;
		$scope.payouts = null;
		$scope.bank = null;
		$scope.type = null;

		$scope.getBank = function () {
			AccountsFactory.getBanks()
				.success(function (data, status) {
					$scope.payouts = data
					if ($scope.payouts.country === 'US') {
						if ($scope.payouts.active_account) {
							$scope.bank = $scope.payouts.active_account;
							$scope.type = 'bank';
						} else if ($scope.payouts.cards.total_count > 0) {
							$scope.bank = _.last($scope.payouts.cards.data);
							$scope.type = 'card';
						}
					} else if ($scope.payouts.country === 'CA') {
						if ($scope.payouts.active_account != null) {
							$scope.bank = $scope.payouts.active_account;
							$scope.type = 'bank';
						}
					}

				})
				.error(function (data, status) {});
		};

		$scope.addBankAcc = function () {
			$scope.addBankShow = true;
		};

		$scope.submitPayId = function () {
			$scope.payIdError = null;
			$scope.payIdLoading = true;
			PaymentFactory.setId($scope.data)
				.then(function (data) {
					$scope.identityVerify = false;
					$scope.payIdLoading = false;
					$scope.getPayId();
				})
				.catch(function (error) {
					$scope.payIdError = error.data.message
					$scope.payIdLoading = false;
				})
		};

		$scope.verifyAccount = function (data) {
			ModalStorage.set(data);
			ModalService.showModal({
					templateUrl: "app/components/modal/modalVerifyBank/modalVerifyBank.html",
					controller: "ModalVerifyBankCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						ModalStorage.end();
						if (result) {
							$scope.getBank();
						}
					});
				});
		};

		$scope.verifyIdentity = function () {
			$scope.identityVerify = true;
		};

		$scope.getPayId = function () {
			PaymentFactory.me()
				.then(function (data) {
					$scope.IdStatus = data.data.status;
				})
				.catch(function (error) {
					console.log(error)
				})
		}

		$scope.getPayId();
		$scope.getBank();

	}]);
