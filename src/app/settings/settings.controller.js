'use strict';

antsquare
  .controller('SettingsCtrl', ['$scope', '$state', '$rootScope', 'Config', 'Auth', function($scope, $state, $rootScope, Config, Auth) {

    $scope.onAccount = false;
    if ($state.current.name.indexOf('settings.transaction') === 0 || $state.current.name.indexOf('settings.payment') === 0 || $state.current.name.indexOf('settings.payout') === 0) {
      $scope.onAccount = true;
    }

    if ($state.current.name.indexOf('settings.profile') === 0 || $state.current.name.indexOf('settings.linking') === 0) {
      $scope.onProfile = true;
    }

    if ($state.current.name.indexOf('settings.store') === 0) {
      $scope.onStore = true;
    }

    $scope.tab = 1;

    $scope.setTab = function(newTab) {
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum) {
      return $scope.tab === tabNum;
    };
    $scope.logout = function() {
      Auth.logout();
    };

    $scope.userInfo = null;
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
    }
    $scope.url = Config;
    $scope.store = Auth.getStoreFn();

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toStateParams, fromState, fromStateParams) {
        if (toState.name.indexOf('settings.transaction') === 0 || toState.name.indexOf('settings.payment') === 0 ||
          toState.name.indexOf('settings.payout') === 0) {
          $scope.onAccount = true;
        } else {
          $scope.onAccount = false
        }

        if (toState.name.indexOf('settings.profile') === 0 ||
          toState.name.indexOf('settings.linking') === 0) {
          $scope.onProfile = true;
        } else {
          $scope.onProfile = false
        }

        if (toState.name.indexOf('settings.store') === 0) {
          $scope.onStore = true;
        } else {
          $scope.onStore = false
        }
      });
  }]);