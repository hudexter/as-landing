'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
   function ($stateProvider, $urlRouterProvider) {


		$stateProvider
    .state('settings.transaction.buying', {
      url: '/transaction_buying',
      views: {
        'transaction': {
          templateUrl: 'app/settings/transaction/buying/transactionBuying.html',
          controller: 'TransactionBuyingCtrl',
        }
      },
      onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
        seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
        seoFactory.setTitle("Antsquare");
        seoFactory.setType("article");
        var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
        seoFactory.setImage1(link);
        seoFactory.setUrl($location.absUrl());
        }]
    })
    .state('settings.transaction.selling', {
      url: '/transaction_selling',
      views: {
        'transaction': {
          templateUrl: 'app/settings/transaction/selling/transactionSelling.html',
          controller: 'TransactionSellingCtrl',
        }
      },
      onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
        seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
        seoFactory.setTitle("Antsquare");
        seoFactory.setType("article");
        var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
        seoFactory.setImage1(link);
        seoFactory.setUrl($location.absUrl());
        }]
    })
    .state('settings.transaction.payouts', {
      url: '/transaction_payouts',
      views: {
        'transaction': {
          templateUrl: 'app/settings/transaction/payouts/payouts.html',
          controller: 'TransactionPayoutsCtrl',
        }
      },
      onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
        seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
        seoFactory.setTitle("Antsquare");
        seoFactory.setType("article");
        var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
        seoFactory.setImage1(link);
        seoFactory.setUrl($location.absUrl());
        }]
    })


  }
])
