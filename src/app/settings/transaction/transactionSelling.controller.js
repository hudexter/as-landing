'use strict';

antsquare
	.controller('TransactionSellingCtrl', ['$scope', 'AccountsFactory', 'uiGridConstants', function ($scope, AccountsFactory, uiGridConstants) {
		$scope.loading = false;

		$scope.pageDetail = {
			page: 1,
			per_page: 10
		};

		$scope.getSellTx = function () {
			$scope.loading = true;
			$scope.buyings = {};
			AccountsFactory.getSellingTx($scope.pageDetail)
				.then(function (data) {
					$scope.buyings = data.data;
					$scope.gridOptions.data = data.data.orders;
					$scope.loading = false;

					$scope.pageDetail = {
						has_more: data.data.has_more,
						page: data.data.page,
						per_page: data.data.per_page,
						total: data.data.total,
						pages: parseInt(Math.ceil(data.data.total / data.data.per_page))
					};
				})
				.catch(function (error) {
					console.log(error);
					$scope.loading = false;
				});
		};

		$scope.gridOptions = {
			enableSorting: true,
			columnDefs: [
				{
					name: 'Date',
					field: 'date',
					enableColumnMenu: false,
					cellTemplate: "<div class=''><small>{{COL_FIELD | amDateFormat:'MMMM Do YYYY'}}</small></div>"
				},
				{
					name: 'Transaction Id',
					field: 'receipt_id',
				},
				{
					name: 'Type',
					field: 'type',
					cellTemplate: "<div class='capitalize'><small>{{COL_FIELD}}</small></div>"
				},
				{
					name: 'Product Name',
					field: 'snapshot',
					cellTemplate: "<div class='capitalize'><small>{{COL_FIELD.name | characters: 20}}</small></div>"
				},
				{
					name: 'Price',
					field: 'charged_amount',
					cellTemplate: "<div class='capitalize'><small>$ {{COL_FIELD | centToDollar}}</small></div>"
				}
				]
		}


		$scope.getSellTx();





	}]);
