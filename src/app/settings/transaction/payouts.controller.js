'use strict';

antsquare
	.controller('TransactionPayoutsCtrl', ['$scope', 'AccountsFactory', 'uiGridConstants', 'ModalStorage', 'ModalService', function ($scope, AccountsFactory, uiGridConstants, ModalStorage, ModalService) {

		$scope.loading = false;

		$scope.pageDetail = {
			page: 1,
			per_page: 10
		};

		$scope.payoutDetail = function (payout) {
      ModalStorage.set(payout);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalPayoutDetail/modalPayoutDetail.html",
          controller: "ModalPayoutDetailCtrl"
        })
        .then(function (modal) {
          modal.close.then(function (result) {
            ModalStorage.end();
          });
        });
		};

		$scope.getPayouts = function () {
			$scope.loading = true;
			AccountsFactory.getPayouts($scope.pageDetail)
				.then(function (data) {

					data.data.payouts = data.data.payouts.map(function (payout) {
						if (payout.status === 'completed' || payout.status === 'paid') {
							payout.status = 'Completed'
						} else if (payout.status === 'in_progress' || payout.status === 'pending' || payout.status === 'new') {
							payout.status = 'Pending'
						}
						return payout
					})
					$scope.gridOptions.data = data.data.payouts;

					$scope.pageDetail = {
						has_more: data.data.has_more,
						page: data.data.page,
						per_page: data.data.per_page,
						total: data.data.total,
						pages: parseInt(Math.ceil(data.data.total / data.data.per_page))
					}

				})
				.catch(function (error) {
					console.log(error);
					$scope.loading = false;
				});
		};


		$scope.gridOptions = {
			enableSorting: true,
			columnDefs: [
				{
					name: 'Date',
					field: 'date',
					enableColumnMenu: false,
					cellTemplate: "<div class=''><small>{{COL_FIELD | amDateFormat:'MMMM Do YYYY'}}</small></div>"
        },
				{
					name: 'Status',
					field: 'status'
        },
				{
					name: 'Reference Id',
					field: 'transaction_ref',
        },
				{
					name: 'Amount',
					field: 'amount',
					cellTemplate: "<div class='capitalize'><small>$ {{COL_FIELD | centToDollar}}</small></div>"
        },
				{
					name: 'Detail',
					field: 'snapshot',
					cellTemplate: "<button ng-click='grid.appScope.payoutDetail(row.entity)' class='btn-sm btn-primary btn'> <small>View Detail</small></button>"
        }
        ]
		};


		$scope.getPayouts();


	}]);
