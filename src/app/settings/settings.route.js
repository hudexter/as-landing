'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('settings', {
        templateUrl: 'app/settings/settings.html',
        controller: 'SettingsCtrl',
        data: 'user'
      })

    .state('settings.wishlist', {
      url: '/wishlist',
      views: {

        'col-12': {
          templateUrl: 'app/settings/wishlist/wishlist.html',
          controller: 'WishlistCtrl',
        }
      },
      onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
        seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
        seoFactory.setTitle("Antsquare");
        seoFactory.setType("article");
        var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
        seoFactory.setImage1(link);
        seoFactory.setUrl($location.absUrl());
      }]
    })

    .state('settings.profile', {
        url: '/profile',
        views: {

          'col-12': {
            templateUrl: 'app/settings/profile/profile.html',
            controller: 'ProfileCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.linking', {
        url: '/linking',
        views: {

          'col-12': {
            templateUrl: 'app/settings/profile/linking.html',
            controller: 'LinkingCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.payment', {
        url: '/payment',
        views: {

          'col-12': {
            templateUrl: 'app/settings/payment/payment.html',
            controller: 'PaymentCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.settingstabs', {
        url: '/settingstabs',
        views: {

          'col-12': {
            templateUrl: 'app/settings/settingstabs.html',
            controller: 'SettingsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.promos', {
        url: '/promos',
        views: {

          'col-12': {
            templateUrl: 'app/settings/payment/promos.html',
            controller: 'PaymentCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.payout', {
        url: '/payout',
        views: {

          'col-12': {
            templateUrl: 'app/settings/payout/payout.html',
            controller: 'PayoutCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.transaction', {
        // url: '/transaction',
        views: {
          'col-12': {
            templateUrl: 'app/settings/transaction/transaction.html',
            controller: 'TransactionCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })

  }
])