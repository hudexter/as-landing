'use strict';

antsquare
  .controller('MyStoreCtrl', ['$scope', '$state', '$cookies', 'StoreFactory', 'Auth', '$rootScope', 'UploadFatory', 'Upload', 'Config', 'GeoLocationService', 'MapService', '$q', function($scope, $state, $cookies, StoreFactory, Auth, $rootScope, UploadFatory, Upload, Config, GeoLocationService, MapService, $q) {

    StoreFactory.me()
      .then(function(data) {
        Auth.setStore(data.data);
      })
      .catch(function(error) {})

    $scope.store = Auth.getStoreFn().stores[0];
    $rootScope.$on('updateStore', function(event, data) {
      $scope.store = data.stores[0];

      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        zoom: 12,
        dragging: false,
        events: {
          idle: function() {
            reverseGeocoding($scope.map.center.latitude, $scope.map.center.longitude)
              .then(function(data) {
                console.log(data)
                $scope.mapResult = data;
              })
          },
        }
      };

    });

    $scope.deleteing = false;
    $scope.clearStore = function() {
      $scope.deleteing = true;
      StoreFactory.clearStore($scope.store)
        .then(function(data) {
          clearStoreForm.reset();
          $scope.deleteing = false;
        })
        .catch(function(error) {
          $scope.deleteing = false;
        })
    };

    // $scope.input = {};
    // //CSV IMPORT
    // $scope.uploadingCSV = false;
    // $scope.$watch('input.files', function() {
    //   var files = $scope.input.files;
    //   if (files && files.length) {
    //     for (var i = 0; i < files.length; i++) {
    //       var index = i;
    //       var file = files[i];

    //       UploadFatory.getSignUrl(file.type)
    //         .then(function(data) {
    //           $scope.uploadCSV2(data.data, file)
    //         })
    //         .catch(function(error) {
    //           console.log(error)
    //         })
    //     }
    //   }
    // });
    // $scope.loadingPct = "";
    // $scope.uploadCSV2 = function(data, file) {
    //   $scope.uploadingCSV = true;
    //   var path = UploadFatory.getCSVPath(file.type);
    //   Upload.upload({
    //       url: Config.S3_URL,
    //       method: 'POST',
    //       fields: {
    //         'key': path,
    //         'acl': 'public-read',
    //         'Content-Type': file.type,
    //         'AWSAccessKeyId': data.AWSAccessKeyId,
    //         'success_action_status': '201',
    //         'Policy': data.s3Policy,
    //         'Signature': data.s3Signature
    //       },
    //       sendObjectsAsJsonBlob: false,
    //       file: file
    //     })
    //     .progress(function(evt) {

    //       $scope.loading = true;
    //       var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    //       $scope.loadingPct = progressPercentage + '%';
    //     })
    //     .error(function(e) {
    //       console.error("Failed:", e);
    //       $scope.uploadingCSV = false;

    //     })
    //     .success(function(data, status, headers, config) {
    //       $scope.uploadingCSV = false;
    //       $scope.input.link = Config.IMAGE_URL + '/' + path;
    //     });
    // }

    // $scope.CSVsubmiting = false;
    // $scope.submitCSV = function() {
    //   $scope.CSVsubmiting = true;
    //   var csv = {
    //     csv_url: $scope.input.link,
    //     store_id: $scope.store.id,
    //   }
    //   StoreFactory.importCSV(csv)
    //     .then(function(data) {
    //       $scope.CSVsubmiting = false;
    //       $scope.csvRes = data;
    //       $scope.data = {};
    //     })
    //     .catch(function(error) {
    //       $scope.CSVsubmiting = false;
    //       $scope.csvRes = error;
    //       $scope.data = {};
    //     })
    // };

    $scope.statusToggle = false;
    $scope.storeStatusToggle = function() {
      $scope.statusToggle = true;
      $scope.store.is_enabled = !$scope.store.is_enabled;
      var data = {
        is_enabled: $scope.store.is_enabled,
        id: $scope.store.id
      };
      StoreFactory.update(data)
        .then(function(data) {
          console.log(data)
          $scope.statusToggle = false;
        })
        .catch(function(error) {
          console.log(error);
          $scope.store.is_enabled = !$scope.store.is_enabled;
          $scope.statusToggle = false;
        })
    }


    $scope.map = {
      center: {
        latitude: $scope.store.lat,
        longitude: $scope.store.lon
      },
      zoom: 12,
      dragging: false,
      events: {
        idle: function() {
          reverseGeocoding($scope.map.center.latitude, $scope.map.center.longitude)
            .then(function(data) {
              $scope.mapResult = data;
            })
        },
      }
    };
    var mapSet = false;
    var yourLocation = {};

    function setMapLocation() {
      if (GeoLocationService.hasLocation()) {
        $scope.map.center.latitude = GeoLocationService.getCoords()
          .latitude;
        $scope.map.center.longitude = GeoLocationService.getCoords()
          .longitude;
      }
    };
    if (!$scope.store.lat) {
      setMapLocation();
    }

    $rootScope.$on('setLocation', function() {
      if (!$scope.store.lat) {
        $scope.map.center.latitude = GeoLocationService.getCoords()
          .latitude;
        $scope.map.center.longitude = GeoLocationService.getCoords()
          .longitude;
      }
    });



    function reverseGeocoding(lat, lon) {
      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(lat, lon);
      var deferred = $q.defer();
      $scope.map.center.latitude = latlng.lat();
      $scope.map.center.longitude = latlng.lng();

      var map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: $scope.map.center.latitude,
          lng: $scope.map.center.longitude
        }
      });

      var request = {
        location: latlng,
        radius: '200'
      };

      var result = {
        nearBy: null,
        address1: null,
        address2: null,
      };

      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, function(results, status) {
        if (status === "OK") {
          result.nearBy = results;
          geocoder.geocode({
            'latLng': latlng
          }, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
              results.forEach(function(obj) {
                for (var key in obj) {
                  // if (obj.types[0] === 'street_address') {
                  //
                  //    result.address1 = obj.address_components[0].long_name + " " + obj.address_components[1].short_name;
                  //    // deferred.resolve(result)
                  // }
                  // if (obj[key][0] === 'political' || obj[key][0] === 'locality') {
                  //    result.address2 = obj.formatted_address;
                  //    // deferred.resolve(result)
                  // }

                  if (obj[key][0] === 'country') {
                    result.country = obj.address_components[0].short_name;
                  }
                }
              })
              var temp = results[0].formatted_address.split(',');
              result.address1 = temp[0]
              result.address2 = temp.splice(1, temp.length).join();

              deferred.resolve(result)
            }
          });

        }
      });
      return deferred.promise;
    }

    $scope.$on('g-places-autocomplete:select', function(e, place) {
      if (!place.geometry) {
        return;
      } else {
        var lat = place.geometry.location.lat();
        var lon = place.geometry.location.lng();

        // $scope.mapResult = palce.formatted_address;
        reverseGeocoding(lat, lon)
          .then(function(data) {
            // console.log(data)
            $scope.mapResult = data;
          })
      }
    });
    $scope.submitAddress = false;
    $scope.storeAddressUpdate = function() {

      var store = {
        id: $scope.store.id,
        lat: $scope.map.center.latitude,
        lon: $scope.map.center.longitude,
        address1: $scope.mapResult.address1,
        address2: $scope.mapResult.address2,
        country: $scope.mapResult.country
      }

      $scope.submitAddress = true;
      StoreFactory.update(store)
        .then(function(data) {
          $scope.submitAddress = false;
          $scope.shell = Auth.getStoreFn();
          $scope.shell.stores[0] = data.data;
          Auth.setStore($scope.shell);
        })
        .catch(function(error) {
          console.log(error)
          $scope.submitAddress = false;
        })
    }



  }]);