'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('settings.store', {
        views: {

          'col-12': {
            templateUrl: 'app/settings/store/store.html',
            controller: 'MyStoreCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.settings', {
        url: '/store_settings',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/settings/settings.html',
            controller: 'MyStoreSettingsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.requests', {
        url: '/requests',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/requests/requests.html',
            controller: 'StoreRequestsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.promotions', {
        url: '/promotions',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/promotions/promotions.html',
            controller: 'StorePromotionsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.moment', {
        url: '/moment',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/moment/moment.html',
            controller: 'StoreMomentCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.products_services', {
        url: '/products_services',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/products/products.html',
            controller: 'StoreProductsCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('settings.store.profile', {
        url: '/store_profile',
        views: {
          'myStore': {
            templateUrl: 'app/settings/store/profile/profile.html',
            controller: 'StoreProfileCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function ($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
  }
])