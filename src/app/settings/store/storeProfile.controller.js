'use strict';

antsquare
  .controller('StoreProfileCtrl', ['$scope', '$rootScope', 'AccountsFactory', 'Auth', 'StoreFactory', 'UploadFatory', 'deviceDetector', 'ModalStorage', 'ModalService', 'Upload', '$cookies', 'toastr', 'Config', '$location', function($scope, $rootScope, AccountsFactory, Auth, StoreFactory, UploadFatory, deviceDetector, ModalStorage, ModalService, Upload, $cookies, toastr, Config, $location) {
    $scope.data = {
      store_name: null,
      handle: null,
      description: null,
      bio: null,
      logo: null,
      cover_image: null,
      facebook: null,
      twitter: null,
      pinterest: null,
      instagram: null,
      youtube: null,
      id: null
    };


    $scope.uploading = false;
    $scope.input = {};
    $scope.preview = "";
    // Check Mobile

    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }
    $scope.hideupdateemail = true;


    StoreFactory.me()
      .then(function(data) {
        Auth.setStore(data.data);
      })
      .catch(function(error) {})


    AccountsFactory.me()
      .then(function(data) {
        $scope.profile = data.data;
        console.log(data.data);
      })
      .catch(function(error) {
        console.log(error)
          // 404 Page
      });





    $scope.submiting = false;
    $scope.tempProfile = null;
    $scope.uploading = false;
    $scope.file = {
      logo: null,
      cover_image: null
    };
    $scope.tempProfile = {
      logo: null,
      cover_image: null
    }

    $scope.preview = "";
    $scope.store = Auth.getStoreFn().stores[0];

    $scope.link1 = $location.path();
    $scope.link2 = $location.absUrl();
    $scope.link = $scope.link2.replace($scope.link1, '/');

    $scope.input = {};
    //CSV IMPORT
    $scope.uploadingCSV = false;
    $scope.$watch('input.files', function() {


      var files = $scope.input.files;

      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var index = i;
          var file = files[i];

          UploadFatory.getSignUrl(file.type)
            .then(function(data) {

              $scope.uploadCSV2(data.data, file);

            })
            .catch(function(error) {
              console.log(error)
            })
        }
      }
    });
    $scope.loadingPct = "";

    $scope.uploadCSV2 = function(data, file) {

      $scope.uploadingCSV = true;
      var path = UploadFatory.getCSVPath(file.type);
      Upload.upload({
          url: Config.S3_URL,
          method: 'POST',
          fields: {
            'key': path,
            'acl': 'public-read',
            'Content-Type': file.type,
            'AWSAccessKeyId': data.AWSAccessKeyId,
            'success_action_status': '201',
            'Policy': data.s3Policy,
            'Signature': data.s3Signature
          },
          sendObjectsAsJsonBlob: false,
          file: file
        })
        .progress(function(evt) {

          $scope.loading = true;
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.loadingPct = progressPercentage + '%';
        })
        .error(function(e) {
          console.error("Failed:", e);
          $scope.uploadingCSV = false;

        })
        .success(function(data, status, headers, config) {
          $scope.uploadingCSV = false;
          $scope.input.link = Config.IMAGE_URL + '/' + path;
        });
    }

    $scope.CSVsubmiting = false;
    $scope.submitCSV = function() {
      $scope.CSVsubmiting = true;
      var csv = {
        csv_url: $scope.input.link,
        store_id: $scope.store.id,
      }

      StoreFactory.importCSV(csv)
        .then(function(data) {
          $scope.CSVsubmiting = false;
          $scope.csvRes = data;
          $scope.data = {};
        })
        .catch(function(error) {
          $scope.CSVsubmiting = false;
          $scope.csvRes = error;
          $scope.data = {};
        })
    };

    $scope.fileInput = function(file, field) {
      var file = $scope.file[field];
      if (file) {
        if (file.size < 10000000) {
          UploadFatory.getSignUrl(file.type)
            .then(function(data) {
              $scope.uploadImage(data.data, file, field)
            })
            .catch(function(error) {
              console.log(error)
            })
        } else {
          $window.alert("The file needs to be under 10MB")
        }
      }
    }

    $scope.uploadImage = function(data, file, field) {
      $scope.uploading = true;
      var path = UploadFatory.getImagePath(file.type);
      Upload.upload({
          url: Config.S3_URL,
          method: 'POST',
          fields: {
            'key': path,
            'acl': 'public-read',
            'Content-Type': file.type,
            'AWSAccessKeyId': data.AWSAccessKeyId,
            'success_action_status': '201',
            'Policy': data.s3Policy,
            'Signature': data.s3Signature
          },
          sendObjectsAsJsonBlob: false,
          file: file
        })
        .progress(function(evt) {
          if (!$scope.uploading) {
            $scope.uploading = true;
          }
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        })
        .error(function(e) {
          // console.error("Failed:", e);
          $scope.uploading = false;
        })
        .success(function(data, status, headers, config) {
          $scope.uploading = false;
          // $scope.product_imgs[index].url = Config.IMAGE_URL + '/' + path;
          $scope.data[field] = Config.IMAGE_URL + '/' + path;
          // $scope.tempProfile[field] = Config.IMAGE_URL + '/' + path;
          // $scope.file = null;
        });
    };

    $scope.cloneStore = function(store) {
      Object.keys($scope.data)
        .map(function(field, index) {
          $scope.data[field] = store[field];
        });
    }

    $scope.submitStoreProfile = function() {

      $scope.submiting = true;
      StoreFactory.update($scope.data)
        .then(function(response) {
          $scope.shell = Auth.getStoreFn();
          $scope.shell.stores[0] = response.data;
          Auth.setStore($scope.shell);
          $scope.submiting = false;
        })
        .catch(function(error) {
          $scope.submiting = false;
        })
      $scope.userSubmitLoading = true;
      if ($scope.tempProfile) {
        $scope.profile.picture = $scope.tempProfile;
      }
      var changes = {
        picture: $scope.data.logo,
        name: $scope.data.store_name,
        //family_name: $scope.data.store_name,
        facebook: $scope.profile.facebook,
        instagram: $scope.profile.instagram,
        twitter: $scope.profile.twitter,
        username: $scope.profile.username
      }


      AccountsFactory.putProfile(changes)
        .then(function(data) {
          for (var k in data.data) {
            $scope.profile[k] = data.data[k];
          }
          Auth.setUser($scope.profile);
          $scope.userSubmitLoading = false;
          toastr.success('You have successfully updated your profile!')
        })
        .catch(function(error) {
          console.log(error)
          toastr.error('Username is already taken!')
          $scope.userSubmitLoading = false;
        });

    }

    $scope.reset = function() {
      $scope.cloneStore($scope.store);
      $scope.file = null;
      $scope.tempProfile = null;
      $scope.preview = "";
      //$scope.profile = _.cloneDeep(JSON.parse($cookies.get('userInfo')));
    }

    $rootScope.$on('updateStore', function(event, data) {
      $scope.store = data.stores[0];
      $scope.cloneStore($scope.store)
    });

    $scope.cloneStore($scope.store)










    $scope.userSubmitLoading = false;

    $scope.$watch('file', function() {
      var file = $scope.file;
      if (file) {
        if (file.size < 10000000) {
          UploadFatory.getSignUrl(file.type)
            .then(function(data) {
              $scope.uploadImage(data.data, file)
            })
            .catch(function(error) {
              console.log(error)
            })
        } else {
          //$window.alert("The file needs to be under 10MB")
        }
      }
    });

    $scope.uploadImage = function(data, file) {
      $scope.uploading = true;
      var path = UploadFatory.getImagePath(file.type);
      Upload.upload({
          url: Config.S3_URL,
          method: 'POST',
          fields: {
            'key': path,
            'acl': 'public-read',
            'Content-Type': file.type,
            'AWSAccessKeyId': data.AWSAccessKeyId,
            'success_action_status': '201',
            'Policy': data.s3Policy,
            'Signature': data.s3Signature
          },
          sendObjectsAsJsonBlob: false,
          file: file
        })
        .progress(function(evt) {
          if (!$scope.uploading) {
            $scope.uploading = true;
          }
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        })
        .error(function(e) {
          // console.error("Failed:", e);
          $scope.uploading = false;
        })
        .success(function(data, status, headers, config) {
          $scope.uploading = false;
          // $scope.product_imgs[index].url = Config.IMAGE_URL + '/' + path;
          $scope.data.logo = Config.IMAGE_URL + '/' + path;
          $scope.file = null;
        });
    };

    $scope.submitUserProfile = function() {
      $scope.userSubmitLoading = true;
      if ($scope.tempProfile) {
        $scope.profile.picture = $scope.tempProfile;
      }
      var changes = {
        picture: $scope.data.logo,
        given_name: $scope.data.store_name,
        family_name: $scope.data.store_name,
        //email: $scope.profile.email,
        username: $scope.profile.username,
      }
      AccountsFactory.putProfile(changes)
        .then(function(data) {
          for (var k in data.data) {
            $scope.profile[k] = data.data[k];
          }
          Auth.setUser($scope.profile);
          $scope.userSubmitLoading = false;
          toastr.success('You have successfully updated your profile!')
        })
        .catch(function(error) {
          console.log(error)
          toastr.success(error.data.message)
          $scope.userSubmitLoading = false;
        });
    }


    $scope.updateEmail = function() {

      $scope.addEmailError = "";
      $scope.data = {
        id: $scope.profile.id,
        email: $scope.profile.newemail,
        //password: $scope.profile.oldpassemail
      };


      AccountsFactory.addEmail($scope.data)
        .then(function(data) {


          $scope.emailVerify = data.data.message + ". Check your email for verification link in 24 hours";
          $scope.profile.email_update.email = $scope.data.email;


        })
        .catch(function(error) {
          $scope.addEmailError = error.data.message;
          $scope.error = error.data.message;

        })


    }
    $scope.verifyEmail = function() {

      AccountsFactory.verifyEmail()
        .then(function(data) {
          $scope.msg = data.data.message;

        })
        .catch(function(error) {
          $scope.error = error.data.message;
        })

    }
    $scope.updatePhone = function() {
      $scope.addPhoneError = "";
      $scope.data = {
        access_token: Auth.getToken()
          .access_token,
        toke: {
          id: $scope.profile.id
        },
        id: $scope.profile.id,
        code: $scope.input.country.code,
        phone_number: $scope.input.phone,
        country: $scope.input.country
      };
      $scope.data.phone_number = '+' + $scope.input.country.code + $scope.input.phone;
      $scope.data.input = $scope.data.phone_number;
      AccountsFactory.setMobile($scope.data)
        .then(function(data) {
          var modalData = {
            token: Auth.getToken(),
            userInfo: $scope.data
          };
          $scope.phoneVerify(modalData);
        })
        .catch(function(error) {
          $scope.addPhoneError = error.data.message
          $scope.error = error.data.message;
        })
    };

    $scope.phoneVerify = function(data) {

      ModalStorage.set(data);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalPhoneVerify/modalPhoneVerify.html",
          controller: "ModalPhoneVerifyCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
            $scope.profile.country = result[1].data.country;
            $scope.profile.phone_number = result[1].data.phone_number;
            $scope.changePhone = false;
            Auth.setUser($scope.profile);
          });
        });
    };


    $scope.updatePassword = function() {

      $scope.addPasswordError = "";
      $scope.data = {
        confirm: $scope.profile.newpass,
        newpass: $scope.profile.newpass,
        oldpass: $scope.profile.oldpass
      };


      AccountsFactory.addPassword($scope.data)
        .then(function(data) {

          console.log(data);
          $scope.passwordVerify = data.data.message;
        })
        .catch(function(error) {
          $scope.addPasswordError = "Old passwords does not match."
          $scope.error = error.data.message;

        })
    }





  }]);