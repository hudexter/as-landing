'use strict';

antsquare
	.controller('StorePromotionsCtrl', ['$scope', '$rootScope', 'Auth', 'StoreFactory', 'CardIntent', 'ModalStorage', 'ModalService', function ($scope, $rootScope, Auth, StoreFactory, CardIntent, ModalStorage, ModalService) {
		$scope.user = Auth.getUserFn();;

		$scope.store = Auth.getStoreFn()
			.stores[0];
		$rootScope.$on('updateStore', function (event, data) {
			$scope.store = data.stores[0];
		});

		$scope.cards = {};

		$scope.pageParams = {
			has_more: false,
			// per_page: 5,
			page: 1,
			u_id: $scope.user.id
		};

		$scope.getAnnouncement = function () {
			StoreFactory.getAnnouncement($scope.store.id, $scope.pageParams)
				.then(function (data) {
					$scope.promotions = [];
					$scope.promotions = data.data.announcements.rows;
				});
		}

		$scope.getAnnouncement();

		$scope.PromotionDeletePrompt = function (promo, index) {
			ModalStorage.set(promo, StoreFactory.deleteAnnouncement);
			ModalService.showModal({
					templateUrl: "app/components/modal/modalSure/modalSure.html",
					controller: "ModalSureCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						ModalStorage.end();
						if (result) {
							setTimeout(function () {
								$scope.promotions = [];
								$scope.getAnnouncement();
							})
						}
					});
				});
		}


	}]);
