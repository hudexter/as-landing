'use strict';

antsquare
  .controller('StoreProductsCtrl', ['$scope', '$rootScope', 'Auth', 'StoreFactory', 'CardIntent', function ($scope, $rootScope, Auth, StoreFactory, CardIntent) {
    $scope.store = Auth.getStoreFn().stores[0];
    $rootScope.$on('updateStore', function (event, data) {
      $scope.store = data.stores[0];
    });

    $scope.cards = {};

    $scope.getProducts = function () {
      StoreFactory.myProducts()
        .then(function (data) {
          $scope.products = data.data.products;

          $scope.cards = data.data;
          $scope.cards.products = data.data.products.map(function (card) {
            return CardIntent.set(card);
          });

        });
    }

    $scope.getProducts();

  }]);