'use strict';

antsquare
	.controller('StoreRequestsCtrl', ['$scope', '$rootScope', 'Auth', 'RequestFactory', 'CardIntent', function ($scope, $rootScope, Auth, RequestFactory, CardIntent) {
    $scope.user = Auth.getUserFn();;

		$scope.store = Auth.getStoreFn().stores[0];
		$rootScope.$on('updateStore', function (event, data) {
			$scope.store = data.stores[0];
		});

		$scope.cards = {};

    $scope.pageParams = {
      has_more: false,
      // per_page: 5,
      page: 1,
      u_id: $scope.user.id,
			uid: $scope.user.id
    };

		$scope.getRequest = function () {
			RequestFactory.getAllRequest($scope.pageParams)
				.then(function (data) {
					$scope.products = data.data.requests;
					$scope.cards = data.data;
					$scope.cards.products = data.data.requests.map(function (card) {
						return CardIntent.set(card);
					});
				});
		}

		$scope.getRequest();

	}]);
