'use strict';

antsquare
  .controller('StoreMomentCtrl', ['$scope', '$rootScope', 'Auth', 'StoreFactory', 'CardIntent', function ($scope, $rootScope, Auth, StoreFactory, CardIntent) {
    $scope.store = Auth.getStoreFn().stores[0];
    $rootScope.$on('updateStore', function (event, data) {
      $scope.store = data.stores[0];
    });

    $scope.cards = {};
    var scrollLoad = false;

    function addLoadMoreCard(scrollLoad) {
      if ($scope.cards.has_more && !scrollLoad) {
        if (scrollLoad === undefined) {
          $scope.cards.products.push({
            type: 'has_more_temp'
          })
        } else {
          $scope.cards.products.push({
            type: 'has_more'
          })
        }

      } else if (!$scope.cards.has_more) {
        $scope.cards.products.push({
          type: 'no_more'
        })
      }
    };
    $scope.productsPageParams = {
      page: 1,
      per_page: 20
    };

    $scope.getMoments = function () {

      StoreFactory.myMoments($scope.productsPageParams)
        .then(function (data) {
          $scope.products = data.data.data;

          $scope.cards = data.data;
          $scope.cards.products = data.data.data.map(function (card) {
            return CardIntent.set(card);
          });
          addLoadMoreCard();
          setTimeout(function () {
            refreshPlate();
          }, 500);
          $scope.loading = false;

        });
    }
    $scope.$on('loadMoreTemp', function () {
      $scope.loading = true;
      $scope.loadingMoreProducts = true;
      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

      StoreFactory.myMoments($scope.productsPageParams)
        .then(function (data) {
          $scope.cards.products.pop();

          $scope.newCards = data.data;

          $scope.newCards.products = data.data.data.map(function (card) {
            return CardIntent.set(card);
          });

          $scope.cards.has_more = $scope.newCards.has_more;
          $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
          addLoadMoreCard();

          refreshPlate();
          $scope.loadingMoreProducts = false;
          $scope.loading = false;
        })
        .catch(function (error) {
          $scope.loading = false;
          console.log(error)
          if (error.status === 400) {
            $scope.error = error.data.message;
          } else {
            $scope.error = error.data.message;
          }
          $scope.loadingMoreProducts = false;
        });

    });

    $scope.$on('loadMore', function () {
      if (!scrollLoad) {
        $scope.cards.products.pop();
        $scrollLoad = true;
      }
      $scope.loadMore();
    });
    $scope.loadingMoreProducts = false;

    $scope.loading = null;
    $scope.loadMore = function () {

      if (scrollLoad && !$scope.loadingMoreProducts) {
        $scope.loading = true;
        $scope.loadingMoreProducts = true;
        $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
        StoreFactory.myMoments($scope.productsPageParams)
          .then(function (data) {
            $scope.newCards = data.data;
            $scope.newCards.products = data.data.data.map(function (card) {
              return CardIntent.set(card);
            });
            $scope.cards.has_more = $scope.newCards.has_more;
            $scope.cards.products = $scope.cards.products.concat(scope.newCards.products);
            addLoadMoreCard(scrollLoad);
            refreshPlate();
            $scope.loadingMoreProducts = false;
            $scope.loading = false;
          })
          .catch(function (error) {
            $scope.loading = false;
            console.log(error)
            if (error.status === 400) {
              $scope.error = error.data.message;
            } else {
              $scope.error = error.data.message;
            }
            $scope.loadingMoreProducts = false;
          });
      }

    }

    $scope.getMoments();

  }]);