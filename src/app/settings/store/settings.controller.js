'use strict';

antsquare
	.controller('MyStoreSettingsCtrl', ['$scope', '$rootScope', 'Auth', function ($scope, $rootScope, Auth) {
		// $scope.store = Auth.getStoreFn().stores[0];
		$rootScope.$on('updateStore', function (event, data) {
			$scope.store = data.stores[0];
		});
		//
	}]);
