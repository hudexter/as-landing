'use strict';

antsquare
  .controller('ProfileCtrl', ['$scope', 'Auth', '$rootScope', 'AccountsFactory', 'deviceDetector', 'ModalStorage', 'ModalService', 'Upload', 'UploadFatory', 'Config', 'toastr', '$cookies', function($scope, Auth, $rootScope, AccountsFactory, deviceDetector, ModalStorage, ModalService, Upload, UploadFatory, Config, toastr, $cookies) {

    $scope.uploading = false;
    $scope.input = {};
    $scope.preview = "";
    // Check Mobile

    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    if (Auth.isLoggedIn()) {
      $scope.profile = _.cloneDeep(JSON.parse($cookies.get('userInfo')));
      // $scope.profile = _.cloneDeep(Auth.getUser);
    }

    $scope.reset = function() {
      $scope.profile = _.cloneDeep(JSON.parse($cookies.get('userInfo')));
      // $scope.profile = _.cloneDeep(Auth.getUser);
      $scope.file = null;
      $scope.tempProfile = null;
    }

    $scope.userSubmitLoading = false;

    $scope.$watch('file', function() {
      var file = $scope.file;
      if (file) {
        if (file.size < 10000000) {
          UploadFatory.getSignUrl(file.type)
            .then(function(data) {
              $scope.uploadImage(data.data, file)
            })
            .catch(function(error) {
              console.log(error)
            })
        } else {
          $window.alert("The file needs to be under 10MB")
        }
      }
    });

    $scope.uploadImage = function(data, file) {
      $scope.uploading = true;
      var path = UploadFatory.getImagePath(file.type);
      Upload.upload({
          url: Config.S3_URL,
          method: 'POST',
          fields: {
            'key': path,
            'acl': 'public-read',
            'Content-Type': file.type,
            'AWSAccessKeyId': data.AWSAccessKeyId,
            'success_action_status': '201',
            'Policy': data.s3Policy,
            'Signature': data.s3Signature
          },
          sendObjectsAsJsonBlob: false,
          file: file
        })
        .progress(function(evt) {
          if (!$scope.uploading) {
            $scope.uploading = true;
          }
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        })
        .error(function(e) {
          // console.error("Failed:", e);
          $scope.uploading = false;
        })
        .success(function(data, status, headers, config) {
          $scope.uploading = false;
          // $scope.product_imgs[index].url = Config.IMAGE_URL + '/' + path;
          $scope.data.logo = Config.IMAGE_URL + '/' + path;
          $scope.file = null;
        });
    };

    $scope.submitUserProfile = function() {
      $scope.userSubmitLoading = true;
      if ($scope.tempProfile) {
        $scope.profile.picture = $scope.tempProfile;
      }
      var changes = {
        picture: $scope.profile.picture,
        given_name: $scope.profile.given_name,
        family_name: $scope.profile.family_name,
        email: $scope.profile.email,
        username: $scope.profile.username,
      }
      AccountsFactory.putProfile(changes)
        .then(function(data) {
          for (var k in data.data) {
            $scope.profile[k] = data.data[k];
          }
          Auth.setUser($scope.profile);
          $scope.userSubmitLoading = false;
          toastr.success('You have successfully updated your profile!')
        })
        .catch(function(error) {
          console.log(error)
          toastr.success(error.data.message)
          $scope.userSubmitLoading = false;
        });
    }

    $scope.updatePhone = function() {
      $scope.addPhoneError = "";
      $scope.data = {
        access_token: Auth.getToken()
          .access_token,
        toke: {
          id: $scope.profile.id
        },
        id: $scope.profile.id,
        code: $scope.input.country.code,
        phone_number: $scope.input.phone,
        country: $scope.input.country
      };
      $scope.data.phone_number = '+' + $scope.input.country.code + $scope.input.phone;
      $scope.data.input = $scope.data.phone_number;
      AccountsFactory.setMobile($scope.data)
        .then(function(data) {
          var modalData = {
            token: Auth.getToken(),
            userInfo: $scope.data
          };
          $scope.phoneVerify(modalData);
        })
        .catch(function(error) {
          $scope.addPhoneError = error.data.message
          $scope.error = error.data.message;
        })
    };

    $scope.phoneVerify = function(data) {
      ModalStorage.set(data);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalPhoneVerify/modalPhoneVerify.html",
          controller: "ModalPhoneVerifyCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
            $scope.profile.country = result[1].data.country;
            $scope.profile.phone_number = result[1].data.phone_number;
            $scope.changePhone = false;
            Auth.setUser($scope.profile);
          });
        });
    };

    $scope.updatePassword = function() {
      $scope.addPasswordError = "";
      $scope.data = {
        confirm: $scope.profile.newpass,
        newpass: $scope.profile.newpass,
        oldpass: $scope.profile.oldpass
      };

      AccountsFactory.addPassword($scope.data)
        .then(function(data) {
          console.log(data)
          $scope.passwordVerify = data.data.message;
        })
        .catch(function(error) {
          $scope.addPasswordError = error.data.message
          $scope.error = error.data.message;
        })
    }

  }]);