'use strict';

antsquare
  .controller('LinkingCtrl', ['$scope', 'Auth', '$rootScope', 'AccountsFactory', 'ezfb', '$cookies', '$q', 'Config', function($scope, Auth, $rootScope, AccountsFactory, ezfb, $cookies, $q, Config) {
    $scope.timeline = {};
    $scope.selectedPage = {};
    $scope.token = Auth.getToken();
    $scope.user = Auth.getUserFn();;
    $scope.pages = [];
    $scope.unlinkFacebook = function(id) {
      $scope.setFbPage = false;
      AccountsFactory.putProfile({
          fb_publish_enabled: false,
          fb_share_to: null
        })
        .then(function(data) {
          $scope.user.fb_publish_enabled = false;
          $scope.user.fb_share_to = null;
          Auth.setUser($scope.user)
          $cookies.remove('fb_token');
        })
        .catch(function(error) {
          console.log(error)
        })

    };

    $scope.linkFacebook = function() {
      ezfb.login(function(response) {

        $cookies.put('fb_token', response.authResponse.accessToken, {
          domain: Config.COOKIE_DOMAIN
        });
        $scope.token = Auth.getToken();
        ezfb.api('/me', function(FBresponse) {
          FBresponse.access_token = response.authResponse.accessToken;
          AccountsFactory.putFBInfo(FBresponse)
            .then(function(response) {
              $scope.user.fb_share_to = FBresponse.id;
              $scope.user.facebook = FBresponse.name;
              $scope.shareTo = {
                name: FBresponse.name,
                id: FBresponse.id,
              };
              $scope.socialInfo.facebook = FBresponse;
              $scope.user.fb_publish_enabled = true;
              $scope.user.fb_share_to = FBresponse.id;
              AccountsFactory.putProfile({
                  fb_publish_enabled: true,
                  fb_share_to: $scope.shareTo.id,
                })
                .then(function(data) {
                  Auth.setUser($scope.user);
                })
                .catch(function(error) {
                  console.log(error)
                });
            })
            .catch(function(error) {
              $scope.error = error.data.message;
              $scope.message = error.data.message;
            })
        });
      }, {
        scope: 'publish_actions,manage_pages,publish_pages',
        return_scopes: true,
        auth_type: 'rerequest',
        enable_profile_selector: true
      });
    };

    $scope.pages = [];
    $scope.setFbPage = false;

    $scope.getPages = function() {
      $scope.pages = [];
      ezfb.api('/me?fields=id,name,picture', $scope.fbToken, function(response) {
        if (!response.error) {
          $scope.timeline = {
            id: response.id,
            name: response.name,
            logo: response.picture.data.url
          };
        }

        $scope.pages.push($scope.timeline);
        // 		// Get Pages
        ezfb.api('/' + $scope.socialInfo.facebook.id + '/accounts', $scope.fbToken, function(pageList) {
          if (!pageList.error) {
            pageList.data.forEach(function(value, index, array) {
              ezfb.api('/' + value.id + '/picture', $scope.fbToken, function(pageDetail) {
                pageList.data[index].logo = pageDetail.data.url;
                $scope.pages.push(pageList.data[index]);
                if (value.id == $scope.user.fb_share_to) {
                  $scope.selectedPage = pageList.data[index];
                }
              });
            });
          }


        });
      });
    }

    $scope.shareToToggle = function() {
      $scope.setFbPage = !$scope.setFbPage;
      $scope.fbToken = {
        access_token: $scope.token.fb_token
      };
      if ($scope.setFbPage) {
        $scope.getPages();
      }
    };

    function fbTokenRefresh() {
      var deferred = $q.defer();
      AccountsFactory.fbTokebRefresh()
        .then(function(data) {
          $cookies.put('fb_token', response.authResponse.accessToken, {
            domain: Config.COOKIE_DOMAIN
          });
          $scope.token = Auth.getToken();
          deferred.resolve(data)
        })
        .catch(function(error) {
          deferred.reject(error)
        });
      return deferred.promise
    };

    $scope.settingPage = false;
    $scope.selectPage = function(selectedPage) {
      $scope.selectedPage = JSON.parse(selectedPage)
      $scope.settingPage = true;
      AccountsFactory.putProfile({
          fb_publish_enabled: true,
          facebook: $scope.selectedPage.id
        })
        .then(function(data) {
          console.log(data);
          $scope.settingPage = false;
          $scope.user.fb_publish_enabled = true;
          $scope.user.fb_share_to = $scope.selectedPage.id;
          $scope.user.facebook = $scope.selectedPage.id;
          Auth.setUser($scope.user)
          console.log($scope.user);
          $scope.setFbPage = false;
        })
        .catch(function(error) {
          $scope.settingPage = false;
        })
    };

    AccountsFactory.getSocial()
      .then(function(response) {
        $scope.socialInfo = response.data;
        if (!_.isEmpty($scope.socialInfo.facebook)) {
          fbTokenRefresh()
            .then(function(data) {
              $scope.getPages();
            })
            .catch(function(error) {
              $scope.getPages();
            })

        }
      });




  }]);