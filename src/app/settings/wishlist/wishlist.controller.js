'use strict';

antsquare
	.controller('WishlistCtrl', ['$scope', 'AccountsFactory', 'deviceDetector', 'Config', 'toastr', 'ProductsFactory', 'angularGridInstance', function ($scope, AccountsFactory, deviceDetector, Config, toastr, ProductsFactory, angularGridInstance) {

		$scope.pageDetail = {
			page: 1,
			per_page: 10,
		};
		
		$scope.search = function () {
			$scope.likes = {};
			AccountsFactory.getMyLikes($scope.pageDetail)
				.then(function (data) {
					$scope.likes = data.data;
					$scope.pageDetail = {
						has_more: data.data.has_more,
						page: data.data.page,
						per_page: data.data.per_page,
						total: data.data.total,
						pages: parseInt(Math.ceil(data.data.total/data.data.per_page))
					}
				})
				.catch(function (error) {
					console.log(error)
				});
		};

		$scope.search();

}]);
