'use strict';

antsquare
	.controller('PaymentCtrl', ['$scope', '$state', 'ModalStorage', 'ModalService', 'AccountsFactory', 'PaymentFactory', 'toastr', function ($scope, $state, ModalStorage, ModalService, AccountsFactory, PaymentFactory, toastr) {

		$scope.cards = [];
		$scope.credits = "";

		$scope.getCards = function () {
			$scope.cards = [];
			AccountsFactory.getCards()
				.then(function (data) {
					$scope.cards = data.cards.data || [];
				});
		}

		$scope.getCredit = function () {
			$scope.credits = "";
			PaymentFactory.myCredit()
				.then(function (data) {
					$scope.credits = data.data;
				})
				.catch(function (error) {
				});
		}

		$scope.creditLoading = false;
		$scope.submitCode = function () {
			$scope.creditLoading = true;
			PaymentFactory.postClaimCoupon($scope.code)
				.then(function (data) {
					$scope.getCredit();
					creditForm.reset();
					toastr.success('You have submitted your code!')
					$scope.creditLoading = false;
				})
				.catch(function (error) {
					$scope.creditError = error.data.message;
					$scope.creditLoading = false;
				});
		}

		$scope.addCard = function () {
			ModalService.showModal({
					templateUrl: "app/components/modal/modalCreditCard/modalCreditCard.html",
					controller: "ModalCreditCardCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						if (result) {
							$scope.getCards();
						}
					});
				});
		}

		$scope.removeConfirm = function (card) {
			var data = {
				title: 'Remove Payment Method',
				msg: 'Are you sure you want to remove this payment method?',
				btn_name: 'Remove Card',
				card: card,
				fn: function (card) {
					return AccountsFactory.removeCard(card.id)
				}
			};
			ModalStorage.set(data);
			ModalService.showModal({
					templateUrl: "app/components/modal/modalConfirm/modalConfirm.html",
					controller: "ModalConfirmCtrl"
				})
				.then(function (modal) {
					modal.close.then(function (result) {
						if (result) {
							$scope.getCards();
						}
						ModalStorage.end();
					});
				});
		}

		$scope.getCredit();
		$scope.getCards();
	}]);
