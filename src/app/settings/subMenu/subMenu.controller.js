'use strict';

antsquare
	.controller('SubMenuCtrl', ['$scope', '$rootScope', '$state', 'Auth', function ($scope, $rootScope, $state, Auth) {

		$scope.user = Auth.getUserFn();
		$scope.store = Auth.getStoreFn();
		$scope.onTx = false;
		if ($state.current.name.indexOf('settings.transaction') === 0) {
			$scope.onTx = true;
		}
		$rootScope.$on('$stateChangeStart',
			function (event, toState, toStateParams, fromState, fromStateParams) {
				if (toState.name.indexOf('settings.transaction') === 0) {
					$scope.onTx = true;
				} else {
					$scope.onTx = false
				}
			});


	}]);
