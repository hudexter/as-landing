'use strict';

angular.module('antsquare')
  .controller('EmailVerifyCtrl', ['$scope', '$state', '$stateParams', 'AccountsFactory',
    function($scope, $state, $stateParams, AccountsFactory) {

      $scope.verifyInfo = {
        id: $stateParams.id,
        email: $stateParams.email,
        code: $stateParams.code
      };

      $scope.data = {};
      AccountsFactory.getEmailVerify($scope.verifyInfo)
        .then(function(data) {
          //  console.log(data);
          $scope.data = data;
          $scope.data.message = "Email Verification was successful.";
        })
        .catch(function(error) {

          $scope.data = error;
          $scope.data.message = error.data.message;
        });

      $scope.toHome = function() {
        $state.go('menu');
      };



    }
  ]);