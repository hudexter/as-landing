'use strict';
antsquare
	.factory('branchFactory', ['Config', '$rootScope', '$location', '$q',
	function (Config, $rootScope, $location, $q) {
			$rootScope.branchReady = null;

			return {
				init: function () {
					branch.init(Config.BRANCH_KEY, function (err, data) {
						if (data) {
							$rootScope.$broadcast('branchReady');
							$rootScope.branchReady = data;
						}
					});
				},
				link: function (state, id) {
					var alias = state + id;
					var uri = state + '/' + id;
					var branchLink = "http://get.antsquare.com/" + alias;

					var deferred = $q.defer();
					branch.link({
              branch_key: Config.BRANCH_KEY,
							channel: 'antsquare',
              url: branchLink,
							alias: alias,
							data: {
								'$desktop_url': $location.protocol() + '://' + $location.host() + '/' + uri,
								'$ios_url': 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8',
								'$ipad_url': 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8',
								"$android_url": "market://details?id=com.antsquare.android",
								'$android_deeplink_path': uri,
								'$ios_deeplink_path': uri,
								'$always_deeplink': true,
								'$deeplink_path': uri,
								'$marketing_title': uri
							}
						},
						function (err, link) {
							if (err) {
								// deferred.reject(err)
								deferred.resolve(branchLink);
							} else {
								// deferred.resolve(link);
								deferred.resolve(branchLink);
							}

						});
					return deferred.promise;
				}
			};
  }]);
