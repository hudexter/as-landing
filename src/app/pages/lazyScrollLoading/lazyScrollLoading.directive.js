'use strict';

angular.module('antsquare')
	.directive('lazyScrollLoading', [ function () {
		return {
			restrict: 'EA',
			template: "<div id='ng-busy-loading-template' class='text-center w-full set-flex flex-center'>\
				 <div class='m-t-xs m-b-xs'>\
					 <i class='icon-spin5 text-lg animate-spin grey'></i>\
				 </div>\
			</div>",
			link: function (scope, element, attr) {

			}
		}
  }]);
