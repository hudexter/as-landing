'use strict';

angular.module('antsquare')
  .directive('home2', ['ProductsFactory', 'AccountsFactory', 'deviceDetector', 'CardIntent', '$rootScope', 'GeoLocationService', '$compile', 'angularGridInstance', '$mixpanel', '$document', 'SearchDataIntent', 'ModalService',
    function(ProductsFactory, AccountsFactory, deviceDetector, CardIntent, $rootScope, GeoLocationService, $compile, angularGridInstance, $mixpanel, $document, SearchDataIntent, ModalService) {
      return {
        restrict: 'E',
        templateUrl: 'app/pages/home2/home2.html',
        link: function(scope, element, attr) {
          // Check Mobile
          scope.isMobile = null;
          scope.infiniteScrollDistance = 3;
          if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
            scope.isMobile = true;
            scope.infiniteScrollDistance = 8;
          } else {
            scope.infiniteScrollDistance = 3;
            scope.isMobile = false;
          }

          scope.isError = false;
          scope.errorMsg = "";

          // Get Location
          scope.coords = null;
          if (GeoLocationService.hasLocation()) {
            scope.coords = GeoLocationService.getCoords();
          }

          $rootScope.$on('setLocation', function() {
            scope.coords = GeoLocationService.getCoords();
          });

          // Set Page
          // Set formData
          scope.pagination = {};
          if (SearchDataIntent.isSet()) {
            scope.formData = SearchDataIntent.resetPagination();
            scope.formData.type = 'product';
          } else {
            scope.formData = SearchDataIntent.start();
            scope.formData.type = 'product';
            scope.formData.is_ugly = false;
          }
          scope.formData.is_pretty = true;

          // Update FormData
          scope.updateFormData = function(formData) {
            formData.query = SearchDataIntent.get().query;
            scope.formData = SearchDataIntent.set(formData);
            delete scope.formData.is_ugly
            scope.startSearch(scope.formData);
          };
          // Load Page
          scope.$watch('coords', function() {
            if (scope.coords) {
              SearchDataIntent.setLocation({
                lat: scope.coords.latitude,
                lon: scope.coords.longitude
              });
              // Commenting the start search as of no need on home page
              //scope.startSearch();
              // Commenting the getImages call as of now no need
              //scope.getImages();
            }
          });

          function refreshPlate() {
            if (angularGridInstance.home) {
              angularGridInstance.home.refresh();
            }
          };

          var scrollLoad = false;

          function addLoadMoreCard(scrollLoad) {
            if (scope.cards.has_more && !scrollLoad) {
              if (scrollLoad === undefined) {
                scope.cards.products.push({
                  type: 'has_more_temp'
                })
              } else {
                scope.cards.products.push({
                  type: 'has_more'
                })
              }

            } else if (!scope.cards.has_more) {
              scope.cards.products.push({
                type: 'no_more'
              })
            }
          };

          scope.imageCard = {};
          scope.getImages = function() {

            ProductsFactory.getImages()
              .then(function(data) {
                scope.imageCard = data.data.stores;
                console.log(scope.imageCard);
              })

          };
          scope.cards = {};

          scope.startSearch = function() {
            scope.loading = true;
            scrollLoad = false;
            SearchDataIntent.resetPagination();
            scope.cards = {};
            var formData = SearchDataIntent.get();
            if (SearchDataIntent.isValidate()) {
              ProductsFactory.search(SearchDataIntent.get())
                .then(function(data) {
                  scope.items = data.data[formData.type + 's'];

                  console.log("In start search");
                  // scope.cards = data.data;
                  // scope.cards.products = data.data.products.map(function(card) {
                  //   return CardIntent.set(card);
                  // });
                  // addLoadMoreCard();
                  // // addLoadMoreCard(scrollLoad);
                  // setTimeout(function() {
                  //   refreshPlate();
                  // }, 500);
                  // scope.loading = false;
                })
                .catch(function(error) {
                  console.log(error)
                  if (error.status === 400) {
                    scope.error = error.data.message;
                  } else {
                    scope.error = error.data.message;
                  }
                  scope.loading = false;
                });
            }
          };

          scope.$on('toTop', function() {
            $document.scrollTop(0, 300);
          })

          scope.$on('loadMoreTemp', function() {
            scope.loading = true;
            scope.loadingMoreProducts = true;
            SearchDataIntent.nextPage();
            ProductsFactory.getCommunities(SearchDataIntent.get())
              .then(function(data) {
                scope.cards.products.pop();
                scope.newCards = data.data;
                scope.newCards.products = data.data.products.map(function(card) {
                  return CardIntent.set(card);
                });
                scope.cards.has_more = scope.newCards.has_more;
                scope.cards.products = scope.cards.products.concat(scope.newCards.products);
                addLoadMoreCard();
                refreshPlate();
                scope.loadingMoreProducts = false;
                scope.loading = false;
              })
              .catch(function(error) {
                scope.loading = false;
                console.log(error)
                if (error.status === 400) {
                  scope.error = error.data.message;
                } else {
                  scope.error = error.data.message;
                }
                scope.loadingMoreProducts = false;
              });

          });

          scope.$on('loadMore', function() {
            if (!scrollLoad) {
              scope.cards.products.pop();
              scrollLoad = true;
            }
            scope.loadMore();
          });
          scope.loadingMoreProducts = false;

          scope.loading = null;
          scope.loadMore = function() {

            if (scrollLoad && !scope.loadingMoreProducts) {
              scope.loading = true;
              scope.loadingMoreProducts = true;
              SearchDataIntent.nextPage();
              ProductsFactory.getCommunities(SearchDataIntent.get())
                .then(function(data) {
                  scope.newCards = data.data;
                  scope.newCards.products = data.data.products.map(function(card) {
                    return CardIntent.set(card);
                  });
                  scope.cards.has_more = scope.newCards.has_more;
                  scope.cards.products = scope.cards.products.concat(scope.newCards.products);
                  addLoadMoreCard(scrollLoad);
                  refreshPlate();
                  scope.loadingMoreProducts = false;
                  scope.loading = false;
                })
                .catch(function(error) {
                  scope.loading = false;
                  console.log(error)
                  if (error.status === 400) {
                    scope.error = error.data.message;
                  } else {
                    scope.error = error.data.message;
                  }
                  scope.loadingMoreProducts = false;
                });
            }

          }

          scope.trackIos = function() {
            setTimeout(function() {
              location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
              $mixpanel.track('l-d-cta-IOS');
            }, 500);
          }

          scope.trackAndroid = function() {
            setTimeout(function() {
              location = 'https://play.google.com/store/apps/details?id=com.antsquare.android';
              $mixpanel.track('l-d-cta-Android');
            }, 500);
          }

          scope.selectedColelction = null;

          scope.collections = [{
              title: "people",
              search: {
                // query: '',
                type: 'user',
                max_price: null,
                min_price: null,
                sort: 'distance',
                is_pretty: true,
              }
            }, {
              title: "moments",
              search: {
                // query: null,
                type: 'moment',
                max_price: null,
                min_price: null,
                sort: 'new',
                is_pretty: true,
              }
            }, {
              title: "stores",
              search: {
                // query: null,
                type: "store",
                max_price: null,
                min_price: null,
                sort: 'distance',
                is_pretty: true,
              }
            },
            // , {
            //   title: "services",
            //   search: {
            //     // query: null,
            //     type: "service",
            //     max_price: null,
            //     min_price: null,
            //     sort: 'distance',
            //     is_pretty: true,
            //   }
            // }, {
            //   title: "products",
            //   search: {
            //     // query: null,
            //     type: "product",
            //     max_price: null,
            //     min_price: null,
            //     sort: 'distance',
            //     is_pretty: true,
            //   }
            // },
            {
              title: "tags",
              search: {
                // query: '',
                type: 'tag',
                max_price: null,
                min_price: null,
                sort: 'distance',
                is_pretty: true,
              }
            }
          ];

          scope.selectCollection = function(index, formData) {
            scope.selectedColelction = index;
            scope.updateFormData(formData);
            scope.formData = formData;
          };

          scope.businessInfoSlides = [{
            title: 'Showcase Your Business',
            // icon: 'https://antsquare-www.imgix.net/merchants3/ic-yourbusiness@2x.png',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/business1.png',
            content: 'Build your online store profile, connect social media pages, and provide information to your customers'
          }, {
            title: 'Manage Current Customers',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/business2.png',
            content: 'Communicate with your customers by using the direct and group chat feature'
          }, {
            title: 'Reach New Customers',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/business3.png',
            content: 'Share products, promotions, and events toward your community'
          }];

          scope.customerInfoSlides = [{
            title: 'Discover Local Business',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/customer1.png',
            content: 'Find new local business, business news, and events'
          }, {
            title: 'Engage with Community',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/customer2.png',
            content: 'Chat with small business owners, receive promotions, and build friendships'
          }, {
            title: 'Share Your Experience',
            icon: 'https://d37pip7vd9logx.cloudfront.net/imgs/customer3.png',
            content: 'Tell others about your favorite small businesses by posting towards your city'
          }];

          scope.getVideo = function() {

            ModalService.showModal({
                templateUrl: "app/components/modal/modalVideo/modalVideo.html",
                controller: "ModalVideoCtrl"
              })
              .then(function(modal) {
                modal.close.then(function(result) {

                });
              });
          }
          scope.videoMp4 = "https://d37pip7vd9logx.cloudfront.net/video/landing4.webm";

          //for getting app download link on mobile phones
          scope.sendDownloadLink = function() {
            var phoneNumber = scope.phoneNumber;
            if (typeof(phoneNumber) != 'undefined' && phoneNumber.length == 10) {
              AccountsFactory.getDownloadLinkOnPhone(phoneNumber)
                .then(function(data) {
                  console.log(data.status);
                  scope.errorMsg = "Download link sent!";
                  console.log("Download link sent!");

                })
                .catch(function(error) {
                  console.log(error);
                  scope.isError = true;
                  scope.errorMsg = error.data;
                })
            } else {
              scope.errorMsg = "Enter valid US number";
              scope.isError = true;
            }
          }

        }
      }
    }
  ]);