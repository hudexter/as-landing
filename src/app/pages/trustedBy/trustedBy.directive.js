'use strict';

angular.module('antsquare')
	.directive('trustedBy', [
	function () {
			return {
				restrict: 'E',
				templateUrl: 'app/pages/trustedBy/trustedBy.html',
				scope: {},
				link: function (scope, element, attr) {
					
					scope.partners = [
						{
							img: "https://antsquare-www.imgix.net/landing/trustedBy/logo_01.png?w=150"
						},
						{
							img: "https://antsquare-www.imgix.net/landing/trustedBy/logo_02.png?w=150"
						},
						{
							img: "https://antsquare-www.imgix.net/landing/trustedBy/logo_03.png?w=150"
						},
						{
							img: "https://antsquare-www.imgix.net/landing/trustedBy/logo_04.png?w=150"
						},
						{
							img: "https://antsquare-www.imgix.net/landing/trustedBy/logo_05.png?w=150"
						}
					];

				}
			}
	}]);
