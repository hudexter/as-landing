'use strict';

angular.module('antsquare')
	.directive('testimonials', ['deviceDetector', '$rootScope', '$window', function (deviceDetector, $rootScope, $window) {
		return {
			restrict: 'E',

			// templateUrl: 'app/pages/testimonials/testimonials.html',
			link: function (scope, element, attr) {


				scope.isMobile = null;


				scope.getTestimoinals = function(){
					var template = 'app/pages/testimonials/testimonials.html';
					if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
						template = 'app/pages/testimonials/testimonials.mobile.html';
					} else {
						template = 'app/pages/testimonials/testimonials.html';
					}
					return template
				}




				scope.myTemplate = "termspri.html"

				var win = angular.element($window);
				win.bind("resize", function (e) {
					// if (win.width() >= 480) {
					element.find('.content-container-copy')
						.height(element.find('#content-container')
							.height());
					// }
				})

				if (win.width() >= 480) {
					element.find('.content-container-copy')
						.height(element.find('#content-container')
							.height());
				}

			},
			template: "<div ng-include='getTestimoinals()'></div>"
		}
	}]);
