'use strict';

angular.module('antsquare')
  .directive('home', ['ProductsFactory', 'deviceDetector', 'CardIntent', '$rootScope', 'GeoLocationService', '$compile', 'angularGridInstance', '$mixpanel', '$document', 'SearchDataIntent',
    function (ProductsFactory, deviceDetector, CardIntent, $rootScope, GeoLocationService, $compile, angularGridInstance, $mixpanel, $document, SearchDataIntent) {
      return {
        restrict: 'E',
        templateUrl: 'app/pages/home/home.html',
        link: function (scope, element, attr) {
          // Check Mobile
          scope.isMobile = null;
          scope.infiniteScrollDistance = 3;
          if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
            scope.isMobile = true;
            scope.infiniteScrollDistance = 8;
          } else {
            scope.infiniteScrollDistance = 3;
            scope.isMobile = false;
          }

          // Get Location
          scope.coords = null;

          if (GeoLocationService.hasLocation()) {
            scope.coords = GeoLocationService.getCoords();
          }

          $rootScope.$on('setLocation', function () {
            scope.coords = GeoLocationService.getCoords();
          });

          // Set Page
          // Set formData
          scope.pagination = {};
          if (SearchDataIntent.isSet()) {
            scope.formData = SearchDataIntent.resetPagination();
          } else {
            scope.formData = SearchDataIntent.start();
            scope.formData.is_ugly = false;
          }

          // Update FormData
          scope.updateFormData = function (formData) {
            scope.formData = SearchDataIntent.set(formData);
            delete scope.formData.is_ugly
            scope.startSearch(scope.formData);
          };
          // Load Page
          scope.$watch('coords', function () {
            if (scope.coords) {
              SearchDataIntent.setLocation({
                lat: scope.coords.latitude,
                lon: scope.coords.longitude
              });
              scope.startSearch();
            }
          });

          function refreshPlate() {
            if (angularGridInstance.home) {
              angularGridInstance.home.refresh();
            }
          };

          var scrollLoad = false;

          function addLoadMoreCard(scrollLoad) {
            if (scope.cards.has_more && !scrollLoad) {
              if (scrollLoad === undefined) {
                scope.cards.products.push({
                  type: 'has_more_temp'
                })
              } else {
                scope.cards.products.push({
                  type: 'has_more'
                })
              }

            } else if (!scope.cards.has_more) {
              scope.cards.products.push({
                type: 'no_more'
              })
            }
          };

          scope.cards = {};

          scope.startSearch = function () {
            scope.loading = true;
            scrollLoad = false;
            SearchDataIntent.resetPagination();
            scope.cards = {};
            if (SearchDataIntent.isValidate()) {
              ProductsFactory.getCommunities(SearchDataIntent.get())
                .then(function (data) {
                  scope.cards = data.data;
                  scope.cards.products = data.data.products.map(function (card) {
                    return CardIntent.set(card);
                  });
                  addLoadMoreCard();
                  // addLoadMoreCard(scrollLoad);
                  setTimeout(function () {
                    refreshPlate();
                  }, 500);
                  scope.loading = false;
                })
                .catch(function (error) {
                  console.log(error)
                  if (error.status === 400) {
                    scope.error = error.data.message;
                  } else {
                    scope.error = error.data.message;
                  }
                  scope.loading = false;
                });
            }

          };

          scope.$on('toTop', function () {
            $document.scrollTop(0, 300);
          })

          scope.$on('loadMoreTemp', function () {
            scope.loading = true;
            scope.loadingMoreProducts = true;
            SearchDataIntent.nextPage();
            ProductsFactory.getCommunities(SearchDataIntent.get())
              .then(function (data) {
                scope.cards.products.pop();
                scope.newCards = data.data;
                scope.newCards.products = data.data.products.map(function (card) {
                  return CardIntent.set(card);
                });
                scope.cards.has_more = scope.newCards.has_more;
                scope.cards.products = scope.cards.products.concat(scope.newCards.products);
                addLoadMoreCard();
                refreshPlate();
                scope.loadingMoreProducts = false;
                scope.loading = false;
              })
              .catch(function (error) {
                scope.loading = false;
                console.log(error)
                if (error.status === 400) {
                  scope.error = error.data.message;
                } else {
                  scope.error = error.data.message;
                }
                scope.loadingMoreProducts = false;
              });

          });

          scope.$on('loadMore', function () {
            if (!scrollLoad) {
              scope.cards.products.pop();
              scrollLoad = true;
            }
            scope.loadMore();
          });
          scope.loadingMoreProducts = false;

          scope.loading = null;
          scope.loadMore = function () {

            if (scrollLoad && !scope.loadingMoreProducts) {
              scope.loading = true;
              scope.loadingMoreProducts = true;
              SearchDataIntent.nextPage();
              ProductsFactory.getCommunities(SearchDataIntent.get())
                .then(function (data) {
                  scope.newCards = data.data;
                  scope.newCards.products = data.data.products.map(function (card) {
                    return CardIntent.set(card);
                  });
                  scope.cards.has_more = scope.newCards.has_more;
                  scope.cards.products = scope.cards.products.concat(scope.newCards.products);
                  addLoadMoreCard(scrollLoad);
                  refreshPlate();
                  scope.loadingMoreProducts = false;
                  scope.loading = false;
                })
                .catch(function (error) {
                  scope.loading = false;
                  console.log(error)
                  if (error.status === 400) {
                    scope.error = error.data.message;
                  } else {
                    scope.error = error.data.message;
                  }
                  scope.loadingMoreProducts = false;
                });
            }

          }

          scope.trackIos = function () {
            $mixpanel.track('l-d-cta-IOS');
          }

          scope.trackAndroid = function () {
            $mixpanel.track('l-d-cta-Android');
          }

          scope.selectedColelction = null;

          scope.collections = [{
              title: "All",
              search: {
                query: null,
                type: '',
                // type: null,
                max_price: null,
                min_price: null,
                sort: 'distance'
              }
            }, {
              title: "Products",
              search: {
                query: null,
                type: "product",
                max_price: null,
                min_price: null,
                sort: 'distance'
              }
            }, {
              title: "Services",
              search: {
                query: null,
                type: "service",
                max_price: null,
                min_price: null,
                sort: 'distance'
              }
            },
            // {
            // 	title: "Request",
            // 	search: {
            // 		query: null,
            // 		type: "request",
            // 		max_price: null,
            // 		min_price: null,
            // 		sort: 'distance'
            // 	}
            // },
            {
              title: "New",
              search: {
                query: null,
                // type: '-request',
                type: null,
                max_price: null,
                min_price: null,
                sort: 'new'
              }
            }, {
              title: "Food",
              search: {
                query: 'Food',
                type: '-request',
                // type: null,
                max_price: null,
                min_price: null,
                sort: 'distance'
              }
            }
          ];

          scope.selectCollection = function (index, formData) {
            scope.selectedColelction = index;
            scope.updateFormData(formData);
            scope.formData = formData;
          };

        }
      }
    }
  ]);