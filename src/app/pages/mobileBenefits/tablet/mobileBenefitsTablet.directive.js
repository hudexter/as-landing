'use strict';

angular.module('antsquare')
	.directive('mobileBenefitsTablet', [
	function () {
			return {
				restrict: 'E',
				templateUrl: 'app/pages/mobileBenefits/tablet/tablet.html'
			}
	}]);
