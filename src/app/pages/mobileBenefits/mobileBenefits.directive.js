'use strict';

angular.module('antsquare')
	.directive('mobileBenefits', [ 'deviceDetector',
	function (deviceDetector) {
			return {
				restrict: 'E',
				templateUrl: 'app/pages/mobileBenefits/mobileBenefits.html',
				scope: {},
				link: function (scope, element, attr) {

					scope.isMobile = null;

					if (deviceDetector.device === "android"  || deviceDetector.device === "iphone" || deviceDetector.device === "ipad" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
						scope.isMobile = true;
					} else {
						scope.isMobile = false;
					}

					scope.point1 = {
						title: "Be a seller & a buyer",
						content: "Browse or request for products and services. Create your own space to sell.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_Pin.svg"
					};
					scope.point2 = {
						title: "Chat with seller",
						content: "Chat before checkout and agree in price, location and date.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_chat.svg"
					};
					scope.point3 = {
						title: "Mobile Shop",
						content: "Shop for products or set up your store with your Android or Apple mobile.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_MobileShop.svg"
					};
					scope.point4 = {
						title: "Pay Securely",
						content: "Avoid cash. Use Antsquare simple and quick payment platform.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_Pay.svg"
					};
					scope.point5 = {
						title: "Join Communities",
						content: "Follow groups that share your own interests within your community.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_Pin.svg"
					};
					scope.point6 = {
						title: "Gain Exposure",
						content: "Be known in your community.",
						img: "https://antsquare-www.imgix.net/mobileBenefits/ic_Exposure.svg"
					};

					scope.sellLink = "https://dashboard.antsquare.com/merchant";
					scope.videoMp4 = "https://antsquare-www.imgix.net/video/test11.mp4";
					scope.videoPoster = "https://antsquare-www.imgix.net/video/videoposter.png?w=450&auto=format,compress";

					function isRetinaDisplay() {
						if (window.matchMedia) {
							var mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
							return (mq && mq.matches || (window.devicePixelRatio > 1));
						}
					}
					if (isRetinaDisplay()) {
						scope.videoMp4 = "https://antsquare-www.imgix.net/video/appSlide3@2x.mp4";
					} else {
						scope.videoMp4 = "https://antsquare-www.imgix.net/video/appSlide2.mp4";
					}

					scope.phoneImg = "https://antsquare-www.imgix.net/mobileBenefits/Phones_Big_original.png?w=470&auto=format,compress";

				}
			}
	}]);
