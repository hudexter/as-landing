'use strict';

angular.module('antsquare')
	.directive('mobileBenefitsDesktop', [
	function () {
			return {
				restrict: 'E',
				templateUrl: 'app/pages/mobileBenefits/desktop/desktop.html'
			}
	}]);
