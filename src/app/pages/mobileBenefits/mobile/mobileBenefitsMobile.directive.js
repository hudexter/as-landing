'use strict';

angular.module('antsquare')
	.directive('mobileBenefitsMobile', [
	function () {
			return {
				restrict: 'E',
				templateUrl: 'app/pages/mobileBenefits/mobile/mobile.html'
			}
	}]);
