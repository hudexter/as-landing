antsquare
	.factory('GeoLocationFactory', ['Config', '$http',
  function (Config, $http) {
			return {
				getLocationByIp: function () {
					return $http({
						url: Config.IPINFO_URL,
						method: "GET",
						params: {
							token: Config.IPINFO_KEY
						}
					})
				}
			};
  }]);

antsquare
	.factory('GeoLocationService', ['$rootScope', '$q',
	  function ($rootScope, $q) {
			var location = null;
			var coords = {};

			return {
				set: function (data, source) {
					location = data;
					$rootScope.$broadcast('setLocation');
				},
				setCoords: function (data, source) {
					if (source === 'html5') {
						coords = data;
					} else {
						coords = data;
					}
				},
				hasLocation: function(){
					return !!location
				},
				get: function () {
					return location;
				},
				getCoords: function () {
					return coords;
				},
				getCoordsFromIp: function (data) {
					var result = data.split(',');
					var setCoords = {
						latitude: result[0],
						longitude: result[1]
					};
					return setCoords
				},
				reverseGeolocation: function(){
					var geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
					var deferred = $q.defer();
					geocoder.geocode({
						'latLng': latlng
					}, function (results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							deferred.resolve(results)
						}
					});
					return deferred.promise
				},
				getCountry: function(){
					var deferred = $q.defer();
					location.forEach(function(obj){
						for (var key in obj) {
							if (key === 'types'){
								obj[key].forEach(function(type){
										if (type ==='country'){
											deferred.resolve(obj.formatted_address)
										}
								})
							}
						}
					})
					return deferred.promise
				},
				getCity: function(){
					var deferred = $q.defer();
					location.forEach(function(obj){
						for (var key in obj) {
							if (key === 'types'){
								obj[key].forEach(function(type){
										if (type ==='locality' || type ==='political'){
											deferred.resolve(obj.formatted_address)
										}
								})
							}
						}
					})
					return deferred.promise
				}

			};
	}]);
