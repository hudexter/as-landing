'use strict';

angular.module('antsquare')
	.directive('scrollEvent', ['$window', function ($window) {
		return {
			restrict: 'AE',
      scope: {
        scrollUpFn: "&",
        scrollDownFn: "&",
				threshold: "@"
      },
			link: function (scope, element, attr) {

				

				var scrollBucket = [0, 0];
				var difference = 0;

				angular.element($window)
					.bind("scroll", function () {
						scrollBucket[1] = scrollBucket[0];
						scrollBucket[0] = this.pageYOffset;

						if (scrollBucket[0] - scrollBucket[1] > 0) {
							difference -= Math.abs(scrollBucket[1] - scrollBucket[0])
						} else {
							difference += Math.abs(scrollBucket[0] - scrollBucket[1])
						}
						// if ((difference > 15 && difference < 35) || difference > 130) {
						if (difference > 30 ) {
							difference = 0;
              scope.scrollUpFn();
							scrollBucket = [this.pageYOffset, this.pageYOffset];
						} else if (difference < -30 && this.pageYOffset > scope.threshold) {
              scope.scrollDownFn();
							difference = 0;
							scrollBucket = [this.pageYOffset, this.pageYOffset];
						}
						else if (difference > 35 ) {
							difference = 0;
							scrollBucket = [this.pageYOffset, this.pageYOffset];
						}

						scope.$apply();
					});

			}
		}
  }]);
