'use strict';

angular.module('antsquare')
	.directive('ngBusy', ['$window', '$rootScope', function ($window, $rootScope) {
		return {
			restrict: 'EA',
			transclude: true,
			template: '<div ng-transclude></div>',
			scope: {
				loading: '=',
				delay: '@'
			},
			link: function (scope, element, attr) {

				var showLoading = function () {
					if (scope.loading) {
						element.find('#inject-loading')
							.append(
								"<div id='ng-busy-loading-template' class='text-center'>\
                   <div class='m-t-lg m-b-lg'>\
                     <i class='icon-spin5 text-lg animate-spin grey'></i>\
                   </div>\
                </div>"
							);
					}
				};

				scope.$watch('loading', function () {
					scope.delay = scope.delay || 0;
					if (scope.loading) {
						setTimeout(function () {
							showLoading();
						}, scope.delay)
					} else {
						element.find('#ng-busy-loading-template')
							.remove();
					}
				});




				




			}
		}
  }]);
