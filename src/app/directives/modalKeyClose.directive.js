'use strict';

angular.module('antsquare')
	.directive('modalKeyClose', [function () {
		return {
			restrict: 'AE',
			scope: {
				closeByKey: '&'
			},
			link: function (scope, element, attr) {
				angular.element(document)
					.bind('keydown keypress', function (e) {
							if (e.keyCode == 32 || e.which == 32 || e.charCode == 32) {
								e.preventDefault();
								return false
							}
							if (e.keyCode == 27 || e.which == 27 || e.charCode == 27) {
								scope.closeByKey();
								angular.element(document).unbind('keydown keypress');
							}
					})

			}
		}
  }]);
