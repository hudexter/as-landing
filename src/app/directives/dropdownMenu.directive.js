'use strict';

angular.module('antsquare')
	.directive('dropdownMenu', ['$window', function ($window) {
		return {
			restrict: 'AE',
			scope: {
				checkList: '='
			},
			link: function (scope, element, attr) {

				var clickPause = false;
				element.addClass('non-active');

				angular.element(document)
					.bind('mouseup', function (event) {
						var target = angular.element(event.target)
						if (!scope.checkList) {
							if (!target.hasClass('dropdown-menu-list-item') && !target.hasClass('dropdown-menu-triangle') && !element.hasClass('non-active')) {
								setTimeout(function () {
								element.addClass('non-active');
							},30);
							} else if (target.hasClass('dropdown-menu-list-item') && target.hasClass('coutnry-flag')) {
								element.addClass('non-active');
							}
						} else {
							if (target.context.id !== element.find('i')
								.context.id && !element.hasClass('non-active')) {
								element.addClass('non-active');
								clickPause = true;
								setTimeout(function () {
									clickPause = false;
								}, 30)
							} else {
								element.addClass('non-active');
							}
						}
					});

				element.bind("click", function (e) {
					var target = angular.element(e.target)
					if (element.hasClass('non-active')) {
						if (!clickPause) {
							element.removeClass('non-active');
						}
					} else if (target.hasClass('dropdown-menu-list-item')) {
						element.addClass('non-active');
					}
				});
			}
		}
  }]);
