'use strict';

antsquare
  .factory('MomentContainer', function () {

    function MomentContainer(moment) {
      if (moment) {
        this.address1 = moment.address1;
        this.address2 = moment.address2;
        this.approved = moment.approved;
      } else {
        this.address1 = null;
        this.address2 = null;
        this.approved = null;
      }
      this.as_business = null,
        this.lat = null,
        this.lon = null,
        this.id = null,
        this.country = null,
        this.currency = null,
        this.flagged = null,
        this.price_in_cents = null,
        this.radius = null,
        this.images = [];
      this.description = null;
      this.name = null;
      this.category_info = null;
      this.store_id = null;
      this.user_id = null;
      this.price = null;
      this.quantity = null;
      this.category = null;
      this.type = 'moment';
      this.is_visible = true;
      this.fbShare = false;
      this.video_url = null;
    }

    return MomentContainer;
  });

antsquare
  .factory('MomentIntent', ['MomentContainer', function (MomentContainer) {
    var momentContainer;

    return {

      start: function (moment) {

        if (moment) {
          momentContainer = new MomentContainer(moment);
        } else {
          momentContainer = new MomentContainer();
        }
        return momentContainer
      },
      newContainer: function (user_id, store_id) {
        var x = new MomentContainer();
        x.user_id = user_id;
        x.store_id = store_id;
        return x;
      },
      hasMoment: function () {
        return (!!momentContainer)
      },

      setImages: function (images) {
        momentContainer.images = images;
      },
      setVideo_url: function (video_url) {
        momentContainer.video_url = video_url;
      },

      setType: function (type) {
        momentContainer.type = type;
      },

      // setImage: function(image) {
      //   productContainer.image = image;
      // },

      setDescription: function (description) {
        momentContainer.description = description;
      },

      setCategory: function (category) {
        momentContainer.category = category;
      },

      setName: function (name) {
        momentContainer.name = name;
      },

      setUser_id: function (user_id) {
        momentContainer.user_id = user_id;
      },

      setStore_id: function (store_id) {
        momentContainer.store_id = store_id;
      },

      setPrice: function (price) {
        momentContainer.price = price;
      },

      setCurrency: function (currency) {
        momentContainer.currency = currency;
      },

      setQuantity: function (quantity) {
        momentContainer.quantity = quantity;
      },

      getContainer: function () {
        if (momentContainer) {
          delete momentContainer.previews
        }
        return momentContainer;
      },

      end: function () {
        momentContainer = {};
      },

      isValidate: function (moment) {
        if (moment.images.length <= 0 || (moment.description === '' || moment.description === null) || (moment.name === '' || moment.name === null)) {
          return false
        } else {
          return true
        }
      }

    };

  }]);