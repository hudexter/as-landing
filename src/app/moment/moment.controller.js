'use strict';

antsquare
  .controller('MomentCtrl', ['$scope', '$state', '$stateParams', 'MomentsFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'ModalStorage', 'ModalService', 'Auth', 'AccountsFactory', 'deviceDetector', 'branchFactory', 'moment', function($scope, $state, $stateParams, MomentsFactory, $window, MapService, GeoLocationService, $rootScope, $location, ModalStorage, ModalService, Auth, AccountsFactory, deviceDetector, branchFactory, moment) {

    var gotMomentInfo = false;
    $scope.loading = true;
    if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
      $scope.isIOS = true;
    } else {
      $scope.isIOS = false;
    }

    if ($rootScope.branchReady) {
      branchFactory.link('moment', $stateParams.moment_id)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }

        })
    }

    $rootScope.$on('branchReady', function(event, data) {
      branchFactory.link('moment', $stateParams.moment_id)
        .then(function(data) {
          if (deviceDetector.isDesktop()) {
            if (deviceDetector.os === 'mac') {
              $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
            } else {
              $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
            }
          } else {
            $scope.branchLink = data;
          }
        })
    });

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
    } else {
      $scope.userInfo = null;
    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        $scope.StoreFollowProduct = false;
        start();
      } else {
        $scope.userInfo = null;
        $scope.StoreFollowProduct = false;
      }
    });

    $scope.orderOnApp = function() {
      if ($rootScope.isMobile) {
        if ($scope.isIOS) {
          // var url = $location.url();
          // url = 'antsquarecom://antsquare.com' + url;
          setTimeout(function() {
            location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
          }, 500);

          // redirect to 'antsquare://www.antsquare.com/product/:id'

        } else {
          var url = $location.url();
          url = 'antsquarecom://antsquare.com' + url;
          location = url;
          setTimeout(function() {
            location = "https://ad.apps.fm/LRMs2kfpF0YQK_qEfUeyCF5KLoEjTszcQMJsV6-2VnHFDLXitVHB6BlL95nuoNYfISXbMQwI-2AOqLYV5gygIechIMlYYx5zMHmF83sYo5BAfQUdy_B62HXVVdz_7CSe"
          }, 500);
        }
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      } else {
        // location = 'https://itunes.apple.com/app/apple-store/id978186974?pt=113689808&ct=mobile.antsquare.com&mt=8';
      }
    };

    // Moment
    var distance = {};

    $scope.characterLimit = 16;
    $scope.coords = undefined;
    $scope.moment = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();

      if ($scope.moment) {
        getPage();
        distance = MapService.getDistance($scope.moment.lat, $scope.moment.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.moment[key] = distance[key];
        }
      }
    });

    $scope.slideLeft = function() {
      if ($scope.slideIndex !== 0) {
        $scope.slideIndex = $scope.slideIndex - 1;
      }
    };

    $scope.slideRight = function() {
      if ($scope.slideIndex < $scope.moment.images.length) {
        $scope.slideIndex = $scope.slideIndex + 1;
      }
    };
    $scope.toSlide = function(index) {
      $scope.slideIndex = index;
    }

    function getShareLinks(moment) {
      // $scope.root = $location.host();
      $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.twLink = "http://twitter.com/intent/tweet?status=" + moment.description + "+%40antsquare+" + "https://www.antsquare.com/moment/" + moment.id;
      "http://twitter.com/intent/tweet?status=" + moment.description + "@antsquare" + "https://www.antsquare.com/moment/" + moment.id;

      $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/moment/" + moment.id + "&amp;media=" + moment.images[0] + "&amp;description=Check it out!"

      $scope.mailLink = "mailto:?Subject=Check out " + moment.description + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/moment/" + moment.id;
    };

    $scope.momentDeletePrompt = function(id) {

      ModalStorage.set(id, MomentsFactory.deleteMoment);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalSure/modalSure.html",
          controller: "ModalSureCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            if (result) {


              $scope.$emit('deleteProduct');
              $state.go('index.home');
            }
          });
        });

    };

    function checkImgContainerSize() {
      // if (window.innerWidth < 768) {
      var imgContainer = angular.element(document.querySelector('#rn-carousel'));
      var x = imgContainer.width();
      imgContainer.height(x);
      // }
    };

    function getPage() {
      var input = {
        moment_id: $stateParams.moment_id,
        viewer_id: null,
        lat: null,
        lon: null
      };
      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id;
        var userid = $scope.userInfo.id;
      };
      if ($scope.coords) {
        input.lat = $scope.coords.latitude;
        input.lon = $scope.coords.longitude;
      }
      if (!gotMomentInfo) {
        MomentsFactory.getMomentPageView(input)
          .then(function(data) {
            gotMomentInfo = true;
            $scope.loading = false;
            $scope.moment = data.data;
            console.log(data.data);
            if (userid != $scope.moment.user_id) {
              $scope.StoreFollowProduct = true;
            }
            $scope.$broadcast('productLoaded');
            if (GeoLocationService.hasLocation()) {
              $scope.coords = GeoLocationService.getCoords();
              distance = MapService.getDistance($scope.moment.lat, $scope.moment.lon, $scope.coords.latitude, $scope.coords.longitude);
              for (var key in distance) {
                $scope.moment[key] = distance[key];
              }
            }
            getShareLinks($scope.moment);
          })
          .catch(function(err) {
            $scope.moment = err.message;
          });
      }

    };

    function start() {

      $scope.moment = undefined;
      getPage();
      checkImgContainerSize();
    }

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;

      if (!$scope.moment.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.moment.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.moment.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.user_info.is_following = !$scope.moment.user_info.is_following;
          })
      }
    };

    $scope.likeToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.moment.liked = !$scope.moment.liked;
      if ($scope.moment.liked) {
        AccountsFactory.likeMoment($stateParams.moment_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.moment.liked = !$scope.moment.liked
          })
      } else {
        AccountsFactory.unlikeMoment($stateParams.moment_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.moment.liked = !$scope.moment.liked;
          })
      }
    };

    $scope.flagMoment = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagMoment')
      $scope.flagged = true;
      MomentsFactory.flagMoment($stateParams.moment_id)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    $scope.showMomentLikes = function() {
      ModalStorage.set($stateParams.moment_id);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalLikes/modalMomentLikes.html",
          controller: "ModalMomentLikesCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.submitComment = function() {
      $rootScope.$broadcast('postComment', $scope.content)
      $scope.content = null;
    }
    start();
    window.onresize = checkImgContainerSize;
    $scope.ready = false;

    $scope.width = $window.innerWidth;
    $scope.card = document.getElementById('card').offsetWidth;
    $scope.cardHeight = document.getElementById('card').offsetHeight;
    $scope.left = document.getElementById('leftPanel').offsetWidth;

    if ($scope.card - 5 > $scope.left) {
      $scope.productDescription = {
        'max-width': $scope.card - $scope.left - 19 + 'px',
        'word-break': 'break-all'
      }
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 131 + 'px',
        // 'min-height': $scope.cardHeight - 131 + 'px',
        'min-height': '130px',
        'max-width': $scope.card - $scope.left - 19 + 'px',
        'overflow-y': 'auto',
        'word-break': 'break-all'
      }
    } else {
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 131 + 'px',
        // 'min-height': $scope.cardHeight - 131 + 'px',
        'min-height': '130px',
        'max-width': $scope.left + 'px',
        'overflow-y': 'auto',
        'word-break': 'break-all'
      }
      $scope.productDescription = {
        'max-width': $scope.left + 'px',
        'word-break': 'break-all'
      }
    }

    $scope.ready = true;

    angular.element($window).bind('resize', function() {
      $scope.ready = false;
      $scope.card = document.getElementById('card').offsetWidth;
      $scope.left = document.getElementById('leftPanel').offsetWidth;
      if ($scope.card - 5 > $scope.left) {
        $scope.productDescription = {
          'max-width': $scope.card - $scope.left - 19 + 'px'
        }
      } else {
        $scope.productDescription = {
          'max-width': $scope.left + 'px'
        }
      }

      $scope.ready = true;
      $scope.$digest();
    });
    // });

  }]);

'use strict';

antsquare.filter('tweetLinky', ['$filter', '$sce',
  function($filter, $sce) {
    return function(text, target) {
      if (!text) return text;

      var replacedText = $filter('linky')(text, target);
      var targetAttr = "";
      var target = "_blank";
      if (angular.isDefined(target)) {
        targetAttr = ' target="' + target + '" ';
      }

      // replace #hashtags
      var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = replacedText.replace(replacePattern1, '$1<a href="hashtag/$2"' + targetAttr + '>#$2</a>');

      // replace @mentions
      var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = replacedText.replace(replacePattern2, '$1<a href="/@$2"' + targetAttr + '>@$2</a>');

      $sce.trustAsHtml(replacedText);
      return replacedText;
    };
  }
]);