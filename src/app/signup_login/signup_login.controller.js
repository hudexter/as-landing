'use strict';

antsquare
  .controller('SignupLoginCtrl', ['$scope', '$state', 'AccountsFactory', 'Auth', 'ezfb', 'ModalStorage', 'ModalService', 'GeoLocationService', '$rootScope', '$timeout', '$cookies', '$q', 'StoreFactory', function($scope, $state, AccountsFactory, Auth, ezfb, ModalStorage, ModalService, GeoLocationService, $rootScope, $timeout, $cookies, $q, StoreFactory) {
    $scope.signupLoading = false;
    $scope.country = null;
    var addr = {};
    $scope.$on('g-places-autocomplete:select', function(e, place) {
      if (!place.geometry) {
        return;
      } else {
        var lat = place.geometry.location.lat();
        var lon = place.geometry.location.lng();

        // $scope.mapResult = palce.formatted_address;
        reverseGeocoding(lat, lon)
          .then(function(data) {
            $scope.mapResult = data;
          })
      }
    });

    $scope.map = {
      center: {
        //latitude: $scope.store.lat,
        //longitude: $scope.store.lon
        latitude: 39.8282,
        longitude: -98.5795
      },
      zoom: 15,
      dragging: false,
      events: {
        idle: function() {
          reverseGeocoding($scope.map.center.latitude, $scope.map.center.longitude)
            .then(function(data) {
              $scope.mapResult = data;
            })
        },
      }
    };

    function setMapLocation() {
      if (GeoLocationService.hasLocation()) {
        console.log("Has Location");
        console.log(GeoLocationService.get());
        $scope.map.center.latitude = GeoLocationService.getCoords()
          .latitude;
        $scope.map.center.longitude = GeoLocationService.getCoords()
          .longitude;
      }
    };

    setMapLocation();


    function reverseGeocoding(lat, lon) {
      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(lat, lon);
      var deferred = $q.defer();
      $scope.map.center.latitude = latlng.lat();
      $scope.map.center.longitude = latlng.lng();

      var map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: $scope.map.center.latitude,
          lng: $scope.map.center.longitude
        }
      });

      var request = {
        location: latlng,
        radius: '200'
      };

      var result = {
        nearBy: null,
        address1: null,
        address2: null,
      };

      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, function(results, status) {
        if (status === "OK") {
          result.nearBy = results;
          geocoder.geocode({
            'latLng': latlng
          }, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
              results.forEach(function(obj) {
                for (var key in obj) {
                  // if (obj.types[0] === 'street_address') {
                  //
                  //    result.address1 = obj.address_components[0].long_name + " " + obj.address_components[1].short_name;
                  //    // deferred.resolve(result)
                  // }
                  // if (obj[key][0] === 'political' || obj[key][0] === 'locality') {
                  //    result.address2 = obj.formatted_address;
                  //    // deferred.resolve(result)
                  // }

                  if (obj[key][0] === 'country') {
                    result.country = obj.address_components[0].short_name;
                  }
                }
              })
              var temp = results[0].formatted_address.split(',');
              result.address1 = temp[0]
              result.address2 = temp.splice(1, temp.length).join();
              addr.address1 = result.address1;
              addr.address2 = result.address2;
              deferred.resolve(result)
            }
          });

        }
      });
      return deferred.promise;
    }


    Auth.reset();
    $scope.form = {};

    $scope.submitForm = function() {

      if (!$scope.signupLoading) {

        $scope.error = "";
        // if (!$scope.gRecaptchaResponse) {
        // 	$scope.error = "Please prove you're not a robot";
        // 	return
        // }
        $scope.signupLoading = true;
        $scope.form.country = $scope.country;
        $scope.form.input = '+' + $scope.country.code + $scope.form.phone;
        $scope.form.lat = $scope.map.center.latitude;
        $scope.form.lon = $scope.map.center.longitude;
        $scope.form.address1 = addr.address1;
        $scope.form.address2 = addr.address2;
        $scope.form.iso2 = $scope.country.code;
        if ($scope.form.businessPhone) {
          $scope.form.business_phone = $scope.form.businessPhone;
        } else {
          $scope.form.business_phone = "";
        }
        if ($scope.form.isBusiness) {
          $scope.form.as_business = $scope.form.isBusiness;
        } else {
          $scope.form.as_business = false;
        }
        if (!$scope.form.email) {
          $scope.form.email = "";
        }

        console.log($scope.form);
        AccountsFactory.registerUser($scope.form)
          .then(function(data) {
            // $mixpanel.track('signup');
            // console.log(data)
            var modalData = {
              token: data.data,
              userInfo: $scope.form
            };
            $scope.phoneVerify(modalData);
            $scope.response = data;
            $scope.signupLoading = false;
            $scope.error = null;
          })
          .catch(function(error) {
            $scope.signupLoading = false;
            if (error.status != 500) {
              $scope.error = error.data.message
            }
            // $modalInstance.close(error);
          });
      }
    };

    $scope.addPhone = function(result) {
      ModalStorage.set(result);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalAddPhone/modalAddPhone.html",
          controller: "ModalAddPhoneCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
            if (result) {
              $scope.phoneVerify(result);
            }
          });
        });
    };

    $scope.phoneVerify = function(data) {
      ModalStorage.set(data);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalPhoneVerify/modalPhoneVerify.html",
          controller: "ModalPhoneVerifyCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
            if (Auth.isLoggedIn()) {
              $state.go('index.home');
            }
          });
        });
    };

    var getStore = function() {
      var deferred = $q.defer();
      StoreFactory.me()
        .then(function(data) {
          Auth.setStore(data.data);
          deferred.resolve(data);
        })
        .catch(function(error) {
          console.log(error)
          deferred.reject(error);
        })
      return deferred.promise;
    };

    var getUser = function() {
      var deferred = $q.defer();
      AccountsFactory.me()
        .then(function(data) {
          Auth.setUser(data.data);
          deferred.resolve(data);
        })
        .catch(function(error) {
          console.log(error)
          deferred.reject(error);
        })
      return deferred.promise;
    };

    $scope.authenticate = function(provider) {
      ezfb.getLoginStatus(function(res) {
        // console.log(res)
      });
      ezfb.login(function(response) {
          $cookies.put('fb_token', response.authResponse.accessToken);
          if (response.status === 'connected') {
            AccountsFactory.userLinking(provider, response.authResponse.accessToken)
              .then(function(response) {

                Auth.setToken(response);
                AccountsFactory.me()
                  .then(function(response) {
                    $q.all([getStore(), getUser()])
                      .then(function(value) {
                        $state.go('index.home')
                      }, function(error) {
                        console.log(error)
                      });
                  });

              })
              .catch(function(error) {
                console.log(error)
                $scope.loading = "error";
                $timeout(function() {
                  $scope.loading = false;
                }, 1500);
              });
          } else {
            console.log("not conencted")
            $scope.loading = "error";
            $timeout(function() {
              $scope.loading = false;
            }, 1500);
          }
        })
        .catch(function(error) {
          console.log(error);
          $scope.loading = "error";
          $timeout(function() {
            $scope.loading = false;
          }, 1500);
        });
    };

  }]);