'use strict';

angular.module('antsquare')
  .factory('Auth', ['$rootScope', '$http', '$cookies', 'Config', '$state', 'intercomFactory', '$q', function($rootScope, $http, $cookies, Config, $state, intercomFactory, $q) {

    var token = {
      firebase_token: $cookies.get('firebase_token') || null,
      access_token: $cookies.get('access_token') || null,
      refresh_token: $cookies.get('refresh_token') || null,
      exp: $cookies.get('token_exp') || null,
      fb_token: $cookies.get('fb_token') || null,
    };
    // var accessLevels = routingConfig.accessLevels,
    //  userRoles = routingConfig.userRoles,
    //  currentUser = $cookies.get('user') || {
    //    username: '',
    //    role: userRoles.public
    //  }
    var userRoles = [];
    var user = {};
    var store = {};
    var refreshing = false;
    if ($cookies.get('userInfo')) {
      user = JSON.parse($cookies.get('userInfo'));
    }

    if ($cookies.get('storeInfo')) {
      store = JSON.parse($cookies.get('storeInfo'));
    }

    // $cookies.remove('user');
    function changeUser(user) {
      angular.extend(currentUser, user);
    }

    $rootScope.$on('updateStore', function(event, data) {
      if (data) {
        store = data
      }
    });

    $rootScope.$on('refreshed', function(event, data) {
      refreshing = false
    });

    $rootScope.$on('updateUser', function(event, data) {
      userRoles = [];
      if (data) {
        user = data;
        $cookies.put('userInfo', JSON.stringify(data), {
          domain: Config.COOKIE_DOMAIN
        });
        userRoles.push('user')
        if (user.is_admin) {
          userRoles.push('admin')
        }
      }
    });

    $rootScope.$on('updateStore', function(event, data) {
      if (data && !(_.isEmpty(data.stores[0]))) {
        delete data.stores[0].availability
        $cookies.put('storeInfo', JSON.stringify(data), {
          domain: Config.COOKIE_DOMAIN
        });
      }
    });

    return {
      getToken: function() {
        token = {
          firebase_token: $cookies.get('firebase_token') || null,
          access_token: $cookies.get('access_token') || null,
          refresh_token: $cookies.get('refresh_token') || null,
          exp: $cookies.get('token_exp') || null,
          fb_token: $cookies.get('fb_token') || null,
        };
        return token
      },
      setUser: function(currentuser) {
        user = currentuser;
        delete user.followers
        delete user.followings
        delete user.phone_update
        delete user.stores
        delete user.email_update
          //delete user.code

        $rootScope.$broadcast('updateUser', user);
      },
      setStore: function(currentStore) {
        store = currentStore;
        $rootScope.$broadcast('updateStore', currentStore);
      },
      setToken: function(response) {
        $cookies.put('firebase_token', response.data.firebase_token, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('access_token', response.data.access_token, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('refresh_token', response.data.refresh_token, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('token_exp', response.data.exp, {
          domain: Config.COOKIE_DOMAIN
        });
      },
      // authorize: function (accessLevel, role) {
      //  if (role === undefined) {
      //    role = currentUser.role;
      //  }
      //  return accessLevel.bitMask & role.bitMask;
      // },
      isLoggedIn: function() {
        return !_.isEmpty(user)
          // if (user === undefined) {
          //  user = currentUser;
          // }
          // return user.role.title === userRoles.user.title || user.role.title === userRoles.admin.title;
      },
      register: function(user, success, error) {
        $http.post('/v2/register', user)
          .success(function(res) {
            changeUser(res);
            success();
          })
          .error(error);
      },
      login: function(data) {
        return $http
          .post(Config.ID_BASEURL + '/v2/api/login', data);
      },
      reset: function() {
        $cookies.put('firebase_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('access_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('refresh_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('token_exp', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('userInfo', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('storeInfo', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('fb_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        token = {};
        userRoles = [];
        user = {};
        store = {};
        $rootScope.$broadcast('updateStore', null);
        $rootScope.$broadcast('updateUser', null);
        var cookies = $cookies.getAll();
        angular.forEach(cookies, function(v, k) {
          $cookies.put(k, null, {
            domain: Config.COOKIE_DOMAIN
          });
          $cookies.remove(k)
        })
        $cookies.put('ENV', Config.ENV, {
          domain: Config.COOKIE_DOMAIN
        });
      },
      logoutFb: function() {
        $cookies.put('fb_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
      },
      logout: function(success, error) {
        $cookies.put('firebase_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('fb_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('access_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('refresh_token', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('token_exp', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('userInfo', null, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('storeInfo', null, {
          domain: Config.COOKIE_DOMAIN
        });
        token = {};
        userRoles = [];
        user = {};
        store = {};
        $rootScope.$broadcast('updateStore', null);
        $rootScope.$broadcast('updateUser', null);
        var cookies = $cookies.getAll();
        angular.forEach(cookies, function(v, k) {
          $cookies.put(k, null, {
            domain: Config.COOKIE_DOMAIN
          });
          $cookies.remove(k)
        })
        $cookies.put('ENV', Config.ENV, {
          domain: Config.COOKIE_DOMAIN
        });

        intercomFactory.logout();
        if ('data' in $state.current) {
          $state.go('index.home');
        }
      },
      refresh: function() {
        if (!refreshing) {
          refreshing = true;
          return $http
            .post(Config.ID_BASEURL + '/v2/token/refresh', token);
        } else {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise
        }

      },
      update: function(new_token) {

        $cookies.put('firebase_token', new_token.firebase_token, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('access_token', new_token.access_token, {
          domain: Config.COOKIE_DOMAIN
        });
        $cookies.put('token_exp', new_token.exp, {
          domain: Config.COOKIE_DOMAIN
        });

        token = {
          fb_token: $cookies.get('fb_token') || null,
          firebase_token: $cookies.get('firebase_token') || null,
          access_token: $cookies.get('access_token') || null,
          refresh_token: $cookies.get('refresh_token') || null,
          exp: $cookies.get('token_exp') || null,
        };
        $rootScope.$broadcast('updateToken');
      },
      userRoles: userRoles,
      getStore: store,
      getUser: user,
      getUserFn: function() {
        return user
      },
      getStoreFn: function() {
        return store
      },
    };
  }]);