'use strict';

antsquare
  .controller('EventCtrl', ['$scope', '$state', '$stateParams', 'EventsFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'ModalStorage', 'ModalService', 'Auth', 'AccountsFactory', 'deviceDetector', 'branchFactory', function($scope, $state, $stateParams, EventsFactory, $window, MapService, GeoLocationService, $rootScope, $location, ModalStorage, ModalService, Auth, AccountsFactory, deviceDetector, branchFactory) {

    if (deviceDetector.device === 'ipad' || deviceDetector.device === 'iphone' || deviceDetector.device === 'ipod') {
      $scope.isIOS = true;
    } else {
      $scope.isIOS = false;
    }

    // if ($rootScope.branchReady) {
    //   branchFactory.link('event', $stateParams.event_id)
    //     .then(function(data) {
    //       if (deviceDetector.isDesktop()) {
    //         if (deviceDetector.os === 'mac') {
    //           $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
    //         } else {
    //           $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
    //         }
    //       } else {
    //         $scope.branchLink = data;
    //       }

    //     })
    // }

    // $rootScope.$on('branchReady', function(event, data) {
    //   branchFactory.link('event', $stateParams.event_id)
    //     .then(function(data) {
    //       if (deviceDetector.isDesktop()) {
    //         if (deviceDetector.os === 'mac') {
    //           $scope.branchLink = "https://itunes.apple.com/us/app/antsquare/id978186974?ls=1&mt=8"
    //         } else {
    //           $scope.branchLink = "https://play.google.com/store/apps/details?id=com.antsquare.android"
    //         }
    //       } else {
    //         $scope.branchLink = data;
    //       }
    //     })
    // });

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
    } else {
      $scope.userInfo = null;
    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        $scope.StoreFollowProduct = false;
        start();
      } else {
        $scope.userInfo = null;
        $scope.StoreFollowProduct = false;
      }
    });
    $scope.map = {
      center: {
        //latitude: $scope.store.lat,
        //longitude: $scope.store.lon
        latitude: 39.8282,
        longitude: -98.5795
      },
      zoom: 14,
      dragging: false,
    };

    $scope.marker = {
      id: 0,
      coords: {
        latitude: 39.8282,
        longitude: -98.5795
      },
      options: { draggable: false },
    };

    // event
    var distance = {};

    $scope.characterLimit = 16;
    $scope.coords = undefined;
    $scope.event = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      getPage();
      if ($scope.event) {
        // getPage();
        distance = MapService.getDistance($scope.event.lat, $scope.event.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.event[key] = distance[key];
        }
      }
    });

    $scope.slideLeft = function() {
      if ($scope.slideIndex !== 0) {
        $scope.slideIndex = $scope.slideIndex - 1;
      }
    };

    $scope.slideRight = function() {
      if ($scope.slideIndex < $scope.event.images.length) {
        $scope.slideIndex = $scope.slideIndex + 1;
      }
    };
    $scope.toSlide = function(index) {
      $scope.slideIndex = index;
    }

    function getShareLinks(event) {
      // $scope.root = $location.host();
      $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/event/" + event.id;

      $scope.twLink = "http://twitter.com/intent/tweet?status=" + event.description + "+%40antsquare+" + "https://www.antsquare.com/event/" + event.id;
      "http://twitter.com/intent/tweet?status=" + event.description + "@antsquare" + "https://www.antsquare.com/event/" + event.id;

      $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/event/" + event.id + "&amp;media=" + event.images[0] + "&amp;description=Check it out!"

      $scope.mailLink = "mailto:?Subject=Check out " + event.description + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/event/" + event.id;
    };

    $scope.eventDeletePrompt = function(id) {

      ModalStorage.set(id, EventsFactory.deleteevent);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalSure/modalSure.html",
          controller: "ModalSureCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            if (result) {


              $scope.$emit('deleteProduct');
              $state.go('index.home');
            }
          });
        });

    };

    function checkImgContainerSize() {
      // if (window.innerWidth < 768) {
      var imgContainer = angular.element(document.querySelector('#rn-carousel'));
      var x = imgContainer.width();
      imgContainer.height(x);
      // }
    };

    function getPage() {
      var input = {
        event_id: $stateParams.event_id,
        viewer_id: null,
        lat: null,
        lon: null
      };
      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id;
        var userid = $scope.userInfo.id;
      };
      if ($scope.coords) {
        input.lat = $scope.coords.latitude;
        input.lon = $scope.coords.longitude;
      }
      EventsFactory.getEventPageView(input)
        .then(function(data) {
          $scope.event = data.data;
          console.log($scope.event);
          $scope.map.center.latitude = data.data.lat;
          $scope.map.center.longitude = data.data.lon;
          $scope.marker.coords.latitude = data.data.lat;
          $scope.marker.coords.longitude = data.data.lon;
          if (userid != $scope.event.user_id) {
            $scope.StoreFollowProduct = true;
          }
          $scope.$broadcast('productLoaded');
          // if (GeoLocationService.hasLocation()) {
          //   $scope.coords = GeoLocationService.getCoords();
          //   distance = MapService.getDistance($scope.event.lat, $scope.event.lon, $scope.coords.latitude, $scope.coords.longitude);
          //   for (var key in distance) {
          //     $scope.event[key] = distance[key];
          //   }
          // }
          getShareLinks($scope.event);
        })
        .catch(function(err) {
          console.log("Here....");
          $scope.event = err.message;
        });
    };

    function start() {

      $scope.event = undefined;
      //getPage();
      checkImgContainerSize();
    }

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.event.user_info.is_following = !$scope.event.user_info.is_following;

      if (!$scope.event.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.event.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.event.user_info.is_following = !$scope.event.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.event.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.event.user_info.is_following = !$scope.event.user_info.is_following;
          })
      }
    };

    $scope.likeToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')
      console.log("inside like toggle");
      // $scope.event.is_liked = !$scope.event.liked;
      if (!$scope.event.is_liked) {
        console.log("like");
        AccountsFactory.likeEvent($stateParams.event_id)
          .then(function(data) {
            console.log(data);
            $scope.event.is_liked = !$scope.event.is_liked;
            $scope.event.total_likes++;
          })
          .catch(function(error) {
            console.log(error);
          })
      } else {
        console.log("unnlike");
        AccountsFactory.unlikeEvent($stateParams.event_id)
          .then(function(data) {
            console.log(data);
            $scope.event.is_liked = !$scope.event.is_liked;
            $scope.event.total_likes--;
          })
          .catch(function(error) {
            console.log(error);

          })
      }
    };

    $scope.flagevent = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'flagevent')
      $scope.flagged = true;
      eventsFactory.flagevent($stateParams.event_id)
        .then(function(data) {})
        .catch(function(error) {
          $scope.flagged = false;
        });
    }

    $scope.showLikes = function() {
      ModalStorage.set($stateParams.event_id);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalLikes/modaleventLikes.html",
          controller: "ModaleventLikesCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.submitComment = function() {
      $rootScope.$broadcast('postComment', $scope.content)
      $scope.content = null;
    }
    start();
    window.onresize = checkImgContainerSize;
    $scope.ready = false;

    $scope.width = $window.innerWidth;
    $scope.card = document.getElementById('card').offsetWidth;
    $scope.cardHeight = document.getElementById('card').offsetHeight;
    $scope.left = document.getElementById('leftPanel').offsetWidth;

    if ($scope.card - 5 > $scope.left) {
      $scope.productDescription = {
        'max-width': $scope.card - $scope.left - 19 + 'px',
        'word-break': 'break-all'
      }
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 131 + 'px',
        // 'min-height': $scope.cardHeight - 131 + 'px',
        'min-height': '130px',
        'max-width': $scope.card - $scope.left - 19 + 'px',
        'overflow-y': 'auto',
        'word-break': 'break-all'
      }
    } else {
      $scope.rightscrollHeight = {
        'max-height': $scope.cardHeight - 131 + 'px',
        // 'min-height': $scope.cardHeight - 131 + 'px',
        'min-height': '130px',
        'max-width': $scope.left + 'px',
        'overflow-y': 'auto',
        'word-break': 'break-all'
      }
      $scope.productDescription = {
        'max-width': $scope.left + 'px',
        'word-break': 'break-all'
      }
    }

    $scope.ready = true;

    angular.element($window).bind('resize', function() {
      $scope.ready = false;
      $scope.card = document.getElementById('card').offsetWidth;
      $scope.left = document.getElementById('leftPanel').offsetWidth;
      if ($scope.card - 5 > $scope.left) {
        $scope.productDescription = {
          'max-width': $scope.card - $scope.left - 19 + 'px'
        }
      } else {
        $scope.productDescription = {
          'max-width': $scope.left + 'px'
        }
      }

      $scope.ready = true;
      $scope.$digest();
    });
    // });

  }]);

'use strict';

antsquare.filter('tweetLinky', ['$filter', '$sce',
  function($filter, $sce) {
    return function(text, target) {
      if (!text) return text;

      var replacedText = $filter('linky')(text, target);
      var targetAttr = "";
      var target = "_blank";
      if (angular.isDefined(target)) {
        targetAttr = ' target="' + target + '" ';
      }

      // replace #hashtags
      var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = replacedText.replace(replacePattern1, '$1<a href="hashtag/$2"' + targetAttr + '>#$2</a>');

      // replace @mentions
      var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = replacedText.replace(replacePattern2, '$1<a href="/@$2"' + targetAttr + '>@$2</a>');

      $sce.trustAsHtml(replacedText);
      return replacedText;
    };
  }
]);