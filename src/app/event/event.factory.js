'use strict';

antsquare
  .factory('EventsFactory', ['$http', 'Config', '$q', function($http, Config, $q) {

    return {
      getEventPageView: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/events/' + data.event_id + '/mpview',
          method: 'GET',
          params: data
        });
      },
      findMomentById: function(id) {
        return $http
          .get(Config.API_BASEURL + '/moments/' + id);
      },
      bulkMoment: function(momentData) {
        return $http
          .post(Config.CORE_BASEURL + '/v5/all/bulk', momentData);
      },
      editMoment: function(moment_id, momentData) {
        return $http
          .put(Config.API_BASEURL + '/moments/' + moment_id, momentData);
      },
      deleteMoment: function(id) {
        return $http
          .delete(Config.API_BASEURL + '/moments/' + id);

      },
      flagMoment: function(id, reason) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/moments/' + id + '/flag',
          method: 'PUT',
          data: {
            'reason': reason
          }
        });
      },
      flagComment: function(id, reason) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/comments/' + id + '/flag',
          method: 'PUT',
          data: {
            'reason': reason
          }
        });
      },
      getComments: function(data, event_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/events/' + event_id + '/comment',
          method: 'GET',
          params: data
        });
      },
      postComment: function(content, event_id) {
        return $http
          .post(Config.CORE_BASEURL + '/v5/events/' + event_id + '/comment', content);
      },
    }
  }]);