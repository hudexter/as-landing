'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'Config', '$locationProvider', '$compileProvider', '$mixpanelProvider', 'ezfbProvider', 'laddaProvider',
  function($stateProvider, $urlRouterProvider, $httpProvider, Config, $locationProvider, $compileProvider, $mixpanelProvider, ezfbProvider, laddaProvider) {

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);

    // Set up Intercom
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    // var myInitFunction = function ($window, $rootScope, ezfbInitParams) {
    //  $window.FB.init({
    //      appId: Config.FACEBOOK_ID,
    //  });
    //  $rootScope.$broadcast('FB.init');
    // };
    //
    // ezfbProvider.setInitFunction(myInitFunction);

    ezfbProvider.setLocale('en_US');
    ezfbProvider.setInitParams({
      appId: Config.FACEBOOK_ID
    });

    // This is `false` by default
    $locationProvider.html5Mode(true)
      .hashPrefix('!');

    $mixpanelProvider.apiKey(Config.MIXPANEL_KEY);

    laddaProvider.setOption({
      style: 'expand-right',
      spinnerSize: 35,
      spinnerColor: '#ffffff'
    });

    // Add Key to Header
    var rejectionList = [];
    var reloadState = false;

    $httpProvider.interceptors.push(['$q', '$cookies', '$injector', 'Config', '$rootScope',
      function($q, $cookies, $injector, Config, $rootScope) {
        return {
          'request': function(config) {
            var http = $injector.get('$http');
            var state = $injector.get('$state');

            var deferred = $q.defer();
            if (!config.file) {
              var access_token = $cookies.get('access_token');
              if (angular.isDefined(access_token)) {
                config.headers.authorization = 'Bearer ' + access_token;
              }
              deferred.resolve(config);
            } else {
              deferred.resolve(config);
            }
            // if (rejectionList.length !== 0 && config.url !== Config.ID_BASEURL + '/token/refresh') {
            //   config.headers = rejectionList[0].config.headers;
            //   config.url = rejectionList[0].config.url;
            //   if ($cookies.get('access_token')) {
            //     config.headers.authorization = 'Bearer ' + $cookies.get('access_token');
            //   } else {
            //     config.headers.authorization = null;
            //   }
            //   rejectionList = _.drop(rejectionList);
            //   if (rejectionList.length === 0) {
            //     state.reload();
            //   }
            //   deferred.resolve(config);
            // } else {
            //   if (!config.file) {
            //     var access_token = $cookies.get('access_token');
            //     if (angular.isDefined(access_token)) {
            //       config.headers.authorization = 'Bearer ' + access_token;
            //     }
            //     deferred.resolve(config);
            //   } else {
            //     deferred.resolve(config);
            //   }
            // }
            return deferred.promise;
          },
          'requestError': function(rejection) {
            return rejection;
          },
          'responseError': function(rejection) {
            $rootScope.HTTPconfig = rejection;

            var http = $injector.get('$http');
            var state = $injector.get('$state');

            // rejectionList.push(rejection)
            var deferred = $q.defer();

            if (rejection.status === 498) {
              rejectionList.push(rejection);
              var refresh_token = {
                refresh_token: $cookies.get('refresh_token')
              };

              http.post(Config.ID_BASEURL + '/token/refresh', refresh_token)
                .then(function(response) {
                  $cookies.put('firebase_token', response.data.firebase_token, {
                    domain: Config.COOKIE_DOMAIN
                  });
                  $cookies.put('access_token', response.data.access_token, {
                    domain: Config.COOKIE_DOMAIN
                  });
                  $injector.get('$http')($rootScope.HTTPconfig.config)
                    .then(function(response) {
                      // we have a successful response - resolve it using deferred
                      deferred.resolve(response);

                    });
                })
                .catch(function(error) {
                  $cookies.remove('access_token');
                  $cookies.remove('refresh_token');
                  $cookies.remove('fb_token');
                  $cookies.remove('firebase_token');
                  state.go('menu');
                  deferred.reject(rejection);
                });
            } else if (rejection.status === 404 || rejection.status === 500 || rejection.status === 503) {
              // var productUrl = /\/v2\/products\/\d+\/pageview/;
              // var storetUrl = /\/v2\/stores\/\d+\/pageview/;
              // if (rejection.config.url.match(productUrl) || rejection.config.url.match(storetUrl)) {
              //   state.go('menu.404');
              // }
              deferred.reject(rejection);
            } else {
              deferred.reject(rejection);
            }
            return deferred.promise;
          },
          'response': function(response) {
            return response;
          }
        };
      }
    ])

  }
])