'use strict';

angular.module('antsquare')
	.controller('StyleCtrl', ['$scope', '$log', 'elements', 'ProductsFactory',
    function ($scope, $log, elements, ProductsFactory) {

			$scope.priceSlider = 150;

			$scope.slider = {
				min: 100,
				max: 180,
				options: {
					floor: 0,
					ceil: 450
				}
			};
    }
  ])
