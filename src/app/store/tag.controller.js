'use strict';

antsquare
  .controller('TagCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {


    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();

      getTag();

      //$scope.twitterlink = "https://twitter.com/tanyafosterblog";
    } else {
      $scope.userInfo = null;

    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        getTag();
      } else {
        $scope.userInfo = null;
        $scope.StoreSettingProfile = false;
        $scope.StoreFollowProduct = true;
      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        getTag($scope.coords);

        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }
    var scrollLoad = false;

    function addLoadMoreCard(scrollLoad) {

      if ($scope.cards.has_more && !scrollLoad) {
        if (scrollLoad === undefined) {
          $scope.cards.products.push({
            type: 'has_more_temp'
          })

        } else {
          $scope.cards.products.push({
            type: 'has_more'
          })
        }

      } else if (!$scope.cards.has_more) {
        $scope.cards.products.push({
          type: 'no_more'
        })
      }
    };

    $scope.$on('loadMoreTemp', function() {
      $scope.loading = true;
      $scope.loadingMoreProducts = true;
      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

      StoreFactory.getTagResources($scope.productsPageParams)
        .then(function(data) {
          $scope.cards.products.pop();

          $scope.newCards = data.data;

          $scope.newCards.products = data.data.data.map(function(card) {
            return CardIntent.set(card);
          });
          $scope.cards.has_more = $scope.newCards.has_more;
          $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
          addLoadMoreCard();
          refreshPlate();
          $scope.loadingMoreProducts = false;
          $scope.loading = false;
        })
        .catch(function(error) {
          $scope.loading = false;
          console.log(error)
          if (error.status === 400) {
            $scope.error = error.data.message;
          } else {
            $scope.error = error.data.message;
          }
          $scope.loadingMoreProducts = false;
        });

    });

    $scope.$on('loadMore', function() {
      if (!scrollLoad) {
        $scope.cards.products.pop();
        scrollLoad = true;
      }
      $scope.loadMore();
    });


    $scope.loadingMoreProducts = false;

    $scope.loading = null;
    $scope.loadMore = function() {


      if (!scrollLoad && !$scope.loadingMoreProducts) {
        $scope.loading = true;
        $scope.loadingMoreProducts = true;
        $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
        StoreFactory.getTagResources($scope.productsPageParams)
          .then(function(data) {

            $scope.newCards = data.data;
            $scope.newCards.products = data.data.data.map(function(card) {
              return CardIntent.set(card);
            });
            $scope.cards.has_more = $scope.newCards.has_more;
            $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
            addLoadMoreCard(scrollLoad);
            refreshPlate();
            $scope.loadingMoreProducts = false;
            $scope.loading = false;
          })
          .catch(function(error) {
            $scope.loading = false;
            console.log(error)
            if (error.status === 400) {
              $scope.error = error.data.message;
            } else {
              $scope.error = error.data.message;
            }
            $scope.loadingMoreProducts = false;
          });
      }

    }

    //$stateParams.tagname = "SanJose";
    $scope.tagnametitle = $stateParams.tagname;




    function getTag() {
      $scope.cards = {};
      $scope.productsPageParams = {
        page: 1,
        per_page: 20

      };
      $scope.productsPageParams.query = $stateParams.tagname;

      StoreFactory.getTagResources($scope.productsPageParams)
        .then(function(data) {
          $scope.cards = data.data;
          $scope.cards.products = data.data.data.map(function(card) {
            return CardIntent.set(card);
          });
          addLoadMoreCard();
          setTimeout(function() {
            refreshPlate();
          }, 500);
          $scope.loading = false;
        });
    };


    function refreshPlate() {
      if (angularGridInstance.home) {
        angularGridInstance.home.refresh();
      }
    };


    $scope.showFollowers = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollower/modalUserFollower.html",
          controller: "ModalUserFollowerCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };

    $scope.vm = {};

    /*$scope.postComment = function (id) {
      if (!$scope.submited) {
        $scope.submited = true;
        StoreFactory.postComment($scope.form, id)
          .then(function (data) {
            console.log(data)
            $scope.vm.comments.unshift(data.data);
            $scope.vm.total++;
            $scope.comments.total++;
            $scope.form.content = "";
            $scope.submited = false;
          })
          .catch(function (error) {
            console.log(error)
            $scope.submited = false;
          })
      }
    };*/

    $scope.showFollowings = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollowing/modalUserFollowing.html",
          controller: "ModalUserFollowingCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.showUserLikes = function() {
      ModalStorage.set($scope.user.id);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalLikes/modalUserLikes.html",
          controller: "ModalUserLikesCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.likeToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
      if ($scope.userfeed[index].target_json.is_liked) {

        AccountsFactory.likeMoment($scope.userfeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeMoment($scope.userfeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
          })
      }
    };
    $scope.likeProductToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
      if ($scope.userfeed[index].target_json.is_liked) {

        AccountsFactory.likeProduct($scope.userfeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeProduct($scope.userfeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
          })
      }
    };

    $scope.followMomentToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;

      if (!$scope.userfeed[index].target_json.is_following) {
        AccountsFactory.unfollowUser($scope.userfeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.userfeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;
          })
      }
    };

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.user.user_info.is_following = !$scope.user.user_info.is_following;

      if (!$scope.user.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.user.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.user.user_info.is_following = !$scope.user.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.user.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.user.user_info.is_following = !$scope.user.user_info.is_following;
          })
      }
    };

    $scope.submitComment = function(id) {

      $rootScope.$broadcast('postComment', $scope.content, id)
      $scope.content = null;
    };





    getTag();







  }]);