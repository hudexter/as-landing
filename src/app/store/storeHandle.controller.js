'use strict';

antsquare
  .controller('StoreHandleCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {
    $scope.StoreSettingProfile = false;
    $scope.StoreFollowProduct = false;

    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
      getStore();
    } else {
      $scope.userInfo = null;

    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        getStore();
      } else {
        $scope.userInfo = null;
        $scope.StoreSettingProfile = false;
        $scope.StoreFollowProduct = true;
      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        getResources($scope.coords);
        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }

    function getStore() {


      var input = {
        handle: $stateParams.store_id,
        viewer_id: null
      };
      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id
      };


      StoreFactory.getStoreByhandle(input)
        .then(function(data) {
          $scope.store = data.data;
          setMap();

          if (input.viewer_id != $scope.store.user_info.id) {
            $scope.StoreFollowProduct = true;
            $scope.StoreSettingProfile = false;
          } else {
            $scope.StoreFollowProduct = false;
            $scope.StoreSettingProfile = true;
          }
          // $scope.root = $location.host();
          $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/store/" + $scope.store.id;

          $scope.twLink = "http://twitter.com/intent/tweet?status=" + $scope.store.store_name + "+%40antsquare+" + "https://www.antsquare.com/store/" + $scope.store.id;
          "http://twitter.com/intent/tweet?status=" + $scope.store.store_name + "@antsquare" + "https://www.antsquare.com/store/" + $scope.store.id;

          $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/store/" + $scope.store.id + "&amp;media=" + $scope.store.logo + "&amp;description=Check it out!"

          $scope.mailLink = "mailto:?Subject=Check out " + $scope.store.store_name + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/store/" + $scope.store.id;

          $scope.youtubelink = "https://www.youtube.com/embed/?listType=user_uploads&list=" + $scope.store.youtube;
          $scope.twitterlink = "https://twitter.com/" + $scope.store.instagram;

          $scope.instalink = "http://whateverorigin.org/get?url=" + "https://www.instagram.com/" + $scope.store.instagram + "/media/";
          var URL = $scope.instalink;
          jQuery(function($) {
            $.ajax({
              url: URL,
              crossOrigin: true,
              dataType: "jsonp", // this is important
              cache: false,
              success: function(response) {
                var data = response.contents;
                for (var i = 0; i < data.items.length; i++) {
                  var image = '<img src="' + data.items[i].images.low_resolution.url + '" alt="" />';
                  $(image).appendTo(".images");
                }
              },
              error: function() {
                //var error = "<p>error processing ajax request</p>";
                //$(error).appendTo(".images");
              }
            });
          });

          refreshPlate();



        })
        .catch(function(err) {
          $scope.store = err.message;
        })
    }
    $scope.productTab = true;

    var scrollLoad = false;

    function addLoadMoreCard(scrollLoad) {
      if ($scope.cards.has_more && !scrollLoad) {
        if (scrollLoad === undefined) {
          $scope.cards.products.push({
            type: 'has_more_temp'
          })
        } else {
          $scope.cards.products.push({
            type: 'has_more'
          })
        }

      } else if (!$scope.cards.has_more) {
        $scope.cards.products.push({
          type: 'no_more'
        })
      }
    };

    function getProducts(coords) {
      $scope.cards = {};
      if (coords) {
        $scope.productsPageParams.lat = coords.latitude;
        $scope.productsPageParams.lon = coords.longitude;
      }
      StoreFactory.getProducts($stateParams.store_id, $scope.productsPageParams)
        .then(function(data) {

          $scope.cards = data.data;

          $scope.cards.products = data.data.products.map(function(card) {

            return CardIntent.set(card);
          });
          addLoadMoreCard();
          setTimeout(function() {
            refreshPlate();
          }, 500);
          $scope.loading = false;

        });
    };

    function getResources(coords) {
      $scope.cards = {};
      if (coords) {
        $scope.productsPageParams.lat = coords.latitude;
        $scope.productsPageParams.lon = coords.longitude;
      }
      StoreFactory.getResourcesByHandle($stateParams.store_id, $scope.productsPageParams)
        .then(function(data) {

          $scope.cards = data.data;

          $scope.cards.products = data.data.resources.map(function(card) {

            return CardIntent.set(card);
          });
          addLoadMoreCard();
          setTimeout(function() {
            refreshPlate();
          }, 500);
          $scope.loading = false;

        });




    };

    $scope.$on('loadMoreTemp', function() {
      $scope.loading = true;
      $scope.loadingMoreProducts = true;
      $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

      StoreFactory.getResources($stateParams.store_id, $scope.productsPageParams)
        .then(function(data) {
          $scope.cards.products.pop();

          $scope.newCards = data.data;

          $scope.newCards.products = data.data.data.map(function(card) {
            return CardIntent.set(card);
          });
          $scope.cards.has_more = $scope.newCards.has_more;
          $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
          addLoadMoreCard();
          refreshPlate();
          $scope.loadingMoreProducts = false;
          $scope.loading = false;
        })
        .catch(function(error) {
          $scope.loading = false;
          console.log(error)
          if (error.status === 400) {
            $scope.error = error.data.message;
          } else {
            $scope.error = error.data.message;
          }
          $scope.loadingMoreProducts = false;
        });

    });

    $scope.$on('loadMore', function() {
      if (!scrollLoad) {
        $scope.cards.products.pop();
        $scrollLoad = true;
      }
      $scope.loadMore();
    });
    $scope.loadingMoreProducts = false;

    $scope.loading = null;
    $scope.loadMore = function() {

      if (scrollLoad && !$scope.loadingMoreProducts) {
        $scope.loading = true;
        $scope.loadingMoreProducts = true;
        $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
        StoreFactory.getResources($stateParams.store_id, $scope.productsPageParams)
          .then(function(data) {
            $scope.newCards = data.data;
            $scope.newCards.products = data.data.data.map(function(card) {
              return CardIntent.set(card);
            });
            $scope.cards.has_more = $scope.newCards.has_more;
            $scope.cards.products = $scope.cards.products.concat(scope.newCards.products);
            addLoadMoreCard(scrollLoad);
            refreshPlate();
            $scope.loadingMoreProducts = false;
            $scope.loading = false;
          })
          .catch(function(error) {
            $scope.loading = false;
            console.log(error)
            if (error.status === 400) {
              $scope.error = error.data.message;
            } else {
              $scope.error = error.data.message;
            }
            $scope.loadingMoreProducts = false;
          });
      }

    }

    function refreshPlate() {
      if (angularGridInstance.home) {
        angularGridInstance.home.refresh();
      }
    };

    $scope.showFollowers = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollower/modalFollower.html",
          controller: "ModalFollowerCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };

    $scope.showFollowings = function() {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
          controller: "ModalFollowingCtrl"
        })
        .then(function(modal) {
          modal.close.then(function(result) {
            ModalStorage.end();
          });
        });
    };
    $scope.likeProductToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
      if ($scope.storefeed[index].target_json.is_liked) {

        AccountsFactory.likeProduct($scope.storefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeProduct($scope.storefeed[index].target_json.id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error);
            $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
          })
      }
    };
    $scope.getShareLinks = function(product) {
      // $scope.root = $location.host();
      $scope.fbLinkSF = "https://www.facebook.com/sharer/sharer.php?u=" + "https://dashboard.antsquare.com/product/" + product.id;

      $scope.twLinkSF = "http://twitter.com/intent/tweet?status=" + product.name + "+%40antsquare+" + "https://dashboard.antsquare.com/product/" + product.id + "%26related=antsquare";
      "http://twitter.com/intent/tweet?status=" + product.name + "@antsquare" + "https://dashboard.antsquare.com/product/" + product.id + "&related=antsquare";

      $scope.pinLinkSF = "https://www.pinterest.com/pin/create/button/?url=" + "https://dashboard.antsquare.com/product/" + product.id + "&amp;media=" + product.images[0] + "&amp;description=Check it out!"

      $scope.mailLinkSF = "mailto:?Subject=Check out " + product.name + " on Antsquare&body=Thought you might like this : https://dashboard.antsquare.com/product/" + product.id;
    };

    $scope.followToggle = function() {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.store.user_info.is_following = !$scope.store.user_info.is_following;

      if (!$scope.store.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.store.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
          })
      }
    };

    $scope.followProductToggle = function(index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;

      if (!$scope.storefeed[index].target_json.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.storefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.storefeed[index].target_json.user_id)
          .then(function(data) {})
          .catch(function(error) {
            console.log(error)
            $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
          })
      }
    };

    $scope.productsPageParams = {
      page: 1,
      per_page: 20
    };

    getStore();
    getResources();
    //getFeed();
    //getUser();
    //$scope.getReviews();
    //getAnnouncement();

    $scope.tab = 1;

    $scope.setTab = function(newTab) {
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum) {
      return $scope.tab === tabNum;
    };

  }]);