'use strict';

antsquare
  .controller('StoreAboutCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {
    $scope.StoreSettingProfile = false;
    $scope.StoreFollowProduct = false;
    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
      getStore();
    } else {
      $scope.userInfo = null;

    }

    $rootScope.$on('updateUser', function(event, data) {
      if (data) {
        $scope.userInfo = data;
        getStore();
      } else {
        $scope.userInfo = null;
        $scope.StoreSettingProfile = false;
        $scope.StoreFollowProduct = true;
      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function() {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        //getProducts($scope.coords);
        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }

    function getStore() {
      var input = {
        store_id: $stateParams.store_id,
        viewer_id: null
      };
      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id
      };

      StoreFactory.getStoreById(input)
        .then(function(data) {
          $scope.store = data.data;
          setMap();

        })
        .catch(function(err) {
          $scope.store = err.message;
        })
    }

    getStore();

  }]);