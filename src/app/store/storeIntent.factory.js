'use strict';

antsquare
  .factory('StoreContainer', function() {

    function StoreContainer() {
      this.id = '';
      this.name = '';
      this.description= '';
      this.lat = '';
      this.lon = '';
      this.logo = '';
      this.cover_image= '';
      this.availability = {};
      this.address1 = '';
      this.availability = {};
      this.handle = '';
    }

    return StoreContainer;
  });

antsquare
  .factory('StoreIntent', ['StoreContainer', function(StoreContainer) {
    var storeContainer;

    return {
      start: function(from_store) {
        storeContainer = new StoreContainer();
        storeContainer = from_store;
      },
      setStore: function(store,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index] = store;
      },
      setName: function(name,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].name = name;
      },

      setAddress: function(address,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].address1 = address;
      },

      setDescription: function(description,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].description = description;
      },
      setHandle: function(handle,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].handle = handle;
      },

      setLocation: function(location,index){
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].lat = location.latitude;
        storeContainer.stores[index].lon = location.longitude;
      },

      setLogo: function(logo,index) {
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].logo = logo;
      },

      setCoverImage: function(coverImage, index) {
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].cover_image = coverImage;
      },
      setAvailability: function(availability, index) {
        if (!index){
          index = 0;
        }
        storeContainer.stores[index].availability = availability;
      },

      getStore: function(index) {
        if (!index){
          index = 0;
        }
        return storeContainer.stores[index];

      },
      getContainer: function() {
        return storeContainer;
      }
    };

  }]);
