  'use strict';

  antsquare
    .controller('UserCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', 'ezfb', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory, ezfb) {
      $scope.StoreSettingProfile = false;
      $scope.StoreFollowProduct = false;
      $scope.tagnametitle = "SanJose";
      $scope.isLoading = true;
      // User
      $scope.viewer = false;
      var instagramData = {};
      var showTotalInstaPics = 5;

      if (Auth.isLoggedIn()) {
        $scope.userInfo = Auth.getUserFn();
        $scope.StoreSettingProfile = true;
        console.log($scope.userInfo);
        if ($stateParams.user_id == $scope.userInfo.id) {
          $scope.store = $scope.userInfo;
          $scope.isLoading = false;
          // setMap();
          $scope.StoreFollowProduct = false;
          $scope.instalink = "https://www.instagram.com/" + $scope.store.instagram + "/media/";
          if ($scope.store.instagram) {
            $.get('https://allorigins.us/get?method=raw&url=' + encodeURIComponent($scope.instalink) + '&callback=?', function(data) {
              instagramData = data;
            });
          }
        } else {
          getUser();
        }


        //$scope.twitterlink = "https://twitter.com/tanyafosterblog";
      } else {
        $scope.userInfo = null;

      }

      $rootScope.$on('updateUser', function(event, data) {
        if (data) {
          $scope.userInfo = data;
        } else {
          $scope.userInfo = null;
          $scope.StoreSettingProfile = false;
          $scope.StoreFollowProduct = true;
        }
      });

      // Check Mobile
      $scope.isMobile = null;
      if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
        $scope.isMobile = true;
      } else {
        $scope.isMobile = false;
      }

      var distance;
      $scope.coords = undefined;
      // $scope.store = undefined;

      // Commenting 
      // $rootScope.$on('setLocation', function() {
      //   $scope.coords = GeoLocationService.getCoords();
      //   if ($scope.store) {
      //     getResources($scope.coords);

      //     distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
      //     for (var key in distance) {
      //       $scope.store[key] = distance[key];
      //     }
      //   }
      // });

      function setMap() {
        $scope.map = {
          center: {
            latitude: $scope.store.lat,
            longitude: $scope.store.lon
          },
          control: {},
          zoom: 12,
          options: {
            scrollwheel: true
          },
          dragging: false
        };
        $scope.marker = new MarkerService($scope.store);
        $scope.marker.options = {
          icon: $scope.marker.icon + "?w=30"
        };
      }

      var scrollLoad = false;

      function addLoadMoreCard(scrollLoad) {

        if ($scope.cards.has_more && !scrollLoad) {
          if (scrollLoad === undefined) {
            $scope.cards.products.push({
              type: 'has_more_temp'
            })

          } else {
            $scope.cards.products.push({
              type: 'has_more'
            })
          }

        } else if (!$scope.cards.has_more) {
          $scope.cards.products.push({
            type: 'no_more'
          })
        }
      };
      // Commenting 
      // $scope.$on('loadMoreTemp', function() {
      //   $scope.loading = true;
      //   $scope.loadingMoreProducts = true;
      //   $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

      //   StoreFactory.getResourcesbyUser($stateParams.user_id, $scope.productsPageParams)
      //     .then(function(data) {
      //       $scope.cards.products.pop();

      //       $scope.newCards = data.data;

      //       $scope.newCards.products = data.data.resources.map(function(card) {
      //         return CardIntent.set(card);
      //       });
      //       $scope.cards.has_more = $scope.newCards.has_more;
      //       $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
      //       addLoadMoreCard();
      //       refreshPlate();
      //       $scope.loadingMoreProducts = false;
      //       $scope.loading = false;
      //     })
      //     .catch(function(error) {
      //       $scope.loading = false;
      //       console.log(error)
      //       if (error.status === 400) {
      //         $scope.error = error.data.message;
      //       } else {
      //         $scope.error = error.data.message;
      //       }
      //       $scope.loadingMoreProducts = false;
      //     });

      // });

      // $scope.$on('loadMore', function() {
      //   if (!scrollLoad) {
      //     $scope.cards.products.pop();
      //     scrollLoad = true;
      //   }
      //   $scope.loadMore();
      // });
      // $scope.loadingMoreProducts = false;

      // $scope.loading = null;
      // $scope.loadMore = function() {


      //   if (!scrollLoad && !$scope.loadingMoreProducts) {
      //     $scope.loading = true;
      //     $scope.loadingMoreProducts = true;
      //     $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
      //     StoreFactory.getResourcesbyUser($stateParams.user_id, $scope.productsPageParams)
      //       .then(function(data) {

      //         $scope.newCards = data.data;
      //         $scope.newCards.products = data.data.resources.map(function(card) {
      //           return CardIntent.set(card);
      //         });
      //         $scope.cards.has_more = $scope.newCards.has_more;
      //         $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
      //         addLoadMoreCard(scrollLoad);
      //         refreshPlate();
      //         $scope.loadingMoreProducts = false;
      //         $scope.loading = false;
      //       })
      //       .catch(function(error) {
      //         $scope.loading = false;
      //         console.log(error)
      //         if (error.status === 400) {
      //           $scope.error = error.data.message;
      //         } else {
      //           $scope.error = error.data.message;
      //         }
      //         $scope.loadingMoreProducts = false;
      //       });
      //   }

      // }

      function getUser() {
        console.log("getuser..");
        var input = {
          user_id: $stateParams.user_id,
          viewer_id: null
        };
        if ($scope.userInfo) {
          input.viewer_id = $scope.userInfo.id;
        };

        StoreFactory.getUserById(input)
          .then(function(data) {
            $scope.viewer = true;
            $scope.store = data.data;
            console.log(data.data);
            $scope.isLoading = false;
            // setMap();
            if (input.viewer_id != $scope.store.user_info.id) {
              $scope.StoreFollowProduct = true;
              // $scope.StoreSettingProfile = false;
            } else {
              $scope.StoreFollowProduct = false;
              // $scope.StoreSettingProfile = true;
            }


            // $scope.root = $location.host();
            // $scope.fbLink = "https://www.facebook.com/sharer/sharer.php?u=" + "https://www.antsquare.com/user/" + $scope.store.id;

            $scope.twLink = "http://twitter.com/intent/tweet?status=" + $scope.store.store_name + "+%40antsquare+" + "https://www.antsquare.com/user/" + $scope.store.id;
            // "http://twitter.com/intent/tweet?status=" + $scope.store.store_name + "@antsquare" + "https://www.antsquare.com/user/" + $scope.store.id;

            $scope.pinLink = "https://www.pinterest.com/pin/create/button/?url=" + "https://www.antsquare.com/user/" + $scope.store.id + "&amp;media=" + $scope.store.logo + "&amp;description=Check it out!"

            $scope.mailLink = "mailto:?Subject=Check out " + $scope.store.store_name + " on Antsquare&body=Thought you might like this : https://www.antsquare.com/user/" + $scope.store.id;
            $scope.twitterlink = "https://twitter.com/" + $scope.store.twitter;
            $scope.youtubelink = "https://www.youtube.com/embed/?listType=user_uploads&list=" + $scope.store.youtube;
            // $scope.instalink = "http://whateverorigin.org/get?url=" + "https://www.instagram.com/" + $scope.store.instagram + "/media/";
            $scope.instalink = "https://www.instagram.com/" + $scope.store.instagram + "/media/";
            //$scope.store.facebook = "mizzu123";
            // var URL = $scope.instalink;

            $.get('https://allorigins.us/get?method=raw&url=' + encodeURIComponent($scope.instalink) + '&callback=?', function(data) {
              instagramData = data;
            });
            refreshPlate();
          })
          .catch(function(err) {
            $scope.store = err.message;
          })
      }



      /*function getUser() {
          var input = {
              user_id: $stateParams.user_id,
              viewer_id: null
          };
          if ($scope.userInfo) {
              input.viewer_id = $scope.userInfo.id;
              var z = $scope.userInfo.username;
          };
          var x = input.viewer_id;
          var y = $stateParams.user_id;
          if (z == y || x == y) {
              $scope.UserFollowProfile = false;
              $scope.UserEditProfile = true;
          } else {
              $scope.UserFollowProfile = true;
              $scope.UserEditProfile = false;
          }
          StoreFactory.getUserById(input)
              .then(function(data) {
                  $scope.user = data.data;
                  //setMap();
                  // $scope.root = $location.host();
                  refreshPlate();
              })
              .catch(function(err) {
                  $scope.user = err.message;
              })
      }*/

      $scope.promos = null;

      function getAnnouncement() {
        $scope.promoPagesParams = {
          page: 1,
          per_page: 4
        };

        StoreFactory.getAnnouncement($stateParams.store_id, $scope.promoPagesParams)
          .then(function(data) {
            $scope.promos = data.data.announcements.rows;
            if (data.data.total > 0) {
              $scope.onSection = 0;
            }
          });
      };

      function getProducts(coords) {
        $scope.cards = {};

        if (coords) {
          $scope.productsPageParams.lat = coords.latitude;
          $scope.productsPageParams.lon = coords.longitude;
        }

        if ($scope.userInfo) {
          $scope.productsPageParams.viewer_id = $scope.userInfo.id;
        };

        StoreFactory.getMoments($stateParams.user_id, $scope.productsPageParams)
          .then(function(data) {
            $scope.cards = data.data;
            $scope.cards.products = data.data.moments.map(function(card) {
              return CardIntent.set(card);
            });
            addLoadMoreCard();
            setTimeout(function() {
              refreshPlate();
            }, 500);
            $scope.loading = false;

          });
      };

      function getResources(coords) {
        $scope.cards = {};
        $scope.coords = GeoLocationService.getCoords();
        //console.log($scope.coords);
        if ($scope.coords) {
          $scope.productsPageParams.lat = $scope.coords.latitude;
          $scope.productsPageParams.lon = $scope.coords.longitude;
        }
        //console.log($scope.productsPageParams.lat);
        StoreFactory.getResourcesbyUser($stateParams.user_id, $scope.productsPageParams)
          .then(function(data) {

            $scope.cards = data.data;

            $scope.cards.products = data.data.resources.map(function(card) {

              return CardIntent.set(card);
            });
            addLoadMoreCard();
            setTimeout(function() {
              refreshPlate();
            }, 500);
            $scope.loading = false;

          });
      };



      $scope.userfeed = [];

      function getFeed() {
        var input = {
          user_id: $stateParams.user_id,
          viewer_id: null
        };
        if ($scope.userInfo) {
          input.viewer_id = $scope.userInfo.id
        };
        StoreFactory.getUserFeed(input)
          .then(function(data) {
            $scope.userfeed = data.data.cards;
            $scope.$broadcast('productLoaded');

          })
          .catch(function(err) {
            $scope.userfeed = err.message;
          });

      };

      function refreshPlate() {
        if (angularGridInstance.home) {
          angularGridInstance.home.refresh();
        }
      };

      $scope.showFollowers = function() {
        ModalStorage.set($scope.store);
        ModalService.showModal({
            templateUrl: "app/components/modal/modalFollower/modalUserFollower.html",
            controller: "ModalUserFollowerCtrl"
          })
          .then(function(modal) {
            modal.close.then(function(result) {
              ModalStorage.end();
            });
          });
      };

      $scope.vm = {};

      /*$scope.postComment = function (id) {
        if (!$scope.submited) {
          $scope.submited = true;
          StoreFactory.postComment($scope.form, id)
            .then(function (data) {
              console.log(data)
              $scope.vm.comments.unshift(data.data);
              $scope.vm.total++;
              $scope.comments.total++;
              $scope.form.content = "";
              $scope.submited = false;
            })
            .catch(function (error) {
              console.log(error)
              $scope.submited = false;
            })
        }
      };*/

      $scope.showFollowings = function() {
        ModalStorage.set($scope.store);
        ModalService.showModal({
            templateUrl: "app/components/modal/modalFollowing/modalUserFollowing.html",
            controller: "ModalUserFollowingCtrl"
          })
          .then(function(modal) {
            modal.close.then(function(result) {
              ModalStorage.end();
            });
          });
      };
      $scope.showUserLikes = function() {
        ModalStorage.set($scope.user.id);
        ModalService.showModal({
            templateUrl: "app/components/modal/modalLikes/modalUserLikes.html",
            controller: "ModalUserLikesCtrl"
          })
          .then(function(modal) {
            modal.close.then(function(result) {
              ModalStorage.end();
            });
          });
      };
      $scope.likeToggle = function(index) {
        if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

        $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
        if ($scope.userfeed[index].target_json.is_liked) {

          AccountsFactory.likeMoment($scope.userfeed[index].target_json.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked
            })
        } else {
          AccountsFactory.unlikeMoment($scope.userfeed[index].target_json.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error);
              $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
            })
        }
      };
      $scope.likeProductToggle = function(index) {
        if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

        $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
        if ($scope.userfeed[index].target_json.is_liked) {

          AccountsFactory.likeProduct($scope.userfeed[index].target_json.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked
            })
        } else {
          AccountsFactory.unlikeProduct($scope.userfeed[index].target_json.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error);
              $scope.userfeed[index].target_json.is_liked = !$scope.userfeed[index].target_json.is_liked;
            })
        }
      };

      $scope.followMomentToggle = function(index) {
        if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

        $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;

        if (!$scope.userfeed[index].target_json.is_following) {
          AccountsFactory.unfollowUser($scope.userfeed[index].target_json.user_id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;
            })
        } else {
          AccountsFactory.followUser($scope.userfeed[index].target_json.user_id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.userfeed[index].target_json.is_following = !$scope.userfeed[index].target_json.is_following;
            })
        }
      };

      $scope.followToggle = function() {
        if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

        $scope.user.user_info.is_following = !$scope.user.user_info.is_following;

        if (!$scope.user.user_info.is_following) {
          AccountsFactory.unfollowUser($scope.user.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.user.user_info.is_following = !$scope.user.user_info.is_following;
            })
        } else {
          AccountsFactory.followUser($scope.user.id)
            .then(function(data) {})
            .catch(function(error) {
              console.log(error)
              $scope.user.user_info.is_following = !$scope.user.user_info.is_following;
            })
        }
      };

      $scope.submitComment = function(id) {

        $rootScope.$broadcast('postComment', $scope.content, id)
        $scope.content = null;
      };

      $scope.reviewPagesParams = {
        page: 1,
        per_page: 10
      };

      $scope.productsPageParams = {
        page: 1,
        per_page: 20,
        viewer_id: null
      };

      // getUser();
      getResources();
      $scope.tab = 1;
      $scope.setTab = function(newTab) {
        $scope.tab = newTab;
        if (newTab == 6) {
          fetchInstagram();
        }
        // if (newTab == 2) {
        //   $('.fb-page').clone(true).replaceAll('#fb-root');
        // }
      };

      $scope.isSet = function(tabNum) {
        return $scope.tab === tabNum;
      };


      var once = true;

      function fetchInstagram() {
        var endNum = 0;

        if (showTotalInstaPics < instagramData.items.length) {
          endNum = showTotalInstaPics;
          $scope.moreInstaPics = true;
        } else {
          endNum = instagramData.items.length;
          $scope.moreInstaPics = false;
        }
        if (once) {
          for (var i = 0; i < endNum; i++) {
            var image = '<img src="' + instagramData.items[i].images.low_resolution.url + '" alt="" width="200" style="padding:5px" />';
            $(image).appendTo(".images");
          }
          once = false;
        }

      }

      $scope.loadMoreInstaPics = function() {
        var endNum = 0;
        if ((showTotalInstaPics + 5) < instagramData.items.length) {
          endNum = showTotalInstaPics + 5;
        } else {
          endNum = instagramData.items.length;
          $scope.moreInstaPics = false;
        }
        for (var i = showTotalInstaPics; i < endNum; i++) {
          var image = '<img src="' + instagramData.items[i].images.low_resolution.url + '" alt="" width="200" style="padding:5px" />';
          $(image).appendTo(".images");
        }
        showTotalInstaPics += 5;
      }


    }]);