'use strict';

antsquare.config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {

    $stateProvider

      .state('store.storefeed', {
        url: '/storefeed',
        views: {
          'col-12': {
            templateUrl: 'app/store/storefeed.html',
            controller: 'StoreFeedCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('store.storeproducts', {
        url: '/storeproducts',
        views: {
          'col-12': {
            templateUrl: 'app/store/storeproduct.html',
            controller: 'StoreProductCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('storeHandle.storeHproducts', {
        url: '/storeproducts',
        views: {
          'col-12': {
            templateUrl: 'app/store/storeproduct.html',
            controller: 'StoreHandleProductCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('store.storeabout', {
        url: '/storeabout',
        views: {
          'col-12': {
            templateUrl: 'app/store/storeabout.html',
            controller: 'StoreAboutCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('storeHandle.storeHabout', {
        url: '/storeabout',
        views: {
          'col-12': {
            templateUrl: 'app/store/storeabout.html',
            controller: 'StoreHandleAboutCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('storeHandle.storeHreview', {
        url: '/storereview',
        views: {
          'col-12': {
            templateUrl: 'app/store/storereview.html',
            controller: 'StoreHandlereviewCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('store.storereview', {
        url: '/storereview',
        views: {
          'col-12': {
            templateUrl: 'app/store/storereview.html',
            controller: 'StoreReviewCtrl',
          }
        },
        onEnter: ['$location', 'seoFactory', function($location, seoFactory) {
          seoFactory.setDescription("We empower users to explore what’s for sale in their community. It’s the best way to support your local economy while discovering items you never would have found otherwise. Here’s how it works.");
          seoFactory.setTitle("Antsquare");
          seoFactory.setType("article");
          var link = "https://antsquare-www.imgix.net/imgs/logo-notext.png?w=300&h=300";
          seoFactory.setImage1(link);
          seoFactory.setUrl($location.absUrl());
        }]
      })
      .state('store.storetwitter', {
        url: '/:twitter',
        views: {
          'col-12': {
            templateUrl: 'app/store/storetwitter.html',
            controller: 'StoreTwitterCtrl',
          }
        },
        Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
          seoFactory.getStoreSeo($stateParams.store_id)
            .then(function(data) {
              seoFactory.setDescription(data.data.description);
              seoFactory.setTitle(data.data.title);
              seoFactory.setType(data.data.type);
              seoFactory.setImage1(data.data.image);
              seoFactory.setUrl($location.absUrl());
            })
          return null
        }]
      })
      .state('user.storetwitter', {
        url: '/:twitter',
        views: {
          'col-12': {
            templateUrl: 'app/store/storetwitter.html',
            controller: 'StoreTwitterCtrl',
          }
        },
        Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
          seoFactory.getStoreSeo($stateParams.store_id)
            .then(function(data) {
              seoFactory.setDescription(data.data.description);
              seoFactory.setTitle(data.data.title);
              seoFactory.setType(data.data.type);
              seoFactory.setImage1(data.data.image);
              seoFactory.setUrl($location.absUrl());
            })
          return null
        }]
      })
      .state('username.storetwitter', {
        url: '/:twitter',
        views: {
          'col-12': {
            templateUrl: 'app/store/storetwitter.html',
            controller: 'StoreTwitterCtrl',
          }
        },
        Store: ['seoFactory', '$stateParams', '$location', function(seoFactory, $stateParams, $location) {
          seoFactory.getStoreSeo($stateParams.store_id)
            .then(function(data) {
              seoFactory.setDescription(data.data.description);
              seoFactory.setTitle(data.data.title);
              seoFactory.setType(data.data.type);
              seoFactory.setImage1(data.data.image);
              seoFactory.setUrl($location.absUrl());
            })
          return null
        }]
      })

  }
])