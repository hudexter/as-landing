'use strict';

antsquare
  .controller('StoreFeedCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function ($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {
    $scope.StoreSettingProfile = false;
    $scope.StoreFollowProduct = false;
    // User
    if (Auth.isLoggedIn()) {
      $scope.userInfo = Auth.getUserFn();
      //getStore();
    } else {
      $scope.userInfo = null;

    }

    $rootScope.$on('updateUser', function (event, data) {
      if (data) {
        $scope.userInfo = data;
        //getStore();
      } else {
        $scope.userInfo = null;
        $scope.StoreSettingProfile = false;
        $scope.StoreFollowProduct = true;
      }
    });

    // Check Mobile
    $scope.isMobile = null;
    if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    var distance;
    $scope.coords = undefined;
    $scope.store = undefined;

    $rootScope.$on('setLocation', function () {
      $scope.coords = GeoLocationService.getCoords();
      if ($scope.store) {
        getProducts($scope.coords);
        distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
        for (var key in distance) {
          $scope.store[key] = distance[key];
        }
      }
    });

    function setMap() {
      $scope.map = {
        center: {
          latitude: $scope.store.lat,
          longitude: $scope.store.lon
        },
        control: {},
        zoom: 12,
        options: {
          scrollwheel: true
        },
        dragging: false
      };
      $scope.marker = new MarkerService($scope.store);
      $scope.marker.options = {
        icon: $scope.marker.icon + "?w=30"
      };
    }

    $scope.storefeed = [];

    function getFeed() {
      var input = {
        store_id: $stateParams.store_id,
        viewer_id: null
      };
      if ($scope.userInfo) {
        input.viewer_id = $scope.userInfo.id
      };
      StoreFactory.getStoreFeed(input)
        .then(function (data) {
          $scope.storefeed = data.data.cards;
          $scope.$broadcast('productLoaded');

        })
        .catch(function (err) {
          $scope.storefeed = err.message;
        });

    };

    $scope.showFollowers = function () {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollower/modalFollower.html",
          controller: "ModalFollowerCtrl"
        })
        .then(function (modal) {
          modal.close.then(function (result) {
            ModalStorage.end();
          });
        });
    };

    $scope.showFollowings = function () {
      ModalStorage.set($scope.store);
      ModalService.showModal({
          templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
          controller: "ModalFollowingCtrl"
        })
        .then(function (modal) {
          modal.close.then(function (result) {
            ModalStorage.end();
          });
        });
    };
    $scope.likeProductToggle = function (index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

      $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
      if ($scope.storefeed[index].target_json.is_liked) {

        AccountsFactory.likeProduct($scope.storefeed[index].target_json.id)
          .then(function (data) {})
          .catch(function (error) {
            console.log(error)
            $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked
          })
      } else {
        AccountsFactory.unlikeProduct($scope.storefeed[index].target_json.id)
          .then(function (data) {})
          .catch(function (error) {
            console.log(error);
            $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
          })
      }
    };
    $scope.getShareLinks = function (product) {
      // $scope.root = $location.host();
      $scope.fbLinkSF = "https://www.facebook.com/sharer/sharer.php?u=" + "https://dashboard.antsquare.com/product/" + product.id;

      $scope.twLinkSF = "http://twitter.com/intent/tweet?status=" + product.name + "+%40antsquare+" + "https://dashboard.antsquare.com/product/" + product.id + "%26related=antsquare";
      "http://twitter.com/intent/tweet?status=" + product.name + "@antsquare" + "https://dashboard.antsquare.com/product/" + product.id + "&related=antsquare";

      $scope.pinLinkSF = "https://www.pinterest.com/pin/create/button/?url=" + "https://dashboard.antsquare.com/product/" + product.id + "&amp;media=" + product.images[0] + "&amp;description=Check it out!"

      $scope.mailLinkSF = "mailto:?Subject=Check out " + product.name + " on Antsquare&body=Thought you might like this : https://dashboard.antsquare.com/product/" + product.id;
    };

    $scope.followProductToggle = function (index) {
      if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

      $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;

      if (!$scope.storefeed[index].target_json.user_info.is_following) {
        AccountsFactory.unfollowUser($scope.storefeed[index].target_json.user_id)
          .then(function (data) {})
          .catch(function (error) {
            console.log(error)
            $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
          })
      } else {
        AccountsFactory.followUser($scope.storefeed[index].target_json.user_id)
          .then(function (data) {})
          .catch(function (error) {
            console.log(error)
            $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
          })
      }
    };

    //getStore();
    //getProducts();
    getFeed();
    //getUser();
    //$scope.getReviews();
    //getAnnouncement();

  }]);