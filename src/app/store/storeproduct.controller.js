'use strict';

antsquare
    .controller('StoreProductCtrl', ['$scope', '$state', '$stateParams', 'StoreFactory', '$window', 'MapService', 'GeoLocationService', '$rootScope', '$location', 'CardIntent', 'angularGridInstance', 'MarkerService', 'ModalService', 'ModalStorage', '$document', 'deviceDetector', 'Auth', 'AccountsFactory', function($scope, $state, $stateParams, StoreFactory, $window, MapService, GeoLocationService, $rootScope, $location, CardIntent, angularGridInstance, MarkerService, ModalService, ModalStorage, $document, deviceDetector, Auth, AccountsFactory) {
        $scope.StoreSettingProfile = false;
        $scope.StoreFollowProduct = false;
        // User
        if (Auth.isLoggedIn()) {
            $scope.userInfo = Auth.getUserFn();
            //getStore();
        } else {
            $scope.userInfo = null;

        }

        $rootScope.$on('updateUser', function(event, data) {
            if (data) {
                $scope.userInfo = data;
                //getStore();
            } else {
                $scope.userInfo = null;
                $scope.StoreSettingProfile = false;
                $scope.StoreFollowProduct = true;
            }
        });

        // Check Mobile
        $scope.isMobile = null;
        if (deviceDetector.device === "android" || deviceDetector.device === "iphone" || deviceDetector.device === "ipod" || deviceDetector.device === "blackberry" || deviceDetector.device === "windows-phone") {
            $scope.isMobile = true;
        } else {
            $scope.isMobile = false;
        }

        var distance;
        $scope.coords = undefined;
        $scope.store = undefined;

        $rootScope.$on('setLocation', function() {
            $scope.coords = GeoLocationService.getCoords();
            if ($scope.store) {
                getProducts($scope.coords);
                distance = MapService.getDistance($scope.store.lat, $scope.store.lon, $scope.coords.latitude, $scope.coords.longitude);
                for (var key in distance) {
                    $scope.store[key] = distance[key];
                }
            }
        });

        function setMap() {
            $scope.map = {
                center: {
                    latitude: $scope.store.lat,
                    longitude: $scope.store.lon
                },
                control: {},
                zoom: 12,
                options: {
                    scrollwheel: true
                },
                dragging: false
            };
            $scope.marker = new MarkerService($scope.store);
            $scope.marker.options = {
                icon: $scope.marker.icon + "?w=30"
            };
        }

        var scrollLoad = false;

        function addLoadMoreCard(scrollLoad) {
            if ($scope.cards.has_more && !scrollLoad) {
                if (scrollLoad === undefined) {
                    $scope.cards.products.push({
                        type: 'has_more_temp'
                    })
                } else {
                    $scope.cards.products.push({
                        type: 'has_more'
                    })
                }

            } else if (!$scope.cards.has_more) {
                $scope.cards.products.push({
                    type: 'no_more'
                })
            }
        };

        function getProducts(coords) {
            $scope.cards = {};
            if (coords) {
                $scope.productsPageParams.lat = coords.latitude;
                $scope.productsPageParams.lon = coords.longitude;
            }
            //$scope.ShowHoverPage = true;
            StoreFactory.getResources($stateParams.store_id, $scope.productsPageParams)
                .then(function(data) {

                    $scope.cards = data.data;


                    $scope.cards.data = data.data.data.map(function(card) {

                        return CardIntent.set(card);
                    });
                    //addLoadMoreCard();
                    setTimeout(function() {
                        refreshPlate();
                    }, 500);
                    $scope.loading = false;

                });
        };

        $scope.$on('loadMoreTemp', function() {
            $scope.loading = true;
            $scope.loadingMoreProducts = true;
            $scope.productsPageParams.page = $scope.productsPageParams.page + 1;

            StoreFactory.getProducts($stateParams.store_id, $scope.productsPageParams)
                .then(function(data) {
                    $scope.cards.products.pop();

                    $scope.newCards = data.data;

                    $scope.newCards.products = data.data.products.map(function(card) {
                        return CardIntent.set(card);
                    });
                    $scope.cards.has_more = $scope.newCards.has_more;
                    $scope.cards.products = $scope.cards.products.concat($scope.newCards.products);
                    addLoadMoreCard();
                    refreshPlate();
                    $scope.loadingMoreProducts = false;
                    $scope.loading = false;
                })
                .catch(function(error) {
                    $scope.loading = false;
                    console.log(error)
                    if (error.status === 400) {
                        $scope.error = error.data.message;
                    } else {
                        $scope.error = error.data.message;
                    }
                    $scope.loadingMoreProducts = false;
                });

        });

        $scope.$on('loadMore', function() {
            if (!scrollLoad) {
                $scope.cards.products.pop();
                $scrollLoad = true;
            }
            $scope.loadMore();
        });
        $scope.loadingMoreProducts = false;

        $scope.loading = null;
        $scope.loadMore = function() {

            if (scrollLoad && !$scope.loadingMoreProducts) {
                $scope.loading = true;
                $scope.loadingMoreProducts = true;
                $scope.productsPageParams.page = $scope.productsPageParams.page + 1;
                StoreFactory.getProducts($stateParams.store_id, $scope.productsPageParams)
                    .then(function(data) {
                        $scope.newCards = data.data;
                        $scope.newCards.products = data.data.products.map(function(card) {
                            return CardIntent.set(card);
                        });
                        $scope.cards.has_more = $scope.newCards.has_more;
                        $scope.cards.products = $scope.cards.products.concat(scope.newCards.products);
                        addLoadMoreCard(scrollLoad);
                        refreshPlate();
                        $scope.loadingMoreProducts = false;
                        $scope.loading = false;
                    })
                    .catch(function(error) {
                        $scope.loading = false;
                        console.log(error)
                        if (error.status === 400) {
                            $scope.error = error.data.message;
                        } else {
                            $scope.error = error.data.message;
                        }
                        $scope.loadingMoreProducts = false;
                    });
            }

        }

        function refreshPlate() {
            if (angularGridInstance.home) {
                angularGridInstance.home.refresh();
            }
        };

        $scope.showFollowers = function() {
            ModalStorage.set($scope.store);
            ModalService.showModal({
                    templateUrl: "app/components/modal/modalFollower/modalFollower.html",
                    controller: "ModalFollowerCtrl"
                })
                .then(function(modal) {
                    modal.close.then(function(result) {
                        ModalStorage.end();
                    });
                });
        };

        $scope.showFollowings = function() {
            ModalStorage.set($scope.store);
            ModalService.showModal({
                    templateUrl: "app/components/modal/modalFollowing/modalFollowing.html",
                    controller: "ModalFollowingCtrl"
                })
                .then(function(modal) {
                    modal.close.then(function(result) {
                        ModalStorage.end();
                    });
                });
        };
        $scope.likeProductToggle = function(index) {
            if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'like')

            $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
            if ($scope.storefeed[index].target_json.is_liked) {

                AccountsFactory.likeProduct($scope.storefeed[index].target_json.id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error)
                        $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked
                    })
            } else {
                AccountsFactory.unlikeProduct($scope.storefeed[index].target_json.id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error);
                        $scope.storefeed[index].target_json.is_liked = !$scope.storefeed[index].target_json.is_liked;
                    })
            }
        };
        $scope.getShareLinks = function(product) {
            // $scope.root = $location.host();
            $scope.fbLinkSF = "https://www.facebook.com/sharer/sharer.php?u=" + "https://dashboard.antsquare.com/product/" + product.id;

            $scope.twLinkSF = "http://twitter.com/intent/tweet?status=" + product.name + "+%40antsquare+" + "https://dashboard.antsquare.com/product/" + product.id + "%26related=antsquare";
            "http://twitter.com/intent/tweet?status=" + product.name + "@antsquare" + "https://dashboard.antsquare.com/product/" + product.id + "&related=antsquare";

            $scope.pinLinkSF = "https://www.pinterest.com/pin/create/button/?url=" + "https://dashboard.antsquare.com/product/" + product.id + "&amp;media=" + product.images[0] + "&amp;description=Check it out!"

            $scope.mailLinkSF = "mailto:?Subject=Check out " + product.name + " on Antsquare&body=Thought you might like this : https://dashboard.antsquare.com/product/" + product.id;
        };

        $scope.followToggle = function() {
            if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

            $scope.store.user_info.is_following = !$scope.store.user_info.is_following;

            if (!$scope.store.user_info.is_following) {
                AccountsFactory.unfollowUser($scope.store.user_id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error)
                        $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
                    })
            } else {
                AccountsFactory.followUser($scope.store.user_id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error)
                        $scope.store.user_info.is_following = !$scope.store.user_info.is_following;
                    })
            }
        };

        $scope.followProductToggle = function(index) {
            if (!$scope.userInfo) return $rootScope.$broadcast('openLoginModal', 'follow')

            $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;

            if (!$scope.storefeed[index].target_json.user_info.is_following) {
                AccountsFactory.unfollowUser($scope.storefeed[index].target_json.user_id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error)
                        $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
                    })
            } else {
                AccountsFactory.followUser($scope.storefeed[index].target_json.user_id)
                    .then(function(data) {})
                    .catch(function(error) {
                        console.log(error)
                        $scope.storefeed[index].target_json.user_info.is_following = !$scope.storefeed[index].target_json.user_info.is_following;
                    })
            }
        };

        $scope.productsPageParams = {
            page: 1,
            per_page: 20
        };

        //getStore();
        getProducts();
        //getFeed();
        //getUser();
        //$scope.getReviews();
        //getAnnouncement();

    }]);