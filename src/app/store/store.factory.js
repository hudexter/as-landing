'use strict';

antsquare
  .factory('StoreFactory', ['$http', 'Config', function($http, Config) {
    var url = Config.API_BASEURL;
    var endpoint = '/stores';

    return {
      getStoreById: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.store_id + '/mpview',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getStoreByhandle: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.handle + '/mpview',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getUserById: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + data.user_id + '/mpview',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getUsernames: function(query) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/all/suggest/username?query=' + query,
          method: 'GET',

        });
      },
      //
      // getStoreById: function (input) {
      //
      //  return $http
      //    .get(Config.CORE_BASEURL + '/v5/stores/' + id + '/mpview')
      // },
      getStoreList: function(query) {
        return $http
          .get(url + '/all/search?&query=' + query)
          .then(function(data) {
            return data.data;
          });
      },
      getStorePageView: function(store_id, viewer_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + store_id + '/pageview',
          method: 'GET',
          params: {
            viewer_id: viewer_id,
          }
        });
      },
      getUserPageView: function(user_id, viewer_id) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + user_id + '/pageview',
          method: 'GET',
          params: {
            viewer_id: viewer_id,
          }
        });
      },
      update: function(store) {
        var id = store.id;
        return $http
          .put(Config.API_BASEURL + '/stores/' + id, store);
      },
      getSchedule: function(store_id) {
        return $http
          .get(url + '/stores/schedule/' + store_id, {
            cache: true
          });
      },
      editSchedule: function(store_id, scheduleData) {
        return $http
          .put(url + '/stores/schedule/' + store_id, {
            monday: scheduleData.monday,
            tuesday: scheduleData.tuesday,
            wednesday: scheduleData.wednesday,
            thursday: scheduleData.thursday,
            friday: scheduleData.friday,
            saturday: scheduleData.saturday,
            sunday: scheduleData.sunday
          });
      },
      me: function() {
        return $http
          .get(Config.API_BASEURL + '/stores/me/');
      },
      myProducts: function() {
        return $http
          .get(Config.API_BASEURL + '/stores/me/products');
      },
      myMoments: function(params) {
        return $http({
          url: Config.API_BASEURL + '/moments/me',
          method: 'GET',
          params: params
        });
      },
      getProducts: function(store_id, params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + store_id + '/products',
          method: 'GET',
          params: params
        });
      },
      getResources: function(store_id, params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + store_id + '/resources',
          method: 'GET',
          params: params
        });
      },
      getResourcesbyUser: function(user_id, params) {

        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + user_id + '/resources',
          method: 'GET',
          params: params
        });
      },
      getTagResources: function(params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/all/search/tags',
          method: 'GET',
          params: params
        });
      },
      getResourcesByHandle: function(store_id, params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/$' + store_id + '/resources',
          method: 'GET',
          params: params
        });
      },
      getProductsByHandle: function(store_id, params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/$' + store_id + '/products',
          method: 'GET',
          params: params
        });
      },
      getMoments: function(user_id, params) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + user_id + '/moments',
          method: 'GET',
          params: params
        });
      },
      getUserFeed: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/users/' + data.user_id + '/publicfeed',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getSocialHomeFeed: function(params) {
        return $http({
          url: Config.API_BASEURL + '/users/social',
          method: 'GET',
          params: params
        });
      },
      getSocialPublicFeed: function(params) {
        return $http({
          url: Config.API_BASEURL + '/users/social',
          method: 'GET',
          params: params
        });
      },
      getStoreFeed: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.store_id + '/publicfeed',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getStoreFeedByHandle: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/$' + data.store_id + '/publicfeed',
          method: 'GET',
          params: {
            viewer_id: data.viewer_id,
          }
        });
      },
      getAnnouncement: function(store_id, params) {
        params = params || null;
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + store_id + '/announcement',
          method: 'GET',
          params: params
        });
      },
      getAnnouncementById: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.store_id + '/announcement/' + data.id,
          method: 'GET',
        });
      },
      postAnnouncement: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/announcement',
          method: 'POST',
          data: data
        });
      },
      putAnnouncement: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.store_id + '/announcement/' + data.id,
          method: 'POST',
          data: data
        });
      },
      deleteAnnouncement: function(data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + data.store_id + '/announcement/' + data.id,
          method: 'DELETE',
          data: data
        });
      },
      getMap: function(lat, lon, width, height) {

        // Waiting for link
        // var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:" + "http://antsqua.re/v24di6" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;

        return "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:http://goo.gl/t1GCnI" + "%7C" + lat + "," + lon;

      },
      getSignMap: function(lat, lon, width, height) {
        // Waiting for link
        // var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:" + "http://antsqua.re/v24di6" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;

        var url = "https://maps.googleapis.com/maps/api/staticmap?size=" + width + "x" + height + "&zoom=15&markers=icon:http://antsqua.re/b4451mg" + "%7C" + lat + "," + lon + "&client=" + Config.GOOGLEMAP_CLIENTID;

        var mapData = {
          'url': url
        };

        return $http
          .post('https://antsquare-loctrack-production.herokuapp.com/sign', mapData);
      },
      claimStore: function(code) {
        return $http({
          url: url + '/stores/claim',
          method: "POST",
          data: code
        })
      },
      clearStore: function(data) {
        return $http
          .put(Config.API_BASEURL + '/stores/' + data.id + '/clear', data);
      },
      getReviews: function(store_id, data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/' + store_id + '/review',
          method: "GET",
          params: data
        })
      },
      getReviewsByHandle: function(store_id, data) {
        return $http({
          url: Config.CORE_BASEURL + '/v5/stores/$' + store_id + '/review',
          method: "GET",
          params: data
        })
      },
      importCSV: function(csv) {
        return $http({
          url: url + '/stores/importcsv',
          method: "POST",
          data: csv
        })
      },
      postComment: function(content, moment_id) {
        return $http
          .post(Config.CORE_BASEURL + '/v5/moments/' + moment_id + '/comment', content);
      },
      uploadPhoto: function(imgUrl) {
        return $http({
          url: Config.UPLOAD_URL + '/save',
          method: "POST",
          data: imgUrl
        });
      }

    };

  }]);