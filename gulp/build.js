'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var app = require('../package.json')
var htmlreplace = require('gulp-html-replace');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

var jsFilter = $.filter('**/*.js', {
  restore: true
});
console.log(process.env.NODE_ENV)
gulp.task('partials', function () {
  return gulp.src([
      path.join(conf.paths.src, '/app/**/*.html'),
      path.join(conf.paths.tmp, '/serve/app/**/*.html')
    ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'antsquare',
      root: 'app'
    }))
    .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('productionBuild', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {
    read: false
  });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html', {
    restore: true
  });
  var jsFilter = $.filter('**/*.js', {
    restore: true
  });
  var cssFilter = $.filter('**/*.css', {
    restore: true
  });
  var assets;

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    // .pipe($.rev())
    .pipe($.rename({
      suffix: "-" + app.version,
    }))
    .pipe(jsFilter)
    .pipe($.preprocess())
    .pipe($.sourcemaps.init())
    .pipe($.ngAnnotate())
    .pipe($.uglify({
      preserveComments: $.uglifySaveLicense
    }))
    .on('error', conf.errorHandler('Uglify'))
    .pipe($.sourcemaps.write('maps'))
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.sourcemaps.init())
    .pipe($.minifyCss({
      processImport: false
    }))
    .pipe($.sourcemaps.write('maps'))
    .pipe(cssFilter.restore)
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(htmlreplace({
      'vendor-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'vendor-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'app-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'app-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'vendor-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'vendor-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      },
      'app-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'app-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      }
    }))
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe($.size({
      title: path.join(conf.paths.dist, '/'),
      showFiles: true
    }));

});

gulp.task('uatBuild', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {
    read: false
  });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html', {
    restore: true
  });
  var jsFilter = $.filter('**/*.js', {
    restore: true
  });
  var cssFilter = $.filter('**/*.css', {
    restore: true
  });
  var assets;

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    // .pipe($.rev())
    .pipe($.rename({
      suffix: "-uat-" + app.version,
    }))
    .pipe(jsFilter)
    .pipe($.preprocess())
    .pipe($.sourcemaps.init())
    .pipe($.ngAnnotate())
    .pipe($.uglify({
      preserveComments: $.uglifySaveLicense
    }))
    .on('error', conf.errorHandler('Uglify'))
    .pipe($.sourcemaps.write('maps'))
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.sourcemaps.init())
    .pipe($.minifyCss({
      processImport: false
    }))
    .pipe($.sourcemaps.write('maps'))
    .pipe(cssFilter.restore)
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(htmlreplace({
      'vendor-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'vendor-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'app-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'app-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'vendor-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'vendor-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      },
      'app-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'app-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      }
    }))
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe($.size({
      title: path.join(conf.paths.dist, '/'),
      showFiles: true
    }));

});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {
    read: false
  });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html', {
    restore: true
  });
  var jsFilter = $.filter('**/*.js', {
    restore: true
  });
  var cssFilter = $.filter('**/*.css', {
    restore: true
  });
  var assets;
  switch (process.env.NODE_ENV) {
  case 'staging-web':
    return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
      .pipe($.inject(partialsInjectFile, partialsInjectOptions))
      .pipe(assets = $.useref.assets())
      .pipe($.rev())
      .pipe(jsFilter)
      .pipe($.preprocess())
      .pipe($.sourcemaps.init())
      .pipe($.ngAnnotate())
      .pipe($.uglify({
        preserveComments: $.uglifySaveLicense
      }))
      .on('error', conf.errorHandler('Uglify'))
      .pipe($.sourcemaps.write('maps'))
      .pipe(jsFilter.restore)
      .pipe(cssFilter)
      .pipe($.sourcemaps.init())
      .pipe($.minifyCss({
        processImport: false
      }))
      .pipe($.sourcemaps.write('maps'))
      .pipe(cssFilter.restore)
      .pipe(assets.restore())
      .pipe($.useref())
      .pipe($.revReplace())
      .pipe(htmlFilter)
      .pipe($.minifyHtml({
        empty: true,
        spare: true,
        quotes: true,
        conditionals: true
      }))
      .pipe(htmlFilter.restore)
      .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
      .pipe($.size({
        title: path.join(conf.paths.dist, '/'),
        showFiles: true
      }));
    break;
  case 'UAT-web':
  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))

  .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    // .pipe($.rev())
    // .pipe($.rename({
    // 	suffix: "-" + app.version,
    // }))
    // .pipe(jsFilter)
    // .pipe($.preprocess())
    // .pipe($.sourcemaps.init())
    // .pipe($.ngAnnotate())
    // .pipe($.uglify({
    //   preserveComments: $.uglifySaveLicense
    // }))
    // .on('error', conf.errorHandler('Uglify'))
    // .pipe($.sourcemaps.write('maps'))
    // .pipe(jsFilter.restore)
    // .pipe(cssFilter)
    // .pipe($.sourcemaps.init())
    // .pipe($.minifyCss({
    //   processImport: false
    // }))
    // .pipe($.sourcemaps.write('maps'))
    // .pipe(cssFilter.restore)
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(htmlreplace({
      'vendor-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'vendor-uat-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'app-css': {
        src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'app-uat-' + app.version + '.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'vendor-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'vendor-uat-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      },
      'app-js': {
        src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'app-uat-' + app.version + '.js',
        tpl: '<script type="text/javascript" src="%s"></script>'
      }
    }))
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe($.size({
      title: path.join(conf.paths.dist, '/'),
      showFiles: true
    }))
    break;
  default:
    return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))

    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
      .pipe(assets = $.useref.assets())
      // .pipe($.rev())
      // .pipe($.rename({
      // 	suffix: "-" + app.version,
      // }))
      // .pipe(jsFilter)
      // .pipe($.preprocess())
      // .pipe($.sourcemaps.init())
      // .pipe($.ngAnnotate())
      // .pipe($.uglify({
      //   preserveComments: $.uglifySaveLicense
      // }))
      // .on('error', conf.errorHandler('Uglify'))
      // .pipe($.sourcemaps.write('maps'))
      // .pipe(jsFilter.restore)
      // .pipe(cssFilter)
      // .pipe($.sourcemaps.init())
      // .pipe($.minifyCss({
      //   processImport: false
      // }))
      // .pipe($.sourcemaps.write('maps'))
      // .pipe(cssFilter.restore)
      .pipe(assets.restore())
      .pipe($.useref())
      .pipe(htmlreplace({
        'vendor-css': {
          src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'vendor-' + app.version + '.css',
          tpl: '<link rel="stylesheet" type="text/css" href="%s">'
        },
        'app-css': {
          src: 'https://d37pip7vd9logx.cloudfront.net/styles/' + 'app-' + app.version + '.css',
          tpl: '<link rel="stylesheet" type="text/css" href="%s">'
        },
        'vendor-js': {
          src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'vendor-' + app.version + '.js',
          tpl: '<script type="text/javascript" src="%s"></script>'
        },
        'app-js': {
          src: 'https://d37pip7vd9logx.cloudfront.net/scripts/' + 'app-' + app.version + '.js',
          tpl: '<script type="text/javascript" src="%s"></script>'
        }
      }))
      .pipe($.revReplace())
      .pipe(htmlFilter)
      .pipe($.minifyHtml({
        empty: true,
        spare: true,
        quotes: true,
        conditionals: true
      }))
      .pipe(htmlFilter.restore)
      .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
      .pipe($.size({
        title: path.join(conf.paths.dist, '/'),
        showFiles: true
      }))

  }
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
      path.join(conf.paths.src, '/**/*'),
      path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss}')
    ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('upload', function () {
  var aws_config = {
    key: 'AKIAIDC4YRYNK5I772NA',
    secret: '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',
    region: 'us-east-1'
  }

  var s3 = require('gulp-s3-upload')(aws_config);

  return gulp.src([path.join(conf.paths.dist, '/**/*.js'), path.join(conf.paths.dist, '/**/*.css'), path.join(conf.paths.dist, '/**/*.map')])

  .pipe(gulp.dest(conf.paths.tmp + '/' + process.env.NODE_ENV))
    .pipe(s3({
      Bucket: 'antsquare-www', //  Required
      ACL: 'public-read' //  Needs to be user-defined
    }, {
      maxRetries: 5
    }));

});

gulp.task('clean', function () {
  return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});


gulp.task('build', ['html', 'fonts', 'other'], function () {

});

gulp.task('compile', ['productionBuild', 'fonts', 'other'], function () {

});

gulp.task('compileUTA', ['uatBuild', 'fonts', 'other'], function () {

});
